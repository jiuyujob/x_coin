//
//  AppDelegate.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/3.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

