//
//  HttpConstant.h
//  YSD_iPhone
//
//  Created by dan on 15/9/28.
//  Copyright (c) 2015年 Yesvion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIColor+HNExtensions.h"

#define ZhuCeURL @"auth/register"  //注册
#define LoginURL @"auth/login"  //登录
#define ForgetPasswordURL @"auth/space/forget-password"  //忘记密码
#define ResetPwdRL @"auth/reset/reset-password" //重置密码
#define sendsmscodeURL @"auth/send-sms-code"  //发送邮箱验证码
#define currencylistURL @"wallet/currency/list"  //读取币种列表接口
#define chainsymbolURL @"wallet/chain/symbol"  //根据symbol获取链接口
#define depositinfoURL @"wallet/deposit/info"  //读取充值地址接口
#define depositdetailURL @"wallet/deposit/detail"  //读取充值详情接口(历史记录)
#define coindepositURL @"wallet/coin/deposit"  //读取代币提现信息接口
#define showecologyindexURL @"ecology/show_ecology_index"  //钱包页面
#define displaydynamicsURL @"article/display_dynamics"  //社区页面
#define withdrawaldetailURL @"wallet/withdrawal/detail"  //获取提现明细接口(历史记录)
#define getuserinfoURL @"user/get-user-info"  //获取用户信息
#define userinvitationURL @"user/invitation"  //邀请
#define modifynickURL @"user/modify-nick"  //修改用户昵称
#define accountinfoURL @"wallet/account/info"  //钱包详情接口
#define changeintoUSDD @"ecology/to_change_into_USDD" //生态转入
#define USDDBagUpdate @"ecology/upgrade_warehouse" //生态升级
#define EcoList @"ecology/show_formula_list" //生态详情

#define accountchange @"wallet/account/change" //兑换代币接口
#define currencytransferlist @"wallet/currency/transfer_list" //读取兑换币种列表接口
#define userteam @"user/team" //展示团队信息收益总和
#define userteammember @"user/team-member" //展示团队人员信息
#define accountdetail @"wallet/account/detail" //钱包明接口
#define showagreementtoroot @"article/admin/show_agreement_to_root" //帮助中心
#define showagreementtouser @"article/show_agreement_to_user" //用户协议
//提币
#define withdrawaladdrlist @"wallet/withdrawal/addr/list" //读取提币地址列表接口
#define withdrawaladdradd @"wallet/withdrawal/addr/add" //创建提现地址接口
#define withdrawaladdrremove @"wallet/withdrawal/addr/remove" //删除提现地址接口
#define withdrawaladdrupdate @"wallet/withdrawal/addr/update" //更新提现地址接口
#define accountwithdrawal @"wallet/account/withdrawal" //提现货币接口
#define currencyquote @"wallet/currency/quote" ///货币报价接口

#define EcoDealList @"ecology/return_page_list_hostry" //交易历史记录
#define SetPayWord @"wallet/user/pay/set-paypwd" //设置支付密码
#define sendPayEmail @"wallet/user/pay/send-email_util" //发送支付密码修改邮件认证
#define resetPayPwd @"wallet/user/pay/reset-paypwd" //修改支付密码
#define issetPay @"wallet/user/pay/is_init" //修改支付密码

//--------------    华丽的分割线  -----------------------
#define accountcurrencydetailissetPay @"wallet/account/currency/detail" //钱包币种详情接口
#define userqrcodechange @"wallet/user/qrcode/change" //收费二维码接口
#define userpaychange @"wallet/user/pay/change" //用户付款接口
#define userpayinfo @"wallet/user/pay/info" //用户付款接口
#define accountpersontransfer @"wallet/account/person/transfer" //个人转账接口
#define accountpersonchargedetail @"wallet/account/person/charge/detail" //个人收款明细接口
#define dappListapi @"ecology/show_group_by_type" //

#define userqrcodepay @"wallet/user/qrcode/pay" //付款二维码接口
#define userpaysetmoney @"wallet/user/pay/set_money" //设置付款码金额接口



//各种颜色
#define kHexColor(HexString) [UIColor colorForHexString:HexString]
#define zitiColor kHexColor(@"#999999")
#define buttonColor kHexColor(@"#08226D")
#define buttonHuiSeColor kHexColor(@"#D8D8D8")
#define HuiSeZiTiColor kHexColor(@"#939393")

//================ localizedString设置语言 ===========//
#define QSLocalizedString(key)   (NSLocalizedStringFromTable(key, @"CustomLocalizable", nil).length ? NSLocalizedStringFromTable(key, @"CustomLocalizable", nil) : @"")
