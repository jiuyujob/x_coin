//
//  JsonTransform.m
//  YSD_iPhone
//
//  Created by dan on 15/7/27.
//  Copyright (c) 2015年 Yesvion. All rights reserved.
//

#import "JsonTransform.h"
#import <sys/utsname.h>

@implementation JsonTransform

// JSON转字典或数组
+ (id)JsonToDictionary:(id)jsonObject
{
    NSData *data = [NSData dataWithData:jsonObject];
    NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    return resultDict;
}

//数组
+ (NSArray *)JsonToArray:(id)jsonObject
{
    NSData *data = [NSData dataWithData:jsonObject];
    NSArray *results = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    return results;
}

+ (NSString *)iphoneType {
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSASCIIStringEncoding];
    
    if ([platform isEqualToString:@"iPhone1,1"]) return @"iPhone2G";
    
    if ([platform isEqualToString:@"iPhone1,2"]) return @"iPhone3G";
    
    if ([platform isEqualToString:@"iPhone2,1"]) return @"iPhone3GS";
    
    if ([platform isEqualToString:@"iPhone3,1"]) return @"iPhone4";
    
    if ([platform isEqualToString:@"iPhone3,2"]) return @"iPhone4";
    
    if ([platform isEqualToString:@"iPhone3,3"]) return @"iPhone4";
    
    if ([platform isEqualToString:@"iPhone4,1"]) return @"iPhone4S";
    
    if ([platform isEqualToString:@"iPhone5,1"]) return @"iPhone5";
    
    if ([platform isEqualToString:@"iPhone5,2"]) return @"iPhone5";
    
    if ([platform isEqualToString:@"iPhone5,3"]) return @"iPhone5c";
    
    if ([platform isEqualToString:@"iPhone5,4"]) return @"iPhone5c";
    
    if ([platform isEqualToString:@"iPhone6,1"]) return @"iPhone5s";
    
    if ([platform isEqualToString:@"iPhone6,2"]) return @"iPhone5s";
    
    if ([platform isEqualToString:@"iPhone7,1"]) return @"iPhone6Plus";
    
    if ([platform isEqualToString:@"iPhone7,2"]) return @"iPhone6";
    
    if ([platform isEqualToString:@"iPhone8,1"]) return @"iPhone6s";
    
    if ([platform isEqualToString:@"iPhone8,2"]) return @"iPhone6sPlus";
    
    if ([platform isEqualToString:@"iPhone8,4"]) return @"iPhoneSE";
    
    if ([platform isEqualToString:@"iPhone9,1"]) return @"iPhone7";
    
    if ([platform isEqualToString:@"iPhone9,2"]) return @"iPhone7Plus";
    
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPodTouch1G";
    
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPodTouch2G";
    
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPodTouch3G";
    
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPodTouch4G";
    
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPodTouch5G";
    
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad2(WiFi)";
    
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad2(GSM)";
    
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad2(CDMA)";
    
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad2(WiFi)";
    
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPadMini(WiFi)";
    
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPadMini(GSM)";
    
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPadMini(GSM+CDMA)";
    
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad3(WiFi)";
    
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad3(GSM+CDMA)";
    
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad3(GSM)";
    
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad4(WiFi)";
    
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad4(GSM)";
    
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad4(GSM+CDMA)";
    
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPadAir(WiFi)";
    
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPadAir(Cellular)";
    
    if ([platform isEqualToString:@"iPad4,3"])      return @"iPadAir";
    
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPadMini2G(WiFi)";
    
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPadMini2G(Cellular)";
    
    if ([platform isEqualToString:@"iPad4,6"])      return @"iPadMini2G";
    
    if ([platform isEqualToString:@"iPad4,7"])      return @"iPadMini3(WiFi)";
    
    if ([platform isEqualToString:@"iPad4,8"])      return @"iPadMini3(Cellular)";
    
    if ([platform isEqualToString:@"iPad4,9"])      return @"iPadMini3(China)";
    
    if ([platform isEqualToString:@"iPad5,3"])      return @"iPadAir2(WiFi)";
    
    if ([platform isEqualToString:@"iPad5,4"])      return @"iPadAir2(Cellular)";
    
    if ([platform isEqualToString:@"AppleTV2,1"])  return @"AppleTV2G";
    
    if ([platform isEqualToString:@"AppleTV3,1"])  return @"AppleTV3";
    
    if ([platform isEqualToString:@"AppleTV3,2"])  return @"AppleTV3(2013)";
    
    if ([platform isEqualToString:@"i386"])        return @"Simulator";
    
    if ([platform isEqualToString:@"x86_64"])      return @"Simulator";
    
    return platform;
    
}


@end
