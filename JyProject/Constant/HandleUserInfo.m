//
//  HandleUserInfo.m
//  YSD_iPhone
//
//  Created by dan on 15/9/28.
//  Copyright (c) 2015年 Yesvion. All rights reserved.
//

#import "HandleUserInfo.h"

@implementation HandleUserInfo


+ (void)clearAllUserInfo {
    
    // 清空所有NSUserDefaults里的数据
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:@"username"]; //用户名
    [userDefault removeObjectForKey:@"password"]; //用户登录密码
    [userDefault removeObjectForKey:@"token"];    //登录用的token
    [userDefault removeObjectForKey:@"hasLogin"];//登录状态
    [userDefault removeObjectForKey:@"userid"];//用户 ID
    [userDefault removeObjectForKey:@"level"];//用户等级

}

@end
