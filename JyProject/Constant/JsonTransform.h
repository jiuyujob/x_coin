//
//  JsonTransform.h
//  YSD_iPhone
//
//  Created by dan on 15/7/27.
//  Copyright (c) 2015年 Yesvion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JsonTransform : NSObject

// 将接口数据转换成字典数组
+ (NSDictionary *)JsonToDictionary:(id)jsonObject;
+ (NSArray *)JsonToArray:(id)jsonObject;

+(NSString *)iphoneType;

@end
