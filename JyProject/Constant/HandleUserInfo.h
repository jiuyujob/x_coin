//
//  HandleUserInfo.h
//  YSD_iPhone
//
//  Created by dan on 15/9/28.
//  Copyright (c) 2015年 Yesvion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HandleUserInfo : NSObject


// 清除用户信息
+ (void)clearAllUserInfo;

@end
