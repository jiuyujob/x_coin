//
//  GWNetWork.m
//  ShanDianJR
//
//  Created by GW on 15/8/25.
//  Copyright (c) 2015年 rongjiahui. All rights reserved.
//

#import "GWNetWork.h"
#import "AFHTTPSessionManager.h"
#import "XLDouYinLoading.h"
#import "PrefixHeader.pch"


@implementation GWNetWork

/**
*  发送get请求
*/
+ (void)getWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id))success failure:(void (^)(NSError *))failure
{
//    [XLDouYinLoading showInView:QSAppKeyWindow];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            manager.responseSerializer = [AFHTTPResponseSerializer serializer];
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            manager.securityPolicy.allowInvalidCertificates = YES;
            manager.securityPolicy.validatesDomainName = NO;
            manager.requestSerializer.timeoutInterval = 8;
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    
     [manager GET:url parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//            [XLDouYinLoading hideInView:QSAppKeyWindow];
            if (success) {

                         id dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
                         success(dic);
            }
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//            [XLDouYinLoading hideInView:QSAppKeyWindow];
            NSLog(@"failure -- error = %@",error);
            failure(error);

        }];
    
}
/**
 *  发送get请求带Token
 */
+ (void)getKotenWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id))success failure:(void (^)(NSError *))failure
{
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
//         [XLDouYinLoading showInView:QSAppKeyWindow];
         AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
         manager.responseSerializer = [AFHTTPResponseSerializer serializer];
         manager.requestSerializer = [AFJSONRequestSerializer serializer];
         manager.securityPolicy.allowInvalidCertificates = YES;
         manager.securityPolicy.validatesDomainName = NO;
         manager.requestSerializer.timeoutInterval = 8;
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    manager.requestSerializer = [AFJSONRequestSerializer serializer];//调出请求头
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"]; //将token封装入请求头
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"token"] forHTTPHeaderField:@"Authorization"]; //@"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiZDczNWM5MjcyNjMxNDIxYmIyMWQ4NDk0ZWJiZWQ5NWMiLCJuYW1lIjoiIiwibW9iaWxlIjoiIiwiZW1haWwiOiIxNTU2MDg5Njk3QHFxLmNvbSIsImZhdGhlcl9pZCI6IiIsIm5hbWVfc3BhY2UiOiIiLCJleHAiOjE1Nzc5NTQyMDksImlzcyI6Im5ld3RyZWtXYW5nIiwibmJmIjoxNTc2NjU3MjA5fQ.AnLE_nXHN2RUVualmM6oCG_jNNW0oEFLMmk9-gDF5Ys"
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", nil];
    //查看请求头
//    NSLog(@"---%@",manager.requestSerializer.HTTPRequestHeaders);
//    NSLog(@"参数----%@",params);
    [manager GET:url parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                  [XLDouYinLoading hideInView:QSAppKeyWindow];
        id dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
        if ([dic isKindOfClass:[NSNull class]]) {
            NSError *error = [[NSError alloc]init];
            failure(error);
        }else{
            if (success) {
            success(dic);
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//        [XLDouYinLoading hideInView:QSAppKeyWindow];
        NSLog(@"failure -- error = %@",error);
        failure(error);

    }];
}

/**
 *  发送POST请求
 */
+ (void)postWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id))success failure:(void (^)(NSError *))failure
{
     [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
//      [XLDouYinLoading showInView:QSAppKeyWindow];
      AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
      manager.responseSerializer = [AFHTTPResponseSerializer serializer];
      manager.securityPolicy.allowInvalidCertificates = YES;
      manager.requestSerializer.timeoutInterval = 8;
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    //是否在证书域字段中验证域名
     manager.securityPolicy.validatesDomainName = NO;
    
       [manager POST:url parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {

       } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
        {
           [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//           [XLDouYinLoading hideInView:QSAppKeyWindow];
          if (success) {

              id dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
              success(dic);
          }

       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//           [XLDouYinLoading hideInView:QSAppKeyWindow];
           NSLog(@"failure -- error = %@",error);
            failure(error);

       }];
}

+ (void)postTokenWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id))success failure:(void (^)(NSError *))failure
{

    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
//        [XLDouYinLoading showInView:QSAppKeyWindow];
          AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//          manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//          manager.securityPolicy.allowInvalidCertificates = YES;
//          manager.securityPolicy.validatesDomainName = NO;
//          manager.requestSerializer.timeoutInterval = 60;
//          manager.requestSerializer = [AFHTTPRequestSerializer serializer];//调出请求头
//          [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"]; //将token封装入请求头
//          [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
          
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    [manager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"token"] forHTTPHeaderField:@"Authorization"]; //@"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiZDczNWM5MjcyNjMxNDIxYmIyMWQ4NDk0ZWJiZWQ5NWMiLCJuYW1lIjoiIiwibW9iaWxlIjoiIiwiZW1haWwiOiIxNTU2MDg5Njk3QHFxLmNvbSIsImZhdGhlcl9pZCI6IiIsIm5hbWVfc3BhY2UiOiIiLCJleHAiOjE1Nzc5NTQyMDksImlzcyI6Im5ld3RyZWtXYW5nIiwibmJmIjoxNTc2NjU3MjA5fQ.AnLE_nXHN2RUVualmM6oCG_jNNW0oEFLMmk9-gDF5Ys"
        //查看请求头
    //    NSLog(@"---%@",manager.requestSerializer.HTTPRequestHeaders);
        NSLog(@"参数----%@",params);
           [manager POST:url parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {

           } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
            {
               [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//               [XLDouYinLoading hideInView:QSAppKeyWindow];
              if (success) {
                  id dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
                  success(dic);
              }

           } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

               [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//               [XLDouYinLoading hideInView:QSAppKeyWindow];
               NSLog(@"failure -- error = %@",error);
                failure(error);

           }];
    
}



+ (void)postidTokenWithURL:(NSString *)url params:(id)params success:(void (^)(id))success failure:(void (^)(NSError *))failure
{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        [manager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"token"] forHTTPHeaderField:@"Authorization"];
            NSLog(@"参数----%@",params);
               [manager POST:url parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {

               } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
                {
                   [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                  if (success) {
                      id dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
                      success(dic);
                  }

               } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

                   [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                   NSLog(@"failure -- error = %@",error);
                    failure(error);

               }];
}
@end
