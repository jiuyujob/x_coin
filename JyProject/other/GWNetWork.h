//
//  GWNetWork.h
//  ShanDianJR
//
//  Created by GW on 15/8/25.
//  Copyright (c) 2015年 rongjiahui. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GWNetWork : NSObject


/**
 *  发送get请求
 */
+ (void)getWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id))success failure:(void (^)(NSError *))failure;
/**
 *  发送get请求带Token
 */
+ (void)getKotenWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id))success failure:(void (^)(NSError *))failure;

/**
 *  发送POST请求
 */
+ (void)postWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id))success failure:(void (^)(NSError *))failure;

/**
*  发送POST请求带Token
*/
+ (void)postTokenWithURL:(NSString *)url params:(NSDictionary *)params success:(void (^)(id))success failure:(void (^)(NSError *))failure;

/**
*  发送POST请求带Token
*/
+ (void)postidTokenWithURL:(NSString *)url params:(id)params success:(void (^)(id))success failure:(void (^)(NSError *))failure;

@end
