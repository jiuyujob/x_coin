//
//  CreateTool.h
//  GouYunQi_iOS
//
//  Created by 胡秋霞 on 2017/2/26.
//  Copyright © 2017年 GouYunQi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CreateTool : NSObject

+(UIImageView *)createImageViewWithFrame:(CGRect)frame imageString:(NSString *)string tag:(NSUInteger)tag;

+ (UILabel *)createLabelWithFrame:(CGRect)frame text:(NSString *)text font:(CGFloat)font textColor:(UIColor *)textColor tag:(NSInteger)tag;

+(UITextField *)createTextFieldWithFrame:(CGRect)frame placeholder:(NSString *)plString delegate:(id)delegate tag:(NSInteger)tag;

+(UIButton *)createButtonWithFrame:(CGRect)frame buttonTitle:(NSString *)title titleColor:(UIColor *)titleColor selectTitleColor:(UIColor *)selectTitleColor tag:(NSUInteger)tag target:(id)target action:(SEL)action size:(NSUInteger)size;

//  图片按钮
+(UIButton *)createButtonWithFrame:(CGRect)frame imageString:(NSString *)string tag:(NSUInteger)tag target:(id)target action:(SEL)action;

+(UIButton *)createButtonImageTitleWithFrame:(CGRect)frame imageString:(NSString *)string buttonTitle:(NSString *)title titleColor:(UIColor *)titleColor tag:(NSUInteger)tag target:(id)target action:(SEL)action size:(NSUInteger)size;

@end
