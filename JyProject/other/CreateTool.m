//
//  CreateTool.m
//  GouYunQi_iOS
//
//  Created by 胡秋霞 on 2017/2/26.
//  Copyright © 2017年 GouYunQi. All rights reserved.
//

#import "CreateTool.h"
#import "HttpConstant.h"
#import "UIControl+recurClick.h"

@implementation CreateTool

+(UIImageView *)createImageViewWithFrame:(CGRect)frame imageString:(NSString *)string tag:(NSUInteger)tag
{
    
    UIImageView *backImage = [[UIImageView alloc] initWithFrame:frame];
    backImage.image = [UIImage imageNamed:string];
    backImage.userInteractionEnabled = YES;
    backImage.tag = tag;
    return backImage;
    
}

+ (UILabel *)createLabelWithFrame:(CGRect)frame text:(NSString *)text font:(CGFloat)font textColor:(UIColor *)textColor tag:(NSInteger)tag{
    
    UILabel * label = [[UILabel alloc]initWithFrame:frame];
    if (tag) {
        
        label.tag = tag;
    }else{
        
        tag = 0;
    }   
    label.font = [UIFont systemFontOfSize:font];
    label.textColor = textColor;
    label.text = text;
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 0;
    label.userInteractionEnabled = YES;
    return label;
}

+(UITextField *)createTextFieldWithFrame:(CGRect)frame placeholder:(NSString *)plString delegate:(id)delegate tag:(NSInteger)tag{
    
    UITextField * textField = [[UITextField alloc]initWithFrame:frame];
    textField.delegate = delegate;
    textField.placeholder = plString;
    textField.tag = tag;
    textField.backgroundColor= [UIColor clearColor];
    textField.font = [UIFont systemFontOfSize:13];
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    attributes[NSForegroundColorAttributeName] = kHexColor(@"#939393");
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:plString attributes:attributes];
    textField.textColor = [UIColor blackColor];
    textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    return textField;
}

+(UIButton *)createButtonWithFrame:(CGRect)frame buttonTitle:(NSString *)title titleColor:(UIColor *)titleColor selectTitleColor:(UIColor *)selectTitleColor tag:(NSUInteger)tag target:(id)target action:(SEL)action size:(NSUInteger)size{
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    button.tag = tag;
    [button setTitle:title forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:size];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    [button setTitleColor:selectTitleColor forState:UIControlStateSelected];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    button.uxy_acceptEventInterval = 2.0;//防止重复点击
    button.layer.cornerRadius = 7;
    return button;
    
}

+(UIButton *)createButtonWithFrame:(CGRect)frame imageString:(NSString *)string   tag:(NSUInteger)tag target:(id)target action:(SEL)action
{
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    button.tag = tag;
    [button setBackgroundImage:[UIImage imageNamed:string] forState:UIControlStateNormal];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    button.uxy_acceptEventInterval = 2.0;//防止重复点击
    return button;
}
+(UIButton *)createButtonImageTitleWithFrame:(CGRect)frame imageString:(NSString *)string buttonTitle:(NSString *)title titleColor:(UIColor *)titleColor tag:(NSUInteger)tag target:(id)target action:(SEL)action size:(NSUInteger)size
{
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    button.tag = tag;
    [button setBackgroundImage:[UIImage imageNamed:string] forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:size];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    button.uxy_acceptEventInterval = 2.0;//防止重复点击
    return button;
}
@end
