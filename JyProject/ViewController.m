//
//  ViewController.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/3.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "ViewController.h"
#import "JYNavVC.h"
#import "JYTabBar.h"
#import "WWLoginCtrl.h"
#import "QSLanguageConfigHelper.h"
#import "XOUpdateM.h"
#import "AppDelegate.h"
@interface ViewController ()
@property UIImageView *logoImg;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self startLayout];
}

-(void) startLayout{
    self.view.backgroundColor = [UIColor whiteColor];
    _logoImg = [[UIImageView alloc]initWithFrame:[UIScreen mainScreen].bounds];
     UIImage *logoImage = [UIImage imageNamed:[self splashImageNameForOrientation:UIDeviceOrientationPortrait]];
     [_logoImg setImage:logoImage];
    [self.view addSubview:_logoImg];
    
    __weak typeof(self)weakself = self;

    [UIView animateWithDuration:2.0 animations:^{
        weakself.logoImg.layer.transform = CATransform3DMakeScale(1.1, 1.1, 1);
    } completion:^(BOOL finished) {
        [self goMain];
    }];
}

-(void)goMain
{
    NSString *strong = [[NSUserDefaults standardUserDefaults] stringForKey:@"QSUserLanguageKey"];
    if ([strong isEqualToString:@"zh-Hans"]) {//zh-Hans,en
         [QSLanguageConfigHelper setSystemLanguageChinese];//设置中文
    }
    else
    {
       
        [QSLanguageConfigHelper setSystemLanguageEnglish];//设置英文
    }

         AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
         if (TheLoginStatus)//TheLoginStatus
         {
             JYTabBar *tabbarVC =  [[JYTabBar alloc]init];
             app.window.rootViewController = tabbarVC;
         }
         else
         {
                WWLoginCtrl *ctrl = [[WWLoginCtrl alloc] init];
                JYNavVC *nav = [[JYNavVC alloc] initWithRootViewController:ctrl];
             app.window.rootViewController = nav;
         }
    
    [[XOUpdateM manager]begin];
}


- (NSString *)splashImageNameForOrientation:(UIDeviceOrientation)orientation {
    CGSize viewSize = self.view.bounds.size;
    NSString* viewOrientation = @"Portrait";
    if (UIDeviceOrientationIsLandscape(orientation)) {
        viewSize = CGSizeMake(viewSize.height, viewSize.width);
        viewOrientation = @"Landscape";
    }
     
    NSArray* imagesDict = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"UILaunchImages"];
    for (NSDictionary* dict in imagesDict) {
        CGSize imageSize = CGSizeFromString(dict[@"UILaunchImageSize"]);
        if (CGSizeEqualToSize(imageSize, viewSize) && [viewOrientation isEqualToString:dict[@"UILaunchImageOrientation"]])
            return dict[@"UILaunchImageName"];
    }
    return nil;
}

@end
