//
//  marketView.m
//  JyProject
//
//  Created by 九域-UI on 2020/1/16.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "marketView.h"
#import "marketViewCell.h"

@implementation marketView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
       if (self) {
           self.backgroundColor = [UIColor clearColor];
           self.ArrayData = [NSMutableArray array];
           [self initSetUI];
           

       }
    return self;
}

- (void)initSetUI{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT-kNavgationBarHeight-64) style:UITableViewStyleGrouped];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
           if (@available(iOS 11.0, *)){
               _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
           }
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,60)];
    
    UILabel *qbLb = [CreateTool createLabelWithFrame:CGRectMake(15,20,100,30) text:QSLocalizedString(@"qs_QuanBu") font:14 textColor:HuiSeZiTiColor tag:0];
    qbLb.textAlignment = NSTextAlignmentLeft;
    [headView addSubview:qbLb];
    
    UILabel *JGLb = [CreateTool createLabelWithFrame:CGRectMake(SCREEN_WIDTH-250,20,140,30) text:QSLocalizedString(@"qs_JiaGe") font:14 textColor:HuiSeZiTiColor tag:0];
    [headView addSubview:JGLb];
    
    
    UILabel *ZDFLb = [CreateTool createLabelWithFrame:CGRectMake(SCREEN_WIDTH-130,20,100,30) text:QSLocalizedString(@"qs_ZhangDieFu") font:14 textColor:HuiSeZiTiColor tag:0];
    ZDFLb.textAlignment = NSTextAlignmentRight;
    [headView addSubview:ZDFLb];
    
    _tableView.tableHeaderView = headView;
    [self addSubview:_tableView];
    
}

- (void)NetworkRequest{
    
//     NSString *URLStr = [NSString stringWithFormat:@"http://192.168.8.126/api/v1/quote/ticker"];
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"token"]= [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,@"quote/ticker"];

    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
         NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
                  NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                  if ([code isEqualToString:@"200"]) {
                      //成功
                      [self.ArrayData removeAllObjects];
                      NSArray *data = DIC[@"data"];
                      [self.ArrayData addObjectsFromArray:data];
                      [self.tableView reloadData];
                  }
                  else if ([code isEqualToString:@"401"])
                  {

                         NotifyRelogin;
                  }
                  else
                  {
                      [JMNotifyView showNotify:DIC[@"msg"]];
                  }

    } failure:^(NSError *error) {

    }];
    
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ArrayData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"myCell22";
    marketViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell)
    {
        cell = [[marketViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (self.ArrayData && indexPath.row < [self.ArrayData count])
    {
        [cell setCellWithInfo:self.ArrayData indexPathRow:indexPath];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    return 80;

}

- (void)upData{
    
    [self NetworkRequest];
}
@end
