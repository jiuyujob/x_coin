//
//  newsView.h
//  JyProject
//
//  Created by 九域-UI on 2020/1/16.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "JYBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface newsView : JYBaseView<UITableViewDelegate,UITableViewDataSource>


@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,retain) NSMutableArray *ArrayData;

-(id)initWithFrame:(CGRect)frame;

@end

NS_ASSUME_NONNULL_END
