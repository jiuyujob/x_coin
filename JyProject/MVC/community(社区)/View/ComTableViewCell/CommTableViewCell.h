//
//  CommTableViewCell.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/14.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "MainTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommTableViewCell : MainTableViewCell

@property (nonatomic,retain) UIView *view;

@property (nonatomic,retain) UIImageView *txImage;
@property (nonatomic, strong) UILabel *titleLb;
@property (nonatomic, strong) UILabel *fsleLb;

@property (nonatomic, strong) UILabel *nrleLb;
@property (nonatomic, strong) UILabel *kgleLb;


- (void)setCellWithInfo:(NSMutableArray *)arrayData indexPathRow:(NSIndexPath*)indexPath;
@end

NS_ASSUME_NONNULL_END
