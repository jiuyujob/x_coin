//
//  marketViewCell.m
//  JyProject
//
//  Created by 九域-UI on 2020/1/16.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "marketViewCell.h"

@implementation marketViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews{
    
    UIView *view = [[UIView alloc] init];
    view.layer.backgroundColor = [UIColor whiteColor].CGColor;
    view.frame = CGRectMake(10,5,SCREEN_WIDTH-20,68);
    view.layer.cornerRadius = 8;
    view.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    view.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    view.layer.shadowOpacity = 0.5;// 阴影透明度，默认0
    view.layer.shadowRadius = 5;// 阴影半径，默认3
    [self.contentView addSubview:view];
    
    self.titleLb = [CreateTool createLabelWithFrame:CGRectMake(10,8,130,25) text:@"BTC/USDT" font:16 textColor:[UIColor blackColor] tag:0];
    self.titleLb.textAlignment = NSTextAlignmentLeft;
    [view addSubview:self.titleLb];
    
    UILabel *jysLb = [CreateTool createLabelWithFrame:CGRectMake(10, self.titleLb.bottom,130,25) text:QSLocalizedString(@"qs_JiaoYiSuo") font:13 textColor:HuiSeZiTiColor tag:0];
    jysLb.textAlignment = NSTextAlignmentLeft;
    [view addSubview:jysLb];
    
    
    self.JiaGeLb = [CreateTool createLabelWithFrame:CGRectMake(view.width-240,0,140,68) text:@"" font:14 textColor:kHexColor(@"#4F98CA") tag:0];
    self.JiaGeLb.backgroundColor = [UIColor clearColor];
    [view addSubview:self.JiaGeLb];
    
    self.zdfLb = [CreateTool createLabelWithFrame:CGRectMake(view.width-90,20,80,30) text:@"" font:12.5 textColor:[UIColor whiteColor] tag:0];
    self.zdfLb.backgroundColor = [UIColor redColor];
    self.zdfLb.layer.cornerRadius = 8;
    self.zdfLb.layer.masksToBounds=YES;
    [view addSubview:self.zdfLb];
    
    
}

- (void)setCellWithInfo:(NSMutableArray *)arrayData indexPathRow:(NSIndexPath*)indexPath;
{
    
    self.titleLb.text = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"symbol"]];
    self.JiaGeLb.text = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"close"]];
    
    NSString * close  = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"close"]];
    NSString * open  = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"open"]];
    
    double cl = [close doubleValue];
    double op = [open doubleValue];
    if (op>0) {
         double count = (cl-op)/op;
        if (count >=0) {
            self.zdfLb.text = [NSString stringWithFormat:@"+%.2f",count];
            self.zdfLb.backgroundColor = kHexColor(@"#52C41A");
        }
        else
        {
            self.zdfLb.text = [NSString stringWithFormat:@"%.2f",count];
            self.zdfLb.backgroundColor = kHexColor(@"#D4380D");
        }
        
    }
    else
    {
        self.zdfLb.text = [NSString stringWithFormat:@"0"];
        self.zdfLb.backgroundColor = kHexColor(@"#52C41A");
    }
    
   
    
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
