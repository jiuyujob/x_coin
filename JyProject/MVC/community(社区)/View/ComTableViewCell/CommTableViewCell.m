//
//  CommTableViewCell.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/14.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "CommTableViewCell.h"

@implementation CommTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews{

}

- (void)setCellWithInfo:(NSMutableArray *)arrayData indexPathRow:(NSIndexPath*)indexPath
{
    NSArray *images_url = arrayData[indexPath.row][@"images_url"];
    self.view = [[UIView alloc] init];
    self.view.layer.backgroundColor = [UIColor whiteColor].CGColor;
    self.view.layer.cornerRadius = 7;
    self.view.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    self.view.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    self.view.layer.shadowOpacity = 0.5;// 阴影透明度，默认0
    self.view.layer.shadowRadius = 5;// 阴影半径，默认3
    [self.contentView addSubview:self.view];
    if (images_url.count == 0)
    {
        self.view.frame = CGRectMake(10,5,SCREEN_WIDTH-20,170);
    }
    else
    {
        self.view.frame = CGRectMake(10,5,SCREEN_WIDTH-20,280);
    }
    
       
        
        self.txImage = [CreateTool createImageViewWithFrame:CGRectMake(10,10,40,40) imageString:@"位图" tag:0];
        self.txImage.layer.cornerRadius = self.txImage.bounds.size.width/2;
        self.txImage.layer.masksToBounds = YES;
        [self.view addSubview:self.txImage];
        
        self.titleLb = [CreateTool createLabelWithFrame:CGRectMake(self.txImage.right+10,self.txImage.y,200,25) text:arrayData[indexPath.row][@"user_name"] font:14 textColor:[UIColor blackColor] tag:0];
        self.titleLb.textAlignment = NSTextAlignmentLeft;
        [self.view addSubview:self.titleLb];
        
        self.fsleLb= [CreateTool createLabelWithFrame:CGRectMake(self.titleLb.x,self.titleLb.bottom,200,25) text:[NSString stringWithFormat:@"%@%@",arrayData[indexPath.row][@"fans_number"],QSLocalizedString(@"qs_fensi")] font:12 textColor:[UIColor blackColor] tag:0];
        self.fsleLb.textAlignment = NSTextAlignmentLeft;
        [self.view addSubview:self.fsleLb];
        
        
        self.nrleLb= [CreateTool createLabelWithFrame:CGRectMake(10, self.txImage.bottom+10,self.view.width-20,80) text:arrayData[indexPath.row][@"article_title"] font:15 textColor:[UIColor blackColor] tag:0];
        self.nrleLb.textAlignment = NSTextAlignmentLeft;
        [self.view addSubview:self.nrleLb];
    
    if (images_url.count == 0)
    {
       
    }
    else
    {
        
               CGFloat btnWidth = 100;  //宽
               CGFloat btnHeight = 90; //高
               for (int i = 0; i < images_url.count; i++)
               {
                   NSString *url = images_url[i];
                   int col = i%3; //列
                   CGFloat margin = (self.view.width - (btnWidth * 3)) / (3 + 1);
                   CGFloat picX = margin + (btnWidth + margin) * col;
                   UIImageView *ViewImage = [CreateTool createImageViewWithFrame:CGRectMake(picX,self.nrleLb.bottom+10,btnWidth,btnHeight) imageString:@"" tag:800+i];
                   [ViewImage sd_setImageWithURL:[NSURL URLWithString:url]];
                   [self.view addSubview:ViewImage];
               }
    }
        
        
        self.kgleLb = [CreateTool createLabelWithFrame:CGRectMake(10,self.view.bottom-35,200,25) text:arrayData[indexPath.row][@"create_time"] font:11 textColor:[UIColor blackColor] tag:0];
        self.kgleLb.textAlignment = NSTextAlignmentLeft;
        [self.view addSubview:self.kgleLb];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
