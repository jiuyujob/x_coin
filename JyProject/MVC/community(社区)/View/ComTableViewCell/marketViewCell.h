//
//  marketViewCell.h
//  JyProject
//
//  Created by 九域-UI on 2020/1/16.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "MainTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface marketViewCell : MainTableViewCell

@property (nonatomic, strong) UILabel *titleLb;
@property (nonatomic, strong) UILabel *JiaGeLb;
@property (nonatomic, strong) UILabel *zdfLb;

- (void)setCellWithInfo:(NSMutableArray *)arrayData indexPathRow:(NSIndexPath*)indexPath;

@end

NS_ASSUME_NONNULL_END
