//
//  newsView.m
//  JyProject
//
//  Created by 九域-UI on 2020/1/16.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "newsView.h"
#import "CommTableViewCell.h"
#import "APPWKWebView.h"

@implementation newsView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
       if (self) {
           self.backgroundColor = [UIColor clearColor];
           self.ArrayData = [NSMutableArray array];
           [self initSetUI];
           [self NetworkRequest];

       }
    return self;
}

- (void)initSetUI{
    
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT-kNavgationBarHeight-64) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
               if (@available(iOS 11.0, *)){
                   _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
               }
        [self addSubview:_tableView];
    
}

#pragma mark - 请求接口
- (void)NetworkRequest{
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"token"]= [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    parameters[@"pageSize"]= @"20";
    parameters[@"page"]= @"1";
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,displaydynamicsURL];

    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
         NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
                  NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                  if ([code isEqualToString:@"200"]) {
                      //成功
                      NSDictionary *data = DIC[@"data"];
                      self.ArrayData = data[@"items"];
                      [self.tableView reloadData];
                  }
                  else if ([code isEqualToString:@"401"])
                  {
                    
                         NotifyRelogin;
                  }
                  else
                  {
                      [JMNotifyView showNotify:DIC[@"msg"]];
                  }

    } failure:^(NSError *error) {
        
    }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ArrayData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"myCell22";
    CommTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell)
    {
        cell = [[CommTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if (self.ArrayData && indexPath.row < [self.ArrayData count])
    {
        [cell setCellWithInfo:self.ArrayData indexPathRow:indexPath];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *images_url = self.ArrayData[indexPath.row][@"images_url"];
    if (images_url.count == 0) {
        return 190;
    }
    else
    {
        return 290;
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    APPWKWebView *web = [APPWKWebView new];
        web.TypeHtml = @"H5";
        web.tit = QSLocalizedString(@"qs_XiangQing");
        web.strURL = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"article_html"]];
       [web setHidesBottomBarWhenPushed:YES];
        [[self getCurrentVC].navigationController pushViewController:web animated:YES];

}

@end
