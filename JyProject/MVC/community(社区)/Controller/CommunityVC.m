//
//  CommunityVC.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/6.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "CommunityVC.h"

@interface CommunityVC ()

@end

@implementation CommunityVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = nil;
//    self.navigationItem.title = QSLocalizedString(@"qs_guanfangxinwen");
    [self setup];
}
- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:YES];
    
  [self.navigationController setNavigationBarHidden:NO animated:animated];
    
    [self.marketView upData];

}
-(void)setup
{
    
    
      self.segmentedView  = [[UIView alloc] initWithFrame:CGRectMake(0,0,200,35)];
      self.segmentedView.backgroundColor = [UIColor clearColor];
      self.segmentedView.layer.cornerRadius = 17;
      self.segmentedView.layer.borderWidth = 1;
      self.segmentedView.layer.borderColor = [buttonColor CGColor];
      self.navigationItem.titleView = self.segmentedView;
    
      self.with = self.segmentedView.frame.size.width/2;
    
       self.indicatorView = [[UIImageView alloc] initWithFrame:CGRectMake(self.with/2-50,0,100,35)];
       self.indicatorView.backgroundColor = buttonColor;
       self.indicatorView.layer.cornerRadius = 17;
       [self.segmentedView addSubview:self.indicatorView];
    
    
    for (int i = 0; i < 2; i++)
    {
        self.segmenteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.segmenteButton.tag = 2000+i;
        self.segmenteButton.frame = CGRectMake(i*self.with,0,self.with,35);
        self.segmenteButton.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];

        if (i==0)
        {
            [self.segmenteButton setTitle:QSLocalizedString(@"qs_HangQin") forState:UIControlStateNormal];
            [self.segmenteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        if (i==1)
        {
            [self.segmenteButton setTitle:QSLocalizedString(@"qs_XinWen") forState:UIControlStateNormal];
            [self.segmenteButton setTitleColor:buttonColor forState:UIControlStateNormal];
        }
        [self.segmenteButton addTarget:self action:@selector(segmenteButtonButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.segmentedView addSubview:self.segmenteButton];
    }
    
    
       self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH,SCREEN_HEIGHT-kNavgationBarHeight)];
       self.scrollView.delegate = self;
       self.scrollView.bounces = NO;
       self.scrollView.pagingEnabled = YES;
       self.scrollView.directionalLockEnabled = YES;
       self.scrollView.contentSize = CGSizeMake(2*SCREEN_WIDTH,SCREEN_HEIGHT);
       self.scrollView.showsHorizontalScrollIndicator = NO;
       self.scrollView.backgroundColor = [UIColor whiteColor];
       [self.view addSubview:self.scrollView];
    
    self.marketView = [[marketView alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT-kNavgationBarHeight)];
    [self.scrollView addSubview:self.marketView];
    
    self.newsView = [[newsView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH,0,SCREEN_WIDTH,SCREEN_HEIGHT-kNavgationBarHeight)];
    [self.scrollView addSubview:self.newsView];

}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat offset = scrollView.contentOffset.x;
    NSInteger Integer = offset/SCREEN_WIDTH;
    [UIView animateWithDuration:0.1 animations:^{
        self.scrollView.contentOffset = CGPointMake(Integer * SCREEN_WIDTH, 0);
        self.indicatorView.transform = CGAffineTransformMakeTranslation(Integer * 200/2,0);
    }];
    for (UIView *view in self.segmentedView.subviews)
    {
        if (view.tag > 1999 && view.tag < 2000+4)
        {
            UIButton *button = (UIButton *)view;
            if (button.tag == Integer+2000)
            {
                [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                button.enabled = NO;//不允许点击
            }
            else
            {
                [button setTitleColor:buttonColor forState:UIControlStateNormal];
                button.enabled = YES;//允许点击
            }
        }
    }
}

- (void)segmenteButtonButton:(UIButton *)but
{
    _investType = but.tag-2000;
    [UIView animateWithDuration:0.3 animations:^{
        self.scrollView.contentOffset = CGPointMake(_investType * SCREEN_WIDTH, 0);
        self.indicatorView.transform = CGAffineTransformMakeTranslation(_investType * 200/2,0);
    }];
    for (UIView *view in self.segmentedView.subviews)
    {
        if (view.tag > 1999 && view.tag < 2000+4)
        {
            UIButton *button = (UIButton *)view;
            if (button.tag == but.tag)
            {
                [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                button.enabled = NO;//不允许点击
            }
            else
            {
                [button setTitleColor:buttonColor forState:UIControlStateNormal];
                button.enabled = YES;//允许点击
            }
        }
    }
    
}
@end
