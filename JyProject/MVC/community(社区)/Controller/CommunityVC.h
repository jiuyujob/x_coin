//
//  CommunityVC.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/6.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"
#import "marketView.h"
#import "newsView.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommunityVC : JYBaseVC<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>

@property (nonatomic,retain) UIView *segmentedView;
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic,retain) UIButton *segmenteButton;
@property (assign, nonatomic) NSInteger investType;

@property (nonatomic, assign) CGFloat with;
@property (nonatomic,retain) UIImageView *indicatorView;


@property (nonatomic,retain) marketView *marketView;
@property (nonatomic,retain) newsView *newsView;

@end

NS_ASSUME_NONNULL_END
