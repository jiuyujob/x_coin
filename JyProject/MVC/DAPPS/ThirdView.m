//
//  ThirdView.m
//  JyProject
//
//  Created by mac on 2020/1/15.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "ThirdView.h"
#import "Masonry.h"
#import "HttpConstant.h"
@implementation ThirdView

-(instancetype)initWithFrame:(CGRect)frame
{
    if ([super initWithFrame:frame]) {
        
        [self startLayout];
    }
    return self;
}

+(instancetype)share{
    ThirdView *t = [[ThirdView alloc]init];
    return t;
}

-(void)startLayout
{
    UIImageView *bg = [[UIImageView alloc]init];
    bg.image = [UIImage imageNamed:@"dappbg"];
    [self addSubview:bg];
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor colorWithRed:243/255.0 green:243/255.0 blue:243/255.0 alpha:1];
    [self addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.width.equalTo(self);
        make.height.mas_offset(326);
    }];
    
    UIImageView *anquan = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"dappanquan"]];
    [view addSubview:anquan];
    [anquan mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_offset(45);
        make.size.mas_offset(CGSizeMake(50, 50));
        make.centerX.equalTo(view);
    }];
    
    UIButton *cancel = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancel setImage:[UIImage imageNamed:@"dapp_quxiao"] forState:UIControlStateNormal];
    [cancel addTarget:self action:@selector(quxiao) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:cancel];
    [cancel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_offset(20);
        make.right.mas_offset(-20);
        make.size.mas_offset(CGSizeMake(25, 25));
    }];
    
    
    UILabel *fangwen = [[UILabel alloc]init];
    fangwen.font = [UIFont fontWithName:@"PingFangSC" size:18];
    fangwen.textColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0];
    fangwen.text = QSLocalizedString(@"qs_fangwendisanfang");
    [view addSubview:fangwen];
    [fangwen mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(anquan.mas_bottom).offset(10);
        make.centerX.equalTo(view);
        make.height.mas_offset(20.5);
    }];
    
    UILabel *xieyi = [[UILabel alloc]init];
    xieyi.font = [UIFont fontWithName:@"PingFangSC" size:14];
    xieyi.textColor = [UIColor colorWithRed:147/255.0 green:147/255.0 blue:147/255.0 alpha:1.0];
    xieyi.text = QSLocalizedString(@"qs_disanfangxieyi");
    xieyi.numberOfLines = 0;
    [view addSubview:xieyi];
    [xieyi mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(fangwen.mas_bottom).offset(20);
        make.left.mas_offset(16);
        make.right.mas_offset(-8);
    }];
    
    UIButton *sure = [UIButton buttonWithType:UIButtonTypeCustom];
    [sure setTitle:QSLocalizedString(@"qs_QueRen") forState:UIControlStateNormal];
    [sure setBackgroundImage:[UIImage imageNamed:@"dapp_btn"] forState:UIControlStateNormal];
    [sure addTarget:self action:@selector(sure) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:sure];
    [sure mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(xieyi.mas_bottom).offset(42);
        make.centerX.equalTo(view);
        make.size.mas_offset(CGSizeMake(320, 70));
    }];
    
    
}
-(void)sure
{
    if (self.gotoend) {
        self.gotoend();
    }
    [self hide];
}
-(void)quxiao
{
    [self hide];
}

-(void)show
{
    UIWindow *wid =  [UIApplication sharedApplication].keyWindow;
    
    self.frame = wid.bounds;
    
    [wid addSubview:self];
}

-(void)hide
{
    [self removeFromSuperview];
}
@end
