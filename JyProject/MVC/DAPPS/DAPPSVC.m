//
//  DAPPSVC.m
//  JyProject
//
//  Created by mac on 2020/1/8.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "DAPPSVC.h"
#import "dappCell.h"
#import "dappModel.h"
#import "ThirdView.h"
#import "APPWKWebView.h"
#import "NoDataView.h"

@interface DAPPSVC ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong)UICollectionView *collection;
@property (nonatomic, strong) dappModel *dapp;
@property (nonatomic, strong) NoDataView *nodatav;
@end

@implementation DAPPSVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"DAPP";//QSLocalizedString(@"qs_TiBi");
    self.navigationItem.leftBarButtonItem = nil;
    [self.view addSubview:self.collection];
    [self.view addSubview:self.nodatav];
}


-(void)httprequest
{
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
         parameters[@"token"]= [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
      NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,dappListapi];
      [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
           NSLog(@"-获取用户-responseDict--%@", responseDict);
          NSDictionary *DIC = responseDict;
                    NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                    if ([code isEqualToString:@"200"]) {
                        //成功
                        NSDictionary *data = DIC[@"data"];
                        self.dapp = [dappModel mj_objectWithKeyValues:data];;
                        [self.collection reloadData];
                    }
                    else if ([code isEqualToString:@"401"])
                    {
                        NotifyRelogin;
                    }
                    else
                    {
                        [JMNotifyView showNotify:DIC[@"msg"]];
                    }
          [self performSelector:@selector(showNoDataView) withObject:nil afterDelay:0.5];
      } failure:^(NSError *error) {
          [self performSelector:@selector(showNoDataView) withObject:nil afterDelay:0.5];
      }];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     [self httprequest];
}


-(void)showNoDataView
{
    if (self.dapp.items.count > 0) {
        self.nodatav.hidden = YES;
    }else{
        self.nodatav.hidden = NO;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(UICollectionView *)collection
{
    if (!_collection) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 2;
        layout.itemSize = CGSizeMake((self.view.width - 2*7 - 2)/2, 120);
        layout.sectionInset = UIEdgeInsetsMake(10, 7, 10, 7);
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collection = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:layout];
        _collection.backgroundColor = [UIColor whiteColor];
        _collection.delegate = self;
        _collection.dataSource = self;
        [_collection registerNib:[UINib nibWithNibName:@"dappCell" bundle:nil] forCellWithReuseIdentifier:@"dappCellID"];
        [_collection registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headID"];
    }
    return _collection;
}

-(NoDataView *)nodatav
{
    if (!_nodatav) {
        _nodatav = [[NoDataView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height)];
        _nodatav.hidden = YES;
    }
    return _nodatav;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    sectionmd *m = self.dapp.items[section];
    return m.values.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.dapp.items.count;
}
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    sectionmd *m = self.dapp.items[indexPath.section];
    playitem *it = m.values[indexPath.row];
    dappCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"dappCellID" forIndexPath:indexPath];
    cell.name.text=[NSString stringWithFormat:@"%@",it.name];
    [cell.img sd_setImageWithURL:[NSURL URLWithString:it.image] placeholderImage:nil];
    return cell;
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
     if([kind isEqualToString:UICollectionElementKindSectionHeader])
       {
           sectionmd *m = self.dapp.items[indexPath.section];
           UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"headID" forIndexPath:indexPath];
           if (![headerView viewWithTag:100000]) {
               UILabel *sectionLab = [[UILabel alloc]initWithFrame:CGRectMake(12, 8, self.view.width, 28)];
               sectionLab.text = m.title;
               sectionLab.tag = 100000;
               sectionLab.font = [UIFont fontWithName:@"PingFangSC" size:20];
               sectionLab.textColor = kHexColor(@"333333");
               [headerView addSubview:sectionLab];
           }
           return headerView;
       }
       return nil;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return (CGSize){self.view.width,44};
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    sectionmd *m = self.dapp.items[indexPath.section];
    playitem *it = m.values[indexPath.row];
    
    ThirdView *v =  [ThirdView share];
    [v show];
    v.gotoend = ^{
        APPWKWebView *web = [APPWKWebView new];
        web.TypeHtml = @"Webview";
        web.tit = it.name;
        web.strURL = [NSString stringWithFormat:@"%@?token=%@&platform=ios&address=%@",it.the_link_address,[[NSUserDefaults standardUserDefaults] stringForKey:@"token"],it.contract_address];
        NSLog(@"%@",web.strURL);
        web.hidesBottomBarWhenPushed =  YES;
        [self.navigationController pushViewController:web animated:YES];
        
        
    };
}
@end
