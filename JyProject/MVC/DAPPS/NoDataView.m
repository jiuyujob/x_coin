//
//  NoDataView.m
//  JyProject
//
//  Created by mac on 2020/1/15.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "NoDataView.h"
#import "Masonry.h"
#import "HttpConstant.h"
@implementation NoDataView


-(instancetype)initWithFrame:(CGRect)frame
{
    if ([super initWithFrame:frame]) {
        [self startLayout];
    }
    return self;
}


-(void)startLayout
{
    UIImageView *image = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"nodata"]];
    
    [self addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_offset(CGSizeMake(138, 138));
        make.top.mas_offset(120);
        make.centerX.equalTo(self);
        
    }];
    
       UILabel *shuju = [[UILabel alloc]init];
       shuju.font = [UIFont fontWithName:@"PingFangSC" size:30];
       shuju.textColor = [UIColor colorWithRed:147/255.0 green:147/255.0 blue:147/255.0 alpha:1.0];
       shuju.text = QSLocalizedString(@"qs_meiyoushuju");
       [self addSubview:shuju];
       [shuju mas_makeConstraints:^(MASConstraintMaker *make) {
           make.top.equalTo(image.mas_bottom).offset(38);
           make.centerX.equalTo(self);
           make.height.mas_offset(42);
       }];
    
    
}

@end
