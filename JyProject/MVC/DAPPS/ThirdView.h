//
//  ThirdView.h
//  JyProject
//
//  Created by mac on 2020/1/15.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ThirdView : UIView

@property (nonatomic, copy) void(^gotoend)(void);
+(instancetype)share;
-(void)show;
-(void)hide;
@end

NS_ASSUME_NONNULL_END
