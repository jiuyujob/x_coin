//
//  dappModel.h
//  JyProject
//
//  Created by mac on 2020/1/14.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface dappModel : NSObject


@property (nonatomic, strong) NSArray *items;


@end

@interface sectionmd : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) NSArray *values;

@end

@interface playitem : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *the_link_address;
@property (nonatomic, copy) NSString *contract_address;


@end

NS_ASSUME_NONNULL_END
