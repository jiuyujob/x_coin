//
//  BillCell.h
//  JyProject
//
//  Created by mac on 2019/12/11.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BillCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *billTypeLb;
@property (weak, nonatomic) IBOutlet UILabel *billTimeLb;
@property (weak, nonatomic) IBOutlet UILabel *billStateLb;
@property (weak, nonatomic) IBOutlet UILabel *billMoneyLb;

@end

NS_ASSUME_NONNULL_END
