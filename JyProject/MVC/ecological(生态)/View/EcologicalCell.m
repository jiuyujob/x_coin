//
//  EcologicalCell.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/15.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "EcologicalCell.h"
#import "DDToDTVC.h"
#import "BillListVC.h"
#import "EcologicalUpVC.h"
#import "EcologicalIntoVC.h"

@implementation EcologicalCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews{
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(12,10,SCREEN_WIDTH-24,269);
    view.backgroundColor = kHexColor(@"ffffff");
    view.layer.cornerRadius = 16;
    view.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.05].CGColor;
    view.layer.shadowOffset = CGSizeMake(0,0);
    view.layer.shadowOpacity = 1;
    view.layer.shadowRadius = 5;
    [self.contentView addSubview:view];
    
        UILabel *ecoLb = [CreateTool createLabelWithFrame:CGRectMake(12,20, 200, 22) text:[NSString stringWithFormat:@"%@",@"USDD"] font:16 textColor:kHexColor(@"2B2B2B") tag:0];
        ecoLb.textAlignment = NSTextAlignmentLeft;
        [view addSubview:ecoLb];

    //持仓
        self.positionLb = [CreateTool createLabelWithFrame:CGRectMake(12,ecoLb.bottom + 16, 150, 17) text:@"" font:12 textColor:kHexColor(@"2B2B2B") tag:0];
        self.positionLb.textAlignment = NSTextAlignmentLeft;
        [view addSubview:self.positionLb];
        
    //级别
        self.levelLb = [CreateTool createLabelWithFrame:CGRectMake(12, self.positionLb.bottom + 12, 150, 17) text:@"" font:12 textColor:kHexColor(@"939393") tag:0];
        self.levelLb.textAlignment = NSTextAlignmentLeft;
        [view addSubview:self.levelLb];

        //杠杆
        self.ggLb = [CreateTool createLabelWithFrame:CGRectMake(12, self.levelLb.bottom + 11, 150, 17) text:@"" font:12 textColor:kHexColor(@"939393") tag:0];
        self.ggLb.textAlignment = NSTextAlignmentLeft;
        [view addSubview:self.ggLb];
        
        //今日算力
        self.jrsuanliLb = [CreateTool createLabelWithFrame:CGRectMake(167, ecoLb.bottom + 16, view.width - 179, 14) text:@"" font:10 textColor:kHexColor(@"2B2B2B") tag:0];
        self.jrsuanliLb.textAlignment = NSTextAlignmentRight;
        [view addSubview:self.jrsuanliLb];
        
        //自由算力
        self.zysuanliLb = [CreateTool createLabelWithFrame:CGRectMake(167, self.jrsuanliLb.bottom + 6, view.width - 179, 14) text:@"" font:10 textColor:kHexColor(@"2B2B2B") tag:0];
        self.zysuanliLb.textAlignment = NSTextAlignmentRight;
        [view addSubview:self.zysuanliLb];
        
        //加速算力
        self.jssuanliLb = [CreateTool createLabelWithFrame:CGRectMake(167, self.zysuanliLb.bottom + 6, view.width - 179, 14) text:@"" font:10 textColor:kHexColor(@"2B2B2B") tag:0];
        self.jssuanliLb.textAlignment = NSTextAlignmentRight;
        [view addSubview:self.jssuanliLb];
        
        //动态算力
        self.dtsuanliLb = [CreateTool createLabelWithFrame:CGRectMake(167, self.jssuanliLb.bottom + 6, view.width - 179, 14) text:@"2" font:10 textColor:kHexColor(@"2B2B2B") tag:0];
        self.dtsuanliLb.textAlignment = NSTextAlignmentRight;
        [view addSubview:self.dtsuanliLb];
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(12, 152, view.width - 24, 1)];
    line.backgroundColor = kHexColor(@"E5E5E5");
    [view addSubview:line];
    
    CGFloat btnWidth = 50;  //宽
    CGFloat btnHeight = 80 ; //高
    CGFloat margin = (view.width - (btnWidth*3) - 2 * 20)/2;
    NSArray *ImaArray =@[@"goumaisuanli",@"shengjidengji",@"zhangdanlishi"];
    NSArray *TitArray =@[QSLocalizedString(@"qs_zhuanru"),QSLocalizedString(@"qs_shengjiLevel"),QSLocalizedString(@"qs_lishi")];
    UIView *currentBtnView = nil;
    for (int i = 0; i < 3; i++)
    {
        UIView *Btnview = nil;
        if (i == 0) {
            Btnview = [[UIView alloc]initWithFrame:CGRectMake(20,line.bottom + 20,btnWidth,btnHeight)];
        }
        else
        {
            Btnview = [[UIView alloc]initWithFrame:CGRectMake(currentBtnView.right + margin,line.bottom + 20,btnWidth,btnHeight)];
        }
        Btnview.tag = 600 + i;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ButClicked:)];
        [Btnview addGestureRecognizer:tap];
        currentBtnView = Btnview;
        [view addSubview:Btnview];
        UIButton *OneBut = [CreateTool createButtonWithFrame:CGRectMake(5,5,41,41) imageString:ImaArray[i] tag:666+i target:self action:nil];
        OneBut.userInteractionEnabled = NO;
        [Btnview addSubview:OneBut];
        UIButton *textBut1 = [CreateTool createButtonWithFrame:CGRectMake(0,OneBut.bottom + 14,Btnview.width,17) buttonTitle:TitArray[i] titleColor:kHexColor(@"08226D") selectTitleColor:[UIColor whiteColor] tag:666+i target:self action:nil size:11];
        textBut1.titleLabel.adjustsFontSizeToFitWidth = YES;
        textBut1.userInteractionEnabled = NO;
        [Btnview addSubview:textBut1];
    }
    
}

-(void)setProj:(Eco_project *)proj
{
    _proj = proj;
    //持仓
        self.positionLb.text = [NSString stringWithFormat:@"%@：%@",QSLocalizedString(@"qs_shengyusuanli"),proj.bocked_balance];
        
    //级别
        self.levelLb.text = [NSString stringWithFormat:@"%@：%@",QSLocalizedString(@"qs_JiBie"),proj.level];
        
    //杠杆
        self.ggLb.text = [NSString stringWithFormat:@"%@：%@",QSLocalizedString(@"qs_GangGan"),proj.return_multiple];
        
    //今日算力
        self.jrsuanliLb.text = [NSString stringWithFormat:@"%@：%@",QSLocalizedString(@"qs_JinRiSuanLi"),proj.to_day_rate];
        
    //自由算力
        self.zysuanliLb.text = [NSString stringWithFormat:@"%@：%@",QSLocalizedString(@"qs_ZiYouSuanLi"),proj.hold_return_rate];
        
    //加速算力
        self.jssuanliLb.text = [NSString stringWithFormat:@"%@：%@",QSLocalizedString(@"qs_JiaSuSuanLi"),proj.recommend_return_rate];
              
    //动态算力
        self.dtsuanliLb.text = [NSString stringWithFormat:@"%@：%@",QSLocalizedString(@"qs_DongTaiSuanLi"),proj.team_return_rate];
}

-(void)ButClicked:(UITapGestureRecognizer *)ges
{
     __weak typeof(self) weakself = self;
    switch (ges.view.tag) {
        case 600:
        {
            if ([_proj.level isEqualToString:@""]) {
                EcologicalUpVC *vc = [[EcologicalUpVC alloc]init];
                 vc.hidesBottomBarWhenPushed = YES;
                 vc.ecologymodel = self.proj;
                [[weakself getCurrentVC].navigationController pushViewController:vc animated:YES];
            }
            else{
              EcologicalIntoVC *vc = [[EcologicalIntoVC alloc]init];
              vc.hidesBottomBarWhenPushed = YES;
              vc.ecologymodel = self.proj;
              [[weakself getCurrentVC].navigationController pushViewController:vc animated:YES];
            }
        }
            break;
        case 601:
        {
            EcologicalUpVC *vc = [[EcologicalUpVC alloc]init];
            vc.hidesBottomBarWhenPushed = YES;
            vc.ecologymodel = self.proj;
           [[weakself getCurrentVC].navigationController pushViewController:vc animated:YES];
        }
            break;
        case 602:
        {
            BillListVC *vc= [[BillListVC alloc]init];
            vc.hidesBottomBarWhenPushed = YES;
            vc.ecologymodel = self.proj;
            [[weakself getCurrentVC].navigationController pushViewController:vc animated:YES];
        }
            break;
        default:
            break;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
