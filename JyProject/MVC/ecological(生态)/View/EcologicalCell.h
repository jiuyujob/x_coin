//
//  EcologicalCell.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/15.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "MainTableViewCell.h"
#import "EcologicalModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface EcologicalCell : MainTableViewCell

@property (nonatomic, strong) UILabel *positionLb;
@property (nonatomic, strong) UILabel *levelLb;
@property (nonatomic, strong) UILabel *ggLb;

@property (nonatomic, strong) UILabel *jrsuanliLb;
@property (nonatomic, strong) UILabel *zysuanliLb;
@property (nonatomic, strong) UILabel *jssuanliLb;
@property (nonatomic, strong) UILabel *dtsuanliLb;
@property (nonatomic, strong)Eco_project *proj;

@end

NS_ASSUME_NONNULL_END
