//
//  EcoDetailCell.h
//  JyProject
//
//  Created by mac on 2019/12/17.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EcoDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ecoLb;

@end

NS_ASSUME_NONNULL_END
