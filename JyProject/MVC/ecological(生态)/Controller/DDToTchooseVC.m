//
//  DDToTchooseVC.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/19.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "DDToTchooseVC.h"

@interface DDToTchooseVC ()

@end

@implementation DDToTchooseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //title=@"资金账户选择"
    self.navigationItem.title = @"资金账户选择";
     self.view.backgroundColor = [UIColor whiteColor];
     self.ArrayData = [NSMutableArray array];
     self.transferData = [NSMutableArray array];
     [self SetinitUI];
     [self NetworkRequest];
}

- (void)SetinitUI{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT-kNavgationBarHeight) style:UITableViewStyleGrouped];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
           if (@available(iOS 11.0, *)){
               _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
           }
     [self.view addSubview:_tableView];
    
}
- (void)NetworkRequest{
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
       parameters[@"token"]= [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
     NSString *URLStr;
    URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,currencytransferlist];//读取兑换币种列表接口

    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
        
        NSLog(@"-获取用户-responseDict--%@", responseDict);
               NSDictionary *DIC = responseDict;
               NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
               if ([code isEqualToString:@"200"]) {
                   //成功
                   dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                       NSDictionary *data  = DIC[@"data"];
                       NSArray *bids  = data[@"list"];
                       [self.ArrayData addObjectsFromArray:bids];
                       NSArray *bs  = data[@"transfer"];
                       [self.transferData addObjectsFromArray:bs];
                       //主线程执行
                       dispatch_async(dispatch_get_main_queue(), ^{
                           [self.tableView reloadData];
                       });
                   });
                   
               }
               else
               {
                   [JMNotifyView showNotify:DIC[@"msg"]];
               }
    } failure:^(NSError *error) {
        
    }];
    

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ArrayData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"myCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];//从重用队列里获取可重用的cell
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if (self.ArrayData && indexPath.row < [self.ArrayData count])
    {
        UIView *view = [[UIView alloc] init];
               view.frame = CGRectMake(10,8,SCREEN_WIDTH-20,50);
               view.layer.backgroundColor = [UIColor whiteColor].CGColor;
               view.layer.cornerRadius = 6;
               view.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
               view.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
               view.layer.shadowOpacity = 0.5;// 阴影透明度，默认0
               view.layer.shadowRadius = 3;// 阴影半径，默认3
               [cell.contentView addSubview:view];
        
        NSString *text;
        text = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"symbol"]];
        UIImageView* Image = [CreateTool createImageViewWithFrame:CGRectMake(SCREEN_WIDTH/2-40,10,30,30) imageString:@"" tag:0];
                   if ([text isEqualToString:@"TFOR"]) {
                          Image.image = [UIImage imageNamed:@"钱包页TFOR"];
                          
                      }
                      else if ([text isEqualToString:@"USDD"])
                      {
                          Image.image = [UIImage imageNamed:@"钱包页USDD"];
                          
                      }
                      else
                      {
                          Image.image = [UIImage imageNamed:@"钱包页USDT"];
                      }
                   
                   [view addSubview:Image];
                   
                   UILabel *label = [CreateTool createLabelWithFrame:CGRectMake(Image.right+10,0,100,50) text:text font:13 textColor:[UIColor blackColor] tag:0];
                   label.textAlignment = NSTextAlignmentLeft;
                   [view addSubview:label];
                   
        
        

//        UILabel *label = [CreateTool createLabelWithFrame:CGRectMake(20,8,SCREEN_WIDTH-40,Scale_X(28)) text:text font:13 textColor:[UIColor whiteColor] tag:0];
//        label.layer.backgroundColor = buttonHuiSeColor.CGColor;
//        label.layer.cornerRadius = 8;
//        [cell.contentView addSubview:label];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return Scale_X(70);
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *text;
    NSString *currency_id;
    text = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"symbol"]];
    //把余额money传出去,兑换 USDD 页面专用
    currency_id = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"currency_id"]];
    self.USDDbuttonClicked(text, currency_id, self.ArrayData[indexPath.row][@"money"],self.transferData[0][@"currency_id"]);
    [self.navigationController popViewControllerAnimated:YES];
    
}
@end
