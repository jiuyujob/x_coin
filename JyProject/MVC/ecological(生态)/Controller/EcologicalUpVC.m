//
//  EcologicalUpVC.m
//  JyProject
//
//  Created by mac on 2019/12/11.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "EcologicalUpVC.h"
#import "CurrencyTypeVC.h"
#import "EcologicalDetail.h"
#import "EcoDDListVC.h"
#import "APPWKWebView.h"
#import "EcoDelModel.h"
@interface EcologicalUpVC ()<UITextFieldDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UILabel *bizhongLb;
@property (nonatomic, strong) UILabel *yuenumLb;
@property (nonatomic, strong) UILabel *yuetypeLb;
@property (nonatomic, strong) UILabel *ddyueLb;
@property (nonatomic, strong) UILabel *clevelLb;
@property (nonatomic, strong) UITextField *numTf;
@property (nonatomic, strong) UILabel *tobeLevelLb;
@property (nonatomic, copy) NSString *keyLv;
@property (nonatomic, strong) EcoDelModel *eco;
@end

@implementation EcologicalUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = QSLocalizedString(@"qs_shengtaishengji");;//
    [self setup];
    [self checkBell];
}


-(void)checkBell
{
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,accountinfoURL];
    [GWNetWork getKotenWithURL:URLStr params:nil success:^(id responseDict) {
        NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
        NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
        if ([code isEqualToString:@"200"]) {
              //成功
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                   NSArray *data = DIC[@"data"];
              
                dispatch_async(dispatch_get_main_queue(), ^{
                   for (int i= 0; i<data.count; i++) {
                       
                       NSString *text = [NSString stringWithFormat:@"%.2f",[data[i][@"balance"] floatValue]];;
                       switch (i) {
                           case 0:
                           {
                               
                           }
                               break;
                               case 1:
                               {
                                   self.yuenumLb.text = text;
                               }
                                   break;
                               case 2:
                               {
                                   
                               }
                                   break;
                           default:
                               break;
                       }
                       
                   }

                });
            });
                   
        }
        else
        {
            [JMNotifyView showNotify:DIC[@"msg"]];
        }

    } failure:^(NSError *error) {
        
    }];
}
-(void)setup
{
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - kNavgationBarHeight)];
    [self.view addSubview:self.scrollView];
//余额
    UIView *yueV = [[UIView alloc]initWithFrame:CGRectMake(12, 10, SCREEN_WIDTH - 24, 123)];
    yueV.backgroundColor = kHexColor(@"ffffff");
    yueV.layer.cornerRadius = 16;
    yueV.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.05].CGColor;
    yueV.layer.shadowOffset = CGSizeMake(0,0);
    yueV.layer.shadowOpacity = 1;
    yueV.layer.shadowRadius = 5;
    [self.scrollView addSubview:yueV];
    
    UILabel *yueLb = [CreateTool createLabelWithFrame:CGRectMake(12, 20, 58, 20) text:QSLocalizedString(@"qs_yueKeyong") font:14 textColor:kHexColor(@"2B2B2B") tag:0];
    yueLb.adjustsFontSizeToFitWidth = YES;
    yueLb.textAlignment = NSTextAlignmentLeft;
    [yueV addSubview:yueLb];
    self.yuenumLb = [CreateTool createLabelWithFrame:CGRectMake(yueLb.right + 20, 20, 100, 20) text:@"" font:14 textColor:kHexColor(@"2B2B2B") tag:0];
    self.yuenumLb.adjustsFontSizeToFitWidth = YES;
    self.yuenumLb.textAlignment = NSTextAlignmentLeft;
    [yueV addSubview:self.yuenumLb];
    self.yuetypeLb = [CreateTool createLabelWithFrame:CGRectMake(yueV.width -60, 20, 50, 20) text:@"USDD" font:14 textColor:kHexColor(@"2B2B2B") tag:0];
    self.yuetypeLb.textAlignment = NSTextAlignmentRight;
    [yueV addSubview:self.yuetypeLb];
//当前等级
    UIView *sep0 = [[UIImageView alloc]initWithFrame:CGRectMake(12, 60, yueV.width - 24, 1)];
    sep0.backgroundColor = kHexColor(@"E5E5E5");
    [yueV addSubview:sep0];

    UILabel *levelLb = [CreateTool createLabelWithFrame:CGRectMake(12, sep0.bottom + 20, 58, 20) text:QSLocalizedString(@"qs_currentlevel") font:14 textColor:kHexColor(@"2B2B2B") tag:0];
    levelLb.textAlignment = NSTextAlignmentLeft;
    levelLb.adjustsFontSizeToFitWidth = YES;
    [yueV addSubview:levelLb];
    self.clevelLb = [CreateTool createLabelWithFrame:CGRectMake(levelLb.right + 20, sep0.bottom + 20, 100, 20) text:self.ecologymodel.level font:14 textColor:kHexColor(@"2B2B2B") tag:0];
    self.clevelLb.textAlignment = NSTextAlignmentLeft;
    self.clevelLb.adjustsFontSizeToFitWidth = YES;
    [yueV addSubview:self.clevelLb];
    UIButton *levelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [levelBtn setImage:[UIImage imageNamed:@"问号"] forState:UIControlStateNormal];
    [levelBtn addTarget:self action:@selector(gotoecoDetail) forControlEvents:UIControlEventTouchUpInside];
    [levelBtn setTitle:nil forState:UIControlStateNormal];
    levelBtn.frame = CGRectMake(yueV.width - 32, sep0.bottom + 22, 20, 20);
    [yueV addSubview:levelBtn];
//可等级等级
    UIView *canUplevelV = [[UIView alloc]initWithFrame:CGRectMake(12, yueV.bottom + 10, SCREEN_WIDTH - 24, 123)];
    canUplevelV.backgroundColor = kHexColor(@"ffffff");
    canUplevelV.layer.cornerRadius = 16;
    canUplevelV.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.05].CGColor;
    canUplevelV.layer.shadowOffset = CGSizeMake(0,0);
    canUplevelV.layer.shadowOpacity = 1;
    canUplevelV.layer.shadowRadius = 5;
    [self.scrollView addSubview:canUplevelV];

    UILabel *canUpleveLb = [CreateTool createLabelWithFrame:CGRectMake(12, 20, 58, 20) text:QSLocalizedString(@"qs_shengjiLevel") font:14 textColor:kHexColor(@"2B2B2B") tag:0];
    canUpleveLb.textAlignment = NSTextAlignmentLeft;
    canUpleveLb.adjustsFontSizeToFitWidth = YES;
    [canUplevelV addSubview:canUpleveLb];
    self.tobeLevelLb = [CreateTool createLabelWithFrame:CGRectMake(canUpleveLb.right + 20, 20, 150, 20) text:@"" font:14 textColor:kHexColor(@"2B2B2B") tag:0];
    self.tobeLevelLb.textAlignment = NSTextAlignmentLeft;
    self.tobeLevelLb.adjustsFontSizeToFitWidth = YES;
    [canUplevelV addSubview:self.tobeLevelLb];
    UIImageView *canUplevelImg = [CreateTool createImageViewWithFrame:CGRectMake(canUplevelV.width - 27, 23, 14, 14) imageString:@"back_white" tag:0];
    canUplevelImg.contentMode = UIViewContentModeScaleAspectFit;
    [canUplevelV addSubview:canUplevelImg];
    UITapGestureRecognizer *canUplevelVtap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(canUplevelVTap)];
    [canUplevelV addGestureRecognizer:canUplevelVtap];
//转入数量
    UIView *sep1 = [[UIImageView alloc]initWithFrame:CGRectMake(12, 60, yueV.width - 24, 1)];
    sep1.backgroundColor = kHexColor(@"E5E5E5");
    [canUplevelV addSubview:sep1];
    
    UILabel *upNumberLb = [CreateTool createLabelWithFrame:CGRectMake(12, sep1.bottom + 20, 58, 20) text:QSLocalizedString(@"qs_zhuanruShu") font:14 textColor:kHexColor(@"2B2B2B") tag:0];
    upNumberLb.textAlignment = NSTextAlignmentLeft;
    upNumberLb.adjustsFontSizeToFitWidth = YES;
    [canUplevelV addSubview:upNumberLb];
    
    self.numTf =  [CreateTool createTextFieldWithFrame:CGRectMake(upNumberLb.right + 20, sep1.bottom + 20, 150, 20) placeholder:QSLocalizedString(@"qs_zhuanruShu") delegate:self tag:666];
    self.numTf.font = [UIFont systemFontOfSize:14];
    self.numTf.adjustsFontSizeToFitWidth = YES;
    self.numTf.keyboardType = UIKeyboardTypeDecimalPad;
    self.numTf.textColor = kHexColor(@"2B2B2B");
    [canUplevelV addSubview:self.numTf];
//升级
    UIButton *upBtn = [CreateTool createButtonImageTitleWithFrame:CGRectMake(28,canUplevelV.bottom+117,SCREEN_WIDTH-56, 70) imageString:@"range_length" buttonTitle:QSLocalizedString(@"qs_shengji") titleColor:[UIColor whiteColor] tag:1000 target:self action:@selector(uplevel:) size:13];
    [self.scrollView addSubview:upBtn];
    
    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, upBtn.bottom + 50);
}

-(void)uplevel:(UIButton *)sender
{
 
    if (self.keyLv.length <1) {
        [JMNotifyView showNotify:QSLocalizedString(@"qs_PleaseSelectUpType")];
        return;
    }
   if (self.numTf.text.length < 1) {
       [JMNotifyView showNotify:QSLocalizedString(@"qs_PleaseshuruNum")];
       return;
   }
    if ([self.numTf.text intValue] >= [self.eco.high_hold intValue] || [self.numTf.text intValue] < [self.eco.low_hold intValue]) {
       [JMNotifyView showNotify:QSLocalizedString(@"qs_PleaseshuruduideNum")];
        return;
    }
    
//    if (![self illegalText:self.numTf.text]) {
//        [JMNotifyView showNotify:QSLocalizedString(@"qs_PleaseshuruduideNum")];
//        return;
//    }
   NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,USDDBagUpdate];
   NSDictionary *param = @{@"ecology_id":self.ecologymodel.Id,
                           @"cion_number":self.numTf.text,
                           @"levelstr":self.keyLv,
                           @"order_id":[[NSUUID UUID] UUIDString]
   };
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
   [GWNetWork postTokenWithURL:URLStr params:param success:^(id response) {
       NSDictionary *DIC = response;
       NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
       [JMNotifyView showNotify:DIC[@"msg"]];
       if ([code isEqualToString:@"200"]) {
           dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
               [self.navigationController popViewControllerAnimated:YES];
           });
       }
       [hud hideAnimated:YES];
   } failure:^(NSError *error) {
       [hud hideAnimated:YES];
   }];
}
-(void)canUplevelVTap
{
    EcologicalDetail *vc = [[EcologicalDetail alloc]init];
    vc.currentLevel = self.ecologymodel.level;
    vc.selectBackBlock = ^(NSString * _Nonnull levelstr, NSString * _Nonnull key, EcoDelModel * _Nonnull Model) {
        self.tobeLevelLb.text = levelstr;
        self.keyLv = key;
        self.eco = Model;
    };
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)gotoecoDetail
{
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
       parameters[@"agreement_type"]= @"生态详情";
       NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,showagreementtoroot];

       [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
            NSLog(@"-获取用户-responseDict--%@", responseDict);
           NSDictionary *DIC = responseDict;
                     NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                     if ([code isEqualToString:@"200"]) {
                         
                         NSArray *data = DIC[@"data"];
                                                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                                    
                                                     APPWKWebView *web = [APPWKWebView new];
                                                     web.TypeHtml = @"H5";
                                                     web.tit = QSLocalizedString(@"qs_shengtaixiangqing");
                                                     web.strURL = [NSString stringWithFormat:@"%@",data[0][@"article_info"]];
                                                     [self.navigationController pushViewController:web animated:YES];
                                                            
                                               });
                         
                         
                       
                     }
                     else if ([code isEqualToString:@"401"])
                     {
                         NotifyRelogin;
                     }
                     else
                     {
                         [JMNotifyView showNotify:DIC[@"msg"]];
                     }

       } failure:^(NSError *error) {
           
       }];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL isHaveDian;

    //判断是否有小数点
    if ([textField.text containsString:@"."]) {
        isHaveDian = YES;
    }else{
        isHaveDian = NO;
    }

    if (string.length > 0) {

        //当前输入的字符
        unichar single = [string characterAtIndex:0];
        NSLog(@"single = %c",single);

        //不能输入.0~9以外的字符
        if (!((single >= '0' && single <= '9') || single == '.')){
            NSLog(@"您输入的格式不正确");
            return NO;
        }

        //只能有一个小数点
        if (isHaveDian && single == '.') {
            NSLog(@"只能输入一个小数点");
            return NO;
        }

        //如果第一位是.则前面加上0
        if ((textField.text.length == 0) && (single == '.')) {
            textField.text = @"0";
        }

        //如果第一位是0则后面必须输入.
        if ([textField.text hasPrefix:@"0"]) {
            if (textField.text.length > 1) {
                NSString *secondStr = [textField.text substringWithRange:NSMakeRange(1, 1)];
                if (![secondStr isEqualToString:@"."]) {
                    NSLog(@"第二个字符必须是小数点");
                    return NO;
                }
            }else{
                if (![string isEqualToString:@"."]) {
                    NSLog(@"第二个字符必须是小数点");
                    return NO;
                }
            }
        }

        //小数点后最多能输入两位
        if (isHaveDian) {
            NSRange ran = [textField.text rangeOfString:@"."];
            //由于range.location是NSUInteger类型的，所以不能通过(range.location - ran.location) > 2来判断
            if (range.location > ran.location) {
                if ([textField.text pathExtension].length > 1) {
                    NSLog(@"小数点后最多有两位小数");
                    return NO;
                }
            }
        }

    }

    return YES;
}

//-(BOOL)illegalText:(NSString *)str
//{
//    NSString *regex = @"^[0-9]+(.[0-9]{1,3})?$";
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
//       // 字符串判断，然后BOOL值
//    BOOL result = [predicate evaluateWithObject:str];
//    if (result) {
//        return YES;
//    }
//    return NO;
//}
@end
