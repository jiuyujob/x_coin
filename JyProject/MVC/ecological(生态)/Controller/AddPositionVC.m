//
//  AddPositionVC.m
//  JyProject
//
//  Created by mac on 2019/12/11.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "AddPositionVC.h"
#import "CurrencyTypeVC.h"
@interface AddPositionVC ()
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UILabel *bizhongLb;
@property (nonatomic, strong) UILabel *yuenumLb;
@property (nonatomic, strong) UILabel *yuetypeLb;
@property (nonatomic, strong) UILabel *chicangLevelLb;
@property (nonatomic, strong) UITextField *numTf;
@end

@implementation AddPositionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = @"新增持仓";//QSLocalizedString(@"qs_TiBi");
    [self setup];
}

-(void)setup{
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - kNavgationBarHeight)];
    [self.view addSubview:self.scrollView];
//提币币种
    UIView *bitV = [[UIView alloc]initWithFrame:CGRectMake(16, 10, SCREEN_WIDTH - 32, 40)];
    // gradient
    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = bitV.bounds;
    gl.startPoint = CGPointMake(0.49, 0);
    gl.endPoint = CGPointMake(0.49, 1);
    gl.colors = @[(__bridge id)[UIColor colorWithRed:149/255.0 green:237/255.0 blue:255/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:0/255.0 green:190/255.0 blue:229/255.0 alpha:1.0].CGColor];
    gl.locations = @[@(0), @(1.0f)];
    bitV.layer.cornerRadius = 4;
    bitV.clipsToBounds = YES;
    [bitV.layer addSublayer:gl];
    [self.scrollView addSubview:bitV];
    UILabel *bitVLb = [CreateTool createLabelWithFrame:CGRectMake(10, 10, 58, 20) text:@"提币币种" font:14 textColor:[UIColor whiteColor] tag:0];
    [bitV addSubview:bitVLb];
    self.bizhongLb = [CreateTool createLabelWithFrame:CGRectMake(bitVLb.right + 20, 10, 100, 20) text:@"TFOR" font:14 textColor:[UIColor whiteColor] tag:0];
    self.bizhongLb.textAlignment = NSTextAlignmentLeft;
    [bitV addSubview:self.bizhongLb];
    UIImageView *bitVImg = [CreateTool createImageViewWithFrame:CGRectMake(bitV.width - 27, 13, 14, 14) imageString:@"back_white" tag:0];
    bitVImg.contentMode = UIViewContentModeScaleAspectFit;
    [bitV addSubview:bitVImg];
    UITapGestureRecognizer *bitVtap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(bitVTap)];
    [bitV addGestureRecognizer:bitVtap];
//余额
    UIImageView *yueV = [[UIImageView alloc]initWithFrame:CGRectMake(16, bitV.bottom + 20, SCREEN_WIDTH - 32, 40)];
    yueV.image = [UIImage imageNamed:@"Rectangle"];
    [self.scrollView addSubview:yueV];
    UILabel *yueLb = [CreateTool createLabelWithFrame:CGRectMake(10, 10, 58, 20) text:QSLocalizedString(@"qs_yueKeyong") font:14 textColor:[UIColor whiteColor] tag:0];
    [yueV addSubview:yueLb];
    self.yuenumLb = [CreateTool createLabelWithFrame:CGRectMake(yueLb.right + 20, 10, 100, 20) text:@"1200.00" font:14 textColor:[UIColor whiteColor] tag:0];
    self.yuenumLb.textAlignment = NSTextAlignmentLeft;
    [yueV addSubview:self.yuenumLb];
    self.yuetypeLb = [CreateTool createLabelWithFrame:CGRectMake(yueV.width -60, 10, 50, 20) text:@"TFOR" font:14 textColor:[UIColor whiteColor] tag:0];
    self.yuetypeLb.textAlignment = NSTextAlignmentRight;
    [yueV addSubview:self.yuetypeLb];
//提示lb
    UILabel *mostLb = [[UILabel alloc]initWithFrame:CGRectMake(104, yueV.bottom + 10, 150, 14)];
    mostLb.text = @"可转1200.00 USDD"; //QSLocalizedString
    mostLb.font = [UIFont systemFontOfSize:12];
    mostLb.textColor = kHexColor(@"#FFCE76");
    mostLb.textAlignment = NSTextAlignmentLeft;
    [self.scrollView addSubview:mostLb];
//持仓等级
    UIView *positionLevelV = [[UIView alloc]initWithFrame:CGRectMake(16, mostLb.bottom + 20, SCREEN_WIDTH - 32, 40)];
    // gradient
    CAGradientLayer *levelgl = [CAGradientLayer layer];
    levelgl.frame = positionLevelV.bounds;
    levelgl.startPoint = CGPointMake(0.49, 0);
    levelgl.endPoint = CGPointMake(0.49, 1);
    levelgl.colors = @[(__bridge id)[UIColor colorWithRed:149/255.0 green:237/255.0 blue:255/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:0/255.0 green:190/255.0 blue:229/255.0 alpha:1.0].CGColor];
    levelgl.locations = @[@(0), @(1.0f)];
    positionLevelV.layer.cornerRadius = 4;
    positionLevelV.clipsToBounds = YES;
    [positionLevelV.layer addSublayer:levelgl];
    [self.scrollView addSubview:positionLevelV];
    UILabel *positionLevelLb = [CreateTool createLabelWithFrame:CGRectMake(10, 10, 58, 20) text:@"持仓等级" font:14 textColor:[UIColor whiteColor] tag:0];
    [positionLevelV addSubview:positionLevelLb];
    self.chicangLevelLb = [CreateTool createLabelWithFrame:CGRectMake(positionLevelLb.right + 20, 10, 150, 20) text:@"粉丝100～500 USDD" font:14 textColor:[UIColor whiteColor] tag:0];
    self.chicangLevelLb.textAlignment = NSTextAlignmentLeft;
    [positionLevelV addSubview:self.chicangLevelLb];
    UIImageView *positionLevelImg = [CreateTool createImageViewWithFrame:CGRectMake(positionLevelV.width - 27, 13, 14, 14) imageString:@"back_white" tag:0];
    positionLevelImg.contentMode = UIViewContentModeScaleAspectFit;
    [positionLevelV addSubview:positionLevelImg];
    UITapGestureRecognizer *positionLevelTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(positionLevelTap)];
    [positionLevelV addGestureRecognizer:positionLevelTap];
    
//存入数量
    UIImageView *intoNumberV = [[UIImageView alloc]initWithFrame:CGRectMake(16, positionLevelV.bottom + 20, SCREEN_WIDTH - 32, 40)];
    intoNumberV.image = [UIImage imageNamed:@"Rectangle"];
    intoNumberV.userInteractionEnabled = YES;
    [self.scrollView addSubview:intoNumberV];
    UILabel *intoNumberLb = [CreateTool createLabelWithFrame:CGRectMake(10, 10, 58, 20) text:@"存入数量" font:14 textColor:[UIColor whiteColor] tag:0];
    [intoNumberV addSubview:intoNumberLb];
    
    self.numTf =  [CreateTool createTextFieldWithFrame:CGRectMake(intoNumberLb.right + 20, 5, 150, 30) placeholder:@"存入数量" delegate:self tag:666];
    self.numTf.font = [UIFont systemFontOfSize:14];
    [intoNumberV addSubview:self.numTf];
    
    UILabel *intoNumberTypeLb = [CreateTool createLabelWithFrame:CGRectMake(yueV.width -60, 10, 50, 20) text:@"USDD" font:14 textColor:[UIColor whiteColor] tag:0];
    intoNumberTypeLb.textAlignment = NSTextAlignmentRight;
    [intoNumberV addSubview:intoNumberTypeLb];
    
//提示
    UILabel *note1Lb = [[UILabel alloc]initWithFrame:CGRectMake(26, intoNumberV.bottom + 10, 150, 14)];
       note1Lb.text = @"存入数量不能为0"; //QSLocalizedString
       note1Lb.font = [UIFont systemFontOfSize:12];
       note1Lb.textColor = [UIColor redColor];
       [self.scrollView addSubview:note1Lb];
    UILabel *note2Lb = [[UILabel alloc]initWithFrame:CGRectMake(26, note1Lb.bottom + 10, 150, 14)];
       note2Lb.text = @"存入数量不能大于1200.00"; //QSLocalizedString
       note2Lb.font = [UIFont systemFontOfSize:12];
       note2Lb.textColor = [UIColor redColor];
       [self.scrollView addSubview:note2Lb];
//升级
    UIButton *addBtn = [CreateTool createButtonWithFrame:CGRectMake(62,note2Lb.bottom+222,SCREEN_WIDTH-124, 30) buttonTitle:@"新增" titleColor:[UIColor whiteColor] selectTitleColor:nil tag:1000 target:self action:@selector(addBtn:) size:13];
    addBtn.backgroundColor = buttonColor;
    addBtn.layer.cornerRadius = 5;
    [self.scrollView addSubview:addBtn];
    
    
    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, addBtn.bottom + 50);
}

- (BOOL) deptNumInputShouldNumber:(NSString *)str
{
   if (str.length == 0) {
        return NO;
    }
    NSString *regex = @"[0-9]*";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    if ([pred evaluateWithObject:str]) {
        return YES;
    }
    return NO;
}
-(void)addBtn:(UIButton *)sender
{
    if (![self deptNumInputShouldNumber:self.numTf.text]) {
        [JMNotifyView showNotify:@"请输入正确的数字"];
        return;
    }
    NSString *url  = @"http://192.168.8.126/api/v1/ecology/create_new_warehouse";
    NSString *levelstr = [self.chicangLevelLb.text substringToIndex:2];
    NSDictionary *param = @{@"user_id":@"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiYWU2M2ViZWI4MmI3NDdlNmEyM2MxYTQwNTBjYWRjNDciLCJuYW1lIjoiIiwibW9iaWxlIjoiIiwiZW1haWwiOiIxNTA0NDgyNzU2N0AxNjMuY29tIiwiZmF0aGVyX2lkIjoiIiwiZXhwIjoxNTc3MzU5MjA5LCJpc3MiOiJuZXd0cmVrV2FuZyIsIm5iZiI6MTU3NjA2MjIwOX0.6F0a8DAT3LFzmKGPdOslLUJogfcWnbbnub3CjyE4ZYM",
                            @"coin_number":self.numTf.text,
                            @"levelstr":levelstr
       };
    [GWNetWork postWithURL:url params:param success:^(NSDictionary *resp) {
        if ([resp isKindOfClass:[NSNull class]]) {
            
            NSLog(@"参数传递错误!");
        }else{
            
        }
    } failure:^(NSError *error) {
        
    }];
}

-(void)bitVTap
{
    CurrencyTypeVC *vc = [CurrencyTypeVC new];
    vc.Type = @"币种";
    vc.buttitCliCK = ^(NSString * _Nullable buttit, NSString * _Nullable currency_id, NSString * _Nullable coin_id, NSString * _Nullable iid) {
         self.bizhongLb.text = buttit;
    };
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)positionLevelTap
{
    CurrencyTypeVC *vc = [CurrencyTypeVC new];
    vc.Type = @"等级";
    vc.buttitCliCK = ^(NSString * _Nullable buttit, NSString * _Nullable currency_id, NSString * _Nullable coin_id, NSString * _Nullable iid) {
         self.chicangLevelLb.text = buttit;
    };
    [self.navigationController pushViewController:vc animated:YES];
}


@end
