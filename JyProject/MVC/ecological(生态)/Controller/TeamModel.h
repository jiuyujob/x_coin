//
//  TeamModel.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/19.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TeamModel : NSObject


@property (nonatomic, copy) NSString *Profit;
@property (nonatomic, copy) NSString *children;
@property (nonatomic, copy) NSString *fans;
@property (nonatomic, copy) NSString *grandson;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *team;

@end

NS_ASSUME_NONNULL_END
