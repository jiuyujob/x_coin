//
//  EcologicalIntoVC.m
//  JyProject
//
//  Created by mac on 2019/12/11.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "EcologicalIntoVC.h"
#import "CurrencyTypeVC.h"
#import "AFNetworking.h"
#import "EcoDDListVC.h"
#import <OpenAL/OpenAL.h>
#import "APPWKWebView.h"
@interface EcologicalIntoVC ()<UITextFieldDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UILabel *bizhongLb;
@property (nonatomic, strong) UILabel *yuenumLb;
@property (nonatomic, strong) UILabel *yuetypeLb;
@property (nonatomic, strong) UILabel *clevelLb;
@property (nonatomic, strong) UILabel *freeDDLb;
@property (nonatomic, strong) UITextField *numTf;
@end

@implementation EcologicalIntoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = QSLocalizedString(@"qs_shengtaiinto");;//
    [self setup];
    [self checkBell];
}
-(void)checkBell
{
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,accountinfoURL];
    NSLog(@"url:%@",URLStr);
    [GWNetWork getKotenWithURL:URLStr params:nil success:^(id responseDict) {
        NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
        NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
        if ([code isEqualToString:@"200"]) {
              //成功
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                   NSArray *data = DIC[@"data"];
              
                dispatch_async(dispatch_get_main_queue(), ^{
                   for (int i= 0; i<data.count; i++) {
                       
                       NSString *text = [NSString stringWithFormat:@"%.2f",[data[i][@"balance"] floatValue]];;
                       switch (i) {
                           case 0:
                           {
                               
                           }
                               break;
                               case 1:
                               {
                                   self.yuenumLb.text = text;
                               }
                                   break;
                               case 2:
                               {
                                   
                               }
                                   break;
                           default:
                               break;
                       }
                       
                   }

                });
            });
                   
        }
        else
        {
            [JMNotifyView showNotify:DIC[@"msg"]];
        }

    } failure:^(NSError *error) {
        
    }];
}
-(void)setup{
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - kNavgationBarHeight)];
    [self.view addSubview:self.scrollView];
//余额
    UIView *yueV = [[UIView alloc]initWithFrame:CGRectMake(12, 10, SCREEN_WIDTH - 24, 123)];
    yueV.backgroundColor = kHexColor(@"ffffff");
    yueV.layer.cornerRadius = 16;
    yueV.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.05].CGColor;
    yueV.layer.shadowOffset = CGSizeMake(0,0);
    yueV.layer.shadowOpacity = 1;
    yueV.layer.shadowRadius = 5;
    [self.scrollView addSubview:yueV];
    UILabel *yueLb = [CreateTool createLabelWithFrame:CGRectMake(12, 20, 58, 20) text:QSLocalizedString(@"qs_yueKeyong") font:14 textColor:kHexColor(@"2B2B2B") tag:0];
    yueLb.textAlignment = NSTextAlignmentLeft;
    yueLb.adjustsFontSizeToFitWidth = YES;
    [yueV addSubview:yueLb];
    self.yuenumLb = [CreateTool createLabelWithFrame:CGRectMake(yueLb.right + 20, 20, 100, 20) text:@"" font:14 textColor:kHexColor(@"2B2B2B") tag:0];
    self.yuenumLb.textAlignment = NSTextAlignmentLeft;
    self.yuenumLb.adjustsFontSizeToFitWidth = YES;
    [yueV addSubview:self.yuenumLb];
    self.yuetypeLb = [CreateTool createLabelWithFrame:CGRectMake(yueV.width -60, 20, 50, 20) text:@"USDD" font:14 textColor:kHexColor(@"2B2B2B") tag:0];
    self.yuetypeLb.textAlignment = NSTextAlignmentRight;
    [yueV addSubview:self.yuetypeLb];
//当前等级
    UIView *sep0 = [[UIImageView alloc]initWithFrame:CGRectMake(12, 60, yueV.width - 24, 1)];
    sep0.backgroundColor = kHexColor(@"E5E5E5");
    [yueV addSubview:sep0];
    
    UILabel *levelLb = [CreateTool createLabelWithFrame:CGRectMake(12, sep0.bottom + 20, 58, 20) text:QSLocalizedString(@"qs_currentlevel") font:14 textColor:kHexColor(@"2B2B2B") tag:0];
    levelLb.textAlignment = NSTextAlignmentLeft;
    levelLb.adjustsFontSizeToFitWidth = YES;
    [yueV addSubview:levelLb];
    self.clevelLb = [CreateTool createLabelWithFrame:CGRectMake(levelLb.right + 20, sep0.bottom + 20, 100, 20) text:self.ecologymodel.level font:14 textColor:kHexColor(@"2B2B2B") tag:0];
    self.clevelLb.textAlignment = NSTextAlignmentLeft;
    [yueV addSubview:self.clevelLb];
    UIButton *levelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [levelBtn setImage:[UIImage imageNamed:@"问号"] forState:UIControlStateNormal];
    [levelBtn addTarget:self action:@selector(gotoecoDetail) forControlEvents:UIControlEventTouchUpInside];
    [levelBtn setTitle:nil forState:UIControlStateNormal];
    levelBtn.frame = CGRectMake(yueV.width - 34, sep0.bottom + 22, 24, 24);
    [yueV addSubview:levelBtn];
//升级数量
    UIView *intoNumberV = [[UIView alloc]initWithFrame:CGRectMake(12, yueV.bottom + 10, SCREEN_WIDTH - 24, 60)];
    intoNumberV.backgroundColor = kHexColor(@"ffffff");
    intoNumberV.layer.cornerRadius = 16;
    intoNumberV.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.05].CGColor;
    intoNumberV.layer.shadowOffset = CGSizeMake(0,0);
    intoNumberV.layer.shadowOpacity = 1;
    intoNumberV.layer.shadowRadius = 5;
    [self.scrollView addSubview:intoNumberV];
    
    UILabel *intoNumberLb = [CreateTool createLabelWithFrame:CGRectMake(12, 20, 58, 20) text:QSLocalizedString(@"qs_zhuanruShu") font:14 textColor:kHexColor(@"2B2B2B") tag:0];
    intoNumberLb.textAlignment = NSTextAlignmentLeft;
    intoNumberLb.adjustsFontSizeToFitWidth = YES;
    [intoNumberV addSubview:intoNumberLb];
    
    self.numTf =  [CreateTool createTextFieldWithFrame:CGRectMake(intoNumberLb.right + 20, 20, 150, 20) placeholder:QSLocalizedString(@"qs_zhuanruShu") delegate:self tag:666];
    self.numTf.font = [UIFont systemFontOfSize:14];
    self.numTf.adjustsFontSizeToFitWidth = YES;
    self.numTf.textColor = kHexColor(@"2B2B2B");
    self.numTf.keyboardType = UIKeyboardTypeDecimalPad;
    [intoNumberV addSubview:self.numTf];
//升级
    UIButton *intoBtn = [CreateTool createButtonImageTitleWithFrame:CGRectMake(28,intoNumberV.bottom+178,SCREEN_WIDTH-56, 70) imageString:@"range_length" buttonTitle:QSLocalizedString(@"qs_zhuanru") titleColor:[UIColor whiteColor] tag:1000 target:self action:@selector(intoBtn:) size:16];
    [self.scrollView addSubview:intoBtn];
    
    
    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, intoBtn.bottom + 50);
}

-(void)intoBtn:(UIButton *)sender
{

    if (self.numTf.text.length < 1) {
        [JMNotifyView showNotify:QSLocalizedString(@"qs_PleaseshuruNum")];
        return;
    }
    
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,changeintoUSDD];
    NSDictionary *param = @{@"ecology_id":self.ecologymodel.Id,
                            @"coin_number":self.numTf.text,
                            @"order_id":[[NSUUID UUID] UUIDString]
    };
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    [GWNetWork postTokenWithURL:URLStr params:param success:^(id response) {
        NSDictionary *DIC = response;
        NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
        [JMNotifyView showNotify:DIC[@"msg"]];
        if ([code isEqualToString:@"200"]) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
        [hud hideAnimated:YES];
    } failure:^(NSError *error) {
        [hud hideAnimated:YES];
    }];
    
}
-(void)bitVTap
{
    CurrencyTypeVC *vc = [CurrencyTypeVC new];
    vc.Type = @"币种";
    vc.buttitCliCK = ^(NSString * _Nullable buttit, NSString * _Nullable currency_id, NSString * _Nullable coin_id, NSString * _Nullable iid) {
        
        self.bizhongLb.text = buttit;
        
    };
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)gotoecoDetail
{
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
       parameters[@"agreement_type"]= @"生态详情";
       NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,showagreementtoroot];

       [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
            NSLog(@"-获取用户-responseDict--%@", responseDict);
           NSDictionary *DIC = responseDict;
                     NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                     if ([code isEqualToString:@"200"]) {
                         
                         NSArray *data = DIC[@"data"];
                                                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                                    
                                                     APPWKWebView *web = [APPWKWebView new];
                                                     web.TypeHtml = @"H5";
                                                     web.tit = QSLocalizedString(@"qs_shengtaixiangqing");
                                                     web.strURL = [NSString stringWithFormat:@"%@",data[0][@"article_info"]];
                                                     [self.navigationController pushViewController:web animated:YES];
                                                            
                                               });
                         
                         
                       
                     }
                     else if ([code isEqualToString:@"401"])
                     {
                         NotifyRelogin;
                     }
                     else
                     {
                         [JMNotifyView showNotify:DIC[@"msg"]];
                     }

       } failure:^(NSError *error) {
           
       }];
//    SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:QSLocalizedString(@"qs_gongnengweikaifang") sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
//    [special withSureClick:^(NSString *string) {
//        EcoDDListVC *vc = [[EcoDDListVC alloc]init];
//        [self.navigationController pushViewController:vc animated:YES];
//    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL isHaveDian;

    //判断是否有小数点
    if ([textField.text containsString:@"."]) {
        isHaveDian = YES;
    }else{
        isHaveDian = NO;
    }

    if (string.length > 0) {

        //当前输入的字符
        unichar single = [string characterAtIndex:0];
        NSLog(@"single = %c",single);

        //不能输入.0~9以外的字符
        if (!((single >= '0' && single <= '9') || single == '.')){
            NSLog(@"您输入的格式不正确");
            return NO;
        }

        //只能有一个小数点
        if (isHaveDian && single == '.') {
            NSLog(@"只能输入一个小数点");
            return NO;
        }

        //如果第一位是.则前面加上0
        if ((textField.text.length == 0) && (single == '.')) {
            textField.text = @"0";
        }

        //如果第一位是0则后面必须输入.
        if ([textField.text hasPrefix:@"0"]) {
            if (textField.text.length > 1) {
                NSString *secondStr = [textField.text substringWithRange:NSMakeRange(1, 1)];
                if (![secondStr isEqualToString:@"."]) {
                    NSLog(@"第二个字符必须是小数点");
                    return NO;
                }
            }else{
                if (![string isEqualToString:@"."]) {
                    NSLog(@"第二个字符必须是小数点");
                    return NO;
                }
            }
        }

        //小数点后最多能输入两位
        if (isHaveDian) {
            NSRange ran = [textField.text rangeOfString:@"."];
            //由于range.location是NSUInteger类型的，所以不能通过(range.location - ran.location) > 2来判断
            if (range.location > ran.location) {
                if ([textField.text pathExtension].length > 1) {
                    NSLog(@"小数点后最多有两位小数");
                    return NO;
                }
            }
        }

    }

    return YES;
}

@end
