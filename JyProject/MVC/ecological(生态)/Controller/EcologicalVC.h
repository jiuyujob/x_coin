//
//  EcologicalVC.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/5.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface EcologicalVC : JYBaseVC<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,retain) UIView* FooterView;
@property (nonatomic,retain) UIView* headerView;

@property (nonatomic, assign) CGFloat zongzz;
@property (nonatomic, assign) CGFloat zongsy;
@end

NS_ASSUME_NONNULL_END
