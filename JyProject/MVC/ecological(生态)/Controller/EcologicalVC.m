//
//  EcologicalVC.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/5.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "EcologicalVC.h"

#import "EcologicalCell.h"
#import "DDToDTVC.h"
#import "BillListVC.h"
#import "EcologicalUpVC.h"
#import "EcologicalIntoVC.h"
#import "EcologicalModel.h"
#import "MJExtension.h"
@interface EcologicalVC ()
@property (nonatomic, strong) UILabel *jdzhichanLb;
@property (nonatomic, strong) UILabel *jrfenhongLb;
@property (nonatomic, strong) UILabel *jiedianLevelLb;
@property (nonatomic, strong) EcologicalModel *ecomd;

@property (nonatomic, strong) UILabel *zongtouzhiLb;
@property (nonatomic, strong) UILabel *zongshouyiLb;
@end

@implementation EcologicalVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = QSLocalizedString(@"qs_ShenTai");//QSLocalizedString(@"qs_TiBi");
    [self setup];
   
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self NetworkRequest];
}

-(void)setup
{
       _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT-kNavgationBarHeight) style:UITableViewStylePlain];
       _tableView.backgroundColor = [UIColor clearColor];
       _tableView.delegate = self;
       _tableView.dataSource = self;
       _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
              if (@available(iOS 11.0, *)){
                  _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    _tableView.tableFooterView = self.FooterView;
    _tableView.tableHeaderView = self.headerView;
    [self.view addSubview:_tableView];
}

-(UIView *)FooterView
{
    if (!_FooterView) {
        self.FooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH,142)];
               self.FooterView.backgroundColor = [UIColor clearColor];
               UIView *fooview = [[UIView alloc] init];
               fooview.frame = CGRectMake(12,10,SCREEN_WIDTH-24,122);
               fooview.backgroundColor = kHexColor(@"ffffff");
               fooview.layer.cornerRadius = 16;
               fooview.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.05].CGColor;
               fooview.layer.shadowOffset = CGSizeMake(0,0);
               fooview.layer.shadowOpacity = 1;
               fooview.layer.shadowRadius = 5;
               [self.FooterView addSubview:fooview];
               
               UILabel *ecoLb = [CreateTool createLabelWithFrame:CGRectMake(12,20, 200, 22) text:[NSString stringWithFormat:@"%@",@"TFOR"] font:16 textColor:kHexColor(@"2B2B2B") tag:0];
               ecoLb.textAlignment = NSTextAlignmentLeft;
               [fooview addSubview:ecoLb];
             
           //节点资产
               self.jdzhichanLb = [CreateTool createLabelWithFrame:CGRectMake(12, ecoLb.bottom + 8, 200, 22) text:@"节点资产:123000.12" font:12 textColor:kHexColor(@"E2E2E2") tag:0];
               self.jdzhichanLb.textAlignment = NSTextAlignmentLeft;
               self.jdzhichanLb.adjustsFontSizeToFitWidth = YES;
               [fooview addSubview:self.jdzhichanLb];
             
           //级别
               self.jiedianLevelLb = [CreateTool createLabelWithFrame:CGRectMake(12, self.jdzhichanLb.bottom + 10, 150, 20) text:@"级别:超级节点" font:14 textColor:kHexColor(@"939393") tag:0];
               self.jiedianLevelLb.textAlignment = NSTextAlignmentLeft;
               [fooview addSubview:self.jiedianLevelLb];
               
           //今日分红
               self.jrfenhongLb = [CreateTool createLabelWithFrame:CGRectMake(217, ecoLb.bottom + 14, fooview.width -  229, 16) text:@"今日分红:1200.3" font:11 textColor:kHexColor(@"2B2B2B") tag:0];
               self.jrfenhongLb.textAlignment = NSTextAlignmentRight;
               self.jrfenhongLb.adjustsFontSizeToFitWidth = YES;
               [fooview addSubview:self.jrfenhongLb];
    }
    return _FooterView;
}

-(UIView *)headerView
{
    if (!_headerView) {
        _headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 142)];
        UIView *hdview = [[UIView alloc] init];
        hdview.frame = CGRectMake(12,10,SCREEN_WIDTH-24,122);
        hdview.backgroundColor = kHexColor(@"ffffff");
        hdview.layer.cornerRadius = 16;
        hdview.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.05].CGColor;
        hdview.layer.shadowOffset = CGSizeMake(0,0);
        hdview.layer.shadowOpacity = 1;
        hdview.layer.shadowRadius = 5;
        [_headerView addSubview:hdview];
        
        UILabel *ecoLb = [CreateTool createLabelWithFrame:CGRectMake(12,20, 200, 22) text:[NSString stringWithFormat:@"%@",QSLocalizedString(@"qs_shengtaizichan")] font:16 textColor:kHexColor(@"2B2B2B") tag:0];
        ecoLb.textAlignment = NSTextAlignmentLeft;
        [hdview addSubview:ecoLb];
        
        //总投资
        _zongtouzhiLb = [CreateTool createLabelWithFrame:CGRectMake(12, ecoLb.bottom + 10, hdview.width - 24 , 22) text:nil font:10 textColor:kHexColor(@"2B2B2B") tag:0];
        _zongtouzhiLb.textAlignment = NSTextAlignmentLeft;
        NSString *zongtouzhi = [NSString stringWithFormat:@"%@%f USDD",QSLocalizedString(@"qs_zongtouzhi"),self.zongzz];
        NSRange range = [zongtouzhi rangeOfString:@"："];
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc]initWithString:zongtouzhi];
        [string addAttributes:@{NSForegroundColorAttributeName: kHexColor(@"2B2B2B"),NSFontAttributeName: [UIFont systemFontOfSize:14]} range:NSMakeRange(0, range.location + 1)];
        [string addAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16], NSForegroundColorAttributeName: kHexColor(@"4F98CA")} range:NSMakeRange(range.location + 1, zongtouzhi.length - range.location - 1)];
        _zongtouzhiLb.attributedText = string;
        [hdview addSubview:_zongtouzhiLb];
             
        //级别
        _zongshouyiLb = [CreateTool createLabelWithFrame:CGRectMake(12, _zongtouzhiLb.bottom + 10, hdview.width - 24, 14) text:nil font:14 textColor:kHexColor(@"2B2B2B") tag:0];
        _zongshouyiLb.textAlignment = NSTextAlignmentLeft;
        _zongshouyiLb.text = [NSString stringWithFormat:@"%@%f",QSLocalizedString(@"qs_zongshouyi"),self.zongsy];
        [hdview addSubview:_zongshouyiLb];
    }
    return _headerView;
}
#pragma mark - 请求接口
- (void)NetworkRequest{
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
       parameters[@"token"]= [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,showecologyindexURL];
    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
         NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
                  NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                  if ([code isEqualToString:@"200"]) {
                      //成功
                      
                      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                          
                          NSDictionary *data = DIC[@"data"];
                          
                          self.ecomd = [EcologicalModel mj_objectWithKeyValues:data];
                          //是否显示超级节点
                          NSString *Super_peer_bool = [NSString stringWithFormat:@"%@",self.ecomd.super_peer_bool];
                        
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [self.tableView reloadData];
                              if ([Super_peer_bool isEqualToString:@"1"])
                              {
                                  self.FooterView.hidden = NO;
                                  //节点资产
                                  NSString *str = [NSString stringWithFormat:@"%@%@",QSLocalizedString(@"qs_JieDianZiChang"),self.ecomd.super_peer.usdd];
                                  NSRange range = [str rangeOfString:@"："];
                                  NSMutableAttributedString *string = [[NSMutableAttributedString alloc]initWithString:str];
                                  [string addAttributes:@{NSForegroundColorAttributeName: kHexColor(@"2B2B2B"),NSFontAttributeName: [UIFont systemFontOfSize:14]} range:NSMakeRange(0, range.location + 1)];
                                  [string addAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16], NSForegroundColorAttributeName: kHexColor(@"4F98CA")} range:NSMakeRange(range.location + 1, str.length - range.location - 1)];
                                  self.jdzhichanLb.attributedText = string;
                                  //级别
                                  self.jiedianLevelLb.text = [NSString stringWithFormat:@"%@：%@",QSLocalizedString(@"qs_JiBie"),self.ecomd.super_peer.level];
                                  
                                  //今日分红
                                  self.jrfenhongLb.text = [NSString stringWithFormat:@"%@：%@",QSLocalizedString(@"qs_JinRiFenHong"),self.ecomd.super_peer.today_a_bouns];
                                  
                              }
                              else
                              {
                                  self.FooterView.hidden = YES;
                              }

                          });
                      });
                      
                  }
                  else if ([code isEqualToString:@"401"])
                  {
                      NotifyRelogin;
                  }
                  else
                  {
                      [JMNotifyView showNotify:DIC[@"msg"]];
                  }

    } failure:^(NSError *error) {
        
    }];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ecomd.ecological_poject.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"myCell";
    EcologicalCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];//从重用队列里获取可重用的cell
    if (!cell)
    {
        cell = [[EcologicalCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (self.ecomd.ecological_poject && indexPath.row < [self.ecomd.ecological_poject count])
    {
        cell.proj = self.ecomd.ecological_poject[indexPath.row];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 289;
}

@end
