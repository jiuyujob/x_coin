//
//  DDToTchooseVC.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/19.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

//兑换 USDD 页面专用
typedef void(^USDDbuttonClick)(NSString* _Nullable  buttit,NSString *_Nullable currid,NSString *_Nullable money,NSString *_Nullable change_id);

NS_ASSUME_NONNULL_BEGIN

@interface DDToTchooseVC : JYBaseVC<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,retain) NSMutableArray *ArrayData;
@property (nonatomic,retain) NSMutableArray *transferData;


@property (nonatomic, copy) USDDbuttonClick USDDbuttonClicked;

@end

NS_ASSUME_NONNULL_END
