//
//  DDToDTVC.m
//  JyProject
//
//  Created by mac on 2019/12/11.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "DDToDTVC.h"
#import "DDToTchooseVC.h"

@interface DDToDTVC ()
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UILabel *DTYueLb;
@property (nonatomic, strong) UILabel *DTpercentLb;
@property (nonatomic, strong) UITextField *numTf;
@property (nonatomic, strong) UILabel *DDGetLb;
@property (nonatomic, strong) UILabel *bizhongLb;
@end

@implementation DDToDTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = QSLocalizedString(@"qs_duihuanusdd");//QSLocalizedString(@"qs_TiBi");
    [self setup];
    
    [self NetworkRequest];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
-(void)setup
{
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - kNavgationBarHeight)];
    [self.view addSubview:self.scrollView];

    UIView *bitV = [[UIView alloc]initWithFrame:CGRectMake(15,20, SCREEN_WIDTH-30,156)];
    bitV.userInteractionEnabled = YES;
    bitV.layer.cornerRadius = 10;
    bitV.backgroundColor = [UIColor whiteColor];
    bitV.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    bitV.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    bitV.layer.shadowOpacity = 0.3;// 阴影透明度，默认0
    bitV.layer.shadowRadius = 3;// 阴影半径，默认3
    [self.scrollView addSubview:bitV];

//资金账户可用
    UILabel *bitVLb = [CreateTool createLabelWithFrame:CGRectMake(10,0,100,50) text:QSLocalizedString(@"qs_ZiJinZhangHu") font:14 textColor:[UIColor blackColor] tag:0];
    bitVLb.textAlignment = NSTextAlignmentLeft;
    [bitV addSubview:bitVLb];
    self.bizhongLb = [CreateTool createLabelWithFrame:CGRectMake(bitVLb.right + 15,0,200,50) text:self.symbol font:15 textColor:[UIColor blackColor] tag:0];
    self.bizhongLb.textAlignment = NSTextAlignmentLeft;
    [bitV addSubview:self.bizhongLb];
    
    UIImageView *line = [CreateTool createImageViewWithFrame:CGRectMake(10,bitVLb.bottom,bitV.width-20,0.3) imageString:@"" tag:0];
    line.backgroundColor = buttonHuiSeColor;
    [bitV addSubview:line];

//余额
    UILabel *yueVLb = [CreateTool createLabelWithFrame:CGRectMake(10,line.bottom,100,50) text:QSLocalizedString(@"qs_yueKeyong") font:14 textColor:[UIColor blackColor] tag:0];
    yueVLb.textAlignment = NSTextAlignmentLeft;
    [bitV addSubview:yueVLb];
    self.DTYueLb = [CreateTool createLabelWithFrame:CGRectMake(yueVLb.right + 15, yueVLb.y, 200,50) text:self.balance font:15 textColor:[UIColor blackColor] tag:0];
    self.DTYueLb.textAlignment = NSTextAlignmentLeft;
    [bitV addSubview:self.DTYueLb];
    self.yueVTypeLb = [CreateTool createLabelWithFrame:CGRectMake(bitV.width -60,yueVLb.y, 50,50) text:self.symbol font:14 textColor:[UIColor blackColor] tag:0];
    self.yueVTypeLb.textAlignment = NSTextAlignmentRight;
    [bitV addSubview:self.yueVTypeLb];
    
    UIImageView *line2 = [CreateTool createImageViewWithFrame:CGRectMake(10,yueVLb.bottom,bitV.width-20,0.3) imageString:@"" tag:0];
    line2.backgroundColor = buttonHuiSeColor;
    [bitV addSubview:line2];
//兑换比例
    UILabel *percentVLb = [CreateTool createLabelWithFrame:CGRectMake(10, line2.bottom,100,50) text:QSLocalizedString(@"qs_jiaoyiduishijia") font:14 textColor:[UIColor blackColor] tag:0];
    percentVLb.textAlignment = NSTextAlignmentLeft;
    [bitV addSubview:percentVLb];
    self.DTpercentLb = [CreateTool createLabelWithFrame:CGRectMake(percentVLb.right + 15,percentVLb.y, 200,50) text:@"" font:15 textColor:[UIColor blackColor] tag:0];
    self.DTpercentLb.textAlignment = NSTextAlignmentLeft;
    [bitV addSubview:self.DTpercentLb];
    self.percentVTypeLb = [CreateTool createLabelWithFrame:CGRectMake(bitV.width - 96,percentVLb.y, 86,50) text:@"" font:14 textColor:[UIColor blackColor] tag:0];
    self.percentVTypeLb.textAlignment = NSTextAlignmentRight;
    [bitV addSubview:self.percentVTypeLb];
    
//转换数量
    UIView *intoNumberV = [[UIView alloc]initWithFrame:CGRectMake(15, bitV.bottom + 20, SCREEN_WIDTH - 30,106)];
    intoNumberV.userInteractionEnabled = YES;
    intoNumberV.layer.cornerRadius = 10;
    intoNumberV.backgroundColor = [UIColor whiteColor];
    intoNumberV.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    intoNumberV.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    intoNumberV.layer.shadowOpacity = 0.3;// 阴影透明度，默认0
    intoNumberV.layer.shadowRadius = 3;// 阴影半径，默认3
    [self.scrollView addSubview:intoNumberV];
    UILabel *intoNumbeVLb = [CreateTool createLabelWithFrame:CGRectMake(10,0,100,50) text:QSLocalizedString(@"qs_zhuanhuan") font:14 textColor:[UIColor blackColor] tag:0];
    intoNumbeVLb.textAlignment = NSTextAlignmentLeft;
    [intoNumberV addSubview:intoNumbeVLb];
    self.numTf =  [CreateTool createTextFieldWithFrame:CGRectMake(intoNumbeVLb.right + 15,0, 200,50) placeholder:QSLocalizedString(@"qs_zhuanhuanshuliang") delegate:self tag:100];
//    [self.numTf addTarget:self action:@selector(textChange) forControlEvents:UIControlEventEditingChanged];
    self.numTf.keyboardType = UIKeyboardTypeDecimalPad;
    [intoNumberV addSubview:self.numTf];
    
    UIImageView *line3 = [CreateTool createImageViewWithFrame:CGRectMake(10,intoNumbeVLb.bottom,intoNumberV.width-20,0.3) imageString:@"" tag:0];
    line3.backgroundColor = buttonHuiSeColor;
    [intoNumberV addSubview:line3];
    
//生态获得数量
    UILabel *getNumVLb = [CreateTool createLabelWithFrame:CGRectMake(10, line3.bottom, 100, 50) text:QSLocalizedString(@"qs_huode") font:14 textColor:[UIColor blackColor] tag:0];
    getNumVLb.textAlignment = NSTextAlignmentLeft;
    [intoNumberV addSubview:getNumVLb];
    self.DDGetLb = [CreateTool createLabelWithFrame:CGRectMake(getNumVLb.right + 15, getNumVLb.y, 200, 50) text:@"" font:15 textColor:[UIColor blackColor] tag:0];
    self.DDGetLb.textAlignment = NSTextAlignmentLeft;
    [intoNumberV addSubview:self.DDGetLb];
    UILabel *getNumTypeLb = [CreateTool createLabelWithFrame:CGRectMake(intoNumberV.width -60, getNumVLb.y, 50, 50) text:@"USDD" font:14 textColor:[UIColor blackColor] tag:0];
    getNumTypeLb.textAlignment = NSTextAlignmentRight;
    [intoNumberV addSubview:getNumTypeLb];

//兑换
    UIButton *switchBtn = [CreateTool createButtonImageTitleWithFrame:CGRectMake(62,intoNumberV.bottom+60,SCREEN_WIDTH-124,60) imageString:@"range_length" buttonTitle:QSLocalizedString(@"qs_duihuan") titleColor:[UIColor whiteColor] tag:1000 target:self action:@selector(switchBtn:) size:15];
    [self.scrollView addSubview:switchBtn];


    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, switchBtn.bottom + 50);
}

//兑换
-(void)switchBtn:(UIButton *)sender
{

    if (self.numTf.text.length == 0) {
        
        [JMNotifyView showNotify:QSLocalizedString(@"qs_shuruzhuanhuanshuliang")];
        return;
    }
    
    double dounumTF = [self.numTf.text doubleValue];
    double douyue = [self.DTYueLb.text doubleValue];
    if (dounumTF>douyue) {
        [JMNotifyView showNotify:QSLocalizedString(@"qs_KeYongYueBuZu")];
        return;
    }
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"currency_id"] = self.currency_id;//代币id
    parameters[@"change_id"] = self.change_id;//兑换id
    parameters[@"money"] = self.numTf.text;//转账金额
    parameters[@"ratio"] = self.DTpercentLb.text;//转账比例
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,accountchange];
    
     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:QSAppKeyWindow animated:YES];
    [GWNetWork postTokenWithURL:URLStr params:parameters success:^(id responseDict) {
        
        NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
        NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
        if ([code isEqualToString:@"200"]) {
            //成功
            SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:QSLocalizedString(@"qs_duihuanchenggong") sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
            [special withSureClick:^(NSString *string) {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
            
        }
        else
        {
            [JMNotifyView showNotify:DIC[@"msg"]];
        }
         [hud hideAnimated:YES];
    } failure:^(NSError *error) {
         [hud hideAnimated:YES];
    }];
    
    
    
    
}

-(void)percentVTap
{
    
}
//选择资金账户
-(void)bitVTap
{
//    __block DDToDTVC *blockSelf = self;
//    DDToTchooseVC *vc = [DDToTchooseVC new];
//    vc.USDDbuttonClicked = ^(NSString * _Nullable buttit, NSString * _Nullable currid, NSString * _Nullable money, NSString * _Nullable change_id) {
//        blockSelf.bizhongLb.text = buttit;
//        blockSelf.yueVTypeLb.text = buttit;
//        blockSelf.DTYueLb.text = [NSString stringWithFormat:@"%@",money];
//        blockSelf.currency_id = currid;
//        blockSelf.change_id = change_id;
//        if ([buttit isEqualToString:@"USDT"]) {
//            self.percentVTypeLb.text = [NSString stringWithFormat:@"USDT/USDD"];
//            self.DTpercentLb.text = self.USDTUSDD;
//        }
//        else
//        {
//            self.percentVTypeLb.text = [NSString stringWithFormat:@"TFOR/USDD"];
//            self.DTpercentLb.text = self.TFORUSDD;
//
//        }
//
//        self.numTf.text = @"";
//        self.DDGetLb.text = @"";
//
//    };
//    [self.navigationController pushViewController:vc animated:YES];
    
}
#pragma mark - 请求接口
- (void)NetworkRequest{
    [XLDouYinLoading showInView:self.view];
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"quote_currency"]= @"usdd";
     NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,currencyquote];
    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
         [XLDouYinLoading hideInView:self.view];
         NSLog(@"-获取用户-responseDict--%@", responseDict);
         NSDictionary *DIC = responseDict;
         NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
        if ([code isEqualToString:@"200"]) {
                        //成功
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                               
                                  NSArray *data = DIC[@"data"];
                             
                               dispatch_async(dispatch_get_main_queue(), ^{
                                  for (int i= 0; i<data.count; i++) {
                                      
                                      NSString *price = [NSString stringWithFormat:@"%@",data[i][@"price"]];
                                      switch (i) {
                                          case 0:
                                          {
                                              self.TFORUSDD = price;
                                          }
                                              break;
                                              case 1:
                                              {
                                                  self.USDTUSDD= price;
                                              }
                                                  break;
                                          default:
                                              break;
                                      }
                                      
                                  }
                                   if ([self.symbol isEqualToString:@"USDT"])
                                   {
                                        self.percentVTypeLb.text = [NSString stringWithFormat:@"USDT/USDD"];
                                        self.DTpercentLb.text = self.USDTUSDD;
                                    }
                                    else
                                    {
                                        self.percentVTypeLb.text = [NSString stringWithFormat:@"TFOR/USDD"];
                                        self.DTpercentLb.text = self.TFORUSDD;

                                    }

                               });
                           });
        
        }
        else
        {
            [JMNotifyView showNotify:DIC[@"msg"]];
        }

    } failure:^(NSError *error) {
         [XLDouYinLoading hideInView:self.view];
    }];

}

-(void)textChange
{
   

}
//禁用第三方输入键盘
- (BOOL)application:(UIApplication *)application shouldAllowExtensionPointIdentifier:(NSString *)extensionPointIdentifier
{
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog(@"%@",self.numTf.text);
     NSRange rangeone = [string rangeOfString:@"-"];
    if(rangeone.location != NSNotFound)
    {
         NSLog(@"有-");
    }
    else
    {
        NSString *tftext = [NSString stringWithFormat:@"%@%@",self.numTf.text,string];
        double dounumTF = [tftext doubleValue];
        double doulast = [self.DTpercentLb.text doubleValue];
        self.DDGetLb.text = [NSString stringWithFormat:@"%.4f",dounumTF*doulast];
    }

    BOOL isHaveDian;
    //判断是否有小数点
    if ([textField.text containsString:@"."]) {
        isHaveDian = YES;
    }else{
        isHaveDian = NO;
    }

    if (string.length > 0) {

        //当前输入的字符
        unichar single = [string characterAtIndex:0];
        NSLog(@"single = %c",single);

        //不能输入.0~9以外的字符
        if (!((single >= '0' && single <= '9') || single == '.')){
            NSLog(@"您输入的格式不正确");
            return NO;
        }
       
        //只能有一个小数点
        if (isHaveDian && single == '.') {
            NSLog(@"只能输入一个小数点");
            return NO;
        }

        //如果第一位是.则前面加上0
        if ((textField.text.length == 0) && (single == '.')) {
            textField.text = @"0";
        }

        //如果第一位是0则后面必须输入.
        if ([textField.text hasPrefix:@"0"]) {
            if (textField.text.length > 1) {
                NSString *secondStr = [textField.text substringWithRange:NSMakeRange(1, 1)];
                if (![secondStr isEqualToString:@"."]) {
                    NSLog(@"第二个字符必须是小数点");
                    return NO;
                }
            }else{
                if (![string isEqualToString:@"."]) {
                    NSLog(@"第二个字符必须是小数点");
                    return NO;
                }
            }
        }

        //小数点后最多能输入两位
        if (isHaveDian) {
            NSRange ran = [textField.text rangeOfString:@"."];
            //由于range.location是NSUInteger类型的，所以不能通过(range.location - ran.location) > 2来判断
            if (range.location > ran.location) {
                if ([textField.text pathExtension].length > 1) {
                    NSLog(@"小数点后最多有两位小数");
                    return NO;
                }
            }
        }
        
    }
    return YES;
}

@end
