//
//  EcologicalDetail.m
//  JyProject
//
//  Created by mac on 2019/12/17.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "EcologicalDetail.h"
#import "EcoDetailCell.h"
#import "EcoDelModel.h"
#import "MJExtension.h"
#import "EcoDDListVC.h"
#import "APPWKWebView.h"
@interface EcologicalDetail ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *array;
@end

@implementation EcologicalDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = QSLocalizedString(@"qs_ZhangHu");//QSLocalizedString(@"qs_TiBi");
    [self setup];
}

-(void)setup
{
    [self.view addSubview:self.tableView];
     UIButton *upBtn = [CreateTool createButtonImageTitleWithFrame:CGRectMake(28,self.tableView.bottom,SCREEN_WIDTH-56, 70) imageString:@"range_length" buttonTitle:QSLocalizedString(@"qs_shengtaixiangqing") titleColor:[UIColor whiteColor] tag:1000 target:self action:@selector(ecodetail) size:16];
    [self.view addSubview:upBtn];
    [self httpRequest];
}

-(void)ecodetail
{
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
       parameters[@"agreement_type"]= @"生态详情";
       NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,showagreementtoroot];

       [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
            NSLog(@"-获取用户-responseDict--%@", responseDict);
           NSDictionary *DIC = responseDict;
                     NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                     if ([code isEqualToString:@"200"]) {
                         
                         NSArray *data = DIC[@"data"];
                                                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                                    
                                                     APPWKWebView *web = [APPWKWebView new];
                                                     web.TypeHtml = @"H5";
                                                     web.tit = QSLocalizedString(@"qs_shengtaixiangqing");
                                                     web.strURL = [NSString stringWithFormat:@"%@",data[0][@"article_info"]];
                                                     [self.navigationController pushViewController:web animated:YES];
                                                            
                                               });
                         
                         
                       
                     }
                     else if ([code isEqualToString:@"401"])
                     {
                         NotifyRelogin;
                     }
                     else
                     {
                         [JMNotifyView showNotify:DIC[@"msg"]];
                     }

       } failure:^(NSError *error) {
           
       }];
    
//    SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:QSLocalizedString(@"qs_gongnengweikaifang") sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
//    [special withSureClick:^(NSString *string) {
//        EcoDDListVC *vc = [[EcoDDListVC alloc]init];
//        [self.navigationController pushViewController:vc animated:YES];
//    }];
}

-(void)httpRequest
{
    
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,EcoList];
    
    [GWNetWork getKotenWithURL:URLStr params:@{} success:^(id response) {
         
        NSLog(@"%@",self.currentLevel);
        NSMutableArray *array = [NSMutableArray arrayWithCapacity:0];
        bool canSave = NO;
        for (NSDictionary *dict in [response objectForKey:@"data"]) {
            EcoDelModel *model = [EcoDelModel mj_objectWithKeyValues:dict];
            NSLog(@"%@",model.level);
            if ([self.currentLevel isEqualToString:@""]) {
                canSave = YES;
            }
            if ([self.currentLevel isEqualToString:@"平民"]) {
                canSave = YES;
            }
            if ([model.level isEqualToString:self.currentLevel] && ![self.currentLevel isEqualToString:@""]) {
                canSave = YES;
                continue;
            }
            if (canSave == YES) {
                [array addObject:model];
            }
        }
        self.array = [array mutableCopy];
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        
    }];
}


-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, SCREEN_HEIGHT - 129 -kNavgationBarHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"EcoDetailCell" bundle:nil] forCellReuseIdentifier:@"EcoDetailCellID"];
    }
    return _tableView;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 72;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EcoDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EcoDetailCellID"];
    
    EcoDelModel *model = self.array[indexPath.row];
    cell.ecoLb.text = [NSString stringWithFormat:@"%@%@~%@ USDD",model.level,model.low_hold,model.high_hold];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EcoDelModel *model = self.array[indexPath.row];
    if (self.selectBackBlock) {
        self.selectBackBlock([NSString stringWithFormat:@"%@%@~%@ USDD",model.level,model.low_hold,model.high_hold],model.level,model);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
