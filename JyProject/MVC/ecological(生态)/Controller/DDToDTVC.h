//
//  DDToDTVC.h
//  JyProject
//
//  Created by mac on 2019/12/11.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface DDToDTVC : JYBaseVC

@property (nonatomic,copy) NSString *symbol;
@property (nonatomic,copy) NSString *currency_id;
@property (nonatomic,copy) NSString *change_id;
@property (nonatomic,copy) NSString *balance;
@property (nonatomic,retain) NSString *last;//最新成交价

@property (nonatomic,retain) UILabel *yueVTypeLb;
@property (nonatomic,retain) UILabel *percentVTypeLb;


@property (nonatomic,retain) NSString *TFORUSDD;
@property (nonatomic,retain) NSString *USDTUSDD;

@end

NS_ASSUME_NONNULL_END
