//
//  EcoDDListVC.m
//  JyProject
//
//  Created by mac on 2019/12/24.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "EcoDDListVC.h"

@interface EcoDDListVC ()

@end

@implementation EcoDDListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = QSLocalizedString(@"qs_shengtaixiangqing");//QSLocalizedString(@"qs_TiBi");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
