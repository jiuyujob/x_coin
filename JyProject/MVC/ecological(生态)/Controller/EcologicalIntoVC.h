//
//  EcologicalIntoVC.h
//  JyProject
//
//  Created by mac on 2019/12/11.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"
#import "EcologicalModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface EcologicalIntoVC : JYBaseVC

@property (nonatomic, strong) Eco_project *ecologymodel;

@end

NS_ASSUME_NONNULL_END
