//
//  EcologicalDetail.h
//  JyProject
//
//  Created by mac on 2019/12/17.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN
@class EcoDelModel;
@interface EcologicalDetail : JYBaseVC

@property (nonatomic, copy) void(^ selectBackBlock)(NSString *levelstr,NSString *key,EcoDelModel *Model);
@property (nonatomic, copy) NSString *currentLevel;
@end

NS_ASSUME_NONNULL_END
