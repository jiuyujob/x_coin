//
//  BillListVC.m
//  JyProject
//
//  Created by mac on 2019/12/11.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "BillListVC.h"
#import "BillCell.h"
#import "EcoHistoryModel.h"
#import "MJExtension.h"
#import "MJRefresh.h"
@interface BillListVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) int page;
@property (nonatomic, strong) EcoHistoryModel *history;
@end

@implementation BillListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = QSLocalizedString(@"qs_zhangdanleixing");
    [self setup];
    [self request];
}
-(void)request
{
    self.page = 1;
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,EcoDealList];
    NSDictionary *param = @{@"ecology_id":self.ecologymodel.Id,
                            @"page":@(1),
                            @"pageSize":@(20)
    };
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    [GWNetWork getKotenWithURL:URLStr params:param success:^(id responseDict) {
       
        NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
        NSString *code = [NSString stringWithFormat:@"%@",responseDict[@"code"]];
        if ([code isEqualToString:@"200"]) {
              //成功
            NSDictionary *data = DIC[@"data"];
            self.history = [EcoHistoryModel mj_objectWithKeyValues:data];
            [self.tableView reloadData];
        }
        else
        {
            [JMNotifyView showNotify:responseDict[@"msg"]];
        }

        [hud hideAnimated:YES];
    } failure:^(NSError *error) {
        [hud hideAnimated:YES];
    }];
}
-(void)loadMoreData
{
    [self.tableView.mj_footer beginRefreshing];
    self.page ++;
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,EcoDealList];
    NSDictionary *param = @{@"ecology_id":self.ecologymodel.Id,
                            @"page":@(self.page),
                            @"pageSize":@(20)
    };
    [GWNetWork getKotenWithURL:URLStr params:param success:^(id responseDict) {
        if (![responseDict isKindOfClass:[NSNull class]]) {
            NSDictionary *DIC = responseDict;
            NSString *code = [NSString stringWithFormat:@"%@",responseDict[@"code"]];
            if ([code isEqualToString:@"200"]) {
                  //成功
                NSDictionary *data = DIC[@"data"];
                EcoHistoryModel *model = [EcoHistoryModel mj_objectWithKeyValues:data];
                NSMutableArray *array = [self.history.items mutableCopy];
                self.history = model;
                [array addObjectsFromArray:model.items];
                self.history.items = [array copy];
                [self.tableView reloadData];
            }
            else
            {
                [JMNotifyView showNotify:responseDict[@"msg"]];
            }

        }
        [self.tableView.mj_footer endRefreshing];
    } failure:^(NSError *error) {
        
    }];
    
}
-(void)setup
{
    [self.view addSubview:self.tableView];
}
#pragma mark -lazyload

-(UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height-kNavgationBarHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
       }
    return _tableView;
}
#pragma mark -tableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 63.0f;
}

#pragma mark -tableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.history.items.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HistoryItem *item = self.history.items[indexPath.row];
    BillCell *cell = [tableView dequeueReusableCellWithIdentifier:@"billcellID"];
    if (!cell) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"BillCell" owner:self options:nil].firstObject;
    }
    cell.backgroundColor = [UIColor clearColor];
    
    
    cell.billTypeLb.text = @"USDD";
    cell.billStateLb.text = item.comment;
    cell.billMoneyLb.text = [item.current_outlay isEqualToString:@"0"]?[NSString stringWithFormat:@"+%@",item.current_revenue]:[NSString stringWithFormat:@"-%@",item.current_outlay];
    cell.billMoneyLb.textColor = [item.current_outlay isEqualToString:@"0"]?[UIColor redColor]:[UIColor greenColor];
    cell.billTimeLb.text = item.create_date;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

@end
