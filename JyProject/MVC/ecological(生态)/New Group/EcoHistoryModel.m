//
//  EcoHistoryModel.m
//  JyProject
//
//  Created by mac on 2019/12/19.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "EcoHistoryModel.h"

@implementation EcoHistoryModel

+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"items" : @"HistoryItem"
             };
}

@end


@implementation HistoryItem

@end
