//
//  EcoHistoryModel.h
//  JyProject
//
//  Created by mac on 2019/12/19.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Page.h"
NS_ASSUME_NONNULL_BEGIN
@interface EcoHistoryModel : NSObject
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) Page *page;

@end

@interface HistoryItem : NSObject

@property (nonatomic, copy) NSString *current_outlay; //本期支出
@property (nonatomic, copy) NSString *current_revenue; //本期支出
@property (nonatomic, copy) NSString *comment; //标题
@property (nonatomic, copy) NSString *create_date; //时间

@end

NS_ASSUME_NONNULL_END
