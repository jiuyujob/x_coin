//
//  EcologicalModel.h
//  JyProject
//
//  Created by mac on 2019/12/14.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class Eco_Super;
@interface EcologicalModel : NSObject

@property (nonatomic, strong) NSArray *ecological_poject;
@property (nonatomic, copy) NSString *ecological_poject_bool; //是否有生态仓库没有就是false
@property (nonatomic, copy) NSString *super_peer_bool;//是否显示超级节点
@property (nonatomic, copy) NSString *usdd;//usdd数量
@property (nonatomic, strong) Eco_Super *super_peer;
@end

@interface Eco_project : NSObject
@property (nonatomic, copy) NSString *Id; //生态id
@property (nonatomic, copy) NSString *bocked_balance; //持币数量
@property (nonatomic, copy) NSString *high_hold; //高位
@property (nonatomic, copy) NSString *hold_return_rate; //自由算力
@property (nonatomic, copy) NSString *level; //等级
@property (nonatomic, copy) NSString *low_hold; //低位
@property (nonatomic, copy) NSString *recommend_return_rate; //直推算力
@property (nonatomic, copy) NSString *return_multiple; //杠杆
@property (nonatomic, copy) NSString *team_return_rate; //动态算力
@property (nonatomic, copy) NSString *to_day_rate; //今日算力

@end


@interface Eco_Super : NSObject
@property (nonatomic, copy) NSString *level; //超级节点的独立属性
@property (nonatomic, copy) NSString *today_a_bouns; //今日分红
@property (nonatomic, copy) NSString *usdd; //总币数
@end

NS_ASSUME_NONNULL_END
