//
//  Page.h
//  JyProject
//
//  Created by mac on 2019/12/19.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Page : NSObject

@property (nonatomic, assign) int totalPage; //总页数
@property (nonatomic, assign) int currentPage; //当前页数

@end

NS_ASSUME_NONNULL_END
