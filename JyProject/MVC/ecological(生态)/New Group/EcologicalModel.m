//
//  EcologicalModel.m
//  JyProject
//
//  Created by mac on 2019/12/14.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "EcologicalModel.h"

@implementation EcologicalModel
+ (NSDictionary *)mj_objectClassInArray
{
    return @{
             @"ecological_poject" : @"Eco_project"
             };
}

@end

@implementation Eco_project
+(NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"Id":@"id"};
}
@end

@implementation Eco_Super

@end
