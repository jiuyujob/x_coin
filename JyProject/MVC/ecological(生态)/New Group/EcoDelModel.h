//
//  EcoDelModel.h
//  JyProject
//
//  Created by mac on 2019/12/17.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface EcoDelModel : NSObject

@property (nonatomic, copy) NSString *high_hold;
@property (nonatomic, copy) NSString *low_hold;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *level;

@end

NS_ASSUME_NONNULL_END
