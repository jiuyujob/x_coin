//
//  WWLoginCtrl.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/10.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "WWLoginCtrl.h"
#import "JYTabBar.h"

@interface WWLoginCtrl ()

@end

@implementation WWLoginCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    _Mimature= false;
    [self initSetUI];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    ResignFirstTextField;
}
- (void)initSetUI
{
    
    UIImageView *image = [CreateTool createImageViewWithFrame:CGRectMake(0,0,SCREEN_WIDTH,Scale_X(280)) imageString:@"登录底图" tag:0];
    [self.view addSubview:image];
    
    FLAnimatedImageView *dongtu = [[FLAnimatedImageView alloc] init];
    dongtu.frame =  CGRectMake(SCREEN_WIDTH/2-100,Scale_X(75),200,180);
    NSString *path = [[NSBundle mainBundle] pathForResource:@"登录页动图" ofType:@"gif"];
    NSData *gifData = [NSData dataWithContentsOfFile:path];
    FLAnimatedImage *animatedImage1 = [FLAnimatedImage animatedImageWithGIFData:gifData];
    dongtu.animatedImage = animatedImage1;
    [self.view addSubview:dongtu];
    
    
    self.emailTF = [CreateTool createTextFieldWithFrame:CGRectMake(50,image. bottom+40,SCREEN_WIDTH-100,50) placeholder:QSLocalizedString(@"qs_YouXiangHao") delegate:self tag:666];
    [self.emailTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview:self.emailTF];
    
    UIImageView *line = [CreateTool createImageViewWithFrame:CGRectMake(40,self.emailTF.bottom,SCREEN_WIDTH-80,0.8) imageString:@"" tag:0];
    line.backgroundColor =kHexColor(@"#E5E5E5");
    [self.view addSubview:line];
    
    
    self.passwordTF = [CreateTool createTextFieldWithFrame:CGRectMake(50,line.bottom+10,SCREEN_WIDTH-100,50) placeholder:QSLocalizedString(@"qs_MiMa") delegate:self tag:667];
    self.passwordTF.secureTextEntry = YES;
    [self.passwordTF addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview:self.passwordTF];
    
    
    UIImageView *line2 = [CreateTool createImageViewWithFrame:CGRectMake(40,self.passwordTF.bottom,SCREEN_WIDTH-80,0.8) imageString:@"" tag:0];
     line2.backgroundColor =kHexColor(@"#E5E5E5");
    [self.view addSubview:line2];
   

    self.PWButton  = [CreateTool createButtonWithFrame:CGRectMake(SCREEN_WIDTH-60,self.passwordTF.y+19,22,17) imageString:@"不可见" tag:0 target:self action:@selector(PWButtonClicked:)];
    [self.view addSubview:self.PWButton];
    
    
    //新用户注册
    UIButton *ZhuCeBut = [CreateTool createButtonWithFrame:CGRectMake(42,line2.bottom+5,100,30) buttonTitle:QSLocalizedString(@"qs_XinYongHuZhuCe") titleColor:kHexColor(@"#999999") selectTitleColor:nil tag:0 target:self action:@selector(ZhuCeButClicked) size:12];
    ZhuCeBut.backgroundColor = [UIColor clearColor];
    [self.view addSubview:ZhuCeBut];

        //忘记密码
    UIButton *WjmmBut = [CreateTool createButtonWithFrame:CGRectMake(SCREEN_WIDTH-128, ZhuCeBut.y,100, 30) buttonTitle:QSLocalizedString(@"qs_WanJiMiMa") titleColor:kHexColor(@"#999999") selectTitleColor:kHexColor(@"#999999") tag:0 target:self action:@selector(WjmmButClicked) size:12];
    [self.view addSubview:WjmmBut];
    WjmmBut.backgroundColor = [UIColor clearColor];


    UIButton *DengLuBut = [CreateTool createButtonImageTitleWithFrame:CGRectMake(50, ZhuCeBut.bottom+40,SCREEN_WIDTH-100,Scale_X(55)) imageString:@"外部按钮背景" buttonTitle:QSLocalizedString(@"qs_DengLu") titleColor:[UIColor whiteColor] tag:700 target:self action:@selector(DengLuButClicked) size:16];
    [self.view addSubview:DengLuBut];
    
}
-(void)textFieldDidChange:(UITextField *)textField{
    
    //过滤空格
    NSString *tem = [[textField.text componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] componentsJoinedByString:@""];
    textField.text = tem;
}
- (void)PWButtonClicked:(UIButton *)but
{
    if (_Mimature)
        {
            self.passwordTF.secureTextEntry = YES;//设置密码保护模式输入,默认为NO
            [but setBackgroundImage:[UIImage imageNamed:@"不可见"] forState:UIControlStateNormal];
            _Mimature = false;
        }
        else
        {
            self.passwordTF.secureTextEntry = NO;//设置密码保护模式输入,默认为NO
            [but setBackgroundImage:[UIImage imageNamed:@"可见"] forState:UIControlStateNormal];
            _Mimature = true;
        }
}


#pragma mark - 登陆
- (void)DengLuButClicked
{
    ResignFirstTextField;
    if (self.emailTF.text.length == 0) {

        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingTianXieYouXianHao")];
        return;
    }
    if (self.passwordTF.text.length == 0) {

        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingTianXieMiMa")];
        return;
    }
   

     NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
        parameters[@"username"]=self.emailTF.text;
        parameters[@"password"]=self.passwordTF.text;
        NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,LoginURL];
        [GWNetWork postWithURL:URLStr params:parameters success:^(id responseDict) {
            NSLog(@"-获取用户-responseDict--%@", responseDict);
            NSDictionary *DIC = responseDict;
            NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
            if ([code isEqualToString:@"200"]) {
                //成功
                NSDictionary *data =DIC[@"data"];
                NSString *token = [NSString stringWithFormat:@"%@",data[@"token"]];
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:self.emailTF.text forKey:@"username"];
                [userDefaults setObject:self.passwordTF.text forKey:@"password"];
                [userDefaults setObject:token forKey:@"token"];
                [userDefaults setBool:YES forKey:@"hasLogin"];
                [userDefaults synchronize];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

                                    JYTabBar *mainTab = [[JYTabBar alloc] init];
                                    QSAppKeyWindow.rootViewController = mainTab;
                                    CATransition * transition = [[CATransition alloc] init];
                                    transition.type = @"reveal";
                                    transition.duration = 0.3;
                                    [QSAppKeyWindow.layer addAnimation:transition forKey:nil];
                                });
            }
            else
            {
                [JMNotifyView showNotify:DIC[@"msg"]];
            }


        } failure:^(NSError *error) {

            NSLog(@"%@",error);
        }];
    
    

}

- (void)WjmmButClicked{
    [self.navigationController pushViewController:[[NSClassFromString(@"WWForgotPassword") alloc] init] animated:YES];
}
- (void)ZhuCeButClicked
{
    
   [self.navigationController pushViewController:[[NSClassFromString(@"WWRegistCtrl") alloc] init] animated:YES];
}

@end
