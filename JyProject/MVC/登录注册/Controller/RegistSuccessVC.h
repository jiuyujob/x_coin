//
//  RegistSuccessVC.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/5.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface RegistSuccessVC : JYBaseVC

@property (nonatomic,copy) NSString *TTitle;
@property (nonatomic,copy) NSString *string;

@end

NS_ASSUME_NONNULL_END
