//
//  XOUpdateM.h
//  JyProject
//
//  Created by mac on 2019/12/26.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@interface XOUpdateM : NSObject
@property (nonatomic, copy) NSString *current_version;
@property (nonatomic, copy) NSString *prev_version;
@property (nonatomic, assign)BOOL canUp;
+ (instancetype)manager;

- (void)begin;
@end

NS_ASSUME_NONNULL_END
