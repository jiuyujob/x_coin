//
//  WWRegistCtrl.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/4.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "WWRegistCtrl.h"
#import "APPWKWebView.h"
#import "RegistSuccessVC.h"

@interface WWRegistCtrl ()

@end

@implementation WWRegistCtrl


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = QSLocalizedString(@"qs_XinYongHuZhuCe");
    self.view.backgroundColor = [UIColor whiteColor];
    _isture = true;
    _Mimature= false;
    _Mimature2= false;
    
    
    self.emailTFiled = [CreateTool createTextFieldWithFrame:CGRectMake(50,0,SCREEN_WIDTH-100,50) placeholder:QSLocalizedString(@"qs_YouXiangHao") delegate:self tag:666];
    [self.view addSubview:self.emailTFiled];
    
    UIImageView *line = [CreateTool createImageViewWithFrame:CGRectMake(40,self.emailTFiled.bottom,SCREEN_WIDTH-80,0.8) imageString:@"" tag:0];
    line.backgroundColor =kHexColor(@"#E5E5E5");
    [self.view addSubview:line];
    
    //登录密码
    self.passwordTFiled = [CreateTool createTextFieldWithFrame:CGRectMake(50,line.bottom+5,SCREEN_WIDTH-80,50) placeholder:QSLocalizedString(@"qs_ShuRuMiMa") delegate:self tag:0];
    self.passwordTFiled.secureTextEntry = YES;
    [self.passwordTFiled addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview:self.passwordTFiled];
    
    self.PWButton  = [CreateTool createButtonWithFrame:CGRectMake(SCREEN_WIDTH-60,self.passwordTFiled.y+15,22,17) imageString:@"不可见" tag:0 target:self action:@selector(PWButtonClicked:)];
    [self.view addSubview:self.PWButton];
    
    UIImageView *line2 = [CreateTool createImageViewWithFrame:CGRectMake(40,self.passwordTFiled.bottom,SCREEN_WIDTH-80,0.8) imageString:@"" tag:0];
    line2.backgroundColor =kHexColor(@"#E5E5E5");
    [self.view addSubview:line2];
    
    
    //再次输入密码
    self.TwopasswordTFiled = [CreateTool createTextFieldWithFrame:CGRectMake(50,line2.bottom+5,SCREEN_WIDTH-100,50) placeholder:QSLocalizedString(@"qs_ZaiChiShuRuMiMa") delegate:self tag:0];
    self.TwopasswordTFiled.secureTextEntry = YES;
    [self.TwopasswordTFiled addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview:self.TwopasswordTFiled];
    
    self.PWTwoButton  = [CreateTool createButtonWithFrame:CGRectMake(SCREEN_WIDTH-60,self.TwopasswordTFiled.y+15,22,17) imageString:@"不可见" tag:0 target:self action:@selector(PWTwoButtonClicked:)];
    [self.view addSubview:self.PWTwoButton];
    
    UIImageView *line3 = [CreateTool createImageViewWithFrame:CGRectMake(40,self.TwopasswordTFiled.bottom,SCREEN_WIDTH-80,0.8) imageString:@"" tag:0];
    line3.backgroundColor =kHexColor(@"#E5E5E5");
    [self.view addSubview:line3];
    
    //邀请码
    self.InviteCodeTFiled = [CreateTool createTextFieldWithFrame:CGRectMake(50,line3.bottom+5,SCREEN_WIDTH-100,50) placeholder:QSLocalizedString(@"qs_YaoQinMa") delegate:self tag:0];
    [self.view addSubview:self.InviteCodeTFiled];
    
    UIImageView *line4 = [CreateTool createImageViewWithFrame:CGRectMake(40,self.InviteCodeTFiled.bottom,SCREEN_WIDTH-80,0.8) imageString:@"" tag:0];
    line4.backgroundColor =kHexColor(@"#E5E5E5");
    [self.view addSubview:line4];
    
    UIButton *ZhuceBut = [CreateTool createButtonImageTitleWithFrame:CGRectMake(50, line4.bottom+40,SCREEN_WIDTH-100,Scale_X(50)) imageString:@"外部按钮背景" buttonTitle:QSLocalizedString(@"qs_FaSongYanZhenLianJie") titleColor:[UIColor whiteColor] tag:700 target:self action:@selector(ZhuceButClicked) size:16];
    [self.view addSubview:ZhuceBut];
    
    UILabel *textLb = [CreateTool createLabelWithFrame:CGRectMake(0,ZhuceBut.bottom+6,SCREEN_WIDTH/2-5,25) text:QSLocalizedString(@"qs_ZhuCeDaiBiaoNiTongYi") font:12 textColor:kHexColor(@"#939393") tag:0];
    textLb.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:textLb];
    
    UILabel *textLb2 = [CreateTool createLabelWithFrame:CGRectMake(SCREEN_WIDTH/2+5,ZhuceBut.bottom+6,SCREEN_WIDTH/2-5,25) text:QSLocalizedString(@"qs_YongHuFuWuXieYi") font:12 textColor:buttonColor tag:0];
    textLb2.textAlignment = NSTextAlignmentLeft;
    textLb2.userInteractionEnabled = YES;
    [self.view addSubview:textLb2];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(YongHuXieYi)];
    [textLb2 addGestureRecognizer:singleTap];
   
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
     
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    ResignFirstTextField;
}

-(void)textFieldDidChange:(UITextField *)textField{
    
    //过滤空格
    NSString *tem = [[textField.text componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] componentsJoinedByString:@""];
    textField.text = tem;
}
- (void)resignTF
{
    [self.emailTFiled resignFirstResponder];
    [self.passwordTFiled resignFirstResponder];
    [self.TwopasswordTFiled resignFirstResponder];
    [self.InviteCodeTFiled resignFirstResponder];
    
}

- (void)PWButtonClicked:(UIButton *)but
{
    if (_Mimature)
        {
            self.passwordTFiled.secureTextEntry = YES;//设置密码保护模式输入,默认为NO
            [but setBackgroundImage:[UIImage imageNamed:@"不可见"] forState:UIControlStateNormal];
            _Mimature = false;
        }
        else
        {
            self.passwordTFiled.secureTextEntry = NO;//设置密码保护模式输入,默认为NO
            [but setBackgroundImage:[UIImage imageNamed:@"可见"] forState:UIControlStateNormal];
            _Mimature = true;
        }
}
- (void)PWTwoButtonClicked:(UIButton *)but
{
    if (_Mimature2)
    {
        self.TwopasswordTFiled.secureTextEntry = YES;//设置密码保护模式输入,默认为NO
        [but setBackgroundImage:[UIImage imageNamed:@"不可见"] forState:UIControlStateNormal];
        _Mimature2 = false;
    }
    else
    {
        self.TwopasswordTFiled.secureTextEntry = NO;//设置密码保护模式输入,默认为NO
        [but setBackgroundImage:[UIImage imageNamed:@"可见"] forState:UIControlStateNormal];
        _Mimature2 = true;
    }
}

- (void)DaGouButClicked:(UIButton *)but
{
    if (_isture)
    {
        [but setBackgroundImage:[UIImage imageNamed:@"打勾"] forState:UIControlStateNormal];
        _isture = false;
    }
    else
    {
        [but setBackgroundImage:[UIImage imageNamed:@"选中"] forState:UIControlStateNormal];
        _isture = true;
    }
}
- (void)ZitiButClicked:(UIButton *)but
{
    if (_isture)
        {
            [self.DaGouBut setBackgroundImage:[UIImage imageNamed:@"打勾"] forState:UIControlStateNormal];
            _isture = false;
        }
        else
        {
            [self.DaGouBut setBackgroundImage:[UIImage imageNamed:@"选中"] forState:UIControlStateNormal];
            _isture = true;
        }
}

#pragma mark - 请求接口
- (void)ZhuceButClicked
{
    
    if (self.emailTFiled.text.length == 0) {
        
        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingTianXieYouXianHao")];
        return;
    }
    if (self.passwordTFiled.text.length == 0) {
        
        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingTianXieMiMa")];
        return;
    }
    if (self.TwopasswordTFiled.text.length == 0) {
        
        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingYanZhengMiMa")];
        return;
    }
    if (![self.TwopasswordTFiled.text isEqualToString:self.passwordTFiled.text]) {
        
        [JMNotifyView showNotify:QSLocalizedString(@"qs_MiMaBuYiZhi")];
        return;
    }
    if (self.InviteCodeTFiled.text.length == 0) {
        
        [JMNotifyView showNotify:QSLocalizedString(@"qs_YaoQinMa")];
        return;
    }
    if (_isture == false) {
        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingGouXuanYongHuXieYi")];
        return;
    }
    
    [self resignTF];

    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"username"]=self.emailTFiled.text;
    parameters[@"password"]=self.passwordTFiled.text;
    parameters[@"rcode"]=self.InviteCodeTFiled.text;
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,ZhuCeURL];

    [GWNetWork postWithURL:URLStr params:parameters success:^(id responseDict) {
        NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
        NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
        if ([code isEqualToString:@"200"]) {
            //注册成功
            RegistSuccessVC *vc = [RegistSuccessVC new];
            vc.title = QSLocalizedString(@"qs_XinYongHuZhuCe");
            vc.string = QSLocalizedString(@"qs_ZhuCeLianJieYiFaSongDaoYouXiang");
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
            [JMNotifyView showNotify:DIC[@"msg"]];
        }
        
        
    } failure:^(NSError *error) {

    }];
}

- (void)YongHuXieYi
{
       [XLDouYinLoading showInView:self.view];
       NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,showagreementtouser];
    [GWNetWork getWithURL:URLStr params:nil success:^(id responseDict) {
        
        
               [XLDouYinLoading hideInView:self.view];
                      NSLog(@"-获取用户-responseDict--%@", responseDict);
                     NSDictionary *DIC = responseDict;
                               NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                               if ([code isEqualToString:@"200"]) {
                                   
                                   NSDictionary *data = DIC[@"data"];
                                   dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                      
                                       APPWKWebView *web = [APPWKWebView new];
                                       web.TypeHtml = @"H5";
                                       web.tit = QSLocalizedString(@"qs_YongHuXieYi");
                                       web.strURL = [NSString stringWithFormat:@"%@",data[@"article_info"]];
                                       [self.navigationController pushViewController:web animated:YES];
                                              
                                 });
                               }
                               else if ([code isEqualToString:@"401"])
                               {
                                   NotifyRelogin;
                               }
                               else
                               {
                                   [JMNotifyView showNotify:DIC[@"msg"]];
                               }
    } failure:^(NSError *error) {
        
          [XLDouYinLoading hideInView:self.view];
    }];
      
}
@end
