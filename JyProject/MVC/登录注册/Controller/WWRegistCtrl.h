//
//  WWRegistCtrl.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/4.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface WWRegistCtrl : JYBaseVC<UITextFieldDelegate>
{
    bool _isture;
    bool _Mimature;
    bool _Mimature2;
}
@property (nonatomic,retain) UITextField *emailTFiled;//邮箱
@property (nonatomic,retain) UITextField *passwordTFiled;//密码
@property (nonatomic,retain) UIButton *PWButton;//密码是否可见
@property (nonatomic,retain) UITextField *TwopasswordTFiled;//验证密码
@property (nonatomic,retain) UIButton *PWTwoButton;//密码是否可见
@property (nonatomic,retain) UITextField *InviteCodeTFiled;//邀请码

@property (nonatomic,retain) UIButton *DaGouBut;

@end

NS_ASSUME_NONNULL_END
