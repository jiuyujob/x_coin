//
//  WWForgotPassword.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/6.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "WWForgotPassword.h"
#import "ForgotView.h"
#import "RegistSuccessVC.h"

@interface WWForgotPassword ()

@end

@implementation WWForgotPassword



- (void)viewDidLoad {
    [super viewDidLoad];
     self.navigationItem.title = QSLocalizedString(@"qs_WanJiMiMa");
     self.view.backgroundColor = [UIColor whiteColor];
    
    ForgotView *review = [[ForgotView alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,600)];
    review.successCliCK = ^{
        [self.navigationController popViewControllerAnimated:YES];
    };
    [self.view addSubview:review];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    ResignFirstTextField;
}
@end
