//
//  RegistSuccessVC.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/5.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "RegistSuccessVC.h"

@interface RegistSuccessVC ()

@end

@implementation RegistSuccessVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = self.TTitle;
       self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
       [leftButton setImage:[UIImage imageNamed:@"arrow_nav"] forState:UIControlStateNormal];
       leftButton.frame = CGRectMake(0, 0,anvWith,anvheight);
       [leftButton addTarget:self action:@selector(leftItemClicked:) forControlEvents:UIControlEventTouchUpInside];
       UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
       [self.navigationItem setLeftBarButtonItem:leftItem animated:YES];
    
    
    UIImageView *image = [CreateTool createImageViewWithFrame:CGRectMake(SCREEN_WIDTH/2-40,SCREEN_HEIGHT/2-190,70,70) imageString:@"注册成功" tag:0];
    [self.view addSubview:image];
    
    UILabel *label = [CreateTool createLabelWithFrame:CGRectZero text:self.string font:16 textColor:[UIColor blackColor] tag:0];
    label.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
           make.top.equalTo(image).offset(90);
           make.left.mas_equalTo(self.view).offset(15);
           make.right.mas_equalTo(self.view).offset(-15);
           make.height.mas_equalTo(@30);
       }];
    
    
    UILabel *label2 = [CreateTool createLabelWithFrame:CGRectZero text:QSLocalizedString (@"qs_QingDaoYouXiangJinXingYanZheng") font:16 textColor:[UIColor blackColor] tag:0];
    label2.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
              make.top.equalTo(label).offset(40);
              make.left.mas_equalTo(self.view).offset(15);
              make.right.mas_equalTo(self.view).offset(-15);
              make.height.mas_equalTo(@30);
          }];
}

- (void)leftItemClicked:(UIBarButtonItem*)item
{
    [self.navigationController popToRootViewControllerAnimated:YES];//推到上一级
}
@end
