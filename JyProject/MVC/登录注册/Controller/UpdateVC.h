//
//  UpdateVC.h
//  JyProject
//
//  Created by mac on 2019/12/25.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface UpdateVC : UIViewController

@property (nonatomic, strong) NSDictionary *firDic;

@property (nonatomic, copy) void(^finishtask)(bool canUp);



@end

NS_ASSUME_NONNULL_END
