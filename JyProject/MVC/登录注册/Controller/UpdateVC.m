//
//  UpdateVC.m
//  JyProject
//
//  Created by mac on 2019/12/25.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "UpdateVC.h"

@interface UpdateVC ()
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *versionLB;
@property (weak, nonatomic) IBOutlet UILabel *contentLb;

@end

@implementation UpdateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBlurEffect *blur2 = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *effectview2 = [[UIVisualEffectView alloc] initWithEffect:blur2];
    effectview2.frame = CGRectMake(0, 0, SCREEN_WIDTH,SCREEN_HEIGHT);
    [self.view addSubview:effectview2];
    [self.view sendSubviewToBack:effectview2];
    
    
    
    NSString *update = [self.firDic objectForKey:@"changelog"];
    NSString *version = [self.firDic objectForKey:@"version"];
    self.versionLB.text = [NSString stringWithFormat:@"新版本v%@",version];
    self.contentLb.text = [NSString stringWithFormat:@"%@",update];
    
}

- (IBAction)update:(id)sender {

    if (self.finishtask) {
        self.finishtask(YES);
    }
    
}
- (IBAction)cancel:(id)sender {
    if (self.finishtask) {
        self.finishtask(NO);
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
