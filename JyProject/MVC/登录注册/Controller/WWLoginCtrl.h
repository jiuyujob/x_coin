//
//  WWLoginCtrl.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/10.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrefixHeader.pch"
#import "CreateTool.h"
#import "Masonry.h"
#import "HttpConstant.h"
#import "HandleUserInfo.h"
#import "GWNetWork.h"
#import "JMNotifyView.h"
#import "SpecialAlertView.h"
#import "FLAnimatedImage.h"
#import "UIView+Frame.h"

NS_ASSUME_NONNULL_BEGIN

@interface WWLoginCtrl : UIViewController<UITextFieldDelegate>
{
    bool _Mimature;
}

@property (nonatomic, strong) UITextField *emailTF;
@property (nonatomic, strong) UITextField *passwordTF;
@property (nonatomic,retain) UIButton *PWButton;//密码是否可见

@end

NS_ASSUME_NONNULL_END
