//
//  XOUpdateM.m
//  JyProject
//
//  Created by mac on 2019/12/26.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "XOUpdateM.h"
#import "UpdateVC.h"


@interface XOUpdateM()

@property (nonatomic, strong) UIWindow *dwindow;

@end

@implementation XOUpdateM

static XOUpdateM* _manager = nil;
static dispatch_once_t _onceToken;

+ (instancetype)manager{
    dispatch_once(&_onceToken, ^{
        _manager = [[super allocWithZone:NULL] init];
    });
    return _manager;
}
+ (instancetype)allocWithZone:(struct _NSZone *)zone{return _manager;}
- (id)copyWithZone:(NSZone *)zone{return _manager;}



- (void)begin{
    
    [self chenckUpdate];
}

-(void)chenckUpdate
{
      self.current_version = [[[NSBundle mainBundle]infoDictionary] objectForKey:@"CFBundleVersion"];
    
      NSString *url = @"http://api.fir.im/apps/latest/5dfca34223389f0bb9d14eee?api_token=c4dc6464c64d20ebbfaa78ce6838fe50";
        [GWNetWork getWithURL:url params:nil success:^(NSDictionary *responseDict) {

            self.prev_version = [responseDict objectForKey:@"version"];

            if ([self.prev_version compare:self.current_version] == NSOrderedDescending) {
                
                UpdateVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"updateVCID"];
                vc.firDic = responseDict;
                self.dwindow =  [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
                
                self.dwindow.backgroundColor = [UIColor clearColor];
                
                self.dwindow.windowLevel = UIWindowLevelAlert;
                
                [self.dwindow makeKeyAndVisible];
                
                self.dwindow.rootViewController = vc;
                
                self.canUp = YES;
                vc.finishtask = ^(bool canUp) {
                    
                    [self closeWindow];
                    if (canUp) {
                        NSString *downloadurl = [responseDict objectForKey:@"install_url"];
                        NSString *URLencodeString = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)downloadurl, NULL, CFSTR(":/?#[]@!$ &'()*+,;=\"<>%{}|\\^~`"), CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding)));
                                           NSString *installURL = [NSString stringWithFormat:@"itms-services://?action=download-manifest&url=%@", URLencodeString];
                                           NSURL *openURL = [NSURL URLWithString:installURL];
                                           [[UIApplication sharedApplication] openURL:openURL];
                    }
                };
                NSLog(@"当前有几个windows：%@",[UIApplication sharedApplication].windows);

            }
        } failure:^(NSError *error) {

        }];
}
- (void)closeWindow
{
    [_dwindow resignKeyWindow];
    _dwindow = nil;
    NSLog(@"当前有几个windows：%@",[UIApplication sharedApplication].windows);
}

@end
