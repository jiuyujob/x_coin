//
//  ForgotView.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/6.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseView.h"

typedef void(^successful)(void);
NS_ASSUME_NONNULL_BEGIN

@interface ForgotView : JYBaseView<UITextFieldDelegate>
{
    bool _Mimature;
    bool _Mimature2;
    NSTimer *_timer;
    int _waiTime;
}

@property (nonatomic,retain) UITextField *emailTFiled;//邮箱
@property (nonatomic,retain) UITextField *VerificationTFiled;//验证码
@property (nonatomic,retain) UIButton *VFButton;//获取验证码
@property (nonatomic,retain) UITextField *passwordTFiled;//密码
@property (nonatomic,retain) UIButton *PWButton;//密码是否可见
@property (nonatomic,retain) UITextField *TwopasswordTFiled;//验证密码
@property (nonatomic,retain) UIButton *PWTwoButton;//密码是否可见

@property (nonatomic, copy) successful successCliCK;

-(id)initWithFrame:(CGRect)frame;

@end

NS_ASSUME_NONNULL_END
