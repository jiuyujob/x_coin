//
//  ForgotView.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/6.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "ForgotView.h"

@implementation ForgotView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
       if (self) {

           _Mimature= false;
           _Mimature2= false;
           self.backgroundColor = [UIColor clearColor];
           [self initSetUI];
           
       }
    return self;
}
- (void)initSetUI
{

    self.emailTFiled = [CreateTool createTextFieldWithFrame:CGRectMake(50,0,SCREEN_WIDTH-100,50) placeholder:QSLocalizedString(@"qs_YouXiangHao") delegate:self tag:666];
    [self.emailTFiled addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:self.emailTFiled];
    
    UIImageView *line = [CreateTool createImageViewWithFrame:CGRectMake(40,self.emailTFiled.bottom,SCREEN_WIDTH-80,0.8) imageString:@"" tag:0];
    line.backgroundColor =kHexColor(@"#E5E5E5");
    [self addSubview:line];
    
    //验证码
    self.VerificationTFiled = [CreateTool createTextFieldWithFrame:CGRectMake(50,line.bottom+5,SCREEN_WIDTH-130,50) placeholder:QSLocalizedString(@"qs_QingShuRuYanZhengMa") delegate:self tag:0];
     [self.VerificationTFiled addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:self.VerificationTFiled];
    
    self.VFButton  = [CreateTool createButtonWithFrame:CGRectMake(SCREEN_WIDTH-100,self.VerificationTFiled.y+10, 83,20) buttonTitle:QSLocalizedString(@"qs_HuoQuYanZhengMa") titleColor:[UIColor whiteColor] selectTitleColor:[UIColor whiteColor] tag:0 target:self action:@selector(VFButtonClicked:) size:11];
    self.VFButton.backgroundColor = buttonColor;
    self.VFButton.layer.cornerRadius = 8;
    [self addSubview:self.VFButton];
    
    UIImageView *line2 = [CreateTool createImageViewWithFrame:CGRectMake(40,self.VerificationTFiled.bottom,SCREEN_WIDTH-80,0.8) imageString:@"" tag:0];
    line2.backgroundColor =kHexColor(@"#E5E5E5");
    [self addSubview:line2];
    
    //登录密码
    self.passwordTFiled = [CreateTool createTextFieldWithFrame:CGRectMake(50,line2.bottom+5,SCREEN_WIDTH-100,50) placeholder:QSLocalizedString(@"qs_ShuRuMiMa") delegate:self tag:0];
    self.passwordTFiled.secureTextEntry = YES;
    [self.passwordTFiled addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:self.passwordTFiled];
    
    self.PWButton  = [CreateTool createButtonWithFrame:CGRectMake(SCREEN_WIDTH-60,self.passwordTFiled.y+15,22,17) imageString:@"不可见" tag:0 target:self action:@selector(PWButtonClicked:)];
    [self addSubview:self.PWButton];
    
    UIImageView *line3 = [CreateTool createImageViewWithFrame:CGRectMake(40,self.passwordTFiled.bottom,SCREEN_WIDTH-80,0.8) imageString:@"" tag:0];
    line3.backgroundColor =kHexColor(@"#E5E5E5");
    [self addSubview:line3];
   
    
    
    //再次输入密码
    self.TwopasswordTFiled = [CreateTool createTextFieldWithFrame:CGRectMake(50,line3.bottom+5,SCREEN_WIDTH-100,50) placeholder:QSLocalizedString(@"qs_ZaiChiShuRuMiMa") delegate:self tag:0];
    self.TwopasswordTFiled.secureTextEntry = YES;
    [self.TwopasswordTFiled addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:self.TwopasswordTFiled];
    
    self.PWTwoButton  = [CreateTool createButtonWithFrame:CGRectMake(SCREEN_WIDTH-60,self.TwopasswordTFiled.y+15,22,17) imageString:@"不可见" tag:0 target:self action:@selector(PWTwoButtonClicked:)];
    [self addSubview:self.PWTwoButton];
    
    UIImageView *line4 = [CreateTool createImageViewWithFrame:CGRectMake(40,self.TwopasswordTFiled.bottom,SCREEN_WIDTH-80,0.8) imageString:@"" tag:0];
       line4.backgroundColor =kHexColor(@"#E5E5E5");
       [self addSubview:line4];

    
    UIButton *ZhuceBut = [CreateTool createButtonImageTitleWithFrame:CGRectMake(50, line4.bottom+40,SCREEN_WIDTH-100,Scale_X(50)) imageString:@"外部按钮背景" buttonTitle:QSLocalizedString(@"qs_FaSongYanZhenLianJie") titleColor:[UIColor whiteColor] tag:700 target:self action:@selector(ZhuceButClicked) size:16];
    [self addSubview:ZhuceBut];
    
}
-(void)textFieldDidChange:(UITextField *)textField{
    
    //过滤空格
    NSString *tem = [[textField.text componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] componentsJoinedByString:@""];
    textField.text = tem;
}
- (void)resignTF
{
    [self.emailTFiled resignFirstResponder];
    [self.passwordTFiled resignFirstResponder];
    [self.TwopasswordTFiled resignFirstResponder];
    [self.VerificationTFiled resignFirstResponder];
    
}

- (void)PWButtonClicked:(UIButton *)but
{
    if (_Mimature)
        {
            self.passwordTFiled.secureTextEntry = YES;//设置密码保护模式输入,默认为NO
            [but setBackgroundImage:[UIImage imageNamed:@"不可见"] forState:UIControlStateNormal];
            _Mimature = false;
        }
        else
        {
            self.passwordTFiled.secureTextEntry = NO;//设置密码保护模式输入,默认为NO
            [but setBackgroundImage:[UIImage imageNamed:@"可见"] forState:UIControlStateNormal];
            _Mimature = true;
        }
}
- (void)PWTwoButtonClicked:(UIButton *)but
{
    if (_Mimature2)
    {
        self.TwopasswordTFiled.secureTextEntry = YES;//设置密码保护模式输入,默认为NO
        [but setBackgroundImage:[UIImage imageNamed:@"不可见"] forState:UIControlStateNormal];
        _Mimature2 = false;
    }
    else
    {
        self.TwopasswordTFiled.secureTextEntry = NO;//设置密码保护模式输入,默认为NO
        [but setBackgroundImage:[UIImage imageNamed:@"可见"] forState:UIControlStateNormal];
        _Mimature2 = true;
    }
}
#pragma mark--开始计时
- (void)startTime {
    
    [_timer invalidate];
    _waiTime = 60;
    [self updateVareyButton];
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateWaitTime) userInfo:nil repeats:YES];
}
- (void)updateVareyButton {
    
    NSString *title = QSLocalizedString(@"qs_HuoQuYanZhengMa");
    if (_waiTime > 0) {
        title = [NSString stringWithFormat:@"%d%@", _waiTime,QSLocalizedString(@"qs_miaohouchongfa")];
        self.VFButton.backgroundColor = [UIColor lightGrayColor];
        self.VFButton.userInteractionEnabled = NO;
    }
    [self.VFButton setTitle:title forState:UIControlStateNormal];
}
- (void)updateWaitTime {
    
    _waiTime = _waiTime - 1;
    [self updateVareyButton];
    if (_waiTime <= 0) {
        self.VFButton.backgroundColor = buttonColor;
        self.VFButton.userInteractionEnabled = YES;
        [_timer invalidate];
        _timer = nil;
    }
    
}

#pragma mark - 验证码接口
-(void)VFButtonClicked:(UIButton *)but
{
    
    if (self.emailTFiled.text.length == 0) {

        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingTianXieYouXianHao")];
        return;
    }
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"mobile"]=self.emailTFiled.text;
    parameters[@"context"]=@"forget";
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,sendsmscodeURL];

    [GWNetWork postWithURL:URLStr params:parameters success:^(id responseDict) {
        NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
        NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
        if ([code isEqualToString:@"200"]) {
            //成功
            [self startTime];
            SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:QSLocalizedString(@"qs_yanzhengmafasongchenggong") sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
            [special withSureClick:^(NSString *string) {
                
            }];
        }
        else
        {
            [JMNotifyView showNotify:DIC[@"msg"]];
        }


    } failure:^(NSError *error) {

    }];
    
}

#pragma mark - 请求接口
- (void)ZhuceButClicked
{

    if (self.emailTFiled.text.length == 0) {
            
            [JMNotifyView showNotify:QSLocalizedString(@"qs_QingTianXieYouXianHao")];
            return;
        }
    if (self.VerificationTFiled.text.length == 0) {
        
        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingShuRuYanZhengMa")];
        return;
    }
        if (self.passwordTFiled.text.length == 0) {
            
            [JMNotifyView showNotify:QSLocalizedString(@"qs_QingTianXieMiMa")];
            return;
        }
        if (self.TwopasswordTFiled.text.length == 0) {
            
            [JMNotifyView showNotify:QSLocalizedString(@"qs_QingYanZhengMiMa")];
            return;
        }
        if (![self.TwopasswordTFiled.text isEqualToString:self.passwordTFiled.text]) {
            
            [JMNotifyView showNotify:QSLocalizedString(@"qs_MiMaBuYiZhi")];
            return;
        }

        [self resignTF];
        NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
        parameters[@"mobile"]=self.emailTFiled.text;
        parameters[@"code"]=self.VerificationTFiled.text;
        parameters[@"new_password"]=self.passwordTFiled.text;
        NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,ForgetPasswordURL];

        [GWNetWork postWithURL:URLStr params:parameters success:^(id responseDict) {
            NSLog(@"-获取用户-responseDict--%@", responseDict);
            NSDictionary *DIC = responseDict;
            NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
            if ([code isEqualToString:@"200"]) {
                //成功
                SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:DIC[@"msg"] sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
                [special withSureClick:^(NSString *string) {
                    self.successCliCK();
                }];
            }
            else
            {
                [JMNotifyView showNotify:DIC[@"msg"]];
            }

        } failure:^(NSError *error) {

        }];
}
@end
