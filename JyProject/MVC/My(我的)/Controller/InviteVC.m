//
//  InviteVC.m
//  JyProject
//
//  Created by mac on 2019/12/13.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "InviteVC.h"
#import "CIImage+Extension.h"

@interface InviteVC ()

@end

@implementation InviteVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = QSLocalizedString(@"qs_YaoQing");
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"baocun"] style:UIBarButtonItemStylePlain target:self action:@selector(baocun)];
    [self NetworkRequest];
}


-(void)baocun
{
    UIImageWriteToSavedPhotosAlbum(self.QrCodeImage.image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

#pragma mark -- <保存到相册>
-(void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    NSString *msg = nil ;
    if(error){
        msg = QSLocalizedString(@"qs_tupianbaocunshibai");
    }else{
        msg = QSLocalizedString(@"qs_tupianbaocunchenggong");
    }
    [JMNotifyView showNotify:msg];
}

#pragma mark - 请求接口
- (void)NetworkRequest{
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
       parameters[@"token"]= [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,userinvitationURL];

    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
         NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
                  NSString *code_cod = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                  if ([code_cod isEqualToString:@"200"]) {
                      //成功
                      NSDictionary *data = DIC[@"data"];
                      self.YaoQInLb.text = data[@"rcode"];
                      //二维码
                      CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
                      [filter setDefaults];
                      
//                       NSString *info = [NSString stringWithFormat:@"http://192.168.8.124:8080/#/share-register?code=%@",data[@"rcode"]];
                      
                      NSString *info = [NSString stringWithFormat:@"https://adminsdf2s.tforbit.com/#/share-register?code=%@",data[@"rcode"]];
                      NSData *infoData = [info dataUsingEncoding:NSUTF8StringEncoding];
                      [filter setValue:infoData forKeyPath:@"inputMessage"];
                      CIImage *outImage = [filter outputImage];
                      self.QrCodeImage.image = [outImage createNonInterpolatedWithSize:130];
                     
                  }
                  else if ([code_cod isEqualToString:@"401"])
                  {
                     
                  }
                  else
                  {
                      [JMNotifyView showNotify:DIC[@"msg"]];
                  }

    } failure:^(NSError *error) {
        
    }];
    
}
- (IBAction)FuZhi:(id)sender {
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.YaoQInLb.text;
    [JMNotifyView showNotify:@"复制成功"];
    
}


@end
