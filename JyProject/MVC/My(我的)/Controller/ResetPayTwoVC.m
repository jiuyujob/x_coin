//
//  ResetPayTwoVC.m
//  JyProject
//
//  Created by mac on 2020/1/13.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "ResetPayTwoVC.h"
#import "SYPasswordView.h"
@interface ResetPayTwoVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (weak, nonatomic) IBOutlet UIButton *resetBtn;
@property (nonatomic, strong) SYPasswordView *pasView;
@end

@implementation ResetPayTwoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = QSLocalizedString(@"qs_resetpayword");//QSLocalizedString(@"qs_TiBi");
    
    self.pasView = [[SYPasswordView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 32, 50)];
    [self.passwordView addSubview:_pasView];
    self.resetBtn.enabled = NO;
    
    __weak typeof(self) weakself = self;
    self.pasView.inputSuccess = ^(BOOL succ) {
       
        if (succ) {
            [weakself.resetBtn setBackgroundImage:[UIImage imageNamed:@"range_length"] forState:UIControlStateNormal];
            weakself.resetBtn.enabled = YES;
        }else
        {
            [weakself.resetBtn setBackgroundImage:[UIImage imageNamed:@"next_false"] forState:UIControlStateNormal];
            weakself.resetBtn.enabled = NO;
        }
    };
    
    
}
- (IBAction)nextStep:(id)sender {
    
    if (self.email == 0) {
        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingTianXieYouXianHao")];
        return;
    }
    if (self.code == 0) {
        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingShuRuYanZhengMa")];
        return;
    }
    if (self.onePayword.length < 6) {
        [JMNotifyView showNotify:QSLocalizedString(@"qs_zhifumimachangdubugou")];
        return;
    }
    if (![self.onePayword isEqualToString:self.pasView.textField.text]) {
        [JMNotifyView showNotify:QSLocalizedString(@"qs_MiMaBuYiZhi")];
        return;
    }

        NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
        parameters[@"email"]=self.email;
        parameters[@"code"]=self.code;
        parameters[@"password"]=self.pasView.textField.text;
        NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,resetPayPwd];

        [GWNetWork postTokenWithURL:URLStr params:parameters success:^(id responseDict) {
            NSLog(@"-获取用户-responseDict--%@", responseDict);
            NSDictionary *DIC = responseDict;
            NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
            if ([code isEqualToString:@"200"]) {
                //成功
                [self performSegueWithIdentifier:@"paySuccessID" sender:nil];
            }
            else
            {
                [JMNotifyView showNotify:DIC[@"msg"]];
            }

        } failure:^(NSError *error) {

        }];
    
    
}


@end
