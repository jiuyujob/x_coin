//
//  QRcodeVC.m
//  JyProject
//
//  Created by mac on 2020/1/13.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "QRcodeVC.h"
#import "CIImage+Extension.h"
@interface QRcodeVC ()
@property (weak, nonatomic) IBOutlet UILabel *shuidema;
@property (weak, nonatomic) IBOutlet UIImageView *ma;

@end

@implementation QRcodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = QSLocalizedString(@"qs_wodeerweima");//QSLocalizedString(@"qs_TiBi");
    [self NetworkRequest];
}
#pragma mark - 请求接口
- (void)NetworkRequest{
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
       parameters[@"token"]= [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,getuserinfoURL];

    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
         NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
                  NSString *code_cod = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                  if ([code_cod isEqualToString:@"200"]) {
                      //成功
                      NSDictionary *data = DIC[@"data"];
                      self.shuidema.text = [NSString stringWithFormat:@"%@%@",data[@"nickname"],QSLocalizedString(@"qs_moumoudeerweima")];
                      CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
                      [filter setDefaults];
                      NSString *info = [NSString stringWithFormat:@"email=%@",data[@"email"]];
                      NSData *infoData = [info dataUsingEncoding:NSUTF8StringEncoding];
                      [filter setValue:infoData forKeyPath:@"inputMessage"];
                      CIImage *outImage = [filter outputImage];
                      self.ma.image = [outImage createNonInterpolatedWithSize:130];
                  }
                  else
                  {
                      [JMNotifyView showNotify:DIC[@"msg"]];
                  }

    } failure:^(NSError *error) {
        
    }];
    
}
@end
