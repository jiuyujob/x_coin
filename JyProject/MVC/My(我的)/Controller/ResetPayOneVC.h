//
//  ResetPayOneVC.h
//  JyProject
//
//  Created by mac on 2020/1/13.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ResetPayOneVC : JYBaseVC

@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *code;

@end

NS_ASSUME_NONNULL_END
