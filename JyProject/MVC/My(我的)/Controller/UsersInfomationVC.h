//
//  UsersInfomationVC.h
//  JyProject
//
//  Created by mac on 2019/12/12.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface UsersInfomationVC : JYBaseVC


@property (weak, nonatomic) IBOutlet UILabel *NameLb;
@property (weak, nonatomic) IBOutlet UILabel *IDLb;
@property (weak, nonatomic) IBOutlet UILabel *emlb;

@end

NS_ASSUME_NONNULL_END
