//
//  NewPayMima.m
//  JyProject
//
//  Created by mac on 2019/12/13.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "NewPayMima.h"
#import "SYPasswordView.h"
#import "ResetPayOneVC.h"
@interface NewPayMima ()
{
    NSTimer *_timer;
    int _waiTime;
    BOOL getVercodeSuccess;
}
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UITextField *verityTF;
@property (weak, nonatomic) IBOutlet UIButton *verityBtn;
@end

@implementation NewPayMima

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = QSLocalizedString(@"qs_resetpayword");//QSLocalizedString(@"qs_TiBi");
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"下一步" style:UIBarButtonItemStylePlain target:self action:@selector(nextstep)];
}

-(void)nextstep
{
    if (self.verityTF.text.length != 4) {
        [JMNotifyView showNotify:QSLocalizedString(@"qs_shuruyanzhengma")];
    }else{
        if (getVercodeSuccess) {
            [self performSegueWithIdentifier:@"pushoneVCID" sender:nil];
        }else{
             [JMNotifyView showNotify:QSLocalizedString(@"qs_yanzhengma")];
        }
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ResetPayOneVC *vc = segue.destinationViewController;
    vc.email = self.emailTF.text;
    vc.code = self.verityTF.text;
}


- (IBAction)getVerity:(id)sender {
    if (self.emailTF.text.length == 0) {
           [JMNotifyView showNotify:QSLocalizedString(@"qs_QingTianXieYouXianHao")];
           return;
       }
       NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
       parameters[@"email"]=self.emailTF.text;
       NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,sendPayEmail];
    [GWNetWork postTokenWithURL:URLStr params:parameters success:^(id responseDict) {
           NSLog(@"-获取用户-responseDict--%@", responseDict);
           NSDictionary *DIC = responseDict;
           NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
           if ([code isEqualToString:@"200"]) {
               //成功
               [self startTime];
               SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:QSLocalizedString(@"qs_yanzhengmafasongchenggong") sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
               [special withSureClick:^(NSString *string) {
                   self->getVercodeSuccess = YES;
               }];
           }
           else
           {
               [JMNotifyView showNotify:DIC[@"msg"]];
           }


       } failure:^(NSError *error) {

       }];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


#pragma mark--开始计时
- (void)startTime {
    
    [_timer invalidate];
    _waiTime = 60;
    [self updateVareyButton];
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateWaitTime) userInfo:nil repeats:YES];
}
- (void)updateVareyButton {
    
    NSString *title = QSLocalizedString(@"qs_HuoQuYanZhengMa");
    if (_waiTime > 0) {
        title = [NSString stringWithFormat:@"%d%@", _waiTime,QSLocalizedString(@"qs_miaohouchongfa")];
        self.verityBtn.userInteractionEnabled = NO;
    }
    [self.verityBtn setTitle:title forState:UIControlStateNormal];
}
- (void)updateWaitTime {
    
    _waiTime = _waiTime - 1;
    [self updateVareyButton];
    if (_waiTime <= 0) {
        self.verityBtn.userInteractionEnabled = YES;
        [_timer invalidate];
        _timer = nil;
    }
    
}

@end
