//
//  TeamOrganizationVC.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/19.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "TeamOrganizationVC.h"
#import "WMZTreeView.h"
#import "WMZMyCell.h"

@interface TeamOrganizationVC ()
@property(nonatomic,strong)WMZTreeView *treeView;

@end

@implementation TeamOrganizationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = QSLocalizedString(@"qs_tuanduizhuzhi");
    [self setinitUI];
    
    [self NetworkRequest];
}

- (void)setinitUI{
    
    WMZTreeViewParam *param =TreeViewParam()
       .wFrameSet(CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT-kNavgationBarHeight))
       .wDataSet([self randomArr:100 level:10]);
       self.treeView = [[WMZTreeView alloc]initWithParam:param];
       [self.view addSubview:self.treeView];
    
    
}

#pragma mark - 请求接口
- (void)NetworkRequest{
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"token"]= [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    parameters[@"user_id"]= [[NSUserDefaults standardUserDefaults] stringForKey:@"userid"];
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,userteammember];

    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
         NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
                  NSString *code_cod = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                  if ([code_cod isEqualToString:@"200"]) {
                
                      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                          
                            NSDictionary *data = DIC[@"data"];
                            
                        
                          dispatch_async(dispatch_get_main_queue(), ^{
                              
                              
                          });
                      });
                      
                  }
                  else
                  {
                      [JMNotifyView showNotify:DIC[@"msg"]];
                  }

    } failure:^(NSError *error) {
        
    }];
}

//随机多少级 每级多少条数据
- (NSArray*)randomArr:(int)num level:(int)level{
    NSMutableArray *arr = [NSMutableArray new];
    NSArray *firstId = @[@"0",@"1",@"2"];
    //第一级
    for (int i = 0; i<firstId.count; i++) {
        NSString *str = [NSString stringWithFormat:@"第1_%d级",i];
        [arr addObject:TreeParam().currentIdSet(firstId[i]).nameSet(str)];
    }
    NSInteger index = 1;
    NSInteger fitstIndex = firstId.count;
    NSInteger endIndex = num+3;
    NSArray *tempArr = [NSArray new];
    while (index<level) {
        NSArray *dataArr = [NSArray arrayWithArray:tempArr.count?tempArr:firstId];
        index+=1;
         NSMutableArray *secondId = [NSMutableArray new];
          for (NSInteger i = fitstIndex; i<endIndex; i++) {
               int y =(arc4random() % (dataArr.count));
               NSString *str = [NSString stringWithFormat:@"第%ld_%ld级",index,i];
               NSString *currentID = [NSString stringWithFormat:@"%ld",i];
               [secondId addObject:currentID];
               WMZTreeParam *param = TreeParam().currentIdSet(currentID).parentIdSet(dataArr[y]).nameSet(str);
               [arr addObject:param];
        }
        fitstIndex = num*index;
        endIndex = num*(index+1);
        tempArr = secondId;
    }
    return [NSArray arrayWithArray:arr];
}
@end
