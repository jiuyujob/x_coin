//
//  MycenterVC.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/6.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface MycenterVC : JYBaseVC

@property (weak, nonatomic) IBOutlet UILabel *nickNameLb;

@property (weak, nonatomic) IBOutlet UILabel *rcodeLb;

@end

NS_ASSUME_NONNULL_END
