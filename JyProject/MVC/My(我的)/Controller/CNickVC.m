//
//  CNickVC.m
//  JyProject
//
//  Created by mac on 2019/12/13.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "CNickVC.h"

@interface CNickVC ()

@end

@implementation CNickVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = QSLocalizedString(@"qs_xiugainichen");//QSLocalizedString(@"qs_TiBi");
    
    UIButton * rightButton = [CreateTool createButtonWithFrame:CGRectMake(0, 0,33, 22) buttonTitle:QSLocalizedString(@"qs_QueRen") titleColor:kHexColor(@"333333") selectTitleColor:nil tag:0 target:self action:@selector(rightItemClicked:) size:16];
      
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    [self.navigationItem setRightBarButtonItem:rightItem animated:YES];
    
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    attributes[NSForegroundColorAttributeName] = kHexColor(@"#999999");
    self.NameTF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:QSLocalizedString(@"qs_QinShuRuNiDeNiCheng") attributes:attributes];
    
}

- (void)rightItemClicked:(UIBarButtonItem*)item
{
    ResignFirstTextField
    if (self.NameTF.text.length == 0) {
        
        [JMNotifyView showNotify:QSLocalizedString(@"qs_QinShuRuNiDeNiCheng")];
        return;
    }
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"nickname"]=self.NameTF.text;
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,modifynickURL];
    
    
    [GWNetWork postTokenWithURL:URLStr params:parameters success:^(id responseDict) {
        NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
        NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
        if ([code isEqualToString:@"200"]) {
            
            //成功
            SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:QSLocalizedString(@"qs_xiugaichenggong") sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
            [special withSureClick:^(NSString *string) {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
           
        }
        else
        {
            [JMNotifyView showNotify:DIC[@"msg"]];
        }
        
        
    } failure:^(NSError *error) {
        
    }];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    ResignFirstTextField;
}
@end
