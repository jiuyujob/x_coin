//
//  SetPayMimaVC.m
//  JyProject
//
//  Created by mac on 2019/12/13.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "SetPayMimaVC.h"
#import "SYPasswordView.h"
@interface SetPayMimaVC ()
@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (nonatomic, strong) SYPasswordView *pasView;
@end

@implementation SetPayMimaVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = QSLocalizedString(@"qs_setpayword");//QSLocalizedString(@"qs_TiBi");
    
    UIBarButtonItem *rightBtnItem = [[UIBarButtonItem alloc]initWithTitle:QSLocalizedString(@"qs_QueRen") style:UIBarButtonItemStylePlain target:self action:@selector(setpayword)];
    self.navigationItem.rightBarButtonItem = rightBtnItem;
    self.pasView = [[SYPasswordView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 32, 50)];
    [self.passwordView addSubview:_pasView];
}

-(void)setpayword
{
    if (self.pasView.textField.text.length < 6) {
        [JMNotifyView showNotify:QSLocalizedString(@"qs_zhifumimachangdubugou")];
        return;
    }
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    [parameters setObject:self.pasView.textField.text forKey:@"password"]; //password
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,SetPayWord];
    [GWNetWork postTokenWithURL:URLStr params:parameters success:^(id responseDict)  {
        NSDictionary *DIC = responseDict;
        NSString *code_cod = [NSString stringWithFormat:@"%@",DIC[@"code"]];
        
        if ([code_cod isEqualToString:@"200"]) {
           //成功
            SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:DIC[@"msg"] sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
            [special withSureClick:^(NSString *string) {
                [self.navigationController popViewControllerAnimated:YES];
            }];

        }else{
            [JMNotifyView showNotify:DIC[@"msg"]];
        }
            
    } failure:^(NSError *error) {
        
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
