//
//  MycenterVC.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/6.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "MycenterVC.h"
#import "MyCenView.h"
#import "SosVC.h"
#import "UsersInfomationVC.h"
#import "EcologicalVC.h"
#import "XOUpdateM.h"
#import "WalleModel.h"
@interface MycenterVC ()
@property (weak, nonatomic) IBOutlet UILabel *updateLb;
@property (weak, nonatomic) IBOutlet UILabel *totalLb;
@property (weak, nonatomic) IBOutlet UILabel *revenueLb;
@property (weak, nonatomic) IBOutlet UIImageView *levelImgView;
@property (nonatomic, assign) CGFloat total;
@property (nonatomic, assign) CGFloat revenue;

@end

@implementation MycenterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
//    self.navigationController.navigationBar.barTintColor = NavigationBarTintColor;
//       self.navigationController.navigationBar.translucent = NO;
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:NaczitiColor,NSFontAttributeName:[UIFont systemFontOfSize:18]}];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.title = nil;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"erweima"] style:UIBarButtonItemStylePlain target:self action:@selector(qrcode)];
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationController.navigationBar.translucent = YES;
    [self NetworkRequest];
    [self getMyAssets];
    self.updateLb.hidden = ![XOUpdateM manager].canUp;
}
-(void)qrcode
{
    [self performSegueWithIdentifier:@"pushQRCodeVC" sender:nil];
}

-(void)getMyAssets
{
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,showecologyindexURL];
    [GWNetWork getKotenWithURL:URLStr params:nil success:^(id responseDict) {
         NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
        NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
        if ([code isEqualToString:@"200"]) {
            //成功
            WalleModel *wall = [WalleModel new];
            wall.Dicdata = [DIC objectForKey:@"data"];
            NSArray *array = wall.Dicdata[@"ecological_poject"];
            CGFloat balance = 0;
            NSString *level = nil;
            for (NSDictionary *dict in array) {
                level = [dict objectForKey:@"level"];
                balance += [[dict objectForKey:@"balance"] floatValue];
            }
            wall.Dicdata = DIC[@"data"];
            self.total = balance;
            self.revenue = [[wall.Dicdata objectForKey:@"usdd"] floatValue];
            self.totalLb.text = [NSString stringWithFormat:@"%@%f",QSLocalizedString(@"qs_zongtouzhi"),self.total];
            self.revenueLb.text = [NSString stringWithFormat:@"%@%f",QSLocalizedString(@"qs_zongshouyi"),self.revenue];
            
            
            NSString *imageName = nil;
            if ([level isEqualToString:@"男爵"]) {
                imageName = @"nanjue";
            }
            else if ([level isEqualToString:@"子爵"]){
                imageName = @"zijue";
            }
            else if ([level isEqualToString:@"侯爵"]){
                imageName = @"houjue";
            }
            else if ([level isEqualToString:@"公爵"]){
                imageName = @"gongjue";
            }
            else if ([level isEqualToString:@"伯爵"]){
                imageName = @"bojue";
            }
            else
            {
                imageName = @"wudengji";
            }
            self.levelImgView.image = [UIImage imageNamed:imageName];
            [[NSUserDefaults standardUserDefaults] setObject:level forKey:@"level"];
        }
        else if ([code isEqualToString:@"401"])
        {
            NotifyRelogin;
        }
        else
        {
            [JMNotifyView showNotify:DIC[@"msg"]];
        }
        
    } failure:^(NSError *error) {

    }];
}
#pragma mark - 请求接口
- (void)NetworkRequest{
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
       parameters[@"token"]= [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,getuserinfoURL];

    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
         NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
                  NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                  if ([code isEqualToString:@"200"]) {
                      //成功
                      NSDictionary *data = DIC[@"data"];
                      self.nickNameLb.text = data[@"nickname"];
                      self.rcodeLb.text = [NSString stringWithFormat:@"%@:%@",QSLocalizedString(@"qs_yaoqingma"),data[@"rcode"]];
                      NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                    [userDefaults setObject:data[@"id"] forKey:@"userid"];
                    [userDefaults synchronize];
                  }
                  else if ([code isEqualToString:@"401"])
                  {
                      NotifyRelogin;
                  }
                  else
                  {
                      [JMNotifyView showNotify:DIC[@"msg"]];
                  }

    } failure:^(NSError *error) {
        
    }];
    
}
- (IBAction)tapUsersCenter:(id)sender {
  
}
- (IBAction)teamTap:(id)sender {
 
}
- (IBAction)inviteTap:(id)sender {
   
}
- (IBAction)safeap:(id)sender {
  
}
- (IBAction)helpTap:(id)sender {
//    SosVC *vc = [SosVC new];
//    [vc setHidesBottomBarWhenPushed:YES];
//    [self.navigationController pushViewController:vc animated:YES];
//    SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:@"功能暂没开放" sureBtnTitle:@"确定" sureBtnColor:buttonColor];
//             [special withSureClick:^(NSString *string) {
//                 
//             }];
   
}
- (IBAction)kefuTap:(id)sender {
    SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:QSLocalizedString(@"qs_gongnengweikaifang") sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
    [special withSureClick:^(NSString *string) {
        
    }];
}
- (IBAction)setTap:(id)sender {
   
}
- (IBAction)pushEcoVC:(id)sender {
    EcologicalVC *vc = [[EcologicalVC alloc]init];
    vc.zongzz = self.total;
    vc.zongsy = self.revenue;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:NO];
}
@end
