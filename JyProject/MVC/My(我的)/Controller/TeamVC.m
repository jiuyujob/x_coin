//
//  TeamVC.m
//  JyProject
//
//  Created by mac on 2019/12/13.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "TeamVC.h"
#import "TeamModel.h"
#import "TeamOrganizationVC.h"
#import "APPWKWebView.h"

@interface TeamVC ()
@property (weak, nonatomic) IBOutlet UIImageView *gifImgView;
@property (weak, nonatomic) IBOutlet UILabel *ZhiTuiLb;
@property (weak, nonatomic) IBOutlet UILabel *erTuiLb;
@property (weak, nonatomic) IBOutlet UILabel *tuanDuiLb;
@property (weak, nonatomic) IBOutlet UILabel *dengJiLb;


@property (nonatomic, strong) TeamModel *Teammd;

@end

@implementation TeamVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = QSLocalizedString(@"qs_TuanDui");
    [self setGif];
    
    [self NetworkRequest];
}


-(void)setGif
{
    NSString *level = [[NSUserDefaults standardUserDefaults] stringForKey:@"level"];
    NSString *EngLishname;
    NSString *name;
    NSString *Color;
    if ([level isEqualToString:@"男爵"]) {
        EngLishname = @"男爵";
        name = QSLocalizedString(@"qs_NanJue");
        Color = @"#CCCBB3";
    }
    else if ([level isEqualToString:@"子爵"]){
        EngLishname = @"子爵";
        name =QSLocalizedString(@"qs_ZiJue");
        Color = @"#E5974A";
    }
    else if ([level isEqualToString:@"侯爵"]){
        EngLishname = @"侯爵";
        name = QSLocalizedString(@"qs_HouJue");
        Color = @"#BABABF";
    }
    else if ([level isEqualToString:@"公爵"]){
        EngLishname = @"公爵";
        name = QSLocalizedString(@"qs_GongJue");
        Color = @"#DCB563";
    }
    else if ([level isEqualToString:@"伯爵"]){
        EngLishname = @"伯爵";
        name = QSLocalizedString(@"qs_BoJue");
        Color = @"#EACC7D";
    }
    else
    {
        EngLishname = @"无等级";
        name = QSLocalizedString(@"qs_WuDengJi");
        Color = @"#BABABF";
    }
    self.dengJiLb.text = name;
    self.dengJiLb.textColor = kHexColor(Color);
    
    //1.找到gif文件路径
    NSURL *fileUrl = [[NSBundle mainBundle] URLForResource:EngLishname withExtension:@"gif"];//加载GIF图片
    
    CGImageSourceRef gifSource = CGImageSourceCreateWithURL((CFURLRef)fileUrl, NULL);//将GIF图片转换成对应的图片源
    size_t frameCout=CGImageSourceGetCount(gifSource);//获取其中图片源个数，即由多少帧图片组成
    NSMutableArray* frames=[[NSMutableArray alloc] init];//定义数组存储拆分出来的图片
    for (size_t i=0; i<frameCout;i++){
    CGImageRef imageRef=CGImageSourceCreateImageAtIndex(gifSource, i, NULL);//从GIF图片中取出源图片
    UIImage* imageName=[UIImage imageWithCGImage:imageRef];//将图片源转换成UIimageView能使用的图片源
    [frames addObject:imageName];//将图片加入数组中
        CGImageRelease(imageRef);
    }
    CFRelease(gifSource);
    self.gifImgView.animationImages = frames;
    self.gifImgView.animationDuration=3;//每次动画时
    self.gifImgView.animationRepeatCount = 1;
    [self.gifImgView startAnimating];//开启动画，此处没有调用播放次数接口，UIImageView默认播放次数为无限次，故这里不做处理
    
    self.gifImgView.image = frames.firstObject;
    
}
#pragma mark - 请求接口
- (void)NetworkRequest{
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
       parameters[@"token"]= [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
//    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,userteam];
     NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,@"/user/team"];

    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
         NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
                  NSString *code_cod = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                  if ([code_cod isEqualToString:@"200"]) {
                
                      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

                            NSDictionary *data = DIC[@"data"];
                            self.Teammd  = [TeamModel mj_objectWithKeyValues:data];

                          dispatch_async(dispatch_get_main_queue(), ^{

                              self.ZhiTuiLb.text = self.Teammd.children;
                              self.erTuiLb.text = self.Teammd.grandson;
                              self.tuanDuiLb.text = self.Teammd.team;
                          });
                      });
                      
                  }
                  else
                  {
                      [JMNotifyView showNotify:DIC[@"msg"]];
                  }

    } failure:^(NSError *error) {
        
    }];
    
}
- (IBAction)YuanDuiClicked:(id)sender {
    APPWKWebView *web = [APPWKWebView new];
    web.TypeHtml = @"Webview";
    web.tit = QSLocalizedString(@"qs_tuanduizhuzhi");
//    web.strURL = [NSString stringWithFormat:@"http://192.168.8.124:8081/#/team/?id=%@&token=%@",[[NSUserDefaults standardUserDefaults] stringForKey:@"userid"],[[NSUserDefaults standardUserDefaults] stringForKey:@"token"]];
    
    //香港测试环境
    //   web.strURL = [NSString stringWithFormat:@" http://admintest.tforcoin.com/static/mobile/#/team/?id=%@&token=%@",[[NSUserDefaults standardUserDefaults] stringForKey:@"userid"],[[NSUserDefaults standardUserDefaults] stringForKey:@"token"]];
    
   
    //正式环境
    web.strURL = [NSString stringWithFormat:@"https://adminsdf2s.tforbit.com/static/mobile/#/team/?id=%@&token=%@",[[NSUserDefaults standardUserDefaults] stringForKey:@"userid"],[[NSUserDefaults standardUserDefaults] stringForKey:@"token"]];
    [self.navigationController pushViewController:web animated:YES];

}

@end
