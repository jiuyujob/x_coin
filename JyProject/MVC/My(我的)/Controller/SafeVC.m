//
//  SafeVC.m
//  JyProject
//
//  Created by mac on 2019/12/13.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "SafeVC.h"
#import "SetPayMimaVC.h"
#import "NewPayMima.h"
@interface SafeVC ()
@property (weak, nonatomic) IBOutlet UILabel *payLb;
@property (nonatomic, assign) BOOL hadPay;
@end

@implementation SafeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = QSLocalizedString(@"qs_AnQuanZhongXin");//QSLocalizedString(@"qs_TiBi");
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self check];
}

-(void)check
{
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,issetPay];
       [GWNetWork getKotenWithURL:URLStr params:nil success:^(id responseDict) {
           NSLog(@"-获取用户-responseDict--%@", responseDict);
           NSDictionary *DIC = responseDict;
           NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
           if (![code isEqualToString:@"200"]) {
               self.hadPay = YES;
           }else{
               self.payLb.text = QSLocalizedString(@"qs_qushezhi");
           }
       } failure:^(NSError *error) {
           
       }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)gotoPayView:(id)sender {
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"MeStoryboard" bundle:nil];
    if (self.hadPay) {
       NewPayMima *vc =  [story instantiateViewControllerWithIdentifier:@"newPayMimaID"];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
       SetPayMimaVC *vc =  [story instantiateViewControllerWithIdentifier:@"setPayMimaVCID"];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
