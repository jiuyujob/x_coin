//
//  SetVC.m
//  JyProject
//
//  Created by mac on 2019/12/13.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "SetVC.h"
#import "QSLanguageConfigHelper.h"
#import "JYTabBar.h"
#import "JYNavVC.h"
#import "WWLoginCtrl.h"
#import "APPWKWebView.h"

@interface SetVC ()
@property (weak, nonatomic) IBOutlet UILabel *versionLb;

@end

@implementation SetVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = QSLocalizedString(@"qs_Shezhi");
    
    NSString *strong = [[NSUserDefaults standardUserDefaults] stringForKey:@"QSUserLanguageKey"];
    if ([strong isEqualToString:@"en"]) {
        self.yuyanLb.text = @"The current English";
    }
    else
    {
        self.yuyanLb.text = QSLocalizedString(@"qs_jiantizhongwen");
    }
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    self.versionLb.text = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
}

- (IBAction)TuiChu:(id)sender {
    
    [HandleUserInfo clearAllUserInfo];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    
                WWLoginCtrl *ctrl = [[WWLoginCtrl alloc] init];
                JYNavVC *nav = [[JYNavVC alloc] initWithRootViewController:ctrl];
                QSAppKeyWindow.rootViewController = nav;
                CATransition * transition = [[CATransition alloc] init];
                transition.type = @"reveal";
                transition.duration = 0.3;
                [QSAppKeyWindow.layer addAnimation:transition forKey:nil];
            });
    
}
- (IBAction)switchClickedd:(id)sender {
    
    NSArray *arrayTitle = @[@"English",QSLocalizedString(@"qs_zhongwen")];
       [self.view createAlertViewTitleArray:arrayTitle textColor:[UIColor blackColor] font:[UIFont systemFontOfSize:16] actionBlock:^(UIButton * _Nullable button, NSInteger didRow) {
           //获取点击事件
           NSLog(@"%@,%ld",button.currentTitle,(long)didRow);
           
           switch (didRow) {
               case 0:
               {
                     [QSLanguageConfigHelper setSystemLanguageEnglish];//设置英文
                     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

                                                   
                                                     QSAppKeyWindow.rootViewController = [JYTabBar new];
                                                     CATransition * transition = [[CATransition alloc] init];
                                                     transition.type = @"reveal";
                                                     transition.duration = 0.3;
                                                     [QSAppKeyWindow.layer addAnimation:transition forKey:nil];
                                                 });
               }
                   break;
                   case 1:
                   {
                       [QSLanguageConfigHelper setSystemLanguageChinese];//设置中文
                       dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

                                                                      
                                                                        QSAppKeyWindow.rootViewController = [JYTabBar new];
                                                                        CATransition * transition = [[CATransition alloc] init];
                                                                        transition.type = @"reveal";
                                                                        transition.duration = 0.3;
                                                                        [QSAppKeyWindow.layer addAnimation:transition forKey:nil];
                                                                    });
                   }
                       break;
               default:
                   break;
           }
           

       }];
}
- (IBAction)fuwuxianyi:(id)sender {
    
    [XLDouYinLoading showInView:self.view];
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,showagreementtouser];
    [GWNetWork getKotenWithURL:URLStr params:nil success:^(id responseDict) {
           [XLDouYinLoading hideInView:self.view];
            NSLog(@"-获取用户-responseDict--%@", responseDict);
           NSDictionary *DIC = responseDict;
                     NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                     if ([code isEqualToString:@"200"]) {
                         
                         NSDictionary *data = DIC[@"data"];
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            
                             APPWKWebView *web = [APPWKWebView new];
                             web.TypeHtml = @"H5";
                             web.tit = QSLocalizedString(@"qs_YongHuXieYi");
                             web.strURL = [NSString stringWithFormat:@"%@",data[@"article_info"]];
                             [self.navigationController pushViewController:web animated:YES];
                                    
                       });
                     }
                     else if ([code isEqualToString:@"401"])
                     {
                         NotifyRelogin;
                     }
                     else
                     {
                         [JMNotifyView showNotify:DIC[@"msg"]];
                     }

       } failure:^(NSError *error) {
           [XLDouYinLoading hideInView:self.view];
       }];

}


@end
