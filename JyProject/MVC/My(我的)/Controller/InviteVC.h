//
//  InviteVC.h
//  JyProject
//
//  Created by mac on 2019/12/13.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface InviteVC : JYBaseVC
@property (weak, nonatomic) IBOutlet UILabel *YaoQInLb;
@property (weak, nonatomic) IBOutlet UILabel *NickName;
@property (weak, nonatomic) IBOutlet UIImageView *QrCodeImage;

@end

NS_ASSUME_NONNULL_END
