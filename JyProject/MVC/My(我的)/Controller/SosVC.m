//
//  SosVC.m
//  JyProject
//
//  Created by mac on 2019/12/13.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "SosVC.h"
#import "APPWKWebView.h"
#import "SOSCell.h"

@interface SosVC ()

@end

@implementation SosVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = QSLocalizedString(@"qs_BangZhuZhongXin");
    self.ArrayData = [NSMutableArray array];
    [self SetinitUI];
    
    [self NetworkRequest];
}

- (void)SetinitUI{
    _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = kHexColor(@"ffffff");
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.layer.cornerRadius = 16;
    _tableView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.05].CGColor;
    _tableView.layer.shadowOffset = CGSizeMake(0,0);
    _tableView.layer.shadowOpacity = 1;
    _tableView.layer.shadowRadius = 5;
    _tableView.clipsToBounds = NO;
    [self.view addSubview:_tableView];
    
}

#pragma mark - 请求接口
- (void)NetworkRequest{
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"agreement_type"]= @"常见问题";
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,showagreementtoroot];

    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
         NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
                  NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                  if ([code isEqualToString:@"200"]) {
                      //成功
                      NSArray *bids  = DIC[@"data"];
                      [self.ArrayData addObjectsFromArray:bids];
                      self.tableView.frame = CGRectMake(12, 20, SCREEN_WIDTH - 24, self.ArrayData.count * 60);
                      [self.tableView reloadData];
                  }
                  else if ([code isEqualToString:@"401"])
                  {
                      NotifyRelogin;
                  }
                  else
                  {
                      [JMNotifyView showNotify:DIC[@"msg"]];
                  }

    } failure:^(NSError *error) {
        
    }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ArrayData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SOSCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    if (!cell) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"SOSCell" owner:self options:nil].firstObject;
    }
    cell.lab.text = [NSString stringWithFormat:@"%ld、%@",indexPath.row + 1,self.ArrayData[indexPath.row][@"article_name"]];
    if (indexPath.row == self.ArrayData.count - 1) {
        cell.sepline.hidden = YES;
    }
    else{
        cell.sepline.hidden = NO;
    }
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    APPWKWebView *web = [APPWKWebView new];
        web.TypeHtml = @"H5";
        web.tit = QSLocalizedString(@"qs_XiangQing");
        web.strURL = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"article_info"]];
        [self.navigationController pushViewController:web animated:YES];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}
@end
