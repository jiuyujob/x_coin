//
//  UsersInfomationVC.m
//  JyProject
//
//  Created by mac on 2019/12/12.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "UsersInfomationVC.h"
#import "JYNavVC.h"
#import "WWLoginCtrl.h"

@interface UsersInfomationVC ()

@end

@implementation UsersInfomationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = QSLocalizedString(@"qs_gerenziliao");//QSLocalizedString(@"qs_TiBi");
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self NetworkRequest];
}
#pragma mark - 请求接口
- (void)NetworkRequest{
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
       parameters[@"token"]= [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,getuserinfoURL];

    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
         NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
                  NSString *code_cod = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                  if ([code_cod isEqualToString:@"200"]) {
                      //成功
                      NSDictionary *data = DIC[@"data"];
                      self.NameLb.text = data[@"nickname"];
                      self.IDLb.text = data[@"id"];
                      self.emlb.text = data[@"email"];
                  }
                  else
                  {
                      [JMNotifyView showNotify:DIC[@"msg"]];
                  }

    } failure:^(NSError *error) {
        
    }];
    
}


@end
