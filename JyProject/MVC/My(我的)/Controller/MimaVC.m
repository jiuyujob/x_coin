//
//  MimaVC.m
//  JyProject
//
//  Created by mac on 2019/12/13.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "MimaVC.h"

@interface MimaVC ()
{
    NSTimer *_timer;
    int _waiTime;
    bool _Mimature;
    bool _Mimature2;
}
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UITextField *verityTF;
@property (weak, nonatomic) IBOutlet UILabel *wrongEmailLb;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UILabel *wrongVerityLb;
@property (weak, nonatomic) IBOutlet UITextField *againPasswordTF;
@property (weak, nonatomic) IBOutlet UILabel *wrongPasswordLb;
@property (weak, nonatomic) IBOutlet UIButton *NoEmilLb;
@property (weak, nonatomic) IBOutlet UILabel *wrongSameLb;
@property (weak, nonatomic) IBOutlet UIButton *getVeriryBtn;
@property (weak, nonatomic) IBOutlet UIButton *seePasswordBtn;
@property (weak, nonatomic) IBOutlet UIButton *seeAgainPasswordBtn;

@end

@implementation MimaVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = QSLocalizedString(@"qs_ChongZhiDengLuMiMa");
    
    self.wrongEmailLb.hidden = YES;
    self.wrongVerityLb.hidden = YES;
    self.wrongPasswordLb.hidden = YES;
    self.wrongSameLb.hidden = YES;
    self.NoEmilLb.hidden = YES;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"提交" style:UIBarButtonItemStylePlain target:self action:@selector(submit:)];
}

- (IBAction)getVerityCode:(id)sender {
    if (self.emailTF.text.length == 0) {
        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingTianXieYouXianHao")];
        return;
    }
    [self.view endEditing:YES];
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"mobile"]=self.emailTF.text;
    parameters[@"context"]=@"reset";
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,sendsmscodeURL];

    [GWNetWork postWithURL:URLStr params:parameters success:^(id responseDict) {
        NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
        NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
        if ([code isEqualToString:@"200"]) {
            //成功
            [self startTime];
            SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:QSLocalizedString(@"qs_yanzhengmafasongchenggong") sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
            [special withSureClick:^(NSString *string) {
                
            }];
        }
        else
        {
            [JMNotifyView showNotify:DIC[@"msg"]];
        }


    } failure:^(NSError *error) {

    }];
}

#pragma mark--开始计时
- (void)startTime {
    
    [_timer invalidate];
    _waiTime = 60;
    [self updateVareyButton];
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateWaitTime) userInfo:nil repeats:YES];
}
- (void)updateVareyButton {
    
    NSString *title = QSLocalizedString(@"qs_HuoQuYanZhengMa");
    if (_waiTime > 0) {
        title = [NSString stringWithFormat:@"%d%@", _waiTime,QSLocalizedString(@"qs_miaohouchongfa")];
        self.getVeriryBtn.userInteractionEnabled = NO;
    }
    [self.getVeriryBtn setTitle:title forState:UIControlStateNormal];
}
- (void)updateWaitTime {
    
    _waiTime = _waiTime - 1;
    [self updateVareyButton];
    if (_waiTime <= 0) {
        self.getVeriryBtn.userInteractionEnabled = YES;
        [_timer invalidate];
        _timer = nil;
    }
    
}


- (void)submit:(id)sender {

    if (self.emailTF.text.length == 0) {
            [JMNotifyView showNotify:QSLocalizedString(@"qs_QingTianXieYouXianHao")];
            return;
        }
    if (self.verityTF.text.length == 0) {
        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingShuRuYanZhengMa")];
        return;
    }
        if (self.passwordTF.text.length == 0) {
            [JMNotifyView showNotify:QSLocalizedString(@"qs_QingTianXieMiMa")];
            return;
        }
        if (self.againPasswordTF.text.length == 0) {
            
            [JMNotifyView showNotify:QSLocalizedString(@"qs_QingYanZhengMiMa")];
            return;
        }
        if (![self.againPasswordTF.text isEqualToString:self.passwordTF.text]) {
            [JMNotifyView showNotify:QSLocalizedString(@"qs_MiMaBuYiZhi")];
            return;
        }

        [self resignTF];
        NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
        parameters[@"mobile"]=self.emailTF.text;
        parameters[@"code"]=self.verityTF.text;
        parameters[@"new_password"]=self.passwordTF.text;
        NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,ResetPwdRL];

        [GWNetWork postTokenWithURL:URLStr params:parameters success:^(id responseDict) {
            NSLog(@"-获取用户-responseDict--%@", responseDict);
            NSDictionary *DIC = responseDict;
            NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
            if ([code isEqualToString:@"200"]) {
                //成功
               [self performSegueWithIdentifier:@"loginPaswordSuccessID" sender:nil];
            }
            else
            {
                [JMNotifyView showNotify:DIC[@"msg"]];
            }

        } failure:^(NSError *error) {

        }];
}
- (void)resignTF
{
    [self.emailTF resignFirstResponder];
    [self.passwordTF resignFirstResponder];
    [self.againPasswordTF resignFirstResponder];
    [self.verityTF resignFirstResponder];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
