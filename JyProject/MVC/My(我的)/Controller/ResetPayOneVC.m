//
//  ResetPayOneVC.m
//  JyProject
//
//  Created by mac on 2020/1/13.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "ResetPayOneVC.h"
#import "SYPasswordView.h"
#import "ResetPayTwoVC.h"
@interface ResetPayOneVC ()
@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (nonatomic, strong) SYPasswordView *pasView;
@end

@implementation ResetPayOneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = QSLocalizedString(@"qs_resetpayword");//QSLocalizedString(@"qs_TiBi");
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"下一步" style:UIBarButtonItemStylePlain target:self action:@selector(nextstep)];
    
    self.pasView = [[SYPasswordView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 32, 50)];
    [self.passwordView addSubview:_pasView];
}

-(void)nextstep
{
    [self performSegueWithIdentifier:@"pushtwoVCID" sender:nil];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ResetPayTwoVC *vc = segue.destinationViewController;
    
    vc.email = self.email;
    vc.code = self.code;
    vc.onePayword = self.pasView.textField.text;
}

@end
