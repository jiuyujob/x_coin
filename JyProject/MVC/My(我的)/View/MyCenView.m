//
//  MyCenView.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/6.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "MyCenView.h"

#define COL_COUNT 3

@implementation MyCenView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
       if (self) {
           self.backgroundColor = [UIColor clearColor];
           [self initSetUI];
           
       }
    return self;
}

- (void)initSetUI
{
    UIImageView *BacKImage = [CreateTool createImageViewWithFrame:CGRectZero imageString:@"背景" tag:0];
    [self addSubview:BacKImage];
    [BacKImage mas_makeConstraints:^(MASConstraintMaker *make) {
        
    }];
    
    
    self.TouXiangImage = [CreateTool createImageViewWithFrame:CGRectMake(self.frame.size.width/2-52,10,70,70) imageString:@"" tag:0];
    self.TouXiangImage.layer.cornerRadius = self.TouXiangImage.bounds.size.width/2;
    self.TouXiangImage.layer.masksToBounds = YES;
    self.TouXiangImage.backgroundColor = [UIColor yellowColor];
    [BacKImage addSubview:self.TouXiangImage];
    
    self.NameLabel = [CreateTool createLabelWithFrame:CGRectZero text:@"用户名" font:15 textColor:[UIColor whiteColor] tag:0];
    [BacKImage addSubview:self.NameLabel];
    [self.NameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(BacKImage).offset(Scale_X(76));
        make.left.mas_equalTo(BacKImage).offset(0);
        make.right.mas_equalTo(BacKImage).offset(-0);
        make.height.mas_equalTo(@25);
    }];
    
    self.YaoQinMaLabel = [CreateTool createLabelWithFrame:CGRectZero text:@"邀请码：HDHFHF" font:14 textColor:[UIColor whiteColor] tag:0];
    [BacKImage addSubview:self.YaoQinMaLabel];
    [self.YaoQinMaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.NameLabel).offset(Scale_X(28));
        make.left.mas_equalTo(BacKImage).offset(0);
        make.right.mas_equalTo(BacKImage).offset(-0);
        make.height.mas_equalTo(@25);
    }];
    
    UIView *View = [[UIView alloc] init];
    View.backgroundColor = [UIColor clearColor];
    View.userInteractionEnabled = YES;
    [self addSubview:View];
    [View mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(BacKImage).offset(Scale_X(128));
        make.left.mas_equalTo(self).offset(0);
        make.right.mas_equalTo(self).offset(-0);
        make.height.mas_equalTo(@210);
    }];
    
        CGFloat btnWidth = 78;  //宽
        CGFloat btnHeight = 60; //高
    
        NSArray *ImaArray =@[@"我的页面核心团队",@"我的页面邀请",@"我的页面安全中心"];
        NSArray *TitArray =@[QSLocalizedString(@"qs_TuanDui"),QSLocalizedString(@"qs_YaoQing"),QSLocalizedString(@"qs_AnQuanZhongXin")];

        for (int i = 0; i<3; i++) {
            int row = i/3; //行
            int col = i%3; //列
            
            CGFloat margin = (SCREEN_WIDTH - (btnWidth * COL_COUNT)) / (COL_COUNT + 1);
            CGFloat picX = margin + (btnWidth + margin) * col;
            CGFloat picY = margin + (btnHeight + margin) * row;
            UIImageView *ViewImage = [CreateTool createImageViewWithFrame:CGRectMake(picX,picY,btnWidth,btnHeight) imageString:@"个人中心圆角" tag:0];
            [View addSubview:ViewImage];
            
            CGFloat Imagewith = ViewImage.frame.size.width;
            UIButton *OneBut = [CreateTool createButtonWithFrame:CGRectMake(Imagewith/2-10.5,10,20,20) imageString:ImaArray[i] tag:900+i target:self action:@selector(ButClicked:)];
            [ViewImage addSubview:OneBut];
                           
                           
            UIButton *textBut1 = [CreateTool createButtonWithFrame:CGRectMake(0,33,Imagewith,17) buttonTitle:TitArray[i] titleColor:[UIColor whiteColor] selectTitleColor:[UIColor whiteColor] tag:900+i target:self action:@selector(ButClicked:) size:12];
             [ViewImage addSubview:textBut1];
        }

}

- (void)ButClicked:(UIButton *)but
{
    switch (but.tag) {
        case 900:
        {
            NSLog(@"1");
        }
            break;
            case 901:
                   {
                       NSLog(@"2");
                   }
                       break;
            case 902:
                   {
                       NSLog(@"3");
                   }
                       break;
            case 903:
                   {
                       NSLog(@"4");
                   }
                       break;
            case 904:
            {
                NSLog(@"4");
            }
                break;
            case 905:
            {
                self.SetUpTheClick();
            }
                break;
        default:
            break;
    }
}

- (void)QieHuanButClicked
{
    [HandleUserInfo clearAllUserInfo];
    
    self.SwitchCliCK();
    
}
@end
