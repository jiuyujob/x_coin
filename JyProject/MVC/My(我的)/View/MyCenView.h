//
//  MyCenView.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/6.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseView.h"

typedef void(^Switchaccount)(void);
typedef void(^SetUpThe)(void);//点击设置

NS_ASSUME_NONNULL_BEGIN

@interface MyCenView : JYBaseView


@property (nonatomic,retain) UIImageView *TouXiangImage;
@property (nonatomic,retain) UILabel *NameLabel;
@property (nonatomic,retain) UILabel *YaoQinMaLabel;

@property (nonatomic, copy) Switchaccount SwitchCliCK;
@property (nonatomic, copy) SetUpThe SetUpTheClick;


-(id)initWithFrame:(CGRect)frame;

@end

NS_ASSUME_NONNULL_END
