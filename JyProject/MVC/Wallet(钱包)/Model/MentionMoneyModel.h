//
//  MentionMoneyModel.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/13.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MentionMoneyModel : NSObject

@property (nonatomic,retain) NSString *coin_id;
@property (nonatomic,retain) NSString *currency_id;
@property (nonatomic,retain) NSString *min_withdrawal;
@property (nonatomic,retain) NSString *money;
@property (nonatomic,retain) NSString *symbol;
@property (nonatomic,retain) NSString *type;
@property (nonatomic,retain) NSString *withdrawal_fee;
@property (nonatomic,retain) NSString *withdrawal_fee_type;

@end

NS_ASSUME_NONNULL_END
