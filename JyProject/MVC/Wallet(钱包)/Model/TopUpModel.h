//
//  TopUpModel.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/10.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TopUpModel : NSObject

@property (nonatomic,retain) NSString *address;
@property (nonatomic,retain) NSString *block_chain_id;
@property (nonatomic,retain) NSString *chain_code;
@property (nonatomic,retain) NSString *chain_name;
@property (nonatomic,retain) NSString *deposit_addr_id;
@property (nonatomic,retain) NSString *status;
@property (nonatomic,retain) NSString *type;
@property (nonatomic,retain) NSString *updated_at;

@end

NS_ASSUME_NONNULL_END
