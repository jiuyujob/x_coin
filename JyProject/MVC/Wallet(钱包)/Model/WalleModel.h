//
//  WalleModel.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/9.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WalleModel : NSObject<NSCoding>

@property (nonatomic,strong) NSDictionary *Dicdata;

@end

NS_ASSUME_NONNULL_END
