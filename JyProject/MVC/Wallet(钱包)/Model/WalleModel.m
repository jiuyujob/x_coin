//
//  WalleModel.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/9.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "WalleModel.h"

@implementation WalleModel

//对象进行解码
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init]){
        
        self.Dicdata = [aDecoder decodeObjectForKey:@"Dicdata"];
        
    }
    return self;
}

//对象进行编码
- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.Dicdata forKey:@"Dicdata"];
}

@end
