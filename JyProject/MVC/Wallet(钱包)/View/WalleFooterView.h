//
//  WalleFooterView.h
//  JyProject
//
//  Created by 九域-UI on 2020/1/13.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "JYBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalleFooterView : JYBaseView<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,retain) NSMutableArray *ArrayData;

@property (nonatomic,retain) NSString *USDDTFORprice;
@property (nonatomic,retain) NSString *USDTTFORprice;

-(id)initWithFrame:(CGRect)frame;

- (void)setCellWithInfo:(NSMutableArray *)arrayData USDDTFORprice:(NSString *)USDDprice USDTTFORprice:(NSString *)USDTprice;

@end

NS_ASSUME_NONNULL_END
