//
//  WalleTabHeadView.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/6.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "WalleTabHeadView.h"

@implementation WalleTabHeadView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
       if (self) {
           self.backgroundColor = [UIColor clearColor];
           [self initSetUI];

       }
    return self;
}

- (void)initSetUI{
    
    UIImageView * BackImage = [CreateTool createImageViewWithFrame:CGRectMake(0,0,SCREEN_WIDTH,Scale_X(260)) imageString:@"登录底图" tag:0];
     [self addSubview:BackImage];
    
    FLAnimatedImageView *dongtu = [[FLAnimatedImageView alloc] init];
    dongtu.frame =  CGRectMake(SCREEN_WIDTH/2-100,Scale_X(50),200,200);
    NSString *path = [[NSBundle mainBundle] pathForResource:@"登录页动图" ofType:@"gif"];
    NSData *gifData = [NSData dataWithContentsOfFile:path];
    FLAnimatedImage *animatedImage1 = [FLAnimatedImage animatedImageWithGIFData:gifData];
    dongtu.animatedImage = animatedImage1;
    [self addSubview:dongtu];
    
    
    UIImageView * touImage = [CreateTool createImageViewWithFrame:CGRectMake(5,Scale_X(90),SCREEN_WIDTH-10,Scale_X(160)) imageString:@"透明底图" tag:0];
    [self addSubview:touImage];
    
    
    
    self.ZiChangLabel = [CreateTool createLabelWithFrame:CGRectMake(20,20,touImage.width,25) text:QSLocalizedString(@"qs_ZiChangZheHe") font:15 textColor:[UIColor blackColor] tag:0];
    self.ZiChangLabel.textAlignment = NSTextAlignmentLeft;
    [touImage addSubview:self.ZiChangLabel];
    
    
    self.ZCresultsLabel = [CreateTool createLabelWithFrame:CGRectMake(20,self.ZiChangLabel.bottom+20,touImage.width-30, 25) text:@"≈0" font:27 textColor:buttonColor tag:0];
    self.ZCresultsLabel.textAlignment = NSTextAlignmentLeft;
    self.ZCresultsLabel.font = [UIFont boldSystemFontOfSize:28];
    [touImage addSubview:self.ZCresultsLabel];
    
    self.Label = [CreateTool createLabelWithFrame:CGRectMake(20,self.ZCresultsLabel.bottom+25,200,20) text:@"" font:14 textColor:[UIColor blackColor] tag:0];
    self.Label.textAlignment = NSTextAlignmentLeft;
    [touImage addSubview:self.Label];
    
    UIButton *button = [CreateTool createButtonWithFrame:CGRectMake(0,0,touImage.width,touImage.height) buttonTitle:@"" titleColor:nil selectTitleColor:nil tag:669 target:self  action:@selector(ButClicked:) size:0];
    button.backgroundColor = [UIColor clearColor];
    [touImage addSubview:button];

    
//    self.DjVIPImage = [CreateTool createImageViewWithFrame:CGRectMake(30,20,20,20) imageString:@"皇冠" tag:0];
//    [self addSubview:self.DjVIPImage];
//
//    self.DJVIPLabel = [CreateTool createLabelWithFrame:CGRectZero text:@"" font:13 textColor:kHexColor(@"#FFCE76") tag:0];
//    [self addSubview:self.DJVIPLabel];
//    [self.DJVIPLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self).offset(20);
//        make.left.mas_equalTo(self.DjVIPImage).offset(24);
//        make.height.mas_equalTo(@22);
//    }];
//    self.DjVIPImage.hidden = YES;
//    self.DJVIPLabel.hidden = YES;

    CGFloat btnWidth = 90;  //宽
    CGFloat btnHeight = 66; //高
    NSArray *ImaArray =@[@"钱包页扫一扫",@"钱包页收钱码验收",@"钱包用付款"];
    NSArray *TitArray =@[QSLocalizedString(@"qs_SaoYiSao"),QSLocalizedString(@"qs_ShouQian"),QSLocalizedString(@"qs_FuQian")];

    for (int i = 0; i < 3; i++)
    {
        int col = i%3; //列
        CGFloat margin = (SCREEN_WIDTH - (btnWidth * 3)) / (3 + 1);
        CGFloat picX = margin + (btnWidth + margin) * col;
        UIImageView *ViewImage = [CreateTool createImageViewWithFrame:CGRectMake(picX,touImage.bottom+15,btnWidth,btnHeight) imageString:@"" tag:800+i];
        [self addSubview:ViewImage];
        CGFloat Imagewith = ViewImage.frame.size.width;

        UIButton *OneBut = [CreateTool createButtonWithFrame:CGRectMake(Imagewith/2-17.5,0,35,35) imageString:ImaArray[i] tag:666+i target:self action:@selector(ButClicked:)];
        [ViewImage addSubview:OneBut];

        UIButton *textBut1 = [CreateTool createButtonWithFrame:CGRectMake(0,36,Imagewith,30) buttonTitle:TitArray[i] titleColor:buttonColor selectTitleColor:nil tag:666+i target:self action:@selector(ButClicked:) size:12];
        [ViewImage addSubview:textBut1];

        UIButton *But = [CreateTool createButtonWithFrame:CGRectMake(picX,Scale_X(220),btnWidth,btnHeight) buttonTitle:@"" titleColor:[UIColor whiteColor] selectTitleColor:[UIColor whiteColor] tag:666+i target:self action:@selector(ButClicked:) size:13];
        But.backgroundColor = [UIColor clearColor];
        [self addSubview:But];
    }
    
}

- (void)setCellWithInfo:(WalleModel*)Model
{
    NSLog(@"头参数参数%@",Model.Dicdata);
    //是否显示超级节点
    NSString *Super_peer_bool = [NSString stringWithFormat:@"%@",Model.Dicdata[@"super_peer_bool"]];
    if ([Super_peer_bool isEqualToString:@"1"])
    {
        self.DjVIPImage.hidden = NO;
        self.DJVIPLabel.hidden = NO;
        NSDictionary *Super_peer = Model.Dicdata[@"super_peer"];
        self.DJVIPLabel.text =  [NSString stringWithFormat:@"%@:%@",QSLocalizedString(@"qs_JiBie"),Super_peer[@"level"]];
    }
    else
    {
        self.DjVIPImage.hidden = YES;
        self.DJVIPLabel.hidden = YES;
    }
}

- (void)setTFORCount:(NSString*)Count tforusdt:(NSString *)pice
{
    self.ZCresultsLabel.text = [NSString stringWithFormat:@"≈%@",Count];
    
    self.Label.text = [NSString stringWithFormat:@"1 USDT = %@ TFOR",pice];
}
- (void)ButClicked:(UIButton *)but
{
    switch (but.tag) {
        case 666:
        {
            self.SaoyisaoClicked();
        }
            break;
            case 667:
                   {
                       self.ShouQianClicked();
                   }
                       break;
            case 668:
                   {
                       self.FuQianClicked();
                   }
                       break;
            case 669:
                   {
                       self.AccountClicked(self.ZCresultsLabel.text,self.Label.text);
                   }
                       break;
        default:
            break;
    }
}

@end
