//
//  MentionMoneyView.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/9.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "MentionMoneyView.h"
#import "CurrencyTypeVC.h"
#import "MentionAddressVC.h"
#import "SetPayMimaVC.h"

@implementation MentionMoneyView

-(id)initWithFrame:(CGRect)frame symbol:(NSString *)symbol currency_id:(NSString *)currency_id{
    self = [super initWithFrame:frame];
       if (self) {
           self.backgroundColor = [UIColor clearColor];
           _Mimature= false;
           self.symbol = symbol;
           self.currency_id = currency_id;
           self.MentionMoneyModel = [MentionMoneyModel new];
           [self initSetUI];
           [self ONENetworkRequest];
           
       }
    return self;
}

-(void)initSetUI{
    
    
    //提币币种
    UIView *OneView = [[UIView alloc] initWithFrame:CGRectMake(10,20,SCREEN_WIDTH-20,400)];
    OneView.layer.backgroundColor = [UIColor whiteColor].CGColor;
    OneView.layer.cornerRadius = 10;
    OneView.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    OneView.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    OneView.layer.shadowOpacity = 0.3;// 阴影透明度，默认0
    OneView.layer.shadowRadius = 5;// 阴影半径，默认3
    [self addSubview:OneView];
    
    UILabel *bizLa = [CreateTool createLabelWithFrame:CGRectMake(8,0,100,50) text:QSLocalizedString(@"qs_TiBiBiZhong") font:13 textColor:[UIColor blackColor] tag:0];
    bizLa.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:bizLa];

    self.TBBZtypeBut = [CreateTool createButtonWithFrame:CGRectMake(bizLa.right+10,0,OneView.width-40,50) buttonTitle:self.symbol titleColor:[UIColor blackColor] selectTitleColor:nil tag:890 target:self action:@selector(ButtonCLicked:) size:13];
    self.TBBZtypeBut.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [OneView addSubview:self.TBBZtypeBut];
    
    UIImageView *line = [CreateTool createImageViewWithFrame:CGRectMake(15,bizLa.bottom,OneView.width-30,0.3) imageString:@"" tag:0];
    line.backgroundColor = buttonHuiSeColor;
    [OneView addSubview:line];

    //链类型
    UILabel *llxLa = [CreateTool createLabelWithFrame:CGRectMake(8,line.bottom,100,50) text:QSLocalizedString(@"qs_LianLeiXing") font:13 textColor:[UIColor blackColor] tag:0];
    llxLa.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:llxLa];

    self.LiantypeBut = [CreateTool createButtonWithFrame:CGRectMake(llxLa.right+10,llxLa.y,OneView.width-40,50) buttonTitle:QSLocalizedString(@"qs_QingXuanZheNiDeLianLeiXing") titleColor:[UIColor blackColor] selectTitleColor:nil tag:891 target:self action:@selector(ButtonCLicked:) size:13];
       self.LiantypeBut.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [OneView addSubview:self.LiantypeBut];

    self.JianTouIm2 = [CreateTool createImageViewWithFrame:CGRectMake(SCREEN_WIDTH-62,llxLa.y+18,10,13) imageString:@"黑色返回" tag:0];
    [OneView addSubview:self.JianTouIm2];

//提币到地址

    UILabel *tiAddLa = [CreateTool createLabelWithFrame:CGRectMake(8,llxLa.bottom,100,60) text:QSLocalizedString(@"qs_TiBiDaoDiZi") font:13 textColor:[UIColor blackColor] tag:0];
    tiAddLa.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:tiAddLa];

    UIImageView *JianTouIm3 = [CreateTool createImageViewWithFrame:CGRectMake(OneView.width-25,tiAddLa.y+28,10,13) imageString:@"黑色返回" tag:0];
    [OneView addSubview:JianTouIm3];

    self.NameAddLa = [CreateTool createLabelWithFrame:CGRectMake(tiAddLa.right,tiAddLa.y,OneView.width-140,30) text:@"" font:12 textColor:[UIColor blackColor] tag:0];
    self.NameAddLa.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:self.NameAddLa];

    self.TwoNameAddLa = [CreateTool createLabelWithFrame:CGRectMake(self.NameAddLa.left,self.NameAddLa.bottom,OneView.width-140,30) text:@"" font:12 textColor:[UIColor blackColor] tag:0];
    self.TwoNameAddLa.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:self.TwoNameAddLa];

    UIButton *threeBut = [CreateTool createButtonWithFrame:CGRectMake(0,self.NameAddLa.y,OneView.width,60) buttonTitle:nil titleColor:nil selectTitleColor:nil tag:892 target:self action:@selector(ButtonCLicked:) size:0];
    [OneView addSubview:threeBut];
//转出数量

    UILabel *ZhuangLa = [CreateTool createLabelWithFrame:CGRectMake(8,tiAddLa.bottom,100,50) text:QSLocalizedString(@"qs_ZhuangChuShuLiang") font:13 textColor:[UIColor blackColor] tag:0];
    ZhuangLa.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:ZhuangLa];

    self.minTFiled = [CreateTool createTextFieldWithFrame:CGRectMake(ZhuangLa.right,ZhuangLa.y,OneView.width-140,50) placeholder:@"" delegate:self tag:100];
    [self.minTFiled addTarget:self action:@selector(minTFiledtextChange) forControlEvents:UIControlEventEditingChanged];
    self.minTFiled.keyboardType = UIKeyboardTypeDecimalPad;
    [OneView addSubview:self.minTFiled];

    self.TypeLa = [CreateTool createLabelWithFrame:CGRectMake(OneView.width-50,ZhuangLa.y,50,50) text:@"" font:13 textColor:[UIColor blackColor] tag:0];
    [OneView addSubview:self.TypeLa];

//当前可用
    self.KeYongLabel = [CreateTool createLabelWithFrame:CGRectMake(8,ZhuangLa.bottom+10,OneView.width-70,25) text:@"" font:13 textColor:HuiSeZiTiColor tag:0];
    [OneView addSubview:self.KeYongLabel];

    UIButton *QuanBuZcBut = [CreateTool createButtonWithFrame:CGRectMake(OneView.width-90,self.KeYongLabel.y,80,20) buttonTitle:QSLocalizedString(@"qs_QuanBuZhuanChu") titleColor:buttonColor selectTitleColor:nil tag:0 target:self action:@selector(QuanBuZcButClicked:) size:11];
    QuanBuZcBut.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [OneView addSubview:QuanBuZcBut];
    
    UIImageView *line2 = [CreateTool createImageViewWithFrame:CGRectMake(15,self.KeYongLabel.bottom+4,OneView.width-30,0.3) imageString:@"" tag:0];
    line2.backgroundColor = buttonHuiSeColor;
    [OneView addSubview:line2];

//手续费

    UILabel *SHouLabel = [CreateTool createLabelWithFrame:CGRectMake(8,line2.bottom,100,50) text:QSLocalizedString(@"qs_ShouXuFei") font:13 textColor:[UIColor blackColor] tag:0];
    SHouLabel.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:SHouLabel];

    self.ShouXuFeiLa = [CreateTool createLabelWithFrame:CGRectMake(SHouLabel.right+10,SHouLabel.y, OneView.width-150,50) text:@"" font:13 textColor:[UIColor blackColor] tag:0];
    self.ShouXuFeiLa.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:self.ShouXuFeiLa];

    self.TypeLa2 = [CreateTool createLabelWithFrame:CGRectMake(OneView.width-50,SHouLabel.y,50, 50) text:@"" font:13 textColor:[UIColor blackColor] tag:0];
    [OneView addSubview:self.TypeLa2];

    //到账数量

    UILabel *DaoZhangLabel = [CreateTool createLabelWithFrame:CGRectMake(8,SHouLabel.bottom,100,50) text:QSLocalizedString(@"qs_DaoZhangShuLiang") font:13 textColor:[UIColor blackColor] tag:0];
    DaoZhangLabel.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:DaoZhangLabel];

    self.DaoZshuLLabel = [CreateTool createLabelWithFrame:CGRectMake(DaoZhangLabel.right,DaoZhangLabel.y, OneView.width-150,50) text:@"" font:13 textColor:[UIColor blackColor] tag:0];
    self.DaoZshuLLabel.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:self.DaoZshuLLabel];

    self.TypeLa3 = [CreateTool createLabelWithFrame:CGRectMake(OneView.width-50,DaoZhangLabel.y,50, 50) text:@"" font:13 textColor:[UIColor blackColor] tag:0];
    [OneView addSubview:self.TypeLa3];
    
    UIImageView *line3 = [CreateTool createImageViewWithFrame:CGRectMake(15,DaoZhangLabel.bottom,OneView.width-30,0.3) imageString:@"" tag:0];
    line3.backgroundColor = buttonHuiSeColor;
    [OneView addSubview:line3];

    //支付密码

    UILabel *ZhiFuLabel = [CreateTool createLabelWithFrame:CGRectMake(8,line3.bottom,100,50) text:QSLocalizedString(@"qs_ZhiFuMiMa") font:13 textColor:[UIColor blackColor] tag:0];
    ZhiFuLabel.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:ZhiFuLabel];

    self.PaypasswordTFiled = [CreateTool createTextFieldWithFrame:CGRectMake(ZhiFuLabel.right,ZhiFuLabel.y,OneView.width-90,50) placeholder:QSLocalizedString(@"qs_QinShuRuZhiFuMiMa") delegate:self tag:666];
    self.PaypasswordTFiled.secureTextEntry = YES;
    self.PaypasswordTFiled.keyboardType = UIKeyboardTypeDecimalPad;
    [OneView addSubview:self.PaypasswordTFiled];

    self.PWButton  = [CreateTool createButtonWithFrame:CGRectMake(OneView.width-35,self.PaypasswordTFiled.y+16,22, 17) imageString:@"不可见" tag:0 target:self action:@selector(PWButtonClicked:)];
    [OneView addSubview:self.PWButton];

    //=======================================

    UILabel *tiBixuZhiLa = [CreateTool createLabelWithFrame:CGRectMake(20,OneView.bottom+10,200,30) text:QSLocalizedString(@"qs_TiBixuZhi") font:14 textColor:kHexColor(@"#939393") tag:0];
    tiBixuZhiLa.textAlignment = NSTextAlignmentLeft;
    [self addSubview:tiBixuZhiLa];

    self.ZuiDiTiXianLa = [CreateTool createLabelWithFrame:CGRectMake(20,tiBixuZhiLa.bottom,SCREEN_WIDTH-20,25) text:QSLocalizedString(@"qs_ZuiDiTiBiJinEWei") font:12.5 textColor:kHexColor(@"#939393") tag:0];
    self.ZuiDiTiXianLa.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.ZuiDiTiXianLa];

    self.TxsxfLa = [CreateTool createLabelWithFrame:CGRectMake(20,self.ZuiDiTiXianLa.bottom,SCREEN_WIDTH-20,25) text:QSLocalizedString(@"qs_TiXianShouXuFei") font:12.5 textColor:kHexColor(@"#939393") tag:0];
    self.TxsxfLa.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.TxsxfLa];


    UIButton *TiBiBut = [CreateTool createButtonImageTitleWithFrame:CGRectMake(50,self.TxsxfLa.bottom+25, SCREEN_WIDTH-100,Scale_X(55)) imageString:@"range_length" buttonTitle:QSLocalizedString(@"qs_TiBi") titleColor:[UIColor whiteColor] tag:1000 target:self action:@selector(TiBiButClicked:) size:14];
    [self addSubview:TiBiBut];
}

//一进来请求链类型
- (void)ONENetworkRequest{
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"symbol"]= self.symbol;
    NSString* URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,chainsymbolURL];
    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
           
           NSLog(@"-获取用户-responseDict--%@", responseDict);
                  NSDictionary *DIC = responseDict;
                  NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                  if ([code isEqualToString:@"200"]) {
                      //成功
                       NSArray *bids  = DIC[@"data"];
                      if (bids.count>0)
                      {
                          
                          [self.LiantypeBut setTitle:bids[0][@"type"] forState:UIControlStateNormal];
                          self.coin_id = [NSString stringWithFormat:@"%@",bids[0][@"coin_id"]];
                          self.iid = [NSString stringWithFormat:@"%@",bids[0][@"id"]];
                          
                          if (bids.count == 1) {
                              self.JianTouIm2.hidden = YES;
                              self.LiantypeBut.userInteractionEnabled = NO;
                          }
                          
                         [self NetworkRequest];
                          
                          
                      }
                        
                  }
                  else
                  {
                      [JMNotifyView showNotify:DIC[@"msg"]];
                  }
       } failure:^(NSError *error) {
           
       }];
    
    
    
}

- (void)ButtonCLicked:(UIButton *)but{
     __weak typeof(self) weakself = self;
    switch (but.tag) {
        case 890:
        {

        }
            break;
            case 891:
                   {
                       if (self.symbol.length == 0) {
                           
                           [JMNotifyView showNotify:QSLocalizedString(@"qs_QingXuanZheNiDeBiZhong")];
                           return;
                       }
                       
                       CurrencyTypeVC *vc = [CurrencyTypeVC new];
                       vc.Type = @"链类型";
                       vc.PageType = @"提币页面";
                       vc.parameter = self.symbol;
                       vc.buttitCliCK = ^(NSString * _Nullable buttit, NSString * _Nullable currency_id, NSString * _Nullable coin_id, NSString * _Nullable iid) {
                           
                           [weakself.LiantypeBut setTitle:buttit forState:UIControlStateNormal];
                           weakself.coin_id = coin_id;
                           weakself.iid = iid;
                           [weakself NetworkRequest];
                           
                       };

                    [[weakself getCurrentVC].navigationController pushViewController:vc animated:YES];
                   }
                       break;
            case 892:
            {

                MentionAddressVC *vc = [MentionAddressVC new];
                //币
                vc.symbol = self.symbol;
                vc.currency_id = self.currency_id;
                //链
                vc.lianName = self.LiantypeBut.titleLabel.text;
                vc.block_chain_id = self.iid;
                
                vc.valueOfClicked = ^(NSString * _Nullable addTit, NSString * _Nullable AddName, NSString * _Nullable withdrawal_addr_id) {
                    
                    weakself.NameAddLa.text = addTit;
                    weakself.TwoNameAddLa.text = AddName;
                    weakself.withdrawal_addr_id = withdrawal_addr_id;
                    
                };
                [[weakself getCurrentVC].navigationController pushViewController:vc animated:YES];
            }
                break;
        default:
            break;
    }
}
#pragma mark - 请求接口
- (void)NetworkRequest{
    
   NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
   parameters[@"token"]= [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
   parameters[@"coin_id"]= self.coin_id;
   
   NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,coindepositURL];
    
        [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {

           NSLog(@"-获取用户-responseDict--%@", responseDict);
            NSDictionary *DIC = responseDict;
            NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
            if ([code isEqualToString:@"200"]) {
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    //成功
                    NSDictionary *data = DIC[@"data"];
                    self.MentionMoneyModel.coin_id = data[@"coin_id"];
                    self.MentionMoneyModel.currency_id = data[@"currency_id"];
                    self.MentionMoneyModel.min_withdrawal = data[@"min_withdrawal"];
                    self.MentionMoneyModel.money = data[@"money"];
                    self.MentionMoneyModel.symbol = data[@"symbol"];
                    self.MentionMoneyModel.type = data[@"type"];
                    self.MentionMoneyModel.withdrawal_fee = data[@"withdrawal_fee"];
                    self.MentionMoneyModel.withdrawal_fee_type = data[@"withdrawal_fee_type"];
                    
                    //主线程执行
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        //最小提币数量
                        NSString *plString = [NSString stringWithFormat:@"%@%@",QSLocalizedString(@"qs_ZuiXiaoTiBiShuLiang"),self.MentionMoneyModel.min_withdrawal];
                        
                        NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
                        attributes[NSForegroundColorAttributeName] = kHexColor(@"#999999");
                        self.minTFiled.attributedPlaceholder = [[NSAttributedString alloc] initWithString:plString attributes:attributes];
                        
                        self.ZuiDiTiXianLa.text =[NSString stringWithFormat:@"%@%@%@",QSLocalizedString(@"qs_ZuiDiTiBiJinEWei"),self.MentionMoneyModel.min_withdrawal,self.MentionMoneyModel.symbol];
                      
                        //当前余额
                        self.KeYongLabel.text = [NSString stringWithFormat:@"%@%@ %@",QSLocalizedString(@"qs_DangQiangKeYong"),self.MentionMoneyModel.money,self.MentionMoneyModel.symbol];
                        
                        //手续费,手续费类型 percent 按百分百比,fixed 固定收取
                        if ([self.MentionMoneyModel.withdrawal_fee_type isEqualToString:@"fixed"]) {
                            
                            self.ShouXuFeiLa.text = [NSString stringWithFormat:@"%@",self.MentionMoneyModel.withdrawal_fee];
                            self.TxsxfLa.text = [NSString stringWithFormat:@"%@%@",QSLocalizedString(@"qs_TiXianShouXuFei"),self.MentionMoneyModel.withdrawal_fee];
                        }
                        else
                        {
                            
                        }
                        
                        self.TypeLa.text =[NSString stringWithFormat:@"%@",self.MentionMoneyModel.symbol];
                        self.TypeLa2.text =[NSString stringWithFormat:@"%@",self.MentionMoneyModel.symbol];
                        self.TypeLa3.text =[NSString stringWithFormat:@"%@",self.MentionMoneyModel.symbol];
                        
                        
                    });
                });

            }
            else
            {
                [JMNotifyView showNotify:DIC[@"msg"]];
            }
        } failure:^(NSError *error) {

        }];

}

- (void)minTFiledtextChange{
    
     //手续费,手续费类型 percent 按百分百比,fixed 固定收取
    if ([self.MentionMoneyModel.withdrawal_fee_type isEqualToString:@"percent"]) {
        double num1 = [self.minTFiled.text doubleValue];
        double num2 = [self.MentionMoneyModel.withdrawal_fee doubleValue];
        self.ShouXuFeiLa.text = [NSString stringWithFormat:@"%.2f",num1*(num2/100)];
        self.TxsxfLa.text = [NSString stringWithFormat:@"%@%.2f%@",QSLocalizedString(@"qs_TiXianShouXuFei"),num1*(num2/100),self.MentionMoneyModel.symbol];
    }
    
    self.DaoZshuLLabel.text = self.minTFiled.text;
    
}

- (void)PWButtonClicked:(UIButton *)but
{
    if (_Mimature)
        {
            self.PaypasswordTFiled.secureTextEntry = YES;//设置密码保护模式输入,默认为NO
            [but setBackgroundImage:[UIImage imageNamed:@"不可见"] forState:UIControlStateNormal];
            _Mimature = false;
        }
        else
        {
            self.PaypasswordTFiled.secureTextEntry = NO;//设置密码保护模式输入,默认为NO
            [but setBackgroundImage:[UIImage imageNamed:@"可见"] forState:UIControlStateNormal];
            _Mimature = true;
        }
}
//全部转出
- (void)QuanBuZcButClicked:(UIButton *)but
{
    NSString *string = [NSString stringWithFormat:@"%@",self.MentionMoneyModel.money];
    if (string == nil || string == NULL || [string isEqualToString:@"0"]) {

    }
    else
    {
         //手续费,手续费类型 percent 按百分百比,fixed 固定收取
        if ([self.MentionMoneyModel.withdrawal_fee_type isEqualToString:@"percent"]) {
            
            double num1 = [self.MentionMoneyModel.money doubleValue];
            double num2 = [self.MentionMoneyModel.withdrawal_fee doubleValue];
            double count = num1/(1+num2/100);
            self.ShouXuFeiLa.text = [NSString stringWithFormat:@"%f",count*(num2/100)];
            self.minTFiled.text = [NSString stringWithFormat:@"%f",count];
            self.TxsxfLa.text = [NSString stringWithFormat:@"%@%.2f%@",QSLocalizedString(@"qs_TiXianShouXuFei"),count*(num2/100),self.MentionMoneyModel.symbol];
            
        }
        else
        {
            //fixed 固定收取
            double num1 = [self.MentionMoneyModel.money doubleValue];
            double num2 = [self.MentionMoneyModel.withdrawal_fee doubleValue];
            
            if (num1>num2) {
                double count = num1-num2;
                self.minTFiled.text = [NSString stringWithFormat:@"%f",count];
            }
            
            
        }
        
        self.DaoZshuLLabel.text = self.minTFiled.text;
    }
   
    
}

//提币
- (void)TiBiButClicked:(UIButton *)but
{
    if (self.symbol.length == 0) {
        
        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingXuanZheNiDeBiZhong")];
        return;
    }
    if (self.iid.length == 0) {
        
        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingXuanZheNiDeLianLeiXing")];
        return;
    }
    if (self.withdrawal_addr_id.length == 0) {
        
        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingXuanZheTiBiDiZi")];
        return;
    }
    
    if (self.minTFiled.text.length == 0) {
         [JMNotifyView showNotify:QSLocalizedString(@"qs_QingTianXieTiBiShuLiang")];
        return;
    }
    if (self.PaypasswordTFiled.text.length == 0) {
         [JMNotifyView showNotify:QSLocalizedString(@"qs_QinShuRuZhiFuMiMa")];
        return;
    }
    double num1 = [self.minTFiled.text doubleValue];
    double num2 = [self.MentionMoneyModel.min_withdrawal doubleValue];
    if (num1<num2) {
        
        [JMNotifyView showNotify:[NSString stringWithFormat:@"%@%@",QSLocalizedString(@"qs_ZuiDiTiBiJinEWei"),self.MentionMoneyModel.min_withdrawal]];
        return;
    }
    
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"money"]= self.minTFiled.text;//金额
    parameters[@"coin_id"]= self.coin_id;
    parameters[@"currency_id"]= self.currency_id;
    parameters[@"withdrawal_addr_id"]= self.withdrawal_addr_id;
    parameters[@"password"]= self.PaypasswordTFiled.text;
    
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,accountwithdrawal];
    [GWNetWork postTokenWithURL:URLStr params:parameters success:^(id responseDict) {
        
        NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
        NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
        if ([code isEqualToString:@"200"]) {
            __weak typeof(self) weakself = self;
            //成功
            SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:QSLocalizedString(@"qs_TiBiChengGong") sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
            [special withSureClick:^(NSString *string) {
                [[weakself getCurrentVC].navigationController popToRootViewControllerAnimated:YES];
            }];
            
        }
        else if ([code isEqualToString:@"101111"])
        {
            __weak typeof(self) weakself = self;
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:QSLocalizedString(@"qs_WenXinTiShi") message:QSLocalizedString(@"qs_QianWangSheZhiZhiFuMiMa") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action1 = [UIAlertAction actionWithTitle:QSLocalizedString(@"qs_queding") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
               UIStoryboard *story = [UIStoryboard storyboardWithName:@"MeStoryboard" bundle:nil];
               SetPayMimaVC *vc =  [story instantiateViewControllerWithIdentifier:@"setPayMimaVCID"];
               [[weakself getCurrentVC].navigationController pushViewController:vc animated:YES];
              
              
            }];
            UIAlertAction *action2 = [UIAlertAction actionWithTitle:QSLocalizedString(@"qs_QuXiao") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            [alert addAction:action1];
            [alert addAction:action2];
            [[weakself getCurrentVC] presentViewController:alert animated:YES completion:nil];
            
            
        }
        else
        {
            [JMNotifyView showNotify:DIC[@"msg"]];
        }
    } failure:^(NSError *error) {
        
    }];
    
    
}
//文本改变时调用
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    
    if (textField == self.PaypasswordTFiled)
    {
        NSString *text = [self.PaypasswordTFiled text];
        NSLog(@"%@",text);
        NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
        
        string = [string stringByReplacingOccurrencesOfString:@"" withString:@""];
        if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
            return NO;
        }
        text = [text stringByReplacingCharactersInRange:range withString:string];
        text = [text stringByReplacingOccurrencesOfString:@"" withString:@""];
        if ([text length] >= 7) {
            return NO;
        }
        
        NSString *newString = @"";
        while (text.length > 0)
        {
            NSString *subString = [text substringToIndex:MIN(text.length,4)];
            newString = [newString stringByAppendingString:subString];
            if (subString.length == 4) {
                newString = [newString stringByAppendingString:@""];
            }
            text = [text substringFromIndex:MIN(text.length, 4)];
        }
        newString = [newString stringByTrimmingCharactersInSet:[characterSet invertedSet]];
        [textField setText:newString];
        
        return NO;
    }
    return YES;
}

@end
