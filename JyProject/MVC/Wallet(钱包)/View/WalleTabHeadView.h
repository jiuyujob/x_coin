//
//  WalleTabHeadView.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/6.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseView.h"
#import "WalleModel.h"
#import "FLAnimatedImage.h"

typedef void(^SaoyisaoClick)(void);//扫一扫
typedef void(^ShouQianClick)(void);//收钱
typedef void(^FuQianClick)(void);//付钱

typedef void(^AccountClick)(NSString * _Nullable zczh,NSString * _Nullable tforusdt);//账户

NS_ASSUME_NONNULL_BEGIN

@interface WalleTabHeadView : JYBaseView

@property (nonatomic,retain) UIImageView *DjVIPImage;
@property (nonatomic,retain) UILabel *DJVIPLabel;
@property (nonatomic,retain) UILabel *ZiChangLabel;
@property (nonatomic,retain) UILabel *ZCresultsLabel;
@property (nonatomic,retain) UILabel *Label;

@property (nonatomic, copy) SaoyisaoClick SaoyisaoClicked;
@property (nonatomic, copy) ShouQianClick ShouQianClicked;
@property (nonatomic, copy) FuQianClick FuQianClicked;
@property (nonatomic, copy) AccountClick AccountClicked;


//资产折合
@property (nonatomic,retain) NSString *TFORbalance;
@property (nonatomic,retain) NSString *USDDbalance;
@property (nonatomic,retain) NSString *USDTbalance;

@property (nonatomic,retain) NSString *USDDTFOR;
@property (nonatomic,retain) NSString *USDTTFOR;


-(id)initWithFrame:(CGRect)frame;

- (void)setCellWithInfo:(WalleModel*)Model;

- (void)setTFORCount:(NSString*)Count tforusdt:(NSString *)pice;

@end

NS_ASSUME_NONNULL_END
