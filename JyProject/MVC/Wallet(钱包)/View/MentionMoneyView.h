//
//  MentionMoneyView.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/9.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseView.h"
#import "MentionMoneyModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MentionMoneyView : JYBaseView<UITextFieldDelegate>
{
    bool _Mimature;
}

@property (nonatomic,retain) MentionMoneyModel *MentionMoneyModel;

@property (nonatomic,retain) UIImageView* JianTouIm2;

//币种类型
@property (nonatomic,retain) UIButton *TBBZtypeBut;
//链类型
@property (nonatomic,retain) UIButton *LiantypeBut;


@property (nonatomic,retain) UILabel *NameAddLa;
@property (nonatomic,retain) UILabel *TwoNameAddLa;
@property (nonatomic,retain) UITextField *minTFiled;//最小提币数量
@property (nonatomic,retain) UILabel *TypeLa,*TypeLa2,*TypeLa3;
@property (nonatomic,retain) UILabel *KeYongLabel;//当前可用
@property (nonatomic,retain) UILabel *ShouXuFeiLa;//手续费
@property (nonatomic,retain) UILabel *DaoZshuLLabel;//到账数量
@property (nonatomic,retain) UITextField *PaypasswordTFiled;//支付密码

@property (nonatomic,retain) UILabel *ZuiDiTiXianLa;//最低提现金额
@property (nonatomic,retain) UILabel *TxsxfLa;//提现手续费

//币种id
@property (nonatomic,retain) NSString *currency_id;//币种id
@property (nonatomic,retain) NSString *symbol;//请求链类型的参数

@property (nonatomic,retain) NSString *coin_id;//
@property (nonatomic,retain) NSString *iid;//链类型的ID
@property (nonatomic,retain) NSString *withdrawal_addr_id;//提现地址id

@property (nonatomic,retain) UIButton *PWButton;//密码是否可见


-(id)initWithFrame:(CGRect)frame symbol:(NSString *)symbol currency_id:(NSString *)currency_id;

@end

NS_ASSUME_NONNULL_END
