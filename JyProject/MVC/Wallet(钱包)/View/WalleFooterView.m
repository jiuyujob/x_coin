//
//  WalleFooterView.m
//  JyProject
//
//  Created by 九域-UI on 2020/1/13.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "WalleFooterView.h"
#import "TheWalleTableViewCell.h"
#import "WalletDetailVC.h"

@implementation WalleFooterView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
       if (self) {
           self.backgroundColor = [UIColor clearColor];
           self.ArrayData  = [NSMutableArray array];
           [self initSetUI];

       }
    return self;
}


- (void)initSetUI{
    
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(15,0,SCREEN_WIDTH-30,Scale_X(340));
    view.layer.backgroundColor =[UIColor whiteColor].CGColor;
    view.layer.cornerRadius = 10;
    view.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    view.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    view.layer.shadowOpacity = 0.3;// 阴影透明度，默认0
    view.layer.shadowRadius = 3;// 阴影半径，默认3
    [self addSubview:view];
    
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0,view.width,view.height) style:UITableViewStylePlain];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.scrollEnabled = NO;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (@available(iOS 11.0, *)){
     _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    
    UIView *touVIew = [[UIView alloc] init];
    touVIew.frame = CGRectMake(0,0,view.width,60);
    touVIew.layer.backgroundColor =[UIColor clearColor].CGColor;
    
        UILabel *ShengTaiLabel = [CreateTool createLabelWithFrame:CGRectMake(15,20,200,30) text:QSLocalizedString(@"qs_ZiChang") font:18 textColor:[UIColor blackColor] tag:0];
        ShengTaiLabel.textAlignment = NSTextAlignmentLeft;
        [touVIew addSubview:ShengTaiLabel];
    
    _tableView.tableHeaderView = touVIew;
    [view addSubview:_tableView];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ArrayData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"myCell";
    TheWalleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];//从重用队列里获取可重用的cell
    if (!cell)
    {
        cell = [[TheWalleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (self.ArrayData && indexPath.row < [self.ArrayData count])
    {
        [cell setCellWithInfo:self.ArrayData indexPathRow:indexPath USDDTFORprice:self.USDDTFORprice USDTTFORprice:self.USDTTFORprice];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return Scale_X(98);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *symbol = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"symbol"]];
    NSString *balance = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"balance"]];
    NSString *account_id = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"account_id"]];
    NSString *currency_id = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"currency_id"]];

    WalletDetailVC *vc = [WalletDetailVC new];
    vc.titName = symbol;
    vc.balance =balance;
    vc.account_id =account_id;
    vc.currency_id = currency_id;
    [vc setHidesBottomBarWhenPushed:YES];
    __weak typeof(self) weakself = self;
    [[weakself getCurrentVC].navigationController pushViewController:vc animated:YES];

}

- (void)setCellWithInfo:(NSMutableArray *)arrayData USDDTFORprice:(NSString *)USDDprice USDTTFORprice:(NSString *)USDTprice
{
    
    self.ArrayData = arrayData;
    self.USDDTFORprice = USDDprice;
    self.USDTTFORprice = USDTprice;
    [self.tableView reloadData];
}


@end
