//
//  TheWalleTableViewCell.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/6.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "MainTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface TheWalleTableViewCell : MainTableViewCell

@property (nonatomic,retain) UIImageView *Image;
@property (nonatomic,retain) UILabel *symbolLb;
@property (nonatomic,retain) UILabel *balanceLb;
@property (nonatomic,retain) UILabel *TFORLb;

- (void)setCellWithInfo:(NSMutableArray *)arrayData indexPathRow:(NSIndexPath*)indexPath USDDTFORprice:(NSString *)USDDprice USDTTFORprice:(NSString *)USDTprice;


@end

NS_ASSUME_NONNULL_END
