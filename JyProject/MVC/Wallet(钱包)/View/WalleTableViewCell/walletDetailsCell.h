//
//  walletDetailsCell.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/20.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "MainTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface walletDetailsCell : MainTableViewCell

@property (nonatomic,retain) UILabel *TFORLabel;
@property (nonatomic,retain) UILabel *typeLabel;
@property (nonatomic,retain) UILabel *RiqiLa;
@property (nonatomic,retain) UILabel *jineLabel;



- (void)setCellWithInfo:(NSMutableArray *)arrayData indexPathRow:(NSIndexPath*)indexPath;

@end

NS_ASSUME_NONNULL_END
