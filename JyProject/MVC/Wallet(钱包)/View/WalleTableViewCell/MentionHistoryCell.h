//
//  MentionHistoryCell.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/13.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "MainTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MentionHistoryCell : MainTableViewCell

@property (nonatomic,retain) UILabel *TFORLabel;
@property (nonatomic,retain) UILabel *SHtypeLabel;
@property (nonatomic,retain) UILabel *ZcslLa;
@property (nonatomic,retain) UILabel *ZcddzlLa;
@property (nonatomic,retain) UILabel *SxfLa;
@property (nonatomic,retain) UILabel *RiqiLa;

- (void)setCellWithInfo:(NSMutableArray *)arrayData indexPathRow:(NSIndexPath*)indexPath;

@end

NS_ASSUME_NONNULL_END
