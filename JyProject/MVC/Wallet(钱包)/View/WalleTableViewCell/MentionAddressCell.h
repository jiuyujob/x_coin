//
//  MentionAddressCell.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/11.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "MainTableViewCell.h"

typedef void(^ZhengLiClick)(NSString* _Nullable  tagstr);

NS_ASSUME_NONNULL_BEGIN

@interface MentionAddressCell : MainTableViewCell

@property (nonatomic,retain) UIView *view;
@property (nonatomic,retain) UIImageView *tbIm;

@property (nonatomic,retain) UILabel *TFORLabel;
@property (nonatomic,retain) UILabel *AddLabel;
@property (nonatomic,retain) UILabel *AddText;


@property (nonatomic, copy) ZhengLiClick ZhengLiClicked;

- (void)setCellWithInfo:(NSMutableArray *)arrayData indexPathRow:(NSIndexPath*)indexPath;

@end

NS_ASSUME_NONNULL_END
