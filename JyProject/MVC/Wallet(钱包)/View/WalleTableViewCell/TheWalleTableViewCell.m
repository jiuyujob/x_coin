//
//  TheWalleTableViewCell.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/6.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "TheWalleTableViewCell.h"

@implementation TheWalleTableViewCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews{

    self.Image = [CreateTool createImageViewWithFrame:CGRectMake(20,Scale_X(16),48,48) imageString:@"" tag:0];
    [self.contentView addSubview:self.Image];
    
    self.symbolLb = [CreateTool createLabelWithFrame:CGRectMake(self.Image.right+20,0,90,Scale_X(80)) text:@"" font:19 textColor:[UIColor blackColor] tag:0];
    self.symbolLb.textAlignment= NSTextAlignmentLeft;
    [self.contentView addSubview:self.symbolLb];
    
    self.balanceLb = [CreateTool createLabelWithFrame:CGRectMake(0,8,SCREEN_WIDTH-40,Scale_X(40)) text:@"" font:16 textColor:kHexColor(@"#4F98CA") tag:0];
    self.balanceLb.textAlignment= NSTextAlignmentRight;
    [self.contentView addSubview:self.balanceLb];
    
    self.TFORLb = [CreateTool createLabelWithFrame:CGRectMake(0,self.balanceLb.bottom-10,SCREEN_WIDTH-40,Scale_X(40)) text:@"" font:13 textColor:buttonHuiSeColor tag:0];
    self.TFORLb.textAlignment= NSTextAlignmentRight;
    self.TFORLb.numberOfLines = 1;
    [self.contentView addSubview:self.TFORLb];
    
    UIImageView *line = [CreateTool createImageViewWithFrame:CGRectMake(self.Image.right+20,self.Image.bottom+35,SCREEN_WIDTH-130,0.6) imageString:@"" tag:0];
    line.backgroundColor = kHexColor(@"#D8D8D8");
    [self.contentView addSubview:line];
    
//    UIImageView *JianTouIm = [CreateTool createImageViewWithFrame:CGRectMake(self.width-1,Scale_X(29),10,14) imageString:@"右箭头" tag:0];
//    [self.contentView addSubview:JianTouIm];
    
}

- (void)setCellWithInfo:(NSMutableArray *)arrayData indexPathRow:(NSIndexPath*)indexPath USDDTFORprice:(NSString *)USDDprice USDTTFORprice:(NSString *)USDTprice
{
    NSString *symbol = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"symbol"]];
    NSString *balance = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"balance"]];
    
    if ([symbol isEqualToString:@"TFOR"]) {
        self.Image.image = [UIImage imageNamed:@"钱包页TFOR"];
        self.TFORLb.text = [NSString stringWithFormat:@"=%@ TFOR",balance];
    }
    else if ([symbol isEqualToString:@"USDD"])
    {
        self.Image.image = [UIImage imageNamed:@"钱包页USDD"];
        
        double USDDba = [balance doubleValue];
        double USDDpr = [USDDprice doubleValue];
        double usddCount = USDDba*USDDpr;
        self.TFORLb.text = [NSString stringWithFormat:@"=%f TFOR",usddCount];
    }
    else
    {
        self.Image.image = [UIImage imageNamed:@"钱包页USDT"];
        double USDTba = [balance doubleValue];
        double USDTpr = [USDTprice doubleValue];
        double usdtCount = USDTba*USDTpr;
        self.TFORLb.text = [NSString stringWithFormat:@"=%f TFOR",usdtCount];
    }
    
    self.symbolLb.text = symbol;
    self.balanceLb.text = [NSString stringWithFormat:@"%@ %@",balance,symbol];

}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
