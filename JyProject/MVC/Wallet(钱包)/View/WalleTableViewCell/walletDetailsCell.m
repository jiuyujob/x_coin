//
//  walletDetailsCell.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/20.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "walletDetailsCell.h"

@implementation walletDetailsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews{
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(15,5,SCREEN_WIDTH-30,80);
    view.layer.backgroundColor = [UIColor whiteColor].CGColor;
    view.layer.cornerRadius = 7;
    view.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    view.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    view.layer.shadowOpacity = 0.3;// 阴影透明度，默认0
    view.layer.shadowRadius = 3;// 阴影半径，默认3
    [self.contentView addSubview:view];
    
    self.TFORLabel = [CreateTool createLabelWithFrame:CGRectMake(10,5,180,25) text:@"" font:15 textColor:[UIColor blackColor] tag:0];
    self.TFORLabel.textAlignment = NSTextAlignmentLeft;
    [view addSubview:self.TFORLabel];
    
    
    self.typeLabel = [CreateTool createLabelWithFrame:CGRectMake(view.right-250,5,220,25) text:@"" font:15 textColor:[UIColor blackColor] tag:0];
    self.typeLabel.textAlignment = NSTextAlignmentRight;
    [view addSubview:self.typeLabel];
    
    
    self.RiqiLa = [CreateTool createLabelWithFrame:CGRectMake(10,self.TFORLabel.bottom+15,200,25) text:@"" font:13 textColor:[UIColor blackColor] tag:0];
    self.RiqiLa.textAlignment = NSTextAlignmentLeft;
    [view addSubview:self.RiqiLa];
    
    
    self.jineLabel = [CreateTool createLabelWithFrame:CGRectMake(view.right-210,self.RiqiLa.y,180,25) text:@"" font:14 textColor:[UIColor redColor] tag:0];
    self.jineLabel.textAlignment = NSTextAlignmentRight;
    [view addSubview:self.jineLabel];
    
}
- (void)setCellWithInfo:(NSMutableArray *)arrayData indexPathRow:(NSIndexPath*)indexPath
{
    
    self.TFORLabel.text = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"symbol"]];
    
    NSString *typeText;
    NSString *type = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"type"]];
    if ([type isEqualToString:@"1"]) {
        typeText = QSLocalizedString(@"qs_ChongZhi");//@"充值";
        self.jineLabel.textColor = [UIColor greenColor];
    }
    else if ([type isEqualToString:@"2"])
    {
         typeText = QSLocalizedString(@"qs_TiBi");//@"提币";
        
    }
    else if ([type isEqualToString:@"3"])
    {
         typeText = QSLocalizedString(@"qs_JieDianFenHong");//@"节点分红";
         self.jineLabel.textColor = [UIColor orangeColor];
    }
    else if ([type isEqualToString:@"4"])
    {
         typeText = QSLocalizedString(@"qs_SuanLiXIfang");//@"算力释放";
         self.jineLabel.textColor = [UIColor orangeColor];
    }
    else if ([type isEqualToString:@"5"])
    {
         typeText = QSLocalizedString(@"qs_zhuanru");//@"转入";
        self.jineLabel.textColor = [UIColor greenColor];
    }
    else if ([type isEqualToString:@"6"])
    {
         typeText = QSLocalizedString(@"qs_shengji");//@"升级";
         self.jineLabel.textColor = buttonColor;
    }
    else if ([type isEqualToString:@"7"])
    {
         typeText = QSLocalizedString(@"qs_duihuan");//@"兑换";
    }
    else
    {
         typeText = QSLocalizedString(@"qs_QiTa");//@"其他";
    }
    
    self.typeLabel.text = typeText;
    self.RiqiLa.text = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"updated_at"]];
    
    NSString *income = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"income"]];
    NSString *spend = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"spend"]];
    
    if ([income isEqualToString:@"0"]) {
        self.jineLabel.text = [NSString stringWithFormat:@"-%@",arrayData[indexPath.row][@"spend"]];
        self.jineLabel.textColor = [UIColor redColor];
    }
    else
    {
        self.jineLabel.text = [NSString stringWithFormat:@"+%@",arrayData[indexPath.row][@"income"]];
        self.jineLabel.textColor = [UIColor greenColor];
    }

}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
