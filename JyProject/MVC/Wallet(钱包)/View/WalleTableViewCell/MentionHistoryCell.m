//
//  MentionHistoryCell.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/13.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "MentionHistoryCell.h"

@implementation MentionHistoryCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews{
    
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(15,5,SCREEN_WIDTH-30,155);
    view.layer.backgroundColor = [UIColor whiteColor].CGColor;
    view.layer.cornerRadius = 6;
    view.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    view.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    view.layer.shadowOpacity = 0.3;// 阴影透明度，默认0
    view.layer.shadowRadius = 3;// 阴影半径，默认3
    [self.contentView addSubview:view];
    
    self.TFORLabel = [CreateTool createLabelWithFrame:CGRectMake(10,10,150,25) text:@"" font:16 textColor:[UIColor blackColor] tag:0];
    self.TFORLabel.textAlignment = NSTextAlignmentLeft;
    [view addSubview:self.TFORLabel];
    
    //状态
    self.SHtypeLabel= [CreateTool createLabelWithFrame:CGRectMake(view.right-125,10,100,25) text:@"" font:13 textColor:buttonColor tag:0];
    self.SHtypeLabel.textAlignment = NSTextAlignmentRight;
    [view addSubview:self.SHtypeLabel];
    
    //转出数量
    self.ZcslLa = [CreateTool createLabelWithFrame:CGRectMake(10,self.TFORLabel.bottom,SCREEN_WIDTH-30,25) text:@"" font:13 textColor:[UIColor blackColor] tag:0];
    self.ZcslLa.textAlignment = NSTextAlignmentLeft;
    [view addSubview:self.ZcslLa];
    
    //转出到地址
    self.ZcddzlLa = [CreateTool createLabelWithFrame:CGRectMake(10,self.ZcslLa.bottom,SCREEN_WIDTH-36,25) text:@"" font:13 textColor:[UIColor blackColor] tag:0];
    self.ZcddzlLa.textAlignment = NSTextAlignmentLeft;
    [view addSubview:self.ZcddzlLa];
    
    //手续费
    self.SxfLa =[CreateTool createLabelWithFrame:CGRectMake(10,self.ZcddzlLa.bottom,SCREEN_WIDTH-30,25) text:@"" font:13 textColor:[UIColor blackColor] tag:0];
    self.SxfLa.textAlignment = NSTextAlignmentLeft;
    [view addSubview:self.SxfLa];
    
    //日期
    self.RiqiLa=  [CreateTool createLabelWithFrame:CGRectMake(10,self.SxfLa.bottom,SCREEN_WIDTH-30,25) text:@"" font:13 textColor:[UIColor blackColor] tag:0];
    self.RiqiLa.textAlignment = NSTextAlignmentLeft;
    [view addSubview:self.RiqiLa];
    
}

- (void)setCellWithInfo:(NSMutableArray *)arrayData indexPathRow:(NSIndexPath*)indexPath
{
    self.TFORLabel.text = [NSString stringWithFormat:@"%@%@",arrayData[indexPath.row][@"symbol"],QSLocalizedString(@"qs_ZhuanChu")];
    NSString *typeText;
    NSString *status = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"status"]];
    if ([status isEqualToString:@"0"]) {
        typeText = QSLocalizedString(@"qs_YiTiJiao"); //@"已提交";
        
    }
    else if ([status isEqualToString:@"1"])
    {
         typeText = QSLocalizedString(@"qs_DaiShenHe"); //@"待审核";
    }
    else if ([status isEqualToString:@"2"])
    {
         typeText = QSLocalizedString(@"qs_ShenHeZhong"); //@"审核中";
    }
    else if ([status isEqualToString:@"3"])
    {
         typeText = QSLocalizedString(@"qs_TongGuo"); //@"通过";
    }
    else if ([status isEqualToString:@"4"])
    {
         typeText = QSLocalizedString(@"qs_BuTongGuo"); //@"不通过";
        self.SHtypeLabel.textColor = [UIColor redColor];
    }
    else if ([status isEqualToString:@"5"])
    {
         typeText = QSLocalizedString(@"qs_YiWanCheng"); //@"已完成";
        self.SHtypeLabel.textColor = [UIColor greenColor];
    }
    else if ([status isEqualToString:@"6"])
    {
         typeText = QSLocalizedString(@"qs_QuXiao"); //@"取消";
        self.SHtypeLabel.textColor = [UIColor redColor];
    }
    else if ([status isEqualToString:@"7"])
    {
         typeText = QSLocalizedString(@"qs_TiXianShiBai"); //@"提现失败";
        self.SHtypeLabel.textColor = [UIColor redColor];
    }
    self.SHtypeLabel.text = typeText;
    
    //转出数量
    self.ZcslLa.text = [NSString stringWithFormat:@"%@:%@",QSLocalizedString(@"qs_ZhuangChuShuLiang"),arrayData[indexPath.row][@"value"]];
    
    //转出到地址
    self.ZcddzlLa.text = [NSString stringWithFormat:@"%@:%@",QSLocalizedString(@"qs_ZhuanChuDaoDiZi"),arrayData[indexPath.row][@"address"]];
    
    //手续费
    self.SxfLa.text = [NSString stringWithFormat:@"%@:%@",QSLocalizedString(@"qs_ShouXuFei"),arrayData[indexPath.row][@"poundage"]];
    
    //日期
    self.RiqiLa.text = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"updated_at"]];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
