//
//  walletDetailCell.m
//  JyProject
//
//  Created by 九域-UI on 2020/1/7.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "walletDetailCell.h"

@implementation walletDetailCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews{

    self.typeLabel = [CreateTool createLabelWithFrame:CGRectMake(15,10,250,22) text:@"" font:15 textColor:[UIColor blackColor] tag:0];
    self.typeLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.typeLabel];
    
    
    self.RiqiLa = [CreateTool createLabelWithFrame:CGRectMake(15,self.typeLabel.bottom+10,200,25) text:@"" font:13 textColor:kHexColor(@"#939393") tag:0];
    self.RiqiLa.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:self.RiqiLa];
    
    
    self.jineLabel = [CreateTool createLabelWithFrame:CGRectMake(SCREEN_WIDTH-210,30,195,25) text:@"" font:14 textColor:[UIColor redColor] tag:0];
    self.jineLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.jineLabel];
    
    UIImageView *line = [[UIImageView alloc] initWithFrame:CGRectMake(15,self.RiqiLa.bottom+10,SCREEN_WIDTH-30,0.6)];
    line.backgroundColor = kHexColor(@"#939393");
    [self.contentView addSubview:line];
    
}

- (void)setCellWithInfo:(NSMutableArray *)arrayData indexPathRow:(NSIndexPath*)indexPath
{

    NSString *typeText;
    NSString *type = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"type"]];
    if ([type isEqualToString:@"1"]) {
        typeText = QSLocalizedString(@"qs_ChongZhi");//@"充值";
        self.jineLabel.textColor = [UIColor greenColor];
    }
    else if ([type isEqualToString:@"2"])
    {
         typeText = QSLocalizedString(@"qs_TiBi");//@"提币";
        
    }
    else if ([type isEqualToString:@"3"])
    {
         typeText = QSLocalizedString(@"qs_JieDianFenHong");//@"节点分红";
         self.jineLabel.textColor = [UIColor orangeColor];
    }
    else if ([type isEqualToString:@"4"])
    {
         typeText = QSLocalizedString(@"qs_SuanLiXIfang");//@"算力释放";
         self.jineLabel.textColor = [UIColor orangeColor];
    }
    else if ([type isEqualToString:@"5"])
    {
         typeText = QSLocalizedString(@"qs_zhuanru");//@"转入";
        self.jineLabel.textColor = [UIColor greenColor];
    }
    else if ([type isEqualToString:@"6"])
    {
         typeText = QSLocalizedString(@"qs_shengji");//@"升级";
         self.jineLabel.textColor = buttonColor;
    }
    else if ([type isEqualToString:@"7"])
    {
         typeText = QSLocalizedString(@"qs_duihuan");//@"兑换";
    }
    else
    {
         typeText = QSLocalizedString(@"qs_QiTa");//@"其他";
    }
    
    self.typeLabel.text = typeText;
    self.RiqiLa.text = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"updated_at"]];
    
    NSString *income = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"income"]];
    NSString *spend = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"spend"]];
    
    if ([income isEqualToString:@"0"]) {
        self.jineLabel.text = [NSString stringWithFormat:@"-%@",arrayData[indexPath.row][@"spend"]];
        self.jineLabel.textColor = [UIColor redColor];
    }
    else
    {
        self.jineLabel.text = [NSString stringWithFormat:@"+%@",arrayData[indexPath.row][@"income"]];
        self.jineLabel.textColor = [UIColor greenColor];
    }

}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
