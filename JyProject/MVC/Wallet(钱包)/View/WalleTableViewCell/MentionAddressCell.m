//
//  MentionAddressCell.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/11.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "MentionAddressCell.h"

@implementation MentionAddressCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews{
    
    self.view = [[UIView alloc] init];
    self.view.frame = CGRectMake(15,8,SCREEN_WIDTH-30,115);
    self.view.layer.backgroundColor = [UIColor whiteColor].CGColor;
    self.view.layer.cornerRadius = 7;
    self.view.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    self.view.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    self.view.layer.shadowOpacity = 0.3;// 阴影透明度，默认0
    self.view.layer.shadowRadius = 3;// 阴影半径，默认3
    [self.contentView addSubview:self.view];
    
//    self.tbIm = [CreateTool createImageViewWithFrame:CGRectMake(28,15,20,20) imageString:@"" tag:0];
//    [self.view addSubview:self.tbIm];

    UIButton *zhengliBut = [CreateTool createButtonWithFrame:CGRectMake(self.view.width-60,10,60,30) buttonTitle:QSLocalizedString(@"qs_ZhengLi") titleColor:buttonColor selectTitleColor:nil tag:0 target:self action:@selector(buttonClicked:) size:13];
    [self.view addSubview:zhengliBut];
    
    self.TFORLabel = [CreateTool createLabelWithFrame:CGRectMake(20,12,150, 25) text:@"" font:14 textColor:[UIColor blackColor] tag:0];
    self.TFORLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:self.TFORLabel];
    
    self.AddLabel = [CreateTool createLabelWithFrame:CGRectMake(20,self.TFORLabel.bottom+10,self.view.width-30, 25) text:@"" font:13 textColor:[UIColor blackColor] tag:0];
    self.AddLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:self.AddLabel];
    
    self.AddText = [CreateTool createLabelWithFrame:CGRectMake(20,self.AddLabel.bottom+10,self.view.width-30, 25) text:@"" font:13 textColor:[UIColor blackColor] tag:0];
    self.AddText.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:self.AddText];
    
    

}


- (void)buttonClicked:(UIButton *)but{
   
    self.ZhengLiClicked([NSString stringWithFormat:@"%ld",(long)but.tag]);
}
- (void)setCellWithInfo:(NSMutableArray *)arrayData indexPathRow:(NSIndexPath*)indexPath{
    
    self.TFORLabel.text = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"currency_name"]];
    if ([self.TFORLabel.text isEqualToString:@"TFOR"]) {
        self.tbIm.image = [UIImage imageNamed:@"提币地址TFOR"];
    }
    else if ([self.TFORLabel.text isEqualToString:@"USDD"])
    {
        self.tbIm.image = [UIImage imageNamed:@"提币地址USDD"];
    }
    else
    {
        self.tbIm.image = [UIImage imageNamed:@"充值小图标"];
    }
        
    self.AddLabel.text = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"name"]];
    self.AddText.text = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"address"]];
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];


}

@end
