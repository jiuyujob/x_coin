//
//  ZhangHuTableViewCell.h
//  JyProject
//
//  Created by 九域-UI on 2020/1/4.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "MainTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZhangHuTableViewCell : MainTableViewCell

@property (nonatomic,retain) UIImageView *Image;
@property (nonatomic,retain) UILabel *symbolLb;
@property (nonatomic,retain) UILabel *balanceLb;
@property (nonatomic,retain) UILabel *blocked_balanceLb;
@property (nonatomic,retain) UILabel *TFORLb;

- (void)setCellWithInfo:(NSMutableArray *)arrayData indexPathRow:(NSIndexPath*)indexPath;

@end

NS_ASSUME_NONNULL_END
