//
//  walletDetailCell.h
//  JyProject
//
//  Created by 九域-UI on 2020/1/7.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "MainTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface walletDetailCell : MainTableViewCell

@property (nonatomic,retain) UILabel *TFORLabel;
@property (nonatomic,retain) UILabel *typeLabel;
@property (nonatomic,retain) UILabel *RiqiLa;
@property (nonatomic,retain) UILabel *jineLabel;



- (void)setCellWithInfo:(NSMutableArray *)arrayData indexPathRow:(NSIndexPath*)indexPath;

@end

NS_ASSUME_NONNULL_END
