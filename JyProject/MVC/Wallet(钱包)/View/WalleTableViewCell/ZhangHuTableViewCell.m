//
//  ZhangHuTableViewCell.m
//  JyProject
//
//  Created by 九域-UI on 2020/1/4.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "ZhangHuTableViewCell.h"

@implementation ZhangHuTableViewCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews{
    
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(10,8,SCREEN_WIDTH-20,Scale_X(80));
    view.layer.backgroundColor = [UIColor whiteColor].CGColor;
    view.layer.cornerRadius = 6;
    view.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    view.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    view.layer.shadowOpacity = 0.5;// 阴影透明度，默认0
    view.layer.shadowRadius = 5;// 阴影半径，默认3
    [self.contentView addSubview:view];
    
    self.Image = [CreateTool createImageViewWithFrame:CGRectMake(20,Scale_X(16),48,48) imageString:@"" tag:0];
    [view addSubview:self.Image];
    
    self.symbolLb = [CreateTool createLabelWithFrame:CGRectMake(self.Image.right+20,0,60,Scale_X(80)) text:@"" font:19 textColor:[UIColor blackColor] tag:0];
    self.symbolLb.textAlignment= NSTextAlignmentLeft;
    [view addSubview:self.symbolLb];
    
    self.balanceLb = [CreateTool createLabelWithFrame:CGRectMake(self.symbolLb.right,10,view.width-210,Scale_X(40)) text:@"" font:16 textColor:buttonColor tag:0];
    self.balanceLb.textAlignment= NSTextAlignmentRight;
    [view addSubview:self.balanceLb];
    
    self.blocked_balanceLb = [CreateTool createLabelWithFrame:CGRectMake(self.symbolLb.right,self.balanceLb.bottom-15,view.width-210,Scale_X(40)) text:@"" font:16 textColor:kHexColor(@"#939393") tag:0];
    self.blocked_balanceLb.textAlignment= NSTextAlignmentRight;
    [view addSubview:self.blocked_balanceLb];
    
    UILabel *kyLb = [CreateTool createLabelWithFrame:CGRectMake(SCREEN_WIDTH-120,self.balanceLb.y,70,Scale_X(40)) text:QSLocalizedString(@"qs_KeYong") font:13 textColor:kHexColor(@"#939393") tag:0];
    kyLb.textAlignment= NSTextAlignmentRight;
    [view addSubview:kyLb];
    
    UILabel *djLb = [CreateTool createLabelWithFrame:CGRectMake(SCREEN_WIDTH-120,self.blocked_balanceLb.y,70,Scale_X(40)) text:QSLocalizedString(@"qs_DongJie") font:13 textColor:kHexColor(@"#939393") tag:0];
       djLb.textAlignment= NSTextAlignmentRight;
       [view addSubview:djLb];
    
    
    UIImageView *JianTouIm = [CreateTool createImageViewWithFrame:CGRectMake(view.width-22,Scale_X(29),10,14) imageString:@"右箭头" tag:0];
    [view addSubview:JianTouIm];
    
}

- (void)setCellWithInfo:(NSMutableArray *)arrayData indexPathRow:(NSIndexPath*)indexPath
{
    NSString *symbol = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"symbol"]];
    NSString *balance = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"balance"]];
    NSString *blocked_balance = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"blocked_balance"]];
    
    if ([symbol isEqualToString:@"TFOR"]) {
        self.Image.image = [UIImage imageNamed:@"钱包页TFOR"];
    }
    else if ([symbol isEqualToString:@"USDD"])
    {
        self.Image.image = [UIImage imageNamed:@"钱包页USDD"];

    }
    else
    {
        self.Image.image = [UIImage imageNamed:@"钱包页USDT"];

    
    }
    
    self.symbolLb.text = symbol;
    self.balanceLb.text = [NSString stringWithFormat:@"%@",balance];
    self.blocked_balanceLb.text = [NSString stringWithFormat:@"%@",blocked_balance];

}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
