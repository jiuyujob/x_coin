//
//  TopUpHistoryCell.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/13.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "TopUpHistoryCell.h"

@implementation TopUpHistoryCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews{
    
  UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(15,5,SCREEN_WIDTH-30,130);
    view.layer.backgroundColor = [UIColor whiteColor].CGColor;
    view.layer.cornerRadius = 7;
    view.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    view.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    view.layer.shadowOpacity = 0.3;// 阴影透明度，默认0
    view.layer.shadowRadius = 3;// 阴影半径，默认3
    [self.contentView addSubview:view];
    
    self.TFORLabel = [CreateTool createLabelWithFrame:CGRectMake(10,5,180,25) text:@"" font:15 textColor:[UIColor blackColor] tag:0];
    self.TFORLabel.textAlignment = NSTextAlignmentLeft;
    [view addSubview:self.TFORLabel];
    
    //充值数量
    UILabel *chongZhiNum  = [CreateTool createLabelWithFrame:CGRectMake(10,self.TFORLabel.bottom,80,25) text:QSLocalizedString(@"qs_ChongZhiShuLiang") font:13 textColor:[UIColor blackColor] tag:0];
    chongZhiNum.textAlignment = NSTextAlignmentLeft;
    [view addSubview:chongZhiNum];
    
    self.czslLabel =[CreateTool createLabelWithFrame:CGRectMake(chongZhiNum.right,chongZhiNum.y,180,25) text:@"" font:13 textColor:[UIColor blackColor] tag:0];
    self.czslLabel.textAlignment = NSTextAlignmentLeft;
    [view addSubview:self.czslLabel];
    
    //转入地址
    UILabel *ZhangRuAdd  = [CreateTool createLabelWithFrame:CGRectMake(10,chongZhiNum.bottom,80,50) text:QSLocalizedString(@"qs_ZhuanRuDiZzi") font:13 textColor:[UIColor blackColor] tag:0];
       ZhangRuAdd.textAlignment = NSTextAlignmentLeft;
       [view addSubview:ZhangRuAdd];
    
    self.zrdzLabel =[CreateTool createLabelWithFrame:CGRectMake(ZhangRuAdd.right,ZhangRuAdd.y,view.width-110,50) text:@"" font:13 textColor:[UIColor blackColor] tag:0];
    self.zrdzLabel.textAlignment = NSTextAlignmentLeft;
    [view addSubview:self.zrdzLabel];
    
    //日期
    self.RiqiLa=  [CreateTool createLabelWithFrame:CGRectMake(10,ZhangRuAdd.bottom,190,25) text:@"" font:12 textColor:[UIColor blackColor] tag:0];
    self.RiqiLa.textAlignment = NSTextAlignmentLeft;
    [view addSubview:self.RiqiLa];
    
}

- (void)setCellWithInfo:(NSMutableArray *)arrayData indexPathRow:(NSIndexPath*)indexPath
{
    self.TFORLabel.text = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"symbol"]];
    //充值数量
    self.czslLabel.text = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"value"]];
    //转入地址
    self.zrdzLabel.text = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"address"]];
    //日期
    self.RiqiLa.text = [NSString stringWithFormat:@"%@",arrayData[indexPath.row][@"updated_at"]];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
