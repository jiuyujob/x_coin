//
//  TheWalletVC.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/3.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "TheWalletVC.h"
#import "TopUpViewController.h"
#import "MentionMoneyVC.h"
#import "AccountVC.h"
#import "UpdateVC.h"
#import "TheWalleTableViewCell.h"
#import "ScanVVC.h"
#import "ZhangHuVC.h"
#import "CurrencyTypeVC.h"


@interface TheWalletVC ()
@end

@implementation TheWalletVC

- (void)viewDidLoad {   
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = nil;
    
    self.ArrayData = [NSMutableArray array];
    [self initSetUI];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    [self ThreeNetworkRequest];
}
- (void)initSetUI
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT-kNavgationBarHeight+20) style:UITableViewStyleGrouped];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.bounces = NO;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
           if (@available(iOS 11.0, *)){
               _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
           }
    
        __block TheWalletVC *blockSelf = self;
        WalleTabHeadView *headvw = [[WalleTabHeadView alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,Scale_X(320))];
        headvw.SaoyisaoClicked = ^{
            
            ScanVVC *vc = [ScanVVC new];
            [vc setHidesBottomBarWhenPushed:YES];
            [blockSelf.navigationController pushViewController:vc animated:YES];
            
        };
        headvw.ShouQianClicked = ^{
            
            CurrencyTypeVC *vc = [CurrencyTypeVC new];
            vc.Type = @"币种";
            vc.PageType = @"收钱";
            [vc setHidesBottomBarWhenPushed:YES];
            [blockSelf.navigationController pushViewController:vc animated:YES];

        };
        headvw.FuQianClicked = ^{
            
            SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:QSLocalizedString(@"qs_gongnengweikaifang") sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
                                     [special withSureClick:^(NSString *string) {
                                         
                                     }];
            
            
//            CurrencyTypeVC *vc = [CurrencyTypeVC new];
//            vc.Type = @"币种";
//            vc.PageType = @"付钱";
//            [vc setHidesBottomBarWhenPushed:YES];
//            [blockSelf.navigationController pushViewController:vc animated:YES];

          
        };
    
    headvw.AccountClicked = ^(NSString * _Nullable zczh, NSString * _Nullable tforusdt) {
        ZhangHuVC *vc  = [ZhangHuVC new];
        vc.ArrayData = self.ArrayData;
        vc.MoneyStr = zczh;
        vc.tforStr = tforusdt;
        [vc setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:vc animated:YES];
    };
    _tableView.tableHeaderView = headvw;
    self.WalleTabHeadView = headvw;
    
    WalleFooterView *footvw = [[WalleFooterView alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,Scale_X(360))];
    _tableView.tableFooterView = footvw;
    self.WalleFooterView = footvw;
    [self.view addSubview:_tableView];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *cellIdentifier = @"myCell";
//    TheWalleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];//从重用队列里获取可重用的cell
//    if (!cell)
//    {
//        cell = [[TheWalleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
//                                       reuseIdentifier:cellIdentifier];
//        cell.backgroundColor = [UIColor clearColor];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    }
//    if (self.ArrayData && indexPath.row < [self.ArrayData count])
//    {
//        [cell setCellWithInfo:self.ArrayData indexPathRow:indexPath USDDTFORprice:self.USDDTFORprice USDTTFORprice:self.USDTTFORprice];
//    }
//    return cell;
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return Scale_X(98);
//}



- (void)ThreeNetworkRequest{

    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"quote_currency"]= @"tfor";
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] stringForKey:@"token"]);
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,currencyquote];
    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {

         NSLog(@"-获取用户-responseDict--%@", responseDict);
         NSDictionary *DIC = responseDict;
         NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
        if ([code isEqualToString:@"200"]) {
                        //成功
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                               
                                  NSArray *data = DIC[@"data"];
                             
                               dispatch_async(dispatch_get_main_queue(), ^{
                                  for (int i= 0; i<data.count; i++) {
                                      
                                      NSString *price = [NSString stringWithFormat:@"%@",data[i][@"price"]];
                                      switch (i) {
                                          case 0:
                                          {
                                              self.USDDTFORprice = price;
                                          }
                                              break;
                                              case 1:
                                              {
                                                  self.USDTTFORprice = price;
                                              }
                                                  break;
                                          default:
                                              break;
                                      }
                                      
                                  }
                                   [self NetworkRequest];
                                   
                               });
                           });
        
        }
        else if ([code isEqualToString:@"401"])
        {
            NotifyRelogin;
        }
        else
        {
            [JMNotifyView showNotify:DIC[@"msg"]];
        }

    } failure:^(NSError *error) {

    }];

}


#pragma mark - 请求接口
- (void)NetworkRequest{

   NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
          parameters[@"token"]= [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
       NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,accountinfoURL];
       [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
           NSLog(@"-获取用户-responseDict--%@", responseDict);
           NSDictionary *DIC = responseDict;
           NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
           if ([code isEqualToString:@"200"]) {
                 //成功
               dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                      [self.ArrayData removeAllObjects];
                      NSArray *data = DIC[@"data"];
                      [self.ArrayData addObjectsFromArray:data];
                   dispatch_async(dispatch_get_main_queue(), ^{
                       
                       for (int i= 0; i<data.count; i++) {
                           
                           NSString *text = [NSString stringWithFormat:@"%@",data[i][@"balance"]];;
                           switch (i) {
                               case 0:
                               {
                                   self.TFORbalance = text;
                               }
                                   break;
                                   case 1:
                                   {
                                       self.USDDbalance = text;
                                   }
                                       break;
                                   case 2:
                                   {
                                       self.USDTbalance= text;
                                   }
                                       break;
                               default:
                                   break;
                           }
                           
                       }
                       //资产折合计算
                       double USDDba = [self.USDDbalance doubleValue];
                       double USDDpr = [self.USDDTFORprice doubleValue];
                       double usddCount = USDDba*USDDpr;
                       
                       double USDTba = [self.USDTbalance doubleValue];
                       double USDTpr = [self.USDTTFORprice doubleValue];
                       double usdtCount = USDTba*USDTpr;
                       
                       double ZongCount = [self.TFORbalance doubleValue]+usddCount+usdtCount;
                       [self.WalleTabHeadView setTFORCount:[NSString stringWithFormat:@"%f",ZongCount] tforusdt:self.USDTTFORprice];
                       
                       [self.WalleFooterView setCellWithInfo:self.ArrayData USDDTFORprice:self.USDDTFORprice USDTTFORprice:self.USDTTFORprice];
                       
//                       [self.tableView reloadData];
                   });
               });
                      
           }
           else if ([code isEqualToString:@"401"])
           {
               NotifyRelogin;
           }
           else
           {
               [JMNotifyView showNotify:DIC[@"msg"]];
           }
       } failure:^(NSError *error) {
           
       }];
}
@end
