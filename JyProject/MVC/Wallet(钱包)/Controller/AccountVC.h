//
//  AccountVC.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/10.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface AccountVC : JYBaseVC


@property (nonatomic,retain) UILabel *ZczhLabel;//资产折合
@property (nonatomic,retain) UILabel *ZczLabel;//资产值
@property (nonatomic,retain) UILabel *ZczhanghuLabel;//资产账户
@property (nonatomic,retain) UILabel *TFORLabel;//资产账户
@property (nonatomic,retain) UILabel *USDTLabel;//资产账户
@property (nonatomic,retain) UILabel *USDDLabel;//资产账户

@property (nonatomic,retain) UILabel *ShenTaiLabel;//生态账户
@property (nonatomic,retain) UILabel *GangGanLabel;//生态账户
@property (nonatomic,retain) UILabel *SuanLiLabel;//生态账户
@property (nonatomic,retain) UILabel *DongJieLabel;//生态账户

@property (nonatomic,copy) NSDictionary *DIC;
@property (nonatomic,copy) NSString *zczhString;

@end

NS_ASSUME_NONNULL_END
