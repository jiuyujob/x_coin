//
//  WalletDetailVC.h
//  JyProject
//
//  Created by 九域-UI on 2020/1/4.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface WalletDetailVC : JYBaseVC<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,retain) NSMutableArray *ArrayData;
@property (nonatomic,assign)NSInteger page;


@property (nonatomic,retain)UILabel* MoneyLb;
@property (nonatomic,retain)UILabel* kyLb;
@property (nonatomic,retain)UILabel* djLb;

@property (nonatomic,copy) NSString *titName;
@property (nonatomic,copy) NSString *balance;
@property (nonatomic,copy) NSString *account_id;
@property (nonatomic,copy) NSString *currency_id;
@property (nonatomic,retain) NSString *change_id;//兑换币ID

@end

NS_ASSUME_NONNULL_END
