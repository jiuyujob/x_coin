//
//  ZhangHuVC.h
//  JyProject
//
//  Created by 九域-UI on 2020/1/4.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZhangHuVC : JYBaseVC<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,copy) NSArray *ArrayData;

@property (nonatomic,copy) NSString *MoneyStr;
@property (nonatomic,copy) NSString *tforStr;

@end

NS_ASSUME_NONNULL_END
