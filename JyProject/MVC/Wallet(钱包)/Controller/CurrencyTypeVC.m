//
//  CurrencyTypeVC.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/9.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "CurrencyTypeVC.h"

@interface CurrencyTypeVC ()

@end

@implementation CurrencyTypeVC
-(UIImageView *)backViewImage
{
    if (!_backViewImage) {
        self.backViewImage = [CreateTool createImageViewWithFrame:CGRectMake(0,0,SCREEN_WIDTH,SCREEN_HEIGHT-kNavgationBarHeight) imageString:@"View背景图" tag:0];
    }
    return _backViewImage;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //title=@"币种类型"
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = QSLocalizedString(@"qs_BiZhongLeiXing");
    if ([self.Type isEqualToString:@"等级"]) {
        self.title = @"账户";
    }
    self.view.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.backViewImage];
    
    [self SetinitUI];
}

- (void)SetinitUI{
    
    if ([self.Type isEqualToString:@"币种"]) {
        UIButton *Onebut = [CreateTool createButtonWithFrame:CGRectZero buttonTitle:@"TFOR" titleColor:[UIColor whiteColor] selectTitleColor:nil tag:900 target:self action:@selector(ButtonClicked:) size:14];
        Onebut.layer.backgroundColor = buttonHuiSeColor.CGColor;
        Onebut.layer.cornerRadius = 7;
        [self.view addSubview:Onebut];
        [Onebut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view).offset(Scale_X(28));
            make.left.mas_equalTo(self.view).offset(20);
            make.right.mas_equalTo(self.view).offset(-20);
            make.height.mas_equalTo(Scale_X(30));
        }];

        UIButton *Twobut = [CreateTool createButtonWithFrame:CGRectZero buttonTitle:@"USDT" titleColor:[UIColor whiteColor] selectTitleColor:nil tag:901 target:self action:@selector(ButtonClicked:) size:13.5];
        Twobut.layer.backgroundColor = buttonHuiSeColor.CGColor;
        Twobut.layer.cornerRadius = 6;
        [self.view addSubview:Twobut];
        [Twobut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(Onebut).offset(Scale_X(48));
            make.left.mas_equalTo(self.view).offset(20);
            make.right.mas_equalTo(self.view).offset(-20);
            make.height.mas_equalTo(Scale_X(30));
        }];
        
        UIButton *Threebut = [CreateTool createButtonWithFrame:CGRectZero buttonTitle:@"USDD" titleColor:[UIColor whiteColor] selectTitleColor:nil tag:902 target:self action:@selector(ButtonClicked:) size:13.5];
        Threebut.layer.backgroundColor = buttonHuiSeColor.CGColor;
        Threebut.layer.cornerRadius = 6;
        [self.view addSubview:Threebut];
        [Threebut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(Twobut).offset(Scale_X(48));
            make.left.mas_equalTo(self.view).offset(20);
            make.right.mas_equalTo(self.view).offset(-20);
            make.height.mas_equalTo(Scale_X(30));
        }];
    }
    else if ([self.Type isEqualToString:@"等级"])
    {
        UIButton *Onebut = [CreateTool createButtonWithFrame:CGRectZero buttonTitle:@"粉丝100～500 USDD" titleColor:[UIColor whiteColor] selectTitleColor:nil tag:900 target:self action:@selector(ButtonClicked:) size:14];
        Onebut.layer.backgroundColor = buttonHuiSeColor.CGColor;
        Onebut.layer.cornerRadius = 7;
        [self.view addSubview:Onebut];
        [Onebut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view).offset(Scale_X(28));
            make.left.mas_equalTo(self.view).offset(20);
            make.right.mas_equalTo(self.view).offset(-20);
            make.height.mas_equalTo(Scale_X(30));
        }];

        UIButton *Twobut = [CreateTool createButtonWithFrame:CGRectZero buttonTitle:@"铁杆501~3000 USDD" titleColor:[UIColor whiteColor] selectTitleColor:nil tag:901 target:self action:@selector(ButtonClicked:) size:13.5];
        Twobut.layer.backgroundColor = buttonHuiSeColor.CGColor;
        Twobut.layer.cornerRadius = 6;
        [self.view addSubview:Twobut];
        [Twobut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(Onebut).offset(Scale_X(48));
            make.left.mas_equalTo(self.view).offset(20);
            make.right.mas_equalTo(self.view).offset(-20);
            make.height.mas_equalTo(Scale_X(30));
        }];
        
        UIButton *Threebut = [CreateTool createButtonWithFrame:CGRectZero buttonTitle:@"代言501~3000 USDD" titleColor:[UIColor whiteColor] selectTitleColor:nil tag:902 target:self action:@selector(ButtonClicked:) size:13.5];
        Threebut.layer.backgroundColor = buttonHuiSeColor.CGColor;
        Threebut.layer.cornerRadius = 6;
        [self.view addSubview:Threebut];
        [Threebut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(Twobut).offset(Scale_X(48));
            make.left.mas_equalTo(self.view).offset(20);
            make.right.mas_equalTo(self.view).offset(-20);
            make.height.mas_equalTo(Scale_X(30));
        }];
        
        UIButton *fourbut = [CreateTool createButtonWithFrame:CGRectZero buttonTitle:@"网红501~3000 USDD" titleColor:[UIColor whiteColor] selectTitleColor:nil tag:903 target:self action:@selector(ButtonClicked:) size:13.5];
        fourbut.layer.backgroundColor = buttonHuiSeColor.CGColor;
        fourbut.layer.cornerRadius = 6;
        [self.view addSubview:fourbut];
        [fourbut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(Threebut).offset(Scale_X(48));
            make.left.mas_equalTo(self.view).offset(20);
            make.right.mas_equalTo(self.view).offset(-20);
            make.height.mas_equalTo(Scale_X(30));
        }];
        
        UIButton *fivebut = [CreateTool createButtonWithFrame:CGRectZero buttonTitle:@"明星501~3000 USDD" titleColor:[UIColor whiteColor] selectTitleColor:nil tag:904 target:self action:@selector(ButtonClicked:) size:13.5];
        fivebut.layer.backgroundColor = buttonHuiSeColor.CGColor;
        fivebut.layer.cornerRadius = 6;
        [self.view addSubview:fivebut];
        [fivebut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(fourbut).offset(Scale_X(48));
            make.left.mas_equalTo(self.view).offset(20);
            make.right.mas_equalTo(self.view).offset(-20);
            make.height.mas_equalTo(Scale_X(30));
        }];
    }
    else
    {
        UIButton *Onebut = [CreateTool createButtonWithFrame:CGRectZero buttonTitle:@"OMIM" titleColor:[UIColor whiteColor] selectTitleColor:nil tag:900 target:self action:@selector(ButtonClicked:) size:14];
        Onebut.layer.backgroundColor = buttonHuiSeColor.CGColor;
        Onebut.layer.cornerRadius = 7;
        [self.view addSubview:Onebut];
        [Onebut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view).offset(Scale_X(28));
            make.left.mas_equalTo(self.view).offset(20);
            make.right.mas_equalTo(self.view).offset(-20);
            make.height.mas_equalTo(Scale_X(30));
        }];

        UIButton *Twobut = [CreateTool createButtonWithFrame:CGRectZero buttonTitle:@"ERC20" titleColor:[UIColor whiteColor] selectTitleColor:nil tag:901 target:self action:@selector(ButtonClicked:) size:13.5];
        Twobut.layer.backgroundColor = buttonHuiSeColor.CGColor;
        Twobut.layer.cornerRadius = 6;
        [self.view addSubview:Twobut];
        [Twobut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(Onebut).offset(Scale_X(48));
            make.left.mas_equalTo(self.view).offset(20);
            make.right.mas_equalTo(self.view).offset(-20);
            make.height.mas_equalTo(Scale_X(30));
        }];
        
    }
    
    
    
}

- (void)ButtonClicked:(UIButton *)but{
    self.buttitCliCK(but.currentTitle);
    [self.navigationController popViewControllerAnimated:YES];
}

@end
