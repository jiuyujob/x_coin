//
//  TransferVC.m
//  JyProject
//
//  Created by 九域-UI on 2020/1/4.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "TransferVC.h"
#import "SYPasswordView.h"

@interface TransferVC ()

@property (nonatomic, strong) SYPasswordView *pasView;

@end

@implementation TransferVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //转账
    self.navigationItem.title = QSLocalizedString(@"qs_ZhuanZhang");
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self initSetUI];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (void)initSetUI{
    
    //币种
    UIView *OneView = [[UIView alloc] initWithFrame:CGRectMake(20,20,SCREEN_WIDTH-40,160)];
    OneView.layer.backgroundColor = [UIColor whiteColor].CGColor;
    OneView.layer.cornerRadius = 10;
    OneView.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    OneView.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    OneView.layer.shadowOpacity = 0.5;// 阴影透明度，默认0
    OneView.layer.shadowRadius = 3;// 阴影半径，默认3
    [self.view addSubview:OneView];
    
    UILabel *bizLa = [CreateTool createLabelWithFrame:CGRectMake(10,10,100,50) text:QSLocalizedString(@"qs_BiZhong") font:13 textColor:[UIColor blackColor] tag:0];
    bizLa.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:bizLa];

    UILabel *typeLb =[CreateTool createLabelWithFrame:CGRectMake(bizLa.right+10,bizLa.y,100,50) text:self.symbol font:14 textColor:[UIColor blackColor] tag:0];
    typeLb.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:typeLb];
    
    UIImageView *line = [CreateTool createImageViewWithFrame:CGRectMake(20,bizLa.bottom,OneView.width-40,0.6) imageString:@"" tag:0];
    line.backgroundColor = kHexColor(@"#D8D8D8");
    [OneView addSubview:line];

//对方账号
    UILabel *zhLa = [CreateTool createLabelWithFrame:CGRectMake(15,line.bottom,100,50) text:QSLocalizedString(@"qs_DuiFangYouXinag") font:13 textColor:[UIColor blackColor] tag:0];
    zhLa.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:zhLa];

    self.ZhTFiled = [CreateTool createTextFieldWithFrame:CGRectMake(zhLa.right+10,zhLa.y,SCREEN_WIDTH-130,50) placeholder:QSLocalizedString(@"qs_YouXiangDiZi") delegate:self tag:0];
    [OneView addSubview:self.ZhTFiled];
    
    UIImageView *line2 = [CreateTool createImageViewWithFrame:CGRectMake(20,zhLa.bottom,OneView.width-40,0.6) imageString:@"" tag:0];
     line2.backgroundColor = kHexColor(@"#D8D8D8");
     [OneView addSubview:line2];

////转账金额
    UILabel *zzjeLa = [CreateTool createLabelWithFrame:CGRectMake(15,line2.bottom,100,50) text:QSLocalizedString(@"qs_ZhuanZhangjinE") font:13 textColor:[UIColor blackColor] tag:0];
    zzjeLa.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:zzjeLa];

    self.MoneyTFiled = [CreateTool createTextFieldWithFrame:CGRectMake(zzjeLa.right+10,zzjeLa.y,SCREEN_WIDTH-130,Scale_X(38)) placeholder:@"0.00" delegate:self tag:100];
    self.MoneyTFiled.keyboardType = UIKeyboardTypeDecimalPad;
    [OneView addSubview:self.MoneyTFiled];

    UILabel *srzfmima = [CreateTool createLabelWithFrame:CGRectMake(15,OneView.bottom+30,OneView.width,25) text:QSLocalizedString(@"qs_QinShuRuZhiFuMiMa") font:14 textColor:[UIColor blackColor] tag:0];
       srzfmima.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:srzfmima];

       self.pasView = [[SYPasswordView alloc] initWithFrame:CGRectMake(20, srzfmima.bottom+10, SCREEN_WIDTH - 40, 50)];
       [self.view addSubview:_pasView];

    UIButton *zzBut = [CreateTool createButtonImageTitleWithFrame:CGRectMake(50,self.pasView.bottom+30,SCREEN_WIDTH-100, Scale_X(55)) imageString:@"range_length" buttonTitle:QSLocalizedString(@"qs_ZhuanZhang") titleColor:[UIColor whiteColor] tag:1000 target:self action:@selector(ButClicked:) size:13];
    [self.view addSubview:zzBut];
    
}

//禁用第三方输入键盘
- (BOOL)application:(UIApplication *)application shouldAllowExtensionPointIdentifier:(NSString *)extensionPointIdentifier
{
    return NO;
}

- (void)ButClicked:(UIButton *)but{
    
    if (self.ZhTFiled.text.length == 0) {
        
        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingTianXieYouXianHao")];
        return;
    }
    if (self.MoneyTFiled.text.length == 0) {
        
        [JMNotifyView showNotify:QSLocalizedString(@"qs_ShuRuJinE")];
        return;
    }
    
    if (self.pasView.textField.text.length == 0) {
           
           [JMNotifyView showNotify:QSLocalizedString(@"qs_QinShuRuZhiFuMiMa")];
           return;
    }
    if (self.pasView.textField.text.length < 6) {
           [JMNotifyView showNotify:QSLocalizedString(@"qs_zhifumimachangdubugou")];
           return;
       }
    
     NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
          parameters[@"money"]= self.MoneyTFiled.text;
          parameters[@"symbol"]= self.symbol;
          parameters[@"email"]= self.ZhTFiled.text;
    parameters[@"pay_password"]= self.pasView.textField.text;
    
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,accountpersontransfer];
    [GWNetWork postTokenWithURL:URLStr params:parameters success:^(id responseDict) {
           
           NSLog(@"-获取用户-responseDict--%@", responseDict);
           NSDictionary *DIC = responseDict;
           NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
           if ([code isEqualToString:@"200"]) {
               
               SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:DIC[@"msg"] sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
               [special withSureClick:^(NSString *string) {
                   [self.navigationController popToRootViewControllerAnimated:YES];
               }];
                         
           }
           else if ([code isEqualToString:@"401"])
           {
               NotifyRelogin;
           }
           else
           {
               [JMNotifyView showNotify:DIC[@"msg"]];
           }
           
       } failure:^(NSError *error) {
           
       }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.self.MoneyTFiled) {
        BOOL isHaveDian;
        //判断是否有小数点
        if ([textField.text containsString:@"."]) {
            isHaveDian = YES;
        }else{
            isHaveDian = NO;
        }

        if (string.length > 0) {

            //当前输入的字符
            unichar single = [string characterAtIndex:0];
            NSLog(@"single = %c",single);

            //不能输入.0~9以外的字符
            if (!((single >= '0' && single <= '9') || single == '.')){
                NSLog(@"您输入的格式不正确");
                return NO;
            }
           
            //只能有一个小数点
            if (isHaveDian && single == '.') {
                NSLog(@"只能输入一个小数点");
                return NO;
            }

            //如果第一位是.则前面加上0
            if ((textField.text.length == 0) && (single == '.')) {
                textField.text = @"0";
            }

            //如果第一位是0则后面必须输入.
            if ([textField.text hasPrefix:@"0"]) {
                if (textField.text.length > 1) {
                    NSString *secondStr = [textField.text substringWithRange:NSMakeRange(1, 1)];
                    if (![secondStr isEqualToString:@"."]) {
                        NSLog(@"第二个字符必须是小数点");
                        return NO;
                    }
                }else{
                    if (![string isEqualToString:@"."]) {
                        NSLog(@"第二个字符必须是小数点");
                        return NO;
                    }
                }
            }

            //小数点后最多能输入两位
            if (isHaveDian) {
                NSRange ran = [textField.text rangeOfString:@"."];
                //由于range.location是NSUInteger类型的，所以不能通过(range.location - ran.location) > 2来判断
                if (range.location > ran.location) {
                    if ([textField.text pathExtension].length > 1) {
                        NSLog(@"小数点后最多有两位小数");
                        return NO;
                    }
                }
            }
            
        }
    }
    
    return YES;
}


@end
