//
//  WalletDetailVC.m
//  JyProject
//
//  Created by 九域-UI on 2020/1/4.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "WalletDetailVC.h"
#import "walletDetailCell.h"
#import "DDToDTVC.h"
#import "TopUpViewController.h"
#import "MentionMoneyVC.h"
#import "TransferVC.h"

@interface WalletDetailVC ()

@end

@implementation WalletDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    self.ArrayData = [NSMutableArray array];
    self.page = 1;
    [self initSetUI];
    [self NetworkRequest];
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}
- (void)initSetUI{
    
    
    UIImageView *BackImage = [CreateTool createImageViewWithFrame:CGRectMake(0,0,SCREEN_WIDTH,230) imageString:@"账户页底图" tag:0];
    [self.view addSubview:BackImage];
    
    UIButton *fanHuiBut = [CreateTool createButtonWithFrame:CGRectMake(15,kNavgationBarHeight-28,15,24) imageString:@"PinLeft" tag:0 target:self action:@selector(fanHuiButClick:)];
    [BackImage addSubview:fanHuiBut];
    
    UILabel *titLb = [CreateTool createLabelWithFrame:CGRectMake(SCREEN_WIDTH/2-60,fanHuiBut.y,120,20) text:self.titName font:16 textColor:[UIColor whiteColor] tag:0];
    titLb.font = [UIFont boldSystemFontOfSize:20];
    [BackImage addSubview:titLb];
    
    self.MoneyLb = [CreateTool createLabelWithFrame:CGRectMake(0,fanHuiBut.bottom+25,BackImage.width,35) text:self.balance font:24 textColor:[UIColor whiteColor] tag:0];
    [BackImage addSubview:self.MoneyLb];

//可用
    self.kyLb =[CreateTool createLabelWithFrame:CGRectMake(0,self.MoneyLb.bottom+10,BackImage.width/2-10,25) text:[NSString stringWithFormat:@"%@:%@",QSLocalizedString(@"qs_KeYong"),self.balance] font:14 textColor:[UIColor whiteColor] tag:0];
    self.kyLb.textAlignment = NSTextAlignmentRight;
    [BackImage addSubview:self.kyLb];

//冻结

    self.djLb =[CreateTool createLabelWithFrame:CGRectMake(BackImage.width/2+10,self.MoneyLb.bottom+10,BackImage.width/2,25) text:@"" font:14 textColor:[UIColor whiteColor] tag:0];
    self.djLb.textAlignment = NSTextAlignmentLeft;
    [BackImage addSubview:self.djLb];


//tabView
    UIButton *qbBut = [CreateTool createButtonWithFrame:CGRectMake(25,BackImage.bottom,90,40) buttonTitle:@"全部" titleColor:[UIColor blackColor] selectTitleColor:nil tag:666 target:self action:@selector(ButClicked:) size:16];
    [self.view addSubview:qbBut];

    UIView *TwoView = [[UIView alloc] initWithFrame:CGRectMake(0,qbBut.bottom+10,SCREEN_WIDTH,SCREEN_HEIGHT-280-kNavgationBarHeight)];
    TwoView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:TwoView];

    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, TwoView.width,TwoView.height) style:UITableViewStylePlain];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (@available(iOS 11.0, *)){
        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    _tableView.tableFooterView = [UIView new];
    [TwoView addSubview:_tableView];

    __weak typeof(self) weakSelf = self;
         RefreshHeaderView * header =  [RefreshHeaderView headerWithRefreshingBlock:^{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    self.page = 1;
                    [self.ArrayData removeAllObjects];
                    [self NetworkRequest];
                    [weakSelf.tableView.fz_header endRefresh];
                    [weakSelf.tableView.fz_footer ResetNoMoreData];

                });
            } AnimationType:(1)];
         self.tableView.fz_header = header;


         RefreshFooterView * footer = [RefreshFooterView FooterWithRefreshingBlock:^{
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                 self.page++;
                 [self NetworkRequest];
                 [weakSelf.tableView.fz_footer endRefreshingWithNoMoreData];

             });
         } AnimationType:(1)];
         self.tableView.fz_footer = footer;

//底部 兑换、转账
    UIButton *dhBut = [CreateTool createButtonWithFrame:CGRectMake(28,SCREEN_HEIGHT-kNavgationBarHeight+10,28,28) imageString:@"兑换" tag:667 target:self action:@selector(ButClicked:)];
    [self.view addSubview:dhBut];

    UIButton *dhLaBut = [CreateTool createButtonWithFrame:CGRectMake(30,dhBut.bottom,60,20) buttonTitle:QSLocalizedString(@"qs_duihuan") titleColor:buttonColor selectTitleColor:nil tag:667 target:self action:@selector(ButClicked:) size:12];
    dhLaBut.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.view addSubview:dhLaBut];


    UIButton *zzBut = [CreateTool createButtonWithFrame:CGRectMake(dhLaBut.right+10,dhBut.y,28,28) imageString:@"转账" tag:668 target:self action:@selector(ButClicked:)];
    [self.view addSubview:zzBut];

    UIButton *zzLaBut = [CreateTool createButtonWithFrame:CGRectMake(zzBut.x+2,zzBut.bottom,60,20) buttonTitle:QSLocalizedString(@"qs_ZhuanZhang") titleColor:buttonColor selectTitleColor:nil tag:668 target:self action:@selector(ButClicked:) size:12];
    zzLaBut.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.view addSubview:zzLaBut];
    
    //转出
    UIButton *zcBut = [CreateTool createButtonImageTitleWithFrame:  CGRectMake(SCREEN_WIDTH/2-40,dhBut.y,SCREEN_WIDTH/4,38)imageString:@"白色边框按钮" buttonTitle:QSLocalizedString(@"qs_ZhuanChu") titleColor:buttonColor tag:670 target:self action:@selector(ButClicked:) size:14];
       [self.view addSubview:zcBut];

    //转入
    UIButton *zrBut = [CreateTool createButtonImageTitleWithFrame:CGRectMake(zcBut.right+20,dhBut.y,SCREEN_WIDTH/4,38) imageString:@"按钮背景" buttonTitle:QSLocalizedString(@"qs_XianQinZhuanRu") titleColor:[UIColor whiteColor] tag:669 target:self action:@selector(ButClicked:) size:14];
    [self.view addSubview:zrBut];


    
}

#pragma mark - 请求接口
- (void)NetworkRequest{
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"account_id"]= self.account_id;
    parameters[@"type"]= @"all";//类型（all全部，income收入，expend支出）
    parameters[@"pageSize"]= @"10";
    parameters[@"page"]= [NSString stringWithFormat:@"%ld",(long)self.page];
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,accountcurrencydetailissetPay];
    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
         NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
                  NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                  if ([code isEqualToString:@"200"]) {
                      //成功
                      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

                          NSDictionary *data = DIC[@"data"];
                          NSDictionary *info = data[@"info"];
                          self.change_id = [NSString stringWithFormat:@"%@",info[@"change_id"]];
                          NSArray *items = data[@"items"];
                          [self.ArrayData addObjectsFromArray:items];
                          //主线程执行
                          dispatch_async(dispatch_get_main_queue(), ^{
                              
                              self.MoneyLb.text = [NSString stringWithFormat:@"%f",[info[@"balance"] doubleValue]+[info[@"blocked_balance"] doubleValue]];
                              self.kyLb.text = [NSString stringWithFormat:@"%@:%@",QSLocalizedString(@"qs_KeYong"),info[@"balance"]];
                              self.djLb.text = [NSString stringWithFormat:@"%@:%@",QSLocalizedString(@"qs_DongJie"),info[@"blocked_balance"]];
                              
                              [self.tableView reloadData];
                          });
                      });
                      
                  }
                  else
                  {
                      [JMNotifyView showNotify:DIC[@"msg"]];
                  }

    } failure:^(NSError *error) {
        
    }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ArrayData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"myCell";
    walletDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];//从重用队列里获取可重用的cell
    if (!cell)
    {
        cell = [[walletDetailCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (self.ArrayData && indexPath.row < [self.ArrayData count])
    {
        [cell setCellWithInfo:self.ArrayData indexPathRow:indexPath];
    }
   
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}
- (void)ButClicked:(UIButton *)but
{
    switch (but.tag) {
        case 667:
        {
            if ([self.titName isEqualToString:@"USDD"]) {
                
                SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:QSLocalizedString(@"qs_BuZhiChiZiBiDuiHuan") sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
                [special withSureClick:^(NSString *string) {
                   
                }];
                return;
            }
            
            //兑换
            DDToDTVC  * vc = [[DDToDTVC alloc]init];
            vc.symbol = self.titName;
            vc.currency_id = self.currency_id;
            vc.balance = self.balance;
            vc.change_id = self.change_id;
            [self.navigationController pushViewController:vc animated:YES];
            
        }
         break;
        case 668:
        {
            //转账
            TransferVC* vc = [[TransferVC alloc]init];
            vc.symbol = self.titName;
            [self.navigationController pushViewController:vc animated:YES];
            
        }
            break;
        case 669:
        {
            //充值
            TopUpViewController *vc = [TopUpViewController new];
            vc.symbol = self.titName;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 670:
        {
            //提币
            MentionMoneyVC *vc = [MentionMoneyVC new];
            vc.symbol = self.titName;
            vc.currency_id = self.currency_id;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        default:
            break;
    }
    
    
}

- (void)fanHuiButClick:(UIButton *)but{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
