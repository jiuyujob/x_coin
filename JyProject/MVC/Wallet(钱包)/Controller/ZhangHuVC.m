//
//  ZhangHuVC.m
//  JyProject
//
//  Created by 九域-UI on 2020/1/4.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "ZhangHuVC.h"
#import "ZhangHuTableViewCell.h"
#import "WalletDetailVC.h"
#import "walletDetailsVC.h"

@interface ZhangHuVC ()

@end

@implementation ZhangHuVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [self initSetUI];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}
- (void)initSetUI{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT-kNavgationBarHeight) style:UITableViewStyleGrouped];
       _tableView.backgroundColor = [UIColor clearColor];
       _tableView.delegate = self;
       _tableView.dataSource = self;
       _tableView.bounces = NO;
       _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
              if (@available(iOS 11.0, *)){
                  _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
              }
    
    
    UIView *BackVIew = [[UIView alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,Scale_X(280))];
    BackVIew.backgroundColor = [UIColor whiteColor];
    
    UIImageView *BackImage = [CreateTool createImageViewWithFrame:CGRectMake(0,0,SCREEN_WIDTH,Scale_X(260)) imageString:@"账户页底图" tag:0];
    [BackVIew addSubview:BackImage];
    
    UIButton *fanHuiBut = [CreateTool createButtonWithFrame:CGRectMake(15,kNavgationBarHeight-28,15,24) imageString:@"PinLeft" tag:0 target:self action:@selector(fanHuiButClick:)];
    [BackImage addSubview:fanHuiBut];
    
    UILabel *titLb = [CreateTool createLabelWithFrame:CGRectMake(SCREEN_WIDTH/2-60,fanHuiBut.y,120,20) text:QSLocalizedString(@"qs_ZhangHu") font:16 textColor:[UIColor whiteColor] tag:0];
    titLb.font = [UIFont boldSystemFontOfSize:20];
    [BackImage addSubview:titLb];
    
    UIButton *youBUt = [CreateTool createButtonWithFrame:CGRectMake(SCREEN_WIDTH-130, fanHuiBut.y,120,20) buttonTitle:QSLocalizedString(@"qs_QianBaoMingXi") titleColor:[UIColor whiteColor] selectTitleColor:nil tag:0 target:self action:@selector(rightItemClicked) size:13];
    youBUt.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [BackImage addSubview:youBUt];
    

    UILabel* ZiChangLabel = [CreateTool createLabelWithFrame:CGRectMake(25,fanHuiBut.bottom+25,200,25) text:QSLocalizedString(@"qs_ZiChangZheHe") font:15 textColor:[UIColor whiteColor] tag:0];
    ZiChangLabel.textAlignment = NSTextAlignmentLeft;
    [BackImage addSubview:ZiChangLabel];

    UILabel* MoneyLabel = [CreateTool createLabelWithFrame:CGRectMake(25,ZiChangLabel.bottom+15,250,30) text:self.MoneyStr font:23 textColor:[UIColor whiteColor] tag:0];
    MoneyLabel.textAlignment = NSTextAlignmentLeft;
    [BackImage addSubview:MoneyLabel];

    UILabel* tforLabel = [CreateTool createLabelWithFrame:CGRectMake(25,MoneyLabel.bottom+15,250,30) text:self.tforStr font:13 textColor:[UIColor whiteColor] tag:0];
    tforLabel.textAlignment = NSTextAlignmentLeft;
    [BackImage addSubview:tforLabel];

    UILabel* zhbzLb = [CreateTool createLabelWithFrame:CGRectMake(25,BackVIew.bottom-20,250,20) text:QSLocalizedString(@"qs_ZhangHuBizhong") font:15 textColor:[UIColor blackColor] tag:0];
    zhbzLb.textAlignment = NSTextAlignmentLeft;
    [BackVIew addSubview:zhbzLb];
    
    _tableView.tableHeaderView = BackVIew;
    [self.view addSubview:_tableView];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ArrayData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"myCell";
    ZhangHuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];//从重用队列里获取可重用的cell
    if (!cell)
    {
        cell = [[ZhangHuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (self.ArrayData && indexPath.row < [self.ArrayData count])
    {
        [cell setCellWithInfo:self.ArrayData indexPathRow:indexPath];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return Scale_X(98);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   NSString *symbol = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"symbol"]];
     NSString *balance = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"balance"]];
     NSString *account_id = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"account_id"]];
     NSString *currency_id = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"currency_id"]];
    
     WalletDetailVC *vc = [WalletDetailVC new];
     vc.titName = symbol;
     vc.balance =balance;
     vc.account_id =account_id;
     vc.currency_id = currency_id;
     [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)rightItemClicked
{
    walletDetailsVC *vc= [[walletDetailsVC alloc]init];
    vc.pageType = @"钱包明细";
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)fanHuiButClick:(UIButton *)but{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
