//
//  TopUpViewController.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/9.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"
#import "TopUpModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TopUpViewController : JYBaseVC
@property (nonatomic,retain) UILabel *DiZiLabel;
@property (strong, nonatomic) UIImageView *codeImage;
@property (strong, nonatomic) UIImageView *jiantou;

@property (strong,nonatomic) TopUpModel *TopUpModel;
@property (nonatomic,retain) UIView *HiddenView;

//链类型
@property (nonatomic,retain) UIButton *LiantypeBut;

//币种id
@property (nonatomic,retain) NSString *currency_id;
@property (nonatomic,copy) NSString *symbol;//请求链类型的参数

//链类型id
@property (nonatomic,retain) NSString *Liantypeid;

//地址
@property (nonatomic,retain) NSString *address;

@end

NS_ASSUME_NONNULL_END
