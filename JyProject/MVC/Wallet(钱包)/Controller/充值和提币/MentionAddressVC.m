//
//  MentionAddressVC.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/11.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "MentionAddressVC.h"
#import "MentionAddressCell.h"
#import "NewAddressVC.h"
#import "finishingAddressVC.h"

@interface MentionAddressVC ()

@end

@implementation MentionAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //title=@"提币地址"
    self.navigationItem.title = QSLocalizedString(@"qs_TiBiDiZi");
    self.view.backgroundColor = [UIColor whiteColor];
    self.ArrayData = [NSMutableArray array];
    [self setinitUI];

}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.ArrayData removeAllObjects];
    [self NetworkRequest];

}
- (void)setinitUI{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT-kNavgationBarHeight) style:UITableViewStyleGrouped];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
           if (@available(iOS 11.0, *)){
               _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
           }
    
    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,80)];
    footView.backgroundColor = [UIColor clearColor];
    
    UIButton *NewAddbut = [CreateTool createButtonImageTitleWithFrame:CGRectMake(50,30,SCREEN_WIDTH-100,Scale_X(55)) imageString:@"range_length" buttonTitle:[NSString stringWithFormat:@"+%@",QSLocalizedString(@"qs_XinDiZi")] titleColor:[UIColor whiteColor] tag:703 target:self action:@selector(ButtonClicked:) size:13];
    [footView addSubview:NewAddbut];
    _tableView.tableFooterView = footView;
    
    [self.view addSubview:_tableView];
}

#pragma mark - 请求接口
- (void)NetworkRequest{
    
   NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
   parameters[@"block_chain_id"]= self.block_chain_id;//链id
   parameters[@"currency_id"]= self.currency_id;//币种id
        NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,withdrawaladdrlist];
        [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {

           NSLog(@"-获取用户-responseDict--%@", responseDict);
            NSDictionary *DIC = responseDict;
            NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
            if ([code isEqualToString:@"200"]) {
                //成功
                NSArray *bids  = DIC[@"data"];
                [self.ArrayData addObjectsFromArray:bids];
                [self.tableView reloadData];
            }
            else
            {
                [JMNotifyView showNotify:DIC[@"msg"]];
            }
        } failure:^(NSError *error) {

        }];

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ArrayData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"myCell";
    MentionAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];//从重用队列里获取可重用的cell
    if (!cell)
    {
        cell = [[MentionAddressCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    __weak typeof(self) weakself = self;
    cell.ZhengLiClicked = ^(NSString * _Nullable tagstr) {
    
        finishingAddressVC *vc = [finishingAddressVC new];
        vc.address = [NSString stringWithFormat:@"%@",weakself.ArrayData[indexPath.row][@"address"]];
        vc.name = [NSString stringWithFormat:@"%@",weakself.ArrayData[indexPath.row][@"name"]];
        vc.withdrawal_addr_id = [NSString stringWithFormat:@"%@",weakself.ArrayData[indexPath.row][@"withdrawal_addr_id"]];
        vc.coin_id = self.block_chain_id;
        [weakself.navigationController pushViewController:vc animated:YES];
    };

    if (self.ArrayData && indexPath.row < [self.ArrayData count])
    {
        [cell setCellWithInfo:self.ArrayData indexPathRow:indexPath];
    }
   
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *name = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"name"]];
    NSString *address = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"address"]];
    NSString *withdrawal_addr_id = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"withdrawal_addr_id"]];
    self.valueOfClicked(name,address,withdrawal_addr_id);
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (void)ButtonClicked:(UIButton *)but
{
    NewAddressVC *vc = [NewAddressVC new];
    vc.symbol = self.symbol;
    vc.currency_id = self.currency_id;
    vc.Liantype = self.lianName;
    vc.iid = self.block_chain_id;
    [self.navigationController pushViewController:vc animated:YES];
}
@end
