//
//  finishingAddressVC.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/25.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "finishingAddressVC.h"
#import "CurrencyTypeVC.h"
#import "MentionAddressVC.h"

@interface finishingAddressVC ()

//删除按钮
@property (nonatomic, strong) UIBarButtonItem *deleteItem;
//完成按钮
@property (nonatomic, strong) UIBarButtonItem *doneItem;

@end

@implementation finishingAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
      //title=@"整理地址"
      self.navigationItem.title = QSLocalizedString(@"qs_ZhengLiDiZi");
      self.view.backgroundColor = [UIColor whiteColor];
      

          
    _deleteItem = [[UIBarButtonItem alloc] initWithTitle:QSLocalizedString(@"qs_ShanChu") style:UIBarButtonItemStylePlain target:self action:@selector(deleteCLicked)];
          _deleteItem.tintColor = [UIColor blackColor];
          
         
        _doneItem = [[UIBarButtonItem alloc] initWithTitle:QSLocalizedString(@"qs_WanCheng") style:UIBarButtonItemStylePlain target:self action:@selector(doneCLicked)];
        _doneItem.tintColor = [UIColor blackColor];
          
        self.navigationItem.rightBarButtonItems = @[_doneItem,_deleteItem];
      
      
      
      [self setinitUI];
}


- (void)setinitUI{
    
    
//    //币种
//    UIView *OneView = [[UIView alloc] initWithFrame:CGRectMake(20,20,SCREEN_WIDTH-40,Scale_X(35))];
//    OneView.layer.backgroundColor = buttonHuiSeColor.CGColor;
//    OneView.layer.cornerRadius = 7;
//    [self.view addSubview:OneView];
//
//    UILabel *bizLa = [CreateTool createLabelWithFrame:CGRectMake(8,0,100,Scale_X(35)) text:QSLocalizedString(@"qs_BiZhong") font:13 textColor:[UIColor whiteColor] tag:0];
//    bizLa.textAlignment = NSTextAlignmentLeft;
//    [OneView addSubview:bizLa];
//
//    self.BZtypeBut = [CreateTool createButtonWithFrame:CGRectMake(bizLa.right+10,0,OneView.width-40,Scale_X(35)) buttonTitle:QSLocalizedString(@"qs_QingXuanZheNiDeBiZhong") titleColor:[UIColor whiteColor] selectTitleColor:nil tag:890 target:self action:@selector(ButtonCLicked:) size:13];
//    self.BZtypeBut.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//    [OneView addSubview:self.BZtypeBut];
//
//    UIImageView *JianTouIm1 = [CreateTool createImageViewWithFrame:CGRectMake(SCREEN_WIDTH-62,Scale_X(10),10,13) imageString:@"右箭头" tag:0];
//    [OneView addSubview:JianTouIm1];
//
//    //链类型
//    UIView *TwoView = [[UIView alloc] initWithFrame:CGRectMake(20,OneView.bottom+20,SCREEN_WIDTH-40,Scale_X(35))];
//    TwoView.layer.backgroundColor = buttonHuiSeColor.CGColor;
//    TwoView.layer.cornerRadius = 7;
//    [self.view addSubview:TwoView];
//
//    UILabel *llxLa = [CreateTool createLabelWithFrame:CGRectMake(8,0,100,Scale_X(35)) text:QSLocalizedString(@"qs_LianLeiXing") font:13 textColor:[UIColor whiteColor] tag:0];
//    llxLa.textAlignment = NSTextAlignmentLeft;
//    [TwoView addSubview:llxLa];
//
//    self.LiantypeBut = [CreateTool createButtonWithFrame:CGRectMake(llxLa.right+10,0,TwoView.width-40,Scale_X(35)) buttonTitle:QSLocalizedString(@"qs_QingXuanZheNiDeLianLeiXing") titleColor:[UIColor whiteColor] selectTitleColor:nil tag:891 target:self action:@selector(ButtonCLicked:) size:13];
//       self.LiantypeBut.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//    [TwoView addSubview:self.LiantypeBut];
//
//    UIImageView *JianTouIm2 = [CreateTool createImageViewWithFrame:CGRectMake(SCREEN_WIDTH-62,Scale_X(10),10,13) imageString:@"右箭头" tag:0];
//    [TwoView addSubview:JianTouIm2];
    
    //地址名称
    UIView *ThreeView = [[UIView alloc] initWithFrame:CGRectMake(20,20,SCREEN_WIDTH-40,Scale_X(35))];
    ThreeView.backgroundColor = [UIColor whiteColor];
    ThreeView.layer.cornerRadius = 8;
    ThreeView.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    ThreeView.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    ThreeView.layer.shadowOpacity = 0.3;// 阴影透明度，默认0
    ThreeView.layer.shadowRadius = 3;// 阴影半径，默认3
    [self.view addSubview:ThreeView];

    UILabel *Addname = [CreateTool createLabelWithFrame:CGRectMake(8,0,100,Scale_X(35)) text:QSLocalizedString(@"qs_DiZiMingCheng") font:13 textColor:[UIColor blackColor] tag:0];
    Addname.textAlignment= NSTextAlignmentLeft;
    [ThreeView addSubview:Addname];

    self.DIZiTFiled = [CreateTool createTextFieldWithFrame:CGRectMake(Addname.right+10,0,SCREEN_WIDTH-130,Scale_X(35)) placeholder:QSLocalizedString(@"qs_QinShuRuDiZiMingCheng") delegate:self tag:666];
    self.DIZiTFiled.text = self.name;
    [ThreeView addSubview:self.DIZiTFiled];


    UIView *FourView = [[UIView alloc] initWithFrame:CGRectMake(20,ThreeView.bottom+20,SCREEN_WIDTH-40,Scale_X(35))];
    FourView.backgroundColor = [UIColor whiteColor];
    FourView.layer.cornerRadius = 8;
    FourView.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    FourView.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    FourView.layer.shadowOpacity = 0.3;// 阴影透明度，默认0
    FourView.layer.shadowRadius = 3;// 阴影半径，默认3
    [self.view addSubview:FourView];

    UILabel *Add = [CreateTool createLabelWithFrame:CGRectMake(8,0,100,Scale_X(35)) text:QSLocalizedString(@"qs_DiZi") font:13 textColor:[UIColor blackColor] tag:0];
    Add.textAlignment= NSTextAlignmentLeft;
    [FourView addSubview:Add];

    self.AddTFiled =[CreateTool createTextFieldWithFrame:CGRectMake(Add.right+10,0,SCREEN_WIDTH-160,Scale_X(35)) placeholder:QSLocalizedString(@"qs_QingShuRuNiDeDiZi") delegate:self tag:666];
     self.AddTFiled.text = self.address;
    [FourView addSubview:self.AddTFiled];
    
}

- (void)deleteCLicked
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:QSLocalizedString(@"qs_WenXinTiShi") message:QSLocalizedString(@"qs_QueRenShanChuDiZi") preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:QSLocalizedString(@"qs_queding") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
        NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
           parameters[@"withdrawal_addr_id"]= self.withdrawal_addr_id;//地址id
           NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,withdrawaladdrremove];
           [GWNetWork postTokenWithURL:URLStr params:parameters success:^(id responseDict) {
               
               NSLog(@"-获取用户-responseDict--%@", responseDict);
               NSDictionary *DIC = responseDict;
                 NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                  if ([code isEqualToString:@"200"]) {
                       //成功
                       SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:QSLocalizedString(@"qs_DiZiShanChuChengGong") sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
                       [special withSureClick:^(NSString *string) {
                           [self.navigationController popViewControllerAnimated:YES];
                     }];
                  }
                  else if ([code isEqualToString:@"401"])
                  {
                       NotifyRelogin;
                  }
                  else
                  {
                       [JMNotifyView showNotify:DIC[@"msg"]];
                  }
               
               
           } failure:^(NSError *error) {
               
           }];
        
      
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:QSLocalizedString(@"qs_QuXiao") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:action1];
    [alert addAction:action2];
    [self presentViewController:alert animated:YES completion:nil];

}

//完成
- (void)doneCLicked
{
    
//    if (self.symbol.length == 0) {
//
//        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingXuanZheNiDeBiZhong")];
//        return;
//    }
//    if (self.coin_id.length == 0) {
//
//        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingXuanZheNiDeLianLeiXing")];
//        return;
//    }
    if (self.DIZiTFiled.text.length == 0) {
                           
        [JMNotifyView showNotify:QSLocalizedString(@"qs_QinShuRuDiZiMingCheng")];
        return;
    }
    if (self.AddTFiled.text.length == 0) {
                           
        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingShuRuNiDeDiZi")];
        return;
    }
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
       parameters[@"block_chain_id"]= self.coin_id;//链id
//       parameters[@"currency_id"]= self.currency_id;//币种id
       parameters[@"address"]= self.AddTFiled.text;//地址
       parameters[@"name"]= self.DIZiTFiled.text;//名称
       parameters[@"withdrawal_addr_id"]= self.withdrawal_addr_id;//地址id
       
       NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,withdrawaladdrupdate];
       [GWNetWork postTokenWithURL:URLStr params:parameters success:^(id responseDict) {
           
           NSLog(@"-获取用户-responseDict--%@", responseDict);
           NSDictionary *DIC = responseDict;
             NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
              if ([code isEqualToString:@"200"]) {
                   //成功
                   SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:QSLocalizedString(@"qs_DiZiGengxinChengGong") sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
                   [special withSureClick:^(NSString *string) {
                       [self.navigationController popViewControllerAnimated:YES];
                 }];
              }
              else if ([code isEqualToString:@"401"])
              {
                   NotifyRelogin;
              }
              else
              {
                   [JMNotifyView showNotify:DIC[@"msg"]];
              }
           
           
       } failure:^(NSError *error) {
           
       }];
    
}

- (void)ButtonCLicked:(UIButton *)but
{
    __weak typeof(self) weakself = self;
       switch (but.tag) {
           case 890:
           {
               CurrencyTypeVC *vc = [CurrencyTypeVC new];
               vc.Type = @"币种";
               vc.buttitCliCK = ^(NSString * _Nullable buttit, NSString * _Nullable currency_id, NSString * _Nullable coin_id, NSString * _Nullable iid) {
                   
                   [weakself.BZtypeBut setTitle:buttit forState:UIControlStateNormal];
                   weakself.symbol = buttit;
                   weakself.currency_id = currency_id;
               };
               [self.navigationController pushViewController:vc animated:YES];
           }
               break;
               case 891:
                      {
                          
                          if (self.symbol.length == 0) {
                                                    
                                 [JMNotifyView showNotify:QSLocalizedString(@"qs_QingXuanZheNiDeBiZhong")];
                                 return;
                             }
                          
                          CurrencyTypeVC *vc = [CurrencyTypeVC new];
                          vc.Type = @"链类型";
                          vc.PageType = @"充值页面";//充值页面取id
                          vc.parameter = self.symbol;
                          vc.buttitCliCK = ^(NSString * _Nullable buttit, NSString * _Nullable currency_id, NSString * _Nullable coin_id, NSString * _Nullable iid) {
                              
                              [weakself.LiantypeBut setTitle:buttit forState:UIControlStateNormal];
                              weakself.coin_id = iid;//取id
                              
                          };
                       [self.navigationController pushViewController:vc animated:YES];
                      }
                          break;
           default:
               break;
       }
}
@end
