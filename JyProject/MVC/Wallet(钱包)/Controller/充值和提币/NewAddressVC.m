//
//  NewAddressVC.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/11.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "NewAddressVC.h"
#import "CurrencyTypeVC.h"
#import "MentionAddressVC.h"

@interface NewAddressVC ()


@end

@implementation NewAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //title=@"新增地址"
    self.navigationItem.title = QSLocalizedString(@"qs_XinZengDiZi");
    self.view.backgroundColor = [UIColor whiteColor];
    

    UIButton * rightButton = [CreateTool createButtonWithFrame:CGRectMake(0, 0,60, 30) buttonTitle:QSLocalizedString(@"qs_WanCheng") titleColor:[UIColor blackColor] selectTitleColor:nil tag:0 target:self action:@selector(doneCLicked) size:15];
                  
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    [self.navigationItem setRightBarButtonItem:rightItem animated:YES];
    
    
    [self setinitUI];
}

- (void)setinitUI{
    
    
    //币种
    UIView *OneView = [[UIView alloc] initWithFrame:CGRectMake(20,20,SCREEN_WIDTH-40,Scale_X(35))];
    OneView.layer.backgroundColor = [UIColor whiteColor].CGColor;
    OneView.layer.cornerRadius = 7;
    OneView.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    OneView.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    OneView.layer.shadowOpacity = 0.3;// 阴影透明度，默认0
    OneView.layer.shadowRadius = 3;// 阴影半径，默认3
    [self.view addSubview:OneView];
    
    UILabel *bizLa = [CreateTool createLabelWithFrame:CGRectMake(8,0,100,Scale_X(35)) text:QSLocalizedString(@"qs_BiZhong") font:13 textColor:[UIColor blackColor] tag:0];
    bizLa.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:bizLa];
    
    self.BZtypeBut = [CreateTool createButtonWithFrame:CGRectMake(bizLa.right+10,0,OneView.width-40,Scale_X(35)) buttonTitle:self.symbol titleColor:[UIColor blackColor] selectTitleColor:nil tag:890 target:self action:@selector(ButtonCLicked:) size:13];
    self.BZtypeBut.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [OneView addSubview:self.BZtypeBut];
    
    
    //链类型
    UIView *TwoView = [[UIView alloc] initWithFrame:CGRectMake(20,OneView.bottom+20,SCREEN_WIDTH-40,Scale_X(35))];
    TwoView.layer.backgroundColor = [UIColor whiteColor].CGColor;
    TwoView.layer.cornerRadius = 7;
    TwoView.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    TwoView.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    TwoView.layer.shadowOpacity = 0.3;// 阴影透明度，默认0
    TwoView.layer.shadowRadius = 3;// 阴影半径，默认3
    [self.view addSubview:TwoView];
    
    UILabel *llxLa = [CreateTool createLabelWithFrame:CGRectMake(8,0,100,Scale_X(35)) text:QSLocalizedString(@"qs_LianLeiXing") font:13 textColor:[UIColor blackColor] tag:0];
    llxLa.textAlignment = NSTextAlignmentLeft;
    [TwoView addSubview:llxLa];

    self.LiantypeBut = [CreateTool createButtonWithFrame:CGRectMake(llxLa.right+10,0,TwoView.width-40,Scale_X(35)) buttonTitle:self.Liantype titleColor:[UIColor blackColor] selectTitleColor:nil tag:891 target:self action:@selector(ButtonCLicked:) size:13];
       self.LiantypeBut.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [TwoView addSubview:self.LiantypeBut];
    
    
    //地址名称
    UIView *ThreeView = [[UIView alloc] initWithFrame:CGRectMake(20,TwoView.bottom+20,SCREEN_WIDTH-40,Scale_X(35))];
    ThreeView.backgroundColor = [UIColor whiteColor];
    ThreeView.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    ThreeView.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    ThreeView.layer.shadowOpacity = 0.3;// 阴影透明度，默认ThreeView
    ThreeView.layer.shadowRadius = 3;// 阴影半径，默认3
    ThreeView.layer.cornerRadius = 8;
    
    [self.view addSubview:ThreeView];

    UILabel *Addname = [CreateTool createLabelWithFrame:CGRectMake(8,0,100,Scale_X(35)) text:QSLocalizedString(@"qs_DiZiMingCheng") font:13 textColor:[UIColor blackColor] tag:0];
    Addname.textAlignment= NSTextAlignmentLeft;
    [ThreeView addSubview:Addname];

    self.DIZiTFiled = [CreateTool createTextFieldWithFrame:CGRectMake(Addname.right+10,0,SCREEN_WIDTH-130,Scale_X(35)) placeholder:QSLocalizedString(@"qs_QinShuRuDiZiMingCheng") delegate:self tag:666];
    [ThreeView addSubview:self.DIZiTFiled];


    UIView *FourView = [[UIView alloc] initWithFrame:CGRectMake(20,ThreeView.bottom+20,SCREEN_WIDTH-40,Scale_X(35))];
    FourView.backgroundColor = [UIColor whiteColor];
    FourView.layer.cornerRadius = 8;
    FourView.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    FourView.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    FourView.layer.shadowOpacity = 0.3;// 阴影透明度，默认ThreeView
    FourView.layer.shadowRadius = 3;// 阴影半径，默认3
    [self.view addSubview:FourView];

    UILabel *Add = [CreateTool createLabelWithFrame:CGRectMake(8,0,100,Scale_X(35)) text:QSLocalizedString(@"qs_DiZi") font:13 textColor:[UIColor blackColor] tag:0];
    Add.textAlignment= NSTextAlignmentLeft;
    [FourView addSubview:Add];

    self.AddTFiled =[CreateTool createTextFieldWithFrame:CGRectMake(Add.right+10,0,SCREEN_WIDTH-160,Scale_X(35)) placeholder:QSLocalizedString(@"qs_QingShuRuNiDeDiZi") delegate:self tag:666];
    [FourView addSubview:self.AddTFiled];
    
}

- (void)deleteCLicked
{
    
}

//完成
- (void)doneCLicked
{
    
    if (self.DIZiTFiled.text.length == 0) {
                           
        [JMNotifyView showNotify:QSLocalizedString(@"qs_QinShuRuDiZiMingCheng")];
        return;
    }
    if (self.AddTFiled.text.length == 0) {
                           
        [JMNotifyView showNotify:QSLocalizedString(@"qs_QingShuRuNiDeDiZi")];
        return;
    }
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"block_chain_id"]= self.iid;//链id
    parameters[@"currency_id"]= self.currency_id;//币种id
    parameters[@"address"]= self.AddTFiled.text;//地址
    parameters[@"name"]= self.DIZiTFiled.text;//名称
    
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,withdrawaladdradd];
    [GWNetWork postTokenWithURL:URLStr params:parameters success:^(id responseDict) {
        
        NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
          NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
           if ([code isEqualToString:@"200"]) {
                //成功
                SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:QSLocalizedString(@"qs_DIZiXinZengChengGong") sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
                [special withSureClick:^(NSString *string) {
                    [self.navigationController popViewControllerAnimated:YES];
              }];
           }
           else if ([code isEqualToString:@"401"])
           {
                NotifyRelogin;
           }
           else
           {
                [JMNotifyView showNotify:DIC[@"msg"]];
           }
        
        
    } failure:^(NSError *error) {
        
    }];
    
}

- (void)ButtonCLicked:(UIButton *)but
{

}
@end
