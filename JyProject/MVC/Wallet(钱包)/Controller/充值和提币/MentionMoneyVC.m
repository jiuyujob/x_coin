//
//  MentionMoneyVC.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/9.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "MentionMoneyVC.h"
#import "MentionMoneyView.h"
#import "MentionHistoryVC.h"

@interface MentionMoneyVC ()

@end

@implementation MentionMoneyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //title=@"提币"
    self.navigationItem.title = QSLocalizedString(@"qs_ZhuanChu");
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton * rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0,80, 30);
    [rightButton setTitle:QSLocalizedString(@"qs_TiBiLiShi") forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    rightButton.titleLabel.font = [UIFont systemFontOfSize:13];
    [rightButton addTarget:self action:@selector(rightItemClicked:) forControlEvents:UIControlEventTouchUpInside];
              
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    [self.navigationItem setRightBarButtonItem:rightItem animated:YES];
    
    [self SetinitUI];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (void)SetinitUI{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT-kNavgationBarHeight) style:UITableViewStyleGrouped];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
           if (@available(iOS 11.0, *)){
               _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
           }
    
     MentionMoneyView *headvw = [[MentionMoneyView alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,700) symbol:self.symbol currency_id:self.currency_id];
    _tableView.tableHeaderView = headvw;
    [self.view addSubview:_tableView];
}

- (void)rightItemClicked:(UIBarButtonItem*)item
{
    MentionHistoryVC *vc = [MentionHistoryVC new];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
