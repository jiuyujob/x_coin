//
//  TopUpViewController.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/9.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "TopUpViewController.h"
#import "CIImage+Extension.h"
#import "CurrencyTypeVC.h"
#import "TopUpHistoryVC.h"
#import "AFHTTPSessionManager.h"

@interface TopUpViewController ()

@end

@implementation TopUpViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    //title=@"充值"
    self.navigationItem.title = QSLocalizedString(@"qs_XianQinZhuanRu");
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton * rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0,80, 30);
    [rightButton setTitle:QSLocalizedString(@"qs_ChongZhiLiShi") forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    rightButton.titleLabel.font = [UIFont systemFontOfSize:13];
    [rightButton addTarget:self action:@selector(rightItemClicked:) forControlEvents:UIControlEventTouchUpInside];
              
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    [self.navigationItem setRightBarButtonItem:rightItem animated:YES];
    
    [self SetinitUI];
    [self OneNetworkRequest];

}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

//首先请求链类型
- (void)OneNetworkRequest{
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
        //链类型
        parameters[@"symbol"]= self.symbol;
//       NSString * URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,chainsymbolURL];
        NSString * URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,@"wallet/coin/symbol"];
       [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
           
           NSLog(@"-获取用户-responseDict--%@", responseDict);
                  NSDictionary *DIC = responseDict;
                  NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                  if ([code isEqualToString:@"200"]) {
                      //成功
                       NSArray *bids  = DIC[@"data"];
                      if (bids.count>0) {
                          
                          self.address = [NSString stringWithFormat:@"%@",bids[0][@"address"]];
                          self.DiZiLabel.text = self.address;
                          [self.LiantypeBut setTitle:bids[0][@"type"] forState:UIControlStateNormal];
                          if(bids.count ==1)
                          {
                              self.jiantou.hidden = YES;
                              self.LiantypeBut.userInteractionEnabled = NO;
                          }
                          
                          self.codeImage.image = nil;
                         //二维码
                          CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
                                         [filter setDefaults];
                                         NSString *info = self.address;
                                         NSData *infoData = [info dataUsingEncoding:NSUTF8StringEncoding];
                                         [filter setValue:infoData forKeyPath:@"inputMessage"];
                                         CIImage *outImage = [filter outputImage];
                                         self.codeImage.image = [outImage createNonInterpolatedWithSize:130];
                          
                          
                      }
                     
                      
                  }
                  else
                  {
                      [JMNotifyView showNotify:DIC[@"msg"]];
                  }
       } failure:^(NSError *error) {
           
       }];
    
    
}
- (void)SetinitUI
{
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(10,30,SCREEN_WIDTH-20,450)];
    backView.backgroundColor = buttonColor;
    backView.layer.cornerRadius = 12;
    [self.view addSubview:backView];
    
    
    UIView *whitelView = [[UIView alloc] initWithFrame:CGRectMake(10,10,backView.width-20,400)];
     whitelView.backgroundColor = [UIColor whiteColor];
     whitelView.layer.cornerRadius = 12;
    [backView addSubview:whitelView];
    
    UILabel *tforLb = [CreateTool createLabelWithFrame:CGRectMake(0,20,whitelView.width,25) text:self.symbol font:16 textColor:[UIColor blackColor] tag:0];
    [whitelView addSubview:tforLb];

//选择链
    self.LiantypeBut = [CreateTool createButtonImageTitleWithFrame:CGRectMake(whitelView.width/2-70,tforLb.bottom+10,140,36) imageString:@"选择链底图" buttonTitle:QSLocalizedString(@"qs_XuanZeLian") titleColor:[UIColor whiteColor] tag:801 target:self action:@selector(ButtonClicked:) size:15];
    [whitelView addSubview:self.LiantypeBut];
    
    self.jiantou = [CreateTool createImageViewWithFrame:CGRectMake(self.LiantypeBut.right-20,self.LiantypeBut.y+10,15,14) imageString:@"白色右箭头" tag:0];
    [whitelView addSubview:self.jiantou];
    
    
    self.HiddenView = [[UIView alloc] initWithFrame:CGRectMake(0,self.LiantypeBut.bottom+10,whitelView.width,310)];
    self.HiddenView.backgroundColor = [UIColor clearColor];
    [whitelView addSubview:self.HiddenView];

    UILabel *qbtitLb = [CreateTool createLabelWithFrame:CGRectMake(0,0,whitelView.width,25) text:[NSString stringWithFormat:@"%@%@%@",QSLocalizedString(@"qs_Nide"),self.symbol,QSLocalizedString(@"qs_QianBaoDIZi")] font:14 textColor:[UIColor blackColor] tag:0];
    [self.HiddenView addSubview:qbtitLb];

    self.DiZiLabel = [CreateTool createLabelWithFrame:CGRectMake(15,qbtitLb.bottom+5,self.HiddenView.width-40,50) text:@"" font:13 textColor:buttonColor tag:0];
    [self.HiddenView addSubview:self.DiZiLabel];

    UIButton *fuzhiBut = [CreateTool createButtonWithFrame:CGRectMake(self.DiZiLabel.right,self.DiZiLabel.y+15,20, 20) imageString:@"新复制" tag:0 target:self action:@selector(fuzhiBut)];
    [self.HiddenView addSubview:fuzhiBut];

        //二维码
    self.codeImage = [[UIImageView alloc] initWithFrame:CGRectMake(whitelView.width/2-85,self.DiZiLabel.bottom+15,170,170)];
    [self.HiddenView addSubview:self.codeImage];

}

- (void)ButtonClicked:(UIButton *)but{
     __weak typeof(self) weakself = self;
    switch (but.tag) {
        case 800:
        {
//            CurrencyTypeVC *vc = [CurrencyTypeVC new];
//            vc.Type = @"币种";
//            vc.buttitCliCK = ^(NSString * _Nullable buttit, NSString * _Nullable currency_id, NSString * _Nullable coin_id, NSString * _Nullable iid) {
//
//                [weakself.BZtypeBut setTitle:buttit forState:UIControlStateNormal];
//                weakself.symbol = buttit;
//                weakself.currency_id = currency_id;
//                if (!(weakself.currency_id.length == 0) && !(weakself.Liantypeid.length ==0)) {
//                    [weakself NetworkRequest];
//                }
//
//            };
//            [weakself.navigationController pushViewController:vc animated:YES];
        }
            break;
            case 801:
            {
//                if (weakself.symbol.length == 0) {
//
//                    [JMNotifyView showNotify:@"请先选择币种"];
//                    return;
//                }
//                CurrencyTypeVC *vc = [CurrencyTypeVC new];
//                vc.Type = @"链类型";
//                vc.PageType = @"充值页面";
//                vc.parameter = weakself.symbol;
//                vc.buttitCliCK = ^(NSString * _Nullable buttit, NSString * _Nullable currency_id, NSString * _Nullable coin_id, NSString * _Nullable iid) {
//
//                    [weakself.LiantypeBut setTitle:buttit forState:UIControlStateNormal];
//                     weakself.Liantypeid = iid;
////                    [weakself NetworkRequest];
//
//                };
//                [weakself.navigationController pushViewController:vc animated:YES];
            }
                break;
        default:
            break;
    }
}
#pragma mark - 请求接口
//- (void)NetworkRequest{
//
//   NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
//   parameters[@"token"]= [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
//   parameters[@"block_chain_id"]= self.Liantypeid;
//    //读取充值地址接口
//        NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,depositinfoURL];
//        [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
//
//           NSLog(@"-获取用户-responseDict--%@", responseDict);
//            NSDictionary *DIC = responseDict;
//            NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
//            if ([code isEqualToString:@"200"]) {
//                //成功
//                NSDictionary *data = DIC[@"data"];
//                self.HiddenView.hidden = NO;
//                self.address = [NSString stringWithFormat:@"%@",data[@"address"]];
//                self.DiZiLabel.text = self.address;
//
//                self.codeImage.image = nil;
//                //二维码
//                CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
//                [filter setDefaults];
//                NSString *info = self.address;
//                NSData *infoData = [info dataUsingEncoding:NSUTF8StringEncoding];
//                [filter setValue:infoData forKeyPath:@"inputMessage"];
//                CIImage *outImage = [filter outputImage];
//                self.codeImage.image = [outImage createNonInterpolatedWithSize:130];
//
//            }
//            else
//            {
//                [JMNotifyView showNotify:DIC[@"msg"]];
//            }
//        } failure:^(NSError *error) {
//
//        }];
//
//}

- (void)rightItemClicked:(UIBarButtonItem*)item
{
    TopUpHistoryVC *vc = [TopUpHistoryVC new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)fuzhiBut{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.DiZiLabel.text;
    
     [JMNotifyView showNotify:@"复制成功"];
}
@end
