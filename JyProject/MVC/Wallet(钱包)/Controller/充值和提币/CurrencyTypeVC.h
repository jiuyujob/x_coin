//
//  CurrencyTypeVC.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/9.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

typedef void(^buttonClick)(NSString* _Nullable  buttit,NSString *_Nullable currency_id,NSString*_Nullable coin_id,NSString*_Nullable iid);

NS_ASSUME_NONNULL_BEGIN

@interface CurrencyTypeVC : JYBaseVC<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,retain) NSMutableArray *ArrayData;

@property (nonatomic, copy) buttonClick buttitCliCK;

@property (nonatomic, copy) NSString *Type;
@property (nonatomic, copy) NSString *PageType;
@property (nonatomic, copy) NSString *parameter;//请求链类型的参数

@end

NS_ASSUME_NONNULL_END
