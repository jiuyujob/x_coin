//
//  MentionHistoryVC.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/13.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface MentionHistoryVC : JYBaseVC<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,retain) NSMutableArray *ArrayData;
@property (nonatomic,assign)NSInteger page;

@end

NS_ASSUME_NONNULL_END
