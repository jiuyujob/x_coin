//
//  CurrencyTypeVC.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/9.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "CurrencyTypeVC.h"
#import "CollectMoneyVC.h"
#import "payVC.h"

@interface CurrencyTypeVC ()

@end

@implementation CurrencyTypeVC
- (void)viewDidLoad {
    [super viewDidLoad];
    //title=@"币种类型"
    if ([self.Type isEqualToString:@"币种"]) {
           self.navigationItem.title = QSLocalizedString(@"qs_BiZhongLeiXing");
       }
       else
       {
           
            self.navigationItem.title = QSLocalizedString(@"qs_LianLeiXing");
       }
   
    self.view.backgroundColor = [UIColor whiteColor];
    self.ArrayData = [NSMutableArray array];
    [self SetinitUI];
    [self NetworkRequest];
}
- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (void)SetinitUI{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT-kNavgationBarHeight) style:UITableViewStyleGrouped];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
           if (@available(iOS 11.0, *)){
               _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
           }
     [self.view addSubview:_tableView];
    
}
- (void)NetworkRequest{
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
       parameters[@"token"]= [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
     NSString *URLStr;
    if ([self.Type isEqualToString:@"币种"]) {
        
       URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,currencylistURL];
    }
    else
    {
        //链类型
        parameters[@"symbol"]= self.parameter;
        URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,chainsymbolURL];
    }
    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
        
        NSLog(@"-获取用户-responseDict--%@", responseDict);
               NSDictionary *DIC = responseDict;
               NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
               if ([code isEqualToString:@"200"]) {
                   //成功
                    NSArray *bids  = DIC[@"data"];
                   [self.ArrayData addObjectsFromArray:bids];
                   [self.tableView reloadData];
               }
               else
               {
                   [JMNotifyView showNotify:DIC[@"msg"]];
               }
    } failure:^(NSError *error) {
        
    }];
    

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ArrayData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"myCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];//从重用队列里获取可重用的cell
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if (self.ArrayData && indexPath.row < [self.ArrayData count])
    {
        UIView *view = [[UIView alloc] init];
        view.frame = CGRectMake(10,8,SCREEN_WIDTH-20,50);
        view.layer.backgroundColor = [UIColor whiteColor].CGColor;
        view.layer.cornerRadius = 6;
        view.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
        view.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
        view.layer.shadowOpacity = 0.5;// 阴影透明度，默认0
        view.layer.shadowRadius = 3;// 阴影半径，默认3
        [cell.contentView addSubview:view];
        
        
        
        NSString *text;
        if ([self.Type isEqualToString:@"币种"])
        {
            text = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"symbol"]];
            UIImageView* Image = [CreateTool createImageViewWithFrame:CGRectMake(SCREEN_WIDTH/2-40,10,30,30) imageString:@"" tag:0];
            if ([text isEqualToString:@"TFOR"]) {
                   Image.image = [UIImage imageNamed:@"钱包页TFOR"];
                   
               }
               else if ([text isEqualToString:@"USDD"])
               {
                   Image.image = [UIImage imageNamed:@"钱包页USDD"];
                   
               }
               else
               {
                   Image.image = [UIImage imageNamed:@"钱包页USDT"];
               }
            
            [view addSubview:Image];
            
            UILabel *label = [CreateTool createLabelWithFrame:CGRectMake(Image.right+10,0,100,50) text:text font:13 textColor:[UIColor blackColor] tag:0];
            label.textAlignment = NSTextAlignmentLeft;
            [view addSubview:label];
            
            
        }
        else
        {
            text = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"type"]];
            UILabel *label = [CreateTool createLabelWithFrame:CGRectMake(0,0,view.width,50) text:text font:13 textColor:[UIColor blackColor] tag:0];
            [view addSubview:label];
        }

    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([self.Type isEqualToString:@"币种"])
    {
        if ([self.PageType isEqualToString:@"收钱"])
        {
            CollectMoneyVC *vc = [CollectMoneyVC new];
            vc.symbol = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"symbol"]];
            vc.currency_id = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"currency_id"]];
            [self.navigationController pushViewController:vc animated:YES];
            
        }
        else if ([self.PageType isEqualToString:@"付钱"])
        {
            payVC *vc = [payVC new];
            vc.symbol = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"symbol"]];
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
            NSString *text = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"symbol"]];
            NSString *currency_id = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"currency_id"]];
            self.buttitCliCK(text,currency_id,@"",@"");
            [self.navigationController popViewControllerAnimated:YES];
                   
        }

    }
    else
    {
        NSString *name = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"type"]];
        NSString *coin_id = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"coin_id"]];
        NSString *iid = [NSString stringWithFormat:@"%@",self.ArrayData[indexPath.row][@"id"]];
        self.buttitCliCK(name,@"", coin_id, iid);
        [self.navigationController popViewControllerAnimated:YES];
    }
   
    
}

@end
