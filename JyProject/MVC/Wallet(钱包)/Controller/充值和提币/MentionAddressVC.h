//
//  MentionAddressVC.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/11.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

typedef void(^valueOfClick)(NSString* _Nullable addTit,NSString *_Nullable AddName,NSString *_Nullable withdrawal_addr_id);

NS_ASSUME_NONNULL_BEGIN

@interface MentionAddressVC : JYBaseVC<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,retain) NSMutableArray *ArrayData;

@property (nonatomic,copy) valueOfClick valueOfClicked;

//链
@property (nonatomic,copy) NSString *lianName;//链
@property (nonatomic,copy) NSString *block_chain_id;//链

//币种
@property (nonatomic,copy) NSString *symbol;
@property (nonatomic,copy) NSString *currency_id;

@end

NS_ASSUME_NONNULL_END
