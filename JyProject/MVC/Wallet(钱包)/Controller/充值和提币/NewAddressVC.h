//
//  NewAddressVC.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/11.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewAddressVC : JYBaseVC<UITextFieldDelegate>

//币种类型
@property (nonatomic,retain) UIButton *BZtypeBut;
//链类型
@property (nonatomic,retain) UIButton *LiantypeBut;

//币种id
@property (nonatomic,copy) NSString *currency_id;
@property (nonatomic,copy) NSString *symbol;

//链
@property (nonatomic,copy) NSString *Liantype;
@property (nonatomic,copy) NSString *iid;


@property (nonatomic,retain) UITextField *DIZiTFiled;//地址名称
@property (nonatomic,retain) UITextField *AddTFiled;//地址

@end

NS_ASSUME_NONNULL_END
