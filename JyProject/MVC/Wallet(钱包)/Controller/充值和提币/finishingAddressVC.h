//
//  finishingAddressVC.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/25.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface finishingAddressVC : JYBaseVC<UITextFieldDelegate>

//币种类型
@property (nonatomic,retain) UIButton *BZtypeBut;
//链类型
@property (nonatomic,retain) UIButton *LiantypeBut;

@property (nonatomic,retain) NSString *symbol;//请求链类型的参数
@property (nonatomic,copy) NSString *coin_id;//
@property (nonatomic,retain) NSString *currency_id;//币种id

@property (nonatomic,retain) UITextField *DIZiTFiled;//地址名称
@property (nonatomic,retain) UITextField *AddTFiled;//地址

@property (nonatomic,copy) NSString *address;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *withdrawal_addr_id;

@end

NS_ASSUME_NONNULL_END
