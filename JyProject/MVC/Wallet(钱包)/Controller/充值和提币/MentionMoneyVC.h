//
//  MentionMoneyVC.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/9.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface MentionMoneyVC : JYBaseVC

@property (nonatomic,strong) UITableView *tableView;

//币种id
@property (nonatomic,copy) NSString *currency_id;
@property (nonatomic,copy) NSString *symbol;//请求链类型的参数

@end

NS_ASSUME_NONNULL_END
