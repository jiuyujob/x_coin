//
//  TransferVC.h
//  JyProject
//
//  Created by 九域-UI on 2020/1/4.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface TransferVC : JYBaseVC<UITextFieldDelegate>

@property (nonatomic,retain) UITextField *ZhTFiled;
@property (nonatomic,retain) UITextField *MoneyTFiled;

@property (nonatomic,copy) NSString *symbol;

@end

NS_ASSUME_NONNULL_END
