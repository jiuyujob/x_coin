//
//  TheWalletVC.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/3.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"
#import "WalleTabHeadView.h"
#import "WalleFooterView.h"
#import "WalleModel.h"

@interface TheWalletVC : JYBaseVC<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,retain) NSMutableArray *ArrayData;

@property (nonatomic,retain) NSString *USDDTFORprice;
@property (nonatomic,retain) NSString *USDTTFORprice;

//资产折合
@property (nonatomic,retain) NSString *TFORbalance;
@property (nonatomic,retain) NSString *USDDbalance;
@property (nonatomic,retain) NSString *USDTbalance;

@property (nonatomic,retain) WalleModel *WalleModel;
@property (nonatomic,retain) WalleTabHeadView* WalleTabHeadView;
@property (nonatomic,retain) WalleFooterView* WalleFooterView;

@end

