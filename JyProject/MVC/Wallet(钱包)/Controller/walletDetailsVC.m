//
//  walletDetailsVC.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/20.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "walletDetailsVC.h"
#import "walletDetailsCell.h"

@interface walletDetailsVC ()

@end

@implementation walletDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //title=@"钱包明细"
    if ([self.pageType isEqualToString:@"钱包明细"])
    {
        self.navigationItem.title = QSLocalizedString(@"qs_QianBaoMingXi");
    }
    else
    {
        self.navigationItem.title = QSLocalizedString(@"qs_ShouKuanJiLu");
    }
      
    self.view.backgroundColor = [UIColor whiteColor];
    self.ArrayData = [NSMutableArray array];
    self.page = 1;
    [self SetinitUI];
    
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)SetinitUI{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT-kNavgationBarHeight) style:UITableViewStyleGrouped];
       _tableView.backgroundColor = [UIColor clearColor];
       _tableView.delegate = self;
       _tableView.dataSource = self;
       _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
       if (@available(iOS 11.0, *)){
           _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
       }
       [self.view addSubview:_tableView];
       
       __weak typeof(self) weakSelf = self;
       RefreshHeaderView * header =  [RefreshHeaderView headerWithRefreshingBlock:^{
              dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                  self.page = 1;
                  [self.ArrayData removeAllObjects];
                  [self NetworkRequest];
                  [weakSelf.tableView.fz_header endRefresh];
                  [weakSelf.tableView.fz_footer ResetNoMoreData];
                  
              });
          } AnimationType:(1)];
       self.tableView.fz_header = header;
       [header beginRefresh];
       
       
       RefreshFooterView * footer = [RefreshFooterView FooterWithRefreshingBlock:^{
           dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
               self.page++;
               [self NetworkRequest];
               [weakSelf.tableView.fz_footer endRefreshingWithNoMoreData];
               
           });
       } AnimationType:(1)];
       self.tableView.fz_footer = footer;
}
#pragma mark - 请求接口
- (void)NetworkRequest{
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"token"]= [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    parameters[@"pageSize"]= @"10";
    parameters[@"page"]= [NSString stringWithFormat:@"%ld",(long)self.page];
    NSString *URLStr;
    if ([self.pageType isEqualToString:@"钱包明细"]) {
        URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,accountdetail];
    }
    else
    {
        //收款记录
        URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,accountpersonchargedetail];
    }
    
    
    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
         NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
                  NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
                  if ([code isEqualToString:@"200"]) {
                      //成功
                      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        
                          NSDictionary *data = DIC[@"data"];
                          NSArray *items = data[@"items"];
                          [self.ArrayData addObjectsFromArray:items];
                          //主线程执行
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [self.tableView reloadData];
                          });
                      });
                      
                  }
                  else
                  {
                      [JMNotifyView showNotify:DIC[@"msg"]];
                  }

    } failure:^(NSError *error) {
        
    }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ArrayData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"myCell";
    walletDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];//从重用队列里获取可重用的cell
    if (!cell)
    {
        cell = [[walletDetailsCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (self.ArrayData && indexPath.row < [self.ArrayData count])
    {
        [cell setCellWithInfo:self.ArrayData indexPathRow:indexPath];
    }
   
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 95;
}
@end
