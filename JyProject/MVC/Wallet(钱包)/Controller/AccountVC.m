//
//  AccountVC.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/10.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "AccountVC.h"
#import "DDToDTVC.h"

@interface AccountVC ()

@end

@implementation AccountVC

- (void)viewDidLoad {
    [super viewDidLoad];
     //title=@"账户"
      self.navigationItem.title = QSLocalizedString(@"qs_ZhangHu");
      self.view.backgroundColor = [UIColor blackColor];
    [self SetinitUI];
    [self NetworkRequest];
    
}

- (void)SetinitUI{
    
    NSLog(@"账户数据%@",self.DIC);
    
    UIView *BackView = [[UIView alloc] initWithFrame:CGRectMake(15,20,SCREEN_WIDTH-30,240)];
    BackView.backgroundColor = buttonHuiSeColor;
    BackView.layer.cornerRadius = 8;
    [self.view addSubview:BackView];
    
    self.ZczhLabel = [CreateTool createLabelWithFrame:CGRectMake(0,10,BackView.width,25) text:QSLocalizedString(@"qs_ZiChangZheHe") font:14 textColor:[UIColor whiteColor] tag:0];
    [BackView addSubview:self.ZczhLabel];
    
    self.ZczLabel = [CreateTool createLabelWithFrame:CGRectMake(0,self.ZczhLabel.bottom+5,BackView.width,25) text:self.zczhString font:14 textColor:[UIColor whiteColor] tag:0];
    [BackView addSubview:self.ZczLabel];
    
//资金账户
    CGFloat Width2 = BackView.width/2;  //宽
    
    self.ZczhanghuLabel = [CreateTool createLabelWithFrame:CGRectMake(10,self.ZczLabel.bottom+25,Width2-20,25) text:QSLocalizedString(@"qs_ZiJinZhangHu") font:15 textColor:[UIColor whiteColor] tag:0];
    self.ZczhanghuLabel.textAlignment = NSTextAlignmentLeft;
    [BackView addSubview:self.ZczhanghuLabel];
    
    self.TFORLabel= [CreateTool createLabelWithFrame:CGRectMake(10,self.ZczhanghuLabel.bottom+5,Width2-20,25) text:@"" font:13 textColor:[UIColor whiteColor] tag:0];
    self.TFORLabel.textAlignment = NSTextAlignmentLeft;
    [BackView addSubview:self.TFORLabel];
    
    self.USDTLabel= [CreateTool createLabelWithFrame:CGRectMake(10,self.TFORLabel.bottom+5,Width2-20,25) text:@"" font:13 textColor:[UIColor whiteColor] tag:0];
    self.USDTLabel.textAlignment = NSTextAlignmentLeft;
    [BackView addSubview:self.USDTLabel];
    
    self.USDDLabel= [CreateTool createLabelWithFrame:CGRectMake(10,self.USDTLabel.bottom+5,Width2-20,25) text:@"" font:13 textColor:[UIColor whiteColor] tag:0];
    self.USDDLabel.textAlignment = NSTextAlignmentLeft;
    [BackView addSubview:self.USDDLabel];
    
//生态账户
    NSArray *array = self.DIC[@"ecological_poject"];
    NSString *ReturnMultiple;//杠杆
    NSString *BockedBalance;//剩余算力
    NSString *Balance;//投资总额
    if (array.count > 0) {
        
        ReturnMultiple = [NSString stringWithFormat:@"%@",array[0][@"return_multiple"]];
        BockedBalance= [NSString stringWithFormat:@"%@",array[0][@"bocked_balance"]];
        Balance = [NSString stringWithFormat:@"%@",array[0][@"balance"]];
    }
    
    self.ShenTaiLabel = [CreateTool createLabelWithFrame:CGRectMake(Width2+10,self.ZczLabel.bottom+25,Width2-10,25) text:QSLocalizedString(@"qs_ShenTaiZhangHu") font:15 textColor:[UIColor whiteColor] tag:0];
    self.ShenTaiLabel.textAlignment = NSTextAlignmentLeft;
    [BackView addSubview:self.ShenTaiLabel];
    
    
    //杠杆
    self.GangGanLabel = [CreateTool createLabelWithFrame:CGRectMake(Width2+10,self.ZczhanghuLabel.bottom+5,Width2-10,25) text:[NSString stringWithFormat:@"%@：%@",QSLocalizedString(@"qs_GangGan"),ReturnMultiple] font:13 textColor:[UIColor whiteColor] tag:0];
    self.GangGanLabel.textAlignment = NSTextAlignmentLeft;
    [BackView addSubview:self.GangGanLabel];
    
    
    //剩余算力
    self.SuanLiLabel = [CreateTool createLabelWithFrame:CGRectMake(Width2+10,self.GangGanLabel.bottom+5,Width2-10,25) text:[NSString stringWithFormat:@"%@：%@",QSLocalizedString(@"qs_ShenYuSuanLi"),BockedBalance] font:13 textColor:[UIColor whiteColor] tag:0];
    self.SuanLiLabel.textAlignment = NSTextAlignmentLeft;
    [BackView addSubview:self.SuanLiLabel];
    
    
    //投资总额
    
    self.DongJieLabel = [CreateTool createLabelWithFrame:CGRectMake(Width2+10,self.SuanLiLabel.bottom+5,Width2-10,25) text:[NSString stringWithFormat:@"%@：%@",QSLocalizedString(@"qs_TouZiZonge"),Balance] font:13 textColor:[UIColor whiteColor] tag:0];
    self.DongJieLabel.textAlignment = NSTextAlignmentLeft;
    [BackView addSubview:self.DongJieLabel];

    UIButton *TiBiBut = [CreateTool createButtonImageTitleWithFrame:CGRectMake(50,BackView.bottom+30,SCREEN_WIDTH-100, Scale_X(35)) imageString:@"range_length" buttonTitle:QSLocalizedString(@"qs_DuiHuanCheng") titleColor:[UIColor whiteColor] tag:1000 target:self action:@selector(QuanBuZcButClicked:) size:13];
   [self.view addSubview:TiBiBut];
    
    
    
}


#pragma mark - 请求接口
- (void)NetworkRequest{
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
       parameters[@"token"]= [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,accountinfoURL];
    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
        NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
        NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
        if ([code isEqualToString:@"200"]) {
              //成功
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                   NSArray *data = DIC[@"data"];
              
                dispatch_async(dispatch_get_main_queue(), ^{
                   for (int i= 0; i<data.count; i++) {
                       
                       NSString *text = [NSString stringWithFormat:@"%@：%@",data[i][@"symbol"],data[i][@"balance"]];;
                       switch (i) {
                           case 0:
                           {
                               self.TFORLabel.text = text;
                           }
                               break;
                               case 1:
                               {
                                   self.USDTLabel.text = text;
                               }
                                   break;
                               case 2:
                               {
                                   self.USDDLabel.text = text;
                               }
                                   break;
                           default:
                               break;
                       }
                       
                   }

                });
            });
                   
        }
        else if ([code isEqualToString:@"401"])
        {
            NotifyRelogin;
        }
        else
        {
            [JMNotifyView showNotify:DIC[@"msg"]];
        }

    } failure:^(NSError *error) {
        
    }];
    
}

- (void)QuanBuZcButClicked:(UIButton *)but{
    DDToDTVC  * vc = [[DDToDTVC alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
