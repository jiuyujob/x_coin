//
//  walletDetailsVC.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/20.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface walletDetailsVC : JYBaseVC<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,retain) NSMutableArray *ArrayData;
@property (nonatomic,assign)NSInteger page;

@property (nonatomic,copy)NSString* pageType;
@end

NS_ASSUME_NONNULL_END
