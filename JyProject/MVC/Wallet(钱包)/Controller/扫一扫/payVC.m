//
//  payVC.m
//  JyProject
//
//  Created by 九域-UI on 2020/1/6.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "payVC.h"
#import "CIImage+Extension.h"
#import "AFHTTPSessionManager.h"
#import "PayFuQianVC.h"

@interface payVC ()

@property (nonatomic,retain) AFHTTPSessionManager *Manager;

@property (nonatomic,retain)NSURLSessionDataTask *DataTask;

@end

@implementation payVC

- (void)viewDidLoad {
    [super viewDidLoad];
      self.navigationItem.title = self.symbol;
      self.view.backgroundColor = [UIColor whiteColor];
      
      [self initSetUI];
    [self NetworkRequest];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (void)initSetUI{
    
    UIView *BackView = [[UIView alloc] initWithFrame:CGRectMake(10,40,SCREEN_WIDTH-20,380)];
    BackView.backgroundColor = buttonColor;
    BackView.layer.cornerRadius = 12;
    [self.view addSubview:BackView];
    
    UIView *whiteView = [[UIView alloc] initWithFrame:CGRectMake(10,10,BackView.width-20,310)];
    whiteView.backgroundColor = [UIColor whiteColor];
    whiteView.layer.cornerRadius = 12;
    [BackView addSubview:whiteView];
                                                                                                              
    
    UILabel *textLb = [CreateTool createLabelWithFrame:CGRectMake(0,30 ,whiteView.width,25) text:QSLocalizedString(@"qs_ZhangShiFuKuangMa") font:15 textColor:[UIColor blackColor] tag:0];
    [whiteView addSubview:textLb];
    
//二维码
    self.codeImage = [[UIImageView alloc] initWithFrame:CGRectMake(whiteView.width/2-90,textLb.bottom+20,180,180)];
    [whiteView addSubview:self.codeImage];
    
    self.textLB = [CreateTool createLabelWithFrame:CGRectMake(5,whiteView.bottom,BackView.width-10,56) text:@"" font:12 textColor:[UIColor whiteColor] tag:0];
    self.textLB.backgroundColor = [UIColor clearColor];
    [BackView addSubview:self.textLB];
    
}
- (void)NetworkRequest{

    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"symbol"]= self.symbol;
    parameters[@"money"]= @"0";
     NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,userqrcodepay];
    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {

         NSLog(@"-获取用户-responseDict--%@", responseDict);
         NSDictionary *DIC = responseDict;
         NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
        if ([code isEqualToString:@"200"]) {
                        //成功
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     
                    NSDictionary *data = DIC[@"data"];
                    self.order_id = data[@"order_id"];
                    self.min_money = data[@"min_money"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                    
                        //二维码
                           self.codeImage.image = nil;
                           CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
                           [filter setDefaults];
                           NSString *info = data[@"qrcode"];
                           NSData *infoData = [info dataUsingEncoding:NSUTF8StringEncoding];
                           [filter setValue:infoData forKeyPath:@"inputMessage"];
                           CIImage *outImage = [filter outputImage];
                           self.codeImage.image = [outImage createNonInterpolatedWithSize:170];
                        
                        self.textLB.text = [NSString stringWithFormat:@"%@%@%@%@%@",QSLocalizedString(@"qs_ZhiChuShuLiangq1"),self.min_money,QSLocalizedString(@"qs_ZhiChuShuLiang2"),self.min_money,QSLocalizedString(@"qs_ZhiChuShuLiang3")];
                               
                        [self TwoNetworkRequest];
                    });
                });
        
        }
        else if ([code isEqualToString:@"401"])
        {
            NotifyRelogin;
        }
        else
        {
            [JMNotifyView showNotify:DIC[@"msg"]];
        }

    } failure:^(NSError *error) {

    }];

}

- (void)TwoNetworkRequest{
    
        self.Manager = [AFHTTPSessionManager manager];
        self.Manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        self.Manager.requestSerializer = [AFJSONRequestSerializer serializer];
        self.Manager.securityPolicy.allowInvalidCertificates = YES;
        self.Manager.securityPolicy.validatesDomainName = NO;
        self.Manager.requestSerializer.timeoutInterval = 30;
        self.Manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        self.Manager.requestSerializer = [AFJSONRequestSerializer serializer];//调出请求头
        [self.Manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"]; //将token封装入请求头
        [self.Manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [self.Manager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"token"] forHTTPHeaderField:@"Authorization"];
        self.Manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", nil];

    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"order_id"]= self.order_id;
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,@"wallet/user/pay/status"];
    self.DataTask = [self.Manager GET:URLStr parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                      [XLDouYinLoading hideInView:QSAppKeyWindow];
          NSDictionary *DIC = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
          NSLog(@"-获取用户-responseDict--%@", DIC);
          NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
            if ([code isEqualToString:@"101138"]) {
                
                [self TwoNetworkRequest];
            }
            else if ([code isEqualToString:@"101137"])
            {
                //二维码失效，更新二维码  
                
                [self NetworkRequest];
            }
            else if ([code isEqualToString:@"200"])
            {
                
                NSLog(@"-获取用户-responseDict--%@", DIC);
                NSDictionary *data = DIC[@"data"];
                PayFuQianVC *vc = [PayFuQianVC new];
                vc.code = data[@"code"];
                vc.from = data[@"from"];
                vc.money = data[@"money"];
                vc.order_id = data[@"order_id"];
                vc.symbol = data[@"symbol"];
                vc.min_money = self.min_money;
                [self.navigationController pushViewController:vc animated:YES];
                
                
//                SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:@"OK" sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
//                [special withSureClick:^(NSString *string) {
//                    [self.navigationController popToRootViewControllerAnimated:YES];
//                }];
            }

            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"failure -- error = %@",error);


        }];
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
     [self.Manager.operationQueue cancelAllOperations];
     [self.DataTask cancel];
}

@end
