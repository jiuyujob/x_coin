//
//  paymentVC.h
//  JyProject
//
//  Created by 九域-UI on 2020/1/7.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface paymentVC : JYBaseVC


@property (nonatomic,retain) UILabel *BzLb;
@property (nonatomic,retain) UILabel *dfzhLb;
@property (nonatomic,retain) UILabel *fkjeLb;

@property (nonatomic,copy) NSString *stringValue;
@property (nonatomic,copy) NSString *moneyStr;

@end

NS_ASSUME_NONNULL_END
