//
//  ScanVVC.m
//  JyProject
//
//  Created by 九域-UI on 2020/1/4.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "ScanVVC.h"
#import "EnterTheAmountVC.h"
#import "paymentVC.h"
#import "PayTheAmount.h"

#define XCenter self.view.center.x
#define YCenter self.view.center.y
#define SHeight 20
#define SWidth (XCenter+30)

@interface ScanVVC ()
{
    NSTimer *_timer;
    int num;
    BOOL upOrDown;
    
}

@end

@implementation ScanVVC

#pragma mark ===========懒加载===========
//device
- (AVCaptureDevice *)device
{
    if (_device == nil) {
        //AVMediaTypeVideo是打开相机
        //AVMediaTypeAudio是打开麦克风
        _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    }
    return _device;
}
//input
- (AVCaptureDeviceInput *)input
{
    if (_input == nil) {
        _input = [AVCaptureDeviceInput deviceInputWithDevice:self.device error:nil];
    }
    return _input;
}
//output  --- output如果不打开就无法输出扫描得到的信息
// 设置输出对象解析数据时感兴趣的范围
// 默认值是 CGRect(x: 0, y: 0, width: 1, height: 1)
// 通过对这个值的观察, 我们发现传入的是比例
// 注意: 参照是以横屏的左上角作为, 而不是以竖屏
//        out.rectOfInterest = CGRect(x: 0, y: 0, width: 0.5, height: 0.5)
- (AVCaptureMetadataOutput *)output
{
    if (_output == nil) {
        _output = [[AVCaptureMetadataOutput alloc]init];
        [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
        //限制扫描区域(上下左右)
        [_output setRectOfInterest:[self rectOfInterestByScanViewRect:_imageView.frame]];
    }
    return _output;
}
- (CGRect)rectOfInterestByScanViewRect:(CGRect)rect {
    CGFloat width = CGRectGetWidth(self.view.frame);
    CGFloat height = CGRectGetHeight(self.view.frame);
    
    CGFloat x = (height - CGRectGetHeight(rect)) / 2 / height;
    CGFloat y = (width - CGRectGetWidth(rect)) / 2 / width;
    
    CGFloat w = CGRectGetHeight(rect) / height;
    CGFloat h = CGRectGetWidth(rect) / width;
    
    return CGRectMake(x, y, w, h);
}

//session
- (AVCaptureSession *)session
{
    if (_session == nil) {
        //session
        _session = [[AVCaptureSession alloc]init];
        [_session setSessionPreset:AVCaptureSessionPresetHigh];
        if ([_session canAddInput:self.input]) {
            [_session addInput:self.input];
        }
        if ([_session canAddOutput:self.output]) {
            [_session addOutput:self.output];
        }
    }
    return _session;
}
//preview
- (AVCaptureVideoPreviewLayer *)preview
{
    if (_preview == nil) {
        _preview = [AVCaptureVideoPreviewLayer layerWithSession:self.session];
    }
    return _preview;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //1 判断是否存在相机
    if (self.device == nil) {
        [JMNotifyView showNotify:@"未检测到相机"];
        return;
    }
    self.navigationItem.title = QSLocalizedString(@"qs_SaoYiSao");
    self.view.backgroundColor = [UIColor blackColor];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
       self.navigationController.navigationBar.barTintColor = NavigationBarTintColor;
          self.navigationController.navigationBar.translucent = NO;
       [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:NaczitiColor,NSFontAttributeName:[UIFont systemFontOfSize:18]}];
    
    //打开定时器，开始扫描
    [self addTimer];
    
    //界面初始化
    [self interfaceSetup];
    
    //初始化扫描
    [self scanSetup];
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [self starScan];
}
#pragma mark ==========初始化工作在这里==========
- (void)viewDidDisappear:(BOOL)animated
{
    //视图退出，关闭扫描
    [self.session stopRunning];
    //关闭定时器
    [_timer setFireDate:[NSDate distantFuture]];
}
//界面初始化
- (void)interfaceSetup
{
    //1 添加扫描框
    [self addImageView];
    
    //添加模糊效果
    [self setOverView];
}
//添加扫描框
- (void)addImageView
{

    _imageView = [[UIImageView alloc]initWithFrame:CGRectMake((SCREEN_WIDTH-SWidth)/2, (SCREEN_HEIGHT -SWidth)/2-kNavgationBarHeight, SWidth, SWidth)];
    //显示扫描框
    _imageView.image = [UIImage imageNamed:@"scanscanBg.png"];
    [self.view addSubview:_imageView];
    _line = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(_imageView.frame)+5, CGRectGetMinY(_imageView.frame)+5, CGRectGetWidth(_imageView.frame), 3)];
    _line.image = [UIImage imageNamed:@"scanLine"];
    [self.view addSubview:_line];
    
//    UILabel *text = [CreateTool createLabelWithFrame:CGRectMake(0, _imageView.y-35,SCREEN_WIDTH,20) text:QSLocalizedString(@"qs_erWeiMaFanRuKuang") font:13.5 textColor:[UIColor whiteColor] tag:0];
//    [self.view addSubview:text];
    
}
//初始化扫描配置
- (void)scanSetup
{
    //2 添加预览图层
    self.preview.frame = self.view.bounds;
    self.preview.videoGravity = AVLayerVideoGravityResize;
    [self.view.layer insertSublayer:self.preview atIndex:0];
    
    //3 设置输出能够解析的数据类型
    //注意:设置数据类型一定要在输出对象添加到回话之后才能设置
    NSString *mediaType = AVMediaTypeVideo;//读取媒体类型
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];//读取设备授权状态
    if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
        
        SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:QSLocalizedString(@"qs_KaiQiXianJiQuanXian") sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
        [special withSureClick:^(NSString *string) {
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            }
        }];
        return;
    }
    
    [self.output setMetadataObjectTypes:@[AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode128Code, AVMetadataObjectTypeQRCode]];
    
    //高质量采集率
    [self.session setSessionPreset:AVCaptureSessionPresetHigh];
    
    //4 开始扫描
    [self.session startRunning];
    
    
    UIButton *fanHuiBut = [CreateTool createButtonWithFrame:CGRectMake(15,35,35,35) imageString:@"扫一扫返回" tag:0 target:self action:@selector(fanHuiButClick:)];
    [self.view addSubview:fanHuiBut];
    
    UIButton *xianceBut = [CreateTool createButtonWithFrame:CGRectMake(SCREEN_WIDTH-50,35,35,35) imageString:@"扫一扫相册" tag:0 target:self action:@selector(rightItemClicked)];
    [self.view addSubview:xianceBut];
    
}

//提示框alert
- (void)showAlertViewWithMessage:(NSString *)message
{
    //弹出提示框后，关闭扫描
    [self.session stopRunning];
    //弹出alert，关闭定时器
    [_timer setFireDate:[NSDate distantFuture]];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"扫描结果" message:[NSString stringWithFormat:@"%@",message] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"完成" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        
        
        //点击alert，开始扫描
        [self.session startRunning];
        //开启定时器
        [_timer setFireDate:[NSDate distantPast]];
    }]];
    [self presentViewController:alert animated:true completion:^{
        
    }];
    
}

#pragma mark ===========添加提示框===========
//提示框alert
- (void)showAlertViewWithTitle:(NSString *)aTitle withMessage:(NSString *)aMessage
{
    
    //弹出提示框后，关闭扫描
    [self.session stopRunning];
    //弹出alert，关闭定时器
    [_timer setFireDate:[NSDate distantFuture]];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:aTitle message:[NSString stringWithFormat:@"%@",aMessage] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"知道了" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        [self starScan];
        //点击alert，开始扫描
    }]];
    [self presentViewController:alert animated:true completion:^{
        
    }];
    
}

#pragma mark ===========扫描的代理方法===========
//得到扫描结果
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    if ([metadataObjects count] > 0)
    {
        AVMetadataMachineReadableCodeObject *metadataObject = [metadataObjects objectAtIndex:0];
        if ([metadataObject isKindOfClass:[AVMetadataMachineReadableCodeObject class]])
        {
            NSString *stringValue = [metadataObject stringValue];
            NSLog(@"%@",stringValue);
             [self.session stopRunning];
            NSRange order_idrange = [stringValue rangeOfString:@"order_id"];
            if(order_idrange.location != NSNotFound)//如果有order_id就是付钱
            {
                PayTheAmount *vc = [PayTheAmount new];
                vc.stringValue = stringValue;
                [self.navigationController pushViewController:vc animated:YES];
            }
            else
            {
                
                NSRange range = [stringValue rangeOfString:@"code"];
                if(range.location != NSNotFound)//NSNotFound表示无限大
                {
                    NSArray *array = [stringValue componentsSeparatedByString:@"&"];
                    NSString *type = array[2];
                    NSArray *typeArray = [type componentsSeparatedByString:@"="];
                    NSString *typeText = typeArray[1];
                    if ([typeText isEqualToString:@"1"]) {
                        
                        EnterTheAmountVC *vc = [EnterTheAmountVC new];
                        vc.stringValue = stringValue;
                        [self.navigationController pushViewController:vc animated:YES];
                        
                    }
                    else
                    {
                        NSString *money = array[3];
                        NSArray *moneyArray = [money componentsSeparatedByString:@"="];
                        NSString *monText = moneyArray[1];
                        
                        paymentVC *vc = [paymentVC new];
                        vc.stringValue = stringValue;
                        vc.moneyStr = monText;
                        [self.navigationController pushViewController:vc animated:YES];
                    }
                    
                }
                else
                {
                    [self showAlertViewWithMessage:@"请扫T生态专属二维码"];
                }
                
                
            }

        }
        
    }
}

#pragma mark ============添加模糊效果============
- (void)setOverView {
    CGFloat width = CGRectGetWidth(self.view.frame);
    CGFloat height = CGRectGetHeight(self.view.frame);
    
    CGFloat x = CGRectGetMinX(_imageView.frame);
    CGFloat y = CGRectGetMinY(_imageView.frame);
    CGFloat w = CGRectGetWidth(_imageView.frame);
    CGFloat h = CGRectGetHeight(_imageView.frame);
    
    [self creatView:CGRectMake(0, 0, width, y)];
    [self creatView:CGRectMake(0, y, x, h)];
    [self creatView:CGRectMake(0, y + h, width, height - y - h)];
    [self creatView:CGRectMake(x + w, y, width - x - w, h)];
}

- (void)creatView:(CGRect)rect {
    CGFloat alpha = 0.5;
    UIColor *backColor = [UIColor blackColor];
    UIView *view = [[UIView alloc] initWithFrame:rect];
    view.backgroundColor = backColor;
    view.alpha = alpha;
    [self.view addSubview:view];
}

#pragma mark ============添加扫描效果============

- (void)addTimer
{
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.008 target:self selector:@selector(timerMethod) userInfo:nil repeats:YES];
}
//控制扫描线上下滚动
- (void)timerMethod
{
    if (upOrDown == NO) {
        num ++;
        _line.frame = CGRectMake(CGRectGetMinX(_imageView.frame)+5, CGRectGetMinY(_imageView.frame)+5+num, CGRectGetWidth(_imageView.frame)-10, 3);
        if (num == (int)(CGRectGetHeight(_imageView.frame)-10)) {
            upOrDown = YES;
        }
    }
    else
    {
        num --;
        _line.frame = CGRectMake(CGRectGetMinX(_imageView.frame)+5, CGRectGetMinY(_imageView.frame)+5+num, CGRectGetWidth(_imageView.frame)-10, 3);
        if (num == 0) {
            upOrDown = NO;
        }
    }
}
//暂定扫描
- (void)stopScan
{
    //弹出提示框后，关闭扫描
    [self.session stopRunning];
    //弹出alert，关闭定时器
    [_timer setFireDate:[NSDate distantFuture]];
    //隐藏扫描线
    _line.hidden = YES;
}
- (void)starScan
{
    //开始扫描
    [self.session startRunning];
    //打开定时器
    [_timer setFireDate:[NSDate distantPast]];
    //显示扫描线
    _line.hidden = NO;
}

- (void)rightItemClicked
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        //关闭扫描
        [self stopScan];
        // 弹出系统相册
        UIImagePickerController *pickVC = [[UIImagePickerController alloc]init];
        pickVC.delegate = self;
        pickVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        pickVC.navigationBar.barTintColor = [UIColor colorWithRed:20.f/255.0 green:24.0/255.0 blue:38.0/255.0 alpha:1];
        //设置右侧取消按钮的字体颜色
        pickVC.navigationBar.tintColor = [UIColor redColor];
        
        self.modalTransitionStyle=UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:pickVC animated:YES completion:nil];
    }
    else
    {
        [self showAlertViewWithTitle:@"打开失败" withMessage:@"相册打开失败。设备不支持访问相册，请在设置->隐私->照片中进行设置！"];
    }
}

//从相册中选取照片&读取相册二维码信息
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    //1 获取选择的图片
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    //初始化一个监听器
    CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:@{CIDetectorAccuracy : CIDetectorAccuracyHigh}];
    [picker dismissViewControllerAnimated:YES completion:^{
        //监测到的结果数组
        NSArray *features = [detector featuresInImage:[CIImage imageWithCGImage:image.CGImage]];
        if (features.count >= 1) {
            //结果对象
            CIQRCodeFeature *feature = [features objectAtIndex:0];
            NSString *stringValue = feature.messageString;
            NSRange order_idrange = [stringValue rangeOfString:@"order_id"];
                       if(order_idrange.location != NSNotFound)//如果有order_id就是付钱
                       {
                           PayTheAmount *vc = [PayTheAmount new];
                           vc.stringValue = stringValue;
                           [self.navigationController pushViewController:vc animated:YES];
                       }
                       else
                       {
                           
                           NSRange range = [stringValue rangeOfString:@"code"];
                           if(range.location != NSNotFound)//NSNotFound表示无限大
                           {
                               NSArray *array = [stringValue componentsSeparatedByString:@"&"];
                               NSString *type = array[2];
                               NSArray *typeArray = [type componentsSeparatedByString:@"="];
                               NSString *typeText = typeArray[1];
                               if ([typeText isEqualToString:@"1"]) {
                                   
                                   EnterTheAmountVC *vc = [EnterTheAmountVC new];
                                   vc.stringValue = stringValue;
                                   [self.navigationController pushViewController:vc animated:YES];
                                   
                               }
                               else
                               {
                                   NSString *money = array[3];
                                   NSArray *moneyArray = [money componentsSeparatedByString:@"="];
                                   NSString *monText = moneyArray[1];
                                   
                                   paymentVC *vc = [paymentVC new];
                                   vc.stringValue = stringValue;
                                   vc.moneyStr = monText;
                                   [self.navigationController pushViewController:vc animated:YES];
                               }
                               
                           }
                           else
                           {
                               [self showAlertViewWithMessage:@"请扫T生态专属二维码"];
                           }
                           
                           
                       }
            
        }
    }];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    //模态方式退出UIImagePickerController
    [picker dismissViewControllerAnimated:YES completion:^{
         [self starScan];
    }];
//    [picker dismissModalViewControllerAnimated:YES];
   
}


- (void)fanHuiButClick:(UIButton *)but{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
