//
//  SetTheAmountVC.h
//  JyProject
//
//  Created by 九域-UI on 2020/1/6.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

typedef void(^MoneyClick)(NSString * _Nullable money);

NS_ASSUME_NONNULL_BEGIN

@interface SetTheAmountVC : JYBaseVC<UITextFieldDelegate>

@property (nonatomic,retain) UITextField *TFiled;

@property (nonatomic, copy) MoneyClick MoneyClicked;

@end

NS_ASSUME_NONNULL_END
