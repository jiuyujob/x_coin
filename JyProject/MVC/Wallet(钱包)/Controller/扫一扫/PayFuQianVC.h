//
//  PayFuQianVC.h
//  JyProject
//
//  Created by 九域-UI on 2020/1/14.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface PayFuQianVC : JYBaseVC

@property (nonatomic,retain) UILabel *DdhLb;
@property (nonatomic,retain) UILabel *BzLb;
@property (nonatomic,retain) UILabel *dfzhLb;
@property (nonatomic,retain) UILabel *fkjeLb;

@property (nonatomic,copy) NSString *code;
@property (nonatomic,copy) NSString *from;
@property (nonatomic,copy) NSString *money;
@property (nonatomic,copy) NSString *min_money;

@property (nonatomic,copy) NSString *order_id;
@property (nonatomic,copy) NSString *symbol;

@end

NS_ASSUME_NONNULL_END
