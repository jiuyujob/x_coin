//
//  PayTheAmount.m
//  JyProject
//
//  Created by 九域-UI on 2020/1/14.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "PayTheAmount.h"

@interface PayTheAmount ()

@end

@implementation PayTheAmount

- (void)viewDidLoad {
    [super viewDidLoad];
        self.navigationItem.title = QSLocalizedString(@"qs_ShuRuJinE");
    self.view.backgroundColor = [UIColor whiteColor];
        
    [self initSetUI];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)initSetUI{
    
    UIView *OneView = [[UIView alloc] initWithFrame:CGRectMake(20,40,SCREEN_WIDTH-40,Scale_X(55))];
    OneView.layer.backgroundColor = [UIColor whiteColor].CGColor;
    OneView.layer.cornerRadius =10;
    OneView.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    OneView.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    OneView.layer.shadowOpacity = 0.5;// 阴影透明度，默认0
    OneView.layer.shadowRadius = 3;// 阴影半径，默认3
    [self.view addSubview:OneView];
    
    
    UILabel *money = [CreateTool createLabelWithFrame:CGRectMake(10,0,80,Scale_X(55)) text:QSLocalizedString(@"qs_jine") font:14 textColor:[UIColor blackColor] tag:0];
    [OneView addSubview:money];
    
    self.TFiled = [CreateTool createTextFieldWithFrame:CGRectMake(money.right,0,SCREEN_WIDTH-130,Scale_X(55)) placeholder:QSLocalizedString(@"qs_ShuRuJinE") delegate:self tag:100];
    self.TFiled.keyboardType = UIKeyboardTypeDecimalPad;
    [OneView addSubview:self.TFiled];
    
    UIButton *zzBut = [CreateTool createButtonImageTitleWithFrame:CGRectMake(50,OneView.bottom+30,SCREEN_WIDTH-100, Scale_X(55)) imageString:@"range_length" buttonTitle:QSLocalizedString(@"qs_queding") titleColor:[UIColor whiteColor] tag:1000 target:self action:@selector(ButClicked:) size:13];
    [self.view addSubview:zzBut];

    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL isHaveDian;
    //判断是否有小数点
    if ([textField.text containsString:@"."]) {
        isHaveDian = YES;
    }else{
        isHaveDian = NO;
    }

    if (string.length > 0) {

        //当前输入的字符
        unichar single = [string characterAtIndex:0];
        NSLog(@"single = %c",single);

        //不能输入.0~9以外的字符
        if (!((single >= '0' && single <= '9') || single == '.')){
            NSLog(@"您输入的格式不正确");
            return NO;
        }
       
        //只能有一个小数点
        if (isHaveDian && single == '.') {
            NSLog(@"只能输入一个小数点");
            return NO;
        }

        //如果第一位是.则前面加上0
        if ((textField.text.length == 0) && (single == '.')) {
            textField.text = @"0";
        }

        //如果第一位是0则后面必须输入.
        if ([textField.text hasPrefix:@"0"]) {
            if (textField.text.length > 1) {
                NSString *secondStr = [textField.text substringWithRange:NSMakeRange(1, 1)];
                if (![secondStr isEqualToString:@"."]) {
                    NSLog(@"第二个字符必须是小数点");
                    return NO;
                }
            }else{
                if (![string isEqualToString:@"."]) {
                    NSLog(@"第二个字符必须是小数点");
                    return NO;
                }
            }
        }

        //小数点后最多能输入两位
        if (isHaveDian) {
            NSRange ran = [textField.text rangeOfString:@"."];
            //由于range.location是NSUInteger类型的，所以不能通过(range.location - ran.location) > 2来判断
            if (range.location > ran.location) {
                if ([textField.text pathExtension].length > 1) {
                    NSLog(@"小数点后最多有两位小数");
                    return NO;
                }
            }
        }
        
    }
    return YES;
}

- (void)ButClicked:(UIButton*)but{
    
    if (self.TFiled.text.length == 0) {
           
           [JMNotifyView showNotify:QSLocalizedString(@"qs_ShuRuJinE")];
           return;
    }
    if([self.TFiled.text doubleValue]<=0)
    {
        [JMNotifyView showNotify:QSLocalizedString(@"qs_JineBuNenWei")];
        return;
    }
    
    NSLog(@"%@",self.stringValue);
    NSArray *array = [self.stringValue componentsSeparatedByString:@"&"];
    NSString *code = array[0];
    NSArray *codeArray = [code componentsSeparatedByString:@"="];
    NSString *codeText = codeArray[1];
    
    NSString *symbol = array[1];
    NSArray *symbolArray = [symbol componentsSeparatedByString:@"="];
    NSString *symbolText = symbolArray[1];
    
    NSString *type = array[2];
    NSArray *typeArray = [type componentsSeparatedByString:@"="];
    NSString *typeText = typeArray[1];
    
    NSString *from = array[4];
    NSArray *fromArray = [from componentsSeparatedByString:@"="];
    NSString *fromText = fromArray[1];
    
    NSString *order_id = array[5];
    NSArray *order_idArray = [order_id componentsSeparatedByString:@"="];
    NSString *order_idText = order_idArray[1];
    
    
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"money"]= self.TFiled.text;
    parameters[@"order_id"]= order_idText;
    
    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,userpaysetmoney];
    [GWNetWork postTokenWithURL:URLStr params:parameters success:^(id responseDict) {
        
        NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
        NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
        if ([code isEqualToString:@"200"])
        {
            SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:DIC[@"msg"] sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
            [special withSureClick:^(NSString *string) {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
        }
        else
        {
            [JMNotifyView showNotify:DIC[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
    }];
    
    
}
@end
