//
//  CollectMoneyVC.h
//  JyProject
//
//  Created by 九域-UI on 2020/1/6.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface CollectMoneyVC : JYBaseVC

@property (nonatomic,retain) UILabel *skrLb;
@property (nonatomic,retain) UILabel *typeLb;
@property (strong, nonatomic) UIImageView *codeImage;
@property (strong, nonatomic) UILabel *moneyLb;

@property (nonatomic,copy) NSString *currency_id;//币种id
@property (nonatomic,copy) NSString *symbol;

@property (nonatomic,retain) NSString *type;//收费类型1：不带金额2：带金额
@property (nonatomic,retain) NSString *money;//金额

@end

NS_ASSUME_NONNULL_END
