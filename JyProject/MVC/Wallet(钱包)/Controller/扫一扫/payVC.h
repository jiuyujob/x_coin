//
//  payVC.h
//  JyProject
//
//  Created by 九域-UI on 2020/1/6.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface payVC : JYBaseVC

@property (nonatomic,retain) UILabel *typeLb;
@property (nonatomic,retain) UILabel *textLB;
@property (strong, nonatomic) UIImageView *codeImage;

@property (nonatomic,copy) NSString *symbol;
@property (nonatomic,retain) NSString *order_id;
@property (nonatomic,retain) NSString *min_money;

@end

NS_ASSUME_NONNULL_END
