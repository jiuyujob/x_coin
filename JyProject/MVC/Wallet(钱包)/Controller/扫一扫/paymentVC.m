//
//  paymentVC.m
//  JyProject
//
//  Created by 九域-UI on 2020/1/7.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "paymentVC.h"
#import "SYPasswordView.h"

@interface paymentVC ()
@property (nonatomic, strong) SYPasswordView *pasView;

@end

@implementation paymentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = QSLocalizedString(@"qs_FuKuan");
    self.view.backgroundColor = [UIColor whiteColor];
        
    [self initSetUI];
    [self NetworkRequest];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)initSetUI{
    
    UIView *OneView = [[UIView alloc] initWithFrame:CGRectMake(20,20,SCREEN_WIDTH-40,180)];
    OneView.layer.backgroundColor = [UIColor whiteColor].CGColor;
    OneView.layer.cornerRadius = 10;
    OneView.layer.shadowColor = [UIColor lightGrayColor].CGColor;// 阴影颜色
    OneView.layer.shadowOffset = CGSizeMake(0,0); // 阴影偏移，默认(0, -3)
    OneView.layer.shadowOpacity = 0.5;// 阴影透明度，默认0
    OneView.layer.shadowRadius = 3;// 阴影半径，默认3
    [self.view addSubview:OneView];
    
    UILabel *titLb = [CreateTool createLabelWithFrame:CGRectMake(15, 15,200, 25) text:QSLocalizedString(@"qs_ZhiFuXinXi") font:16 textColor:[UIColor blackColor] tag:0];
    titLb.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:titLb];
    
    //币种
    self.BzLb = [CreateTool createLabelWithFrame:CGRectMake(9,titLb.bottom+15,OneView.width,25) text:@"" font:14 textColor:[UIColor blackColor] tag:0];
    self.BzLb.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:self.BzLb];
    
    //对方账号
    self.dfzhLb= [CreateTool createLabelWithFrame:CGRectMake(15,self.BzLb.bottom+15,OneView.width,25) text:@"" font:14 textColor:[UIColor blackColor] tag:0];
    self.dfzhLb.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:self.dfzhLb];
    
    //付款金额
    self.fkjeLb= [CreateTool createLabelWithFrame:CGRectMake(15,self.dfzhLb.bottom+15,OneView.width,25) text:@"" font:14 textColor:[UIColor blackColor] tag:0];
    self.fkjeLb.textAlignment = NSTextAlignmentLeft;
    [OneView addSubview:self.fkjeLb];
    
    
    UILabel *srzfmima = [CreateTool createLabelWithFrame:CGRectMake(20,OneView.bottom+30,OneView.width,25) text:QSLocalizedString(@"qs_QinShuRuZhiFuMiMa") font:14 textColor:[UIColor blackColor] tag:0];
    srzfmima.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:srzfmima];
    
    self.pasView = [[SYPasswordView alloc] initWithFrame:CGRectMake(20, srzfmima.bottom+10, SCREEN_WIDTH - 40, 50)];
    [self.view addSubview:_pasView];
    
    
    UIButton *zzBut = [CreateTool createButtonImageTitleWithFrame:CGRectMake(50,self.pasView.bottom+60,SCREEN_WIDTH-100, Scale_X(55)) imageString:@"range_length" buttonTitle:QSLocalizedString(@"qs_ZhuanZhang") titleColor:[UIColor whiteColor] tag:1000 target:self action:@selector(ButClicked:) size:13];
    [self.view addSubview:zzBut];
}

#pragma mark - 请求接口
- (void)NetworkRequest{

      NSLog(@"%@",self.stringValue);
       NSArray *array = [self.stringValue componentsSeparatedByString:@"&"];
       NSString *code = array[0];
       NSArray *codeArray = [code componentsSeparatedByString:@"="];
       NSString *codeText = codeArray[1];
   
       NSString *symbol = array[1];
       NSArray *symbolArray = [symbol componentsSeparatedByString:@"="];
       NSString *symbolText = symbolArray[1];
   
       NSString *type = array[2];
       NSArray *typeArray = [type componentsSeparatedByString:@"="];
       NSString *typeText = typeArray[1];
   
       NSString *from = array[4];
       NSArray *fromArray = [from componentsSeparatedByString:@"="];
       NSString *fromText = fromArray[1];
   
       NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
       parameters[@"code"]= codeText;
       parameters[@"symbol"]= symbolText;
       parameters[@"type"]= typeText;
       parameters[@"money"]= self.moneyStr;
       parameters[@"from"]= fromText;
   
       NSLog(@"%@",parameters);

       NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,userpayinfo];
       [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {
           NSLog(@"-获取用户-responseDict--%@", responseDict);
           NSDictionary *DIC = responseDict;
           NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
           if ([code isEqualToString:@"200"]) {
                 //成功
               dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                      
                   NSDictionary *data = DIC[@"data"];
                   dispatch_async(dispatch_get_main_queue(), ^{
                       
                       self.BzLb.text = [NSString stringWithFormat:@"%@:%@",QSLocalizedString(@"qs_BiZhong"),data[@"symbol"]];
                       self.dfzhLb.text = [NSString stringWithFormat:@"%@:%@",QSLocalizedString(@"qs_DuiFangZhangHao"),data[@"email"]];
                       self.fkjeLb.text = [NSString stringWithFormat:@"%@:%@",QSLocalizedString(@"qs_FuKuangJinE"),data[@"money"]];
                      
                   });
               });
                      
           }
           else if ([code isEqualToString:@"401"])
           {
               NotifyRelogin;
           }
           else
           {
               [JMNotifyView showNotify:DIC[@"msg"]];
           }
       } failure:^(NSError *error) {
           
       }];
}

- (void)ButClicked:(UIButton*)but{
    
    if (self.pasView.textField.text.length == 0) {
           
           [JMNotifyView showNotify:QSLocalizedString(@"qs_QinShuRuZhiFuMiMa")];
           return;
    }
    if (self.pasView.textField.text.length < 6) {
           [JMNotifyView showNotify:QSLocalizedString(@"qs_zhifumimachangdubugou")];
           return;
       }
    
       NSLog(@"%@",self.stringValue);
        NSArray *array = [self.stringValue componentsSeparatedByString:@"&"];
        NSString *code = array[0];
        NSArray *codeArray = [code componentsSeparatedByString:@"="];
        NSString *codeText = codeArray[1];
    
        NSString *symbol = array[1];
        NSArray *symbolArray = [symbol componentsSeparatedByString:@"="];
        NSString *symbolText = symbolArray[1];
    
        NSString *type = array[2];
        NSArray *typeArray = [type componentsSeparatedByString:@"="];
        NSString *typeText = typeArray[1];
    
        NSString *from = array[4];
        NSArray *fromArray = [from componentsSeparatedByString:@"="];
        NSString *fromText = fromArray[1];
    
        NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
        parameters[@"code"]= codeText;
        parameters[@"symbol"]= symbolText;
        parameters[@"type"]= typeText;
        parameters[@"money"]= self.moneyStr;
        parameters[@"from"]= fromText;
        parameters[@"pay_password"]= self.pasView.textField.text;

    NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,userpaychange];
    
    [GWNetWork postTokenWithURL:URLStr params:parameters success:^(id responseDict) {
        
        NSLog(@"-获取用户-responseDict--%@", responseDict);
        NSDictionary *DIC = responseDict;
        NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
        if ([code isEqualToString:@"200"]) {
            
            SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:DIC[@"msg"] sureBtnTitle:QSLocalizedString(@"qs_queding") sureBtnColor:buttonColor];
            [special withSureClick:^(NSString *string) {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }];
                      
        }
        else if ([code isEqualToString:@"401"])
        {
            NotifyRelogin;
        }
        else
        {
            [JMNotifyView showNotify:DIC[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        
    }];
    
}
@end
