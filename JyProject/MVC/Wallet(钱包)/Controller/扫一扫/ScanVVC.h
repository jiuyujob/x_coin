//
//  ScanVVC.h
//  JyProject
//
//  Created by 九域-UI on 2020/1/4.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrefixHeader.pch"
#import "CreateTool.h"
#import "Masonry.h"
#import "HttpConstant.h"
#import "HandleUserInfo.h"
#import "GWNetWork.h"
#import "JMNotifyView.h"
#import "SpecialAlertView.h"
#import "FLAnimatedImage.h"
#import "UIView+Frame.h"
#import "UIView+ViewController.h"
#import "UIView+LLXAlertPop.h"
#import "RefreshComponent.h"
#import "UIScrollView+Custom.h"
#import "UIScrollView+Refresh.h"
#import "MJExtension.h"
#import "XLDouYinLoading.h"
#import "UIImageView+WebCache.h"
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ScanVVC : UIViewController<AVCaptureMetadataOutputObjectsDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (strong, nonatomic) AVCaptureDevice * device; //捕获设备，默认后置摄像头
@property (strong, nonatomic) AVCaptureDeviceInput * input; //输入设备
@property (strong, nonatomic) AVCaptureMetadataOutput * output;//输出设备，需要指定他的输出类型及扫描范围
@property (strong, nonatomic) AVCaptureSession * session; //AVFoundation框架捕获类的中心枢纽，协调输入输出设备以获得数据
@property (strong, nonatomic) AVCaptureVideoPreviewLayer * preview;//展示捕获图像的图层，是CALayer的子类

@property (strong, nonatomic) UIPinchGestureRecognizer *pinchGes;//缩放手势
//扫描框
@property (nonatomic,strong)UIImageView *imageView;

//扫描线
@property (nonatomic,retain)UIImageView *line;

@end

NS_ASSUME_NONNULL_END
