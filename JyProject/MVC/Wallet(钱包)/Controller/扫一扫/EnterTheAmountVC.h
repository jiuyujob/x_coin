//
//  EnterTheAmountVC.h
//  JyProject
//
//  Created by 九域-UI on 2020/1/7.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface EnterTheAmountVC : JYBaseVC<UITextFieldDelegate>

@property (nonatomic,retain) UITextField *TFiled;

@property (nonatomic,copy) NSString *stringValue;

@end

NS_ASSUME_NONNULL_END
