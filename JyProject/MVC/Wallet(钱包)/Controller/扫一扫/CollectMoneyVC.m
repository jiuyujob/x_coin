//
//  CollectMoneyVC.m
//  JyProject
//
//  Created by 九域-UI on 2020/1/6.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "CollectMoneyVC.h"
#import "CIImage+Extension.h"
#import "SetTheAmountVC.h"
#import "walletDetailsVC.h"

@interface CollectMoneyVC ()

@end

@implementation CollectMoneyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // 收钱
    self.navigationItem.title = self.symbol;
    self.view.backgroundColor = [UIColor whiteColor];
    self.type = @"1";
    self.money = @"0";
    
    [self initSetUI];
    [self NetworkRequest];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:animated];

}
- (void)initSetUI{
    
    UIView *BackView = [[UIView alloc] initWithFrame:CGRectMake(10,20,SCREEN_WIDTH-20,450)];
     BackView.backgroundColor = buttonColor;
     BackView.layer.cornerRadius = 12;
    [self.view addSubview:BackView];
    
    UIView *whitelView = [[UIView alloc] initWithFrame:CGRectMake(10,10,BackView.width-20,390)];
     whitelView.backgroundColor = [UIColor whiteColor];
     whitelView.layer.cornerRadius = 12;
    [BackView addSubview:whitelView];
    
    
    UILabel *sysLb = [CreateTool createLabelWithFrame:CGRectMake(0,10,whitelView.width,25) text:QSLocalizedString(@"qs_XiangWoFuQian") font:15 textColor:[UIColor blackColor] tag:0];
    [whitelView addSubview:sysLb];

    self.skrLb = [CreateTool createLabelWithFrame:CGRectMake(0,sysLb.bottom+10,whitelView.width,25) text:@"" font:14 textColor:kHexColor(@"#999999") tag:0];
    [whitelView addSubview:self.skrLb];
    
    self.codeImage = [[UIImageView alloc] initWithFrame:CGRectMake(whitelView.width/2-95,self.skrLb.bottom+10,190,190)];
    [whitelView addSubview:self.codeImage];


    self.moneyLb = [CreateTool createLabelWithFrame:CGRectMake(0,self.codeImage.bottom+10,whitelView.width,25) text:@"" font:22 textColor:[UIColor blackColor] tag:0];
    [whitelView addSubview:self.moneyLb];

    UIButton *szseBut = [CreateTool createButtonWithFrame:CGRectMake(self.codeImage.x-20,self.moneyLb.bottom+10,80,35) buttonTitle:QSLocalizedString(@"qs_SheZhiJinE") titleColor:buttonColor selectTitleColor:nil tag:900 target:self action:@selector(ButtonClicked:) size:14];
    [whitelView addSubview:szseBut];

    UIImageView *line = [[UIImageView alloc] initWithFrame:CGRectMake(whitelView.width/2,szseBut.y+7,1,18)];
    line.backgroundColor = [UIColor lightGrayColor];
    [whitelView addSubview:line];

    UIButton *bctpBut = [CreateTool createButtonWithFrame:CGRectMake(self.codeImage.right-60,szseBut.y,80,35) buttonTitle:QSLocalizedString(@"qs_BaoCunTuPian") titleColor:buttonColor selectTitleColor:nil tag:901 target:self action:@selector(ButtonClicked:) size:14];
    [whitelView addSubview:bctpBut];


    UIImageView *tbImage = [CreateTool createImageViewWithFrame:CGRectMake(15,whitelView.bottom+15,21,21) imageString:@"收钱记录" tag:0];
    [BackView addSubview:tbImage];

    UILabel *skjlLb = [CreateTool createLabelWithFrame:CGRectMake(tbImage.right+10,tbImage.y-3,150,25) text:QSLocalizedString(@"qs_ShouKuanJiLu") font:15 textColor:[UIColor whiteColor] tag:0];
    skjlLb.textAlignment = NSTextAlignmentLeft;
    [BackView addSubview:skjlLb];

    UIImageView *jiantou = [CreateTool createImageViewWithFrame:CGRectMake(BackView.width-28,tbImage.y+3,15,14) imageString:@"白色右箭头" tag:0];
    [BackView addSubview:jiantou];

    UIButton *skjlBut = [CreateTool createButtonWithFrame:CGRectMake(0,tbImage.y,BackView.width,60) buttonTitle:@"" titleColor:nil selectTitleColor:nil tag:902 target:self action:@selector(ButtonClicked:) size:0];
    skjlBut.backgroundColor = [UIColor clearColor];
    [BackView addSubview:skjlBut];
    
    
}

- (void)NetworkRequest{

    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    parameters[@"currency_id"]= self.currency_id;
    parameters[@"type"]= self.type;
    parameters[@"symbol"]= self.symbol;
    parameters[@"money"]= self.money;
     NSString *URLStr = [NSString stringWithFormat:@"%@%@",kBaseUrlString,userqrcodechange];
    [GWNetWork getKotenWithURL:URLStr params:parameters success:^(id responseDict) {

         NSLog(@"-获取用户-responseDict--%@", responseDict);
         NSDictionary *DIC = responseDict;
         NSString *code = [NSString stringWithFormat:@"%@",DIC[@"code"]];
        if ([code isEqualToString:@"200"]) {
                        //成功
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     
                    NSDictionary *data = DIC[@"data"];
                                 
                             
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        //
                        self.skrLb.text = [NSString stringWithFormat:@"%@:%@(%@)",QSLocalizedString(@"qs_ShouKuanRen"),data[@"user_name"],data[@"email"]];
                        //二维码
                           self.codeImage.image = nil;
                           CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
                           [filter setDefaults];
                           NSString *info = data[@"qrcode"];
                           NSData *infoData = [info dataUsingEncoding:NSUTF8StringEncoding];
                           [filter setValue:infoData forKeyPath:@"inputMessage"];
                           CIImage *outImage = [filter outputImage];
                           self.codeImage.image = [outImage createNonInterpolatedWithSize:170];
                                   
                    });
                });
        
        }
        else if ([code isEqualToString:@"401"])
        {
            NotifyRelogin;
        }
        else
        {
            [JMNotifyView showNotify:DIC[@"msg"]];
        }

    } failure:^(NSError *error) {

    }];

}


-(void)ButtonClicked:(UIButton *)but{
    __block CollectMoneyVC *blockSelf = self;
    switch (but.tag) {
        case 900:
        {
            SetTheAmountVC *vc = [SetTheAmountVC new];
            vc.MoneyClicked = ^(NSString * _Nullable money) {
                
                blockSelf.moneyLb.text = money;
                blockSelf.type = @"2";
                blockSelf.money= money;
                [blockSelf NetworkRequest];
            };
            [self.navigationController pushViewController:vc animated:YES];
            
        }
            break;
        case 901:
        {
            [self makeScreenShotCompletion:^(UIImage *image) {
                
            }];
        }
            break;
        case 902:
        {
            walletDetailsVC *vc = [walletDetailsVC new];
            vc.pageType = @"收款记录";
            [self.navigationController pushViewController:vc animated:YES];
        }
        default:
            break;
    }
    
}
-(void)makeScreenShotCompletion:(void(^)(UIImage * image))completion{
    //开启上下文  <span style="font-family: Arial, Helvetica, sans-serif;">设置截屏大小</span>
    UIGraphicsBeginImageContext(QSAppKeyWindow.bounds.size);
    [QSAppKeyWindow.layer renderInContext:UIGraphicsGetCurrentContext()];
    //获取图片
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    //关闭上下文
    UIGraphicsEndImageContext();
    completion(image);
    
    /**
     *  将图片保存到本地相册
     */
    UIImageWriteToSavedPhotosAlbum(image, self , @selector(saveImage:didFinishSavingWithError:contextInfo:), nil);//保存图片到照片库
    
}
- (void)saveImage:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error == nil) {
        [JMNotifyView showNotify:QSLocalizedString(@"已经存入手机相册")];
    }
    else
    {
        [JMNotifyView showNotify:QSLocalizedString(@"保存失败")];
    }
    
}
@end
