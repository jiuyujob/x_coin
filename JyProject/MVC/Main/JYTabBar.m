//
//  JYTabBar.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/3.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYTabBar.h"
#import "PrefixHeader.pch"
#import "JYNavVC.h"
#import "TheWalletVC.h"
#import "EcologicalVC.h"
#import "CommunityVC.h"
#import "MycenterVC.h"
#import "DAPPSVC.h"

@interface JYTabBar ()
{
    NSInteger _currentIndex;
}
@end

@implementation JYTabBar
+ (void)initialize
{
    // 通过appearance统一设置所有UITabBarItem的文字属性
    UITabBarItem *tabBarItem = [UITabBarItem appearance];
    UINavigationBar * navigationBar = [UINavigationBar appearance];
    //    //返回按钮的箭头颜色
    [navigationBar setTintColor:[UIColor whiteColor]];
    //    //    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-100, 0) forBarMetrics:UIBarMetricsDefault];
        //    /** 设置默认状态 */
//    NSMutableDictionary *norDict = @{}.mutableCopy;
//    norDict[NSFontAttributeName] = norDict[NSFontAttributeName];
//    norDict[NSFontAttributeName] = [UIFont systemFontOfSize:11];
//    norDict[NSForegroundColorAttributeName] = [UIColor whiteColor];
//    [tabBarItem setTitleTextAttributes:norDict forState:UIControlStateNormal];
     [[UITabBar appearance] setUnselectedItemTintColor:kHexColor(@"#939393")];
    
    
    NSMutableDictionary *attr3=[NSMutableDictionary dictionary];
        attr3[NSFontAttributeName]=[UIFont systemFontOfSize:11];
    [[UITabBarItem appearance]setTitleTextAttributes:attr3 forState:UIControlStateNormal];
        
        
    NSMutableDictionary *attr4=[NSMutableDictionary dictionary];
        attr4[NSFontAttributeName]=[UIFont systemFontOfSize:11];
    [[UITabBarItem appearance]setTitleTextAttributes:attr4 forState:UIControlStateSelected];
    
    //
    //    /** 设置选中状态 */
    NSMutableDictionary *selDict = @{}.mutableCopy;
    selDict[NSFontAttributeName] = selDict[NSFontAttributeName];
    selDict[NSFontAttributeName] = [UIFont systemFontOfSize:11];
    selDict[NSForegroundColorAttributeName] = buttonColor;
    [tabBarItem setTitleTextAttributes:selDict forState:UIControlStateSelected];

    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
     self.delegate = self;
        /* 钱包 */
    [self setUpChildControllerWith:[[TheWalletVC alloc]init] norImage:[UIImage imageNamed:@"钱包默认"] selImage:[UIImage imageNamed:@"钱包高亮"] title:QSLocalizedString(@"qs_QianBao")];

    /*行情 */
    [self setUpChildControllerWith:[[CommunityVC alloc]init] norImage:[UIImage imageNamed:@"生态默认"] selImage:[UIImage imageNamed:@"生态高亮"] title:QSLocalizedString(@"qs_HangQin")];

    /*DAPPS*/
    [self setUpChildControllerWith:[[DAPPSVC alloc]init] norImage:[UIImage imageNamed:@"DAPP默认"] selImage:[UIImage imageNamed:@"DAPP高亮"] title:@"DAPPS"];
    
    /*我的 */
    MycenterVC *mevc = [[UIStoryboard storyboardWithName:@"MeStoryboard" bundle:nil] instantiateInitialViewController];
    [self setUpChildControllerWith:mevc norImage:[UIImage imageNamed:@"我的默认"] selImage:[UIImage imageNamed:@"我的高亮"] title:QSLocalizedString(@"qs_WoDe")];
    
        
        /** 设置tabar工具条 */
        //[self.tabBar setBackgroundImage:[UIImage imageNamed:@"tabbar-light"]];
//        self.tabBar.backgroundColor = colorwithrgb(5, 16, 34, 1);
    // 设置一个自定义 View,大小等于 tabBar 的大小
       UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,100)];
       bgView.backgroundColor = kHexColor(@"#FFFFFF");
       [self.tabBar insertSubview:bgView atIndex:0];
    
}

- (void)setUpChildControllerWith:(UIViewController *)childVc norImage:(UIImage *)norImage selImage:(UIImage *)selImage title:(NSString *)title
{
    JYNavVC *nav = [[JYNavVC alloc] initWithRootViewController:childVc];
    childVc.title = title;
    UIImage *image = norImage;
    image=[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    childVc.tabBarItem.image = image;
    
    UIImage *towimage = selImage;
    towimage=[towimage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    childVc.tabBarItem.selectedImage = towimage;
    //不让tabbar底部有渲染的关键代码
    [self addChildViewController:nav];
    
}

@end
