//
//  APPWKWebView.h
//  JyProject
//
//  Created by 九域-UI on 2019/12/5.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"
#import <WebKit/WebKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface APPWKWebView : JYBaseVC<WKUIDelegate,WKNavigationDelegate,WKScriptMessageHandler, UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@property (nonatomic,retain) WKWebView *webView;
@property (nonatomic,copy) NSString *strURL;
@property (nonatomic,copy) NSString *tit;
@property (nonatomic,copy) NSString *TypeHtml;
@property(nonatomic, strong) UIProgressView *progressView; //进度条
@property (nonatomic, strong) UIButton *leftButton;

@end

NS_ASSUME_NONNULL_END
