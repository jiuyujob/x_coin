//
//  JYBaseVC.m
//  JyProject
//
//  Created by 九域-UI on 2019/12/3.
//  Copyright © 2019 九域-UI. All rights reserved.
//

#import "JYBaseVC.h"
#import "JYNavVC.h"
#import "WWLoginCtrl.h"

@interface JYBaseVC ()
{
    UIImageView *_lineVIew;//导航栏下面的横线消除
}
@end

@implementation JYBaseVC
-(UIImageView*)findlineviw:(UIView*)view{
    
    if ([view isKindOfClass:[UIImageView class]]&&view.bounds.size.height<=1.0) {
        return (UIImageView*) view;
    }for (UIImageView *subview in view.subviews) {
        UIImageView *lineview = [self findlineviw:subview];
        if (lineview) {
            return lineview;
        }
    }
    return nil;
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    _lineVIew.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     _lineVIew = [self findlineviw:self.navigationController.navigationBar];

    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:18]}];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
    self.navigationController.navigationBar.tintColor  = kHexColor(@"333333");
    //左栏目
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:[UIImage imageNamed:@"fanhui"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0,anvWith,anvheight);
    [leftButton addTarget:self action:@selector(leftItemClicked:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [self.navigationItem setLeftBarButtonItem:leftItem animated:YES];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LogBackInVC) name:@"LogBackIn" object:nil];
}



- (void)leftItemClicked:(UIBarButtonItem*)item
{
    [self.navigationController popViewControllerAnimated:YES];//推到上一级
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _lineVIew.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    _lineVIew.hidden = YES;
}

- (void)LogBackInVC
{
            SpecialAlertView *special = [[SpecialAlertView alloc]initWithTitleImage:nil messageTitle:QSLocalizedString(@"qs_WenXinTiShi") messageString:@"登录已失效，请重新登录" sureBtnTitle:@"确定" sureBtnColor:buttonColor];
            [special withSureClick:^(NSString *string) {
    
                [HandleUserInfo clearAllUserInfo];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                          
                           WWLoginCtrl *ctrl = [[WWLoginCtrl alloc] init];
                           JYNavVC *nav = [[JYNavVC alloc] initWithRootViewController:ctrl];
                           QSAppKeyWindow.rootViewController = nav;
                           CATransition * transition = [[CATransition alloc] init];
                           transition.type = @"reveal";
                           transition.duration = 0.3;
                           [QSAppKeyWindow.layer addAnimation:transition forKey:nil];
                       });
            }];
    
    
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleDefault;
}
@end
