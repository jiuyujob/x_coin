

#import "UIView+ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
@implementation UIView (ViewController)

- (UIViewController *)viewController {
    UIResponder *next = self.nextResponder;
    
    do {
        
        //判断响应者是否为视图控制器
        if ([next isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)next;
        }
        
        next = next.nextResponder;
        
    } while (next != nil);
    
    return nil;
}

- (UITableViewCell *)tableViewCell
{
    UIResponder *next = self.nextResponder;
    do {
        
        //判断响应者是否为视图控制器
        if ([next isKindOfClass:[UITableViewCell class]]) {
            return (UITableViewCell *)next;
        }
        
        next = next.nextResponder;
        
    } while (next != nil);
    
    return nil;
}
/**
 *  获取当前UIViewController
 */
-(UIViewController *)getCurrentVC{
    UIViewController *result = nil;
    id object = [self nextResponder];
    while (![object isKindOfClass:[UIViewController class]] && object != nil) {
        object = [object nextResponder];
    }
    result = (UIViewController*)object;
    return result;
}

@end
