//
//  CALayer+XibConfig.m
//  JyProject
//
//  Created by mac on 2020/1/11.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import "CALayer+XibConfig.h"


@implementation CALayer (XibConfig)

- (void)setBorderUIColor:(UIColor *)color {
    self.borderColor = color.CGColor;
}


- (UIColor*)borderUIColor {
    return [UIColor colorWithCGColor:self.borderColor];
}


-(void)setShadowUIColor:(UIColor*)color {
    self.shadowColor = color.CGColor;
}

-(UIColor *)shadowUIColor {
    return [UIColor colorWithCGColor:self.shadowColor];
}

@end
