//
//  CALayer+XibConfig.h
//  JyProject
//
//  Created by mac on 2020/1/11.
//  Copyright © 2020 九域-UI. All rights reserved.
//

#import <UIKit/UIKit.h>


#import <QuartzCore/QuartzCore.h>

NS_ASSUME_NONNULL_BEGIN

@interface CALayer (XibConfig)
@property(nonatomic, assign) UIColor *borderUIColor;
@property(nonatomic, assign) UIColor *shadowUIColor;
@end

NS_ASSUME_NONNULL_END
