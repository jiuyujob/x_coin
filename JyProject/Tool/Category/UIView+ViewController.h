

#import <UIKit/UIKit.h>

@interface UIView (ViewController)

- (UIViewController *)viewController;

- (UITableViewCell *)tableViewCell;
/**
 *  获取当前UIViewController
 */
-(UIViewController *)getCurrentVC;

@end
